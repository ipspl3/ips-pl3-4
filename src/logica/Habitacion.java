package logica;

import java.sql.SQLException;

public class Habitacion {

	public String id;
	public String tipo;
	public String modalidad = "ninguna";
	public int modalidad_id;
	public String estado = "libre";
	public String comentario;
	public String planta;
	
	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Habitacion(String f) throws SQLException{
		id = f;
		tipo = DataBase.TipoHabitación(f);
	}
	
	public String getId()
	{
		return id;
	}
	
	@Override
	public String toString(){
		if (modalidad.equals("ninguna")){
			return "Habitación " + id + " de tipo " + tipo;
		}
		else{
			return "Habitación " + id + " de tipo " + tipo + ", " + modalidad;
		}
	}
	
	public void setModalidad(String modalidad, int modalidad_id) {
		this.modalidad = modalidad;
		this.modalidad_id = modalidad_id;
		System.out.println(modalidad_id);
	}
}
