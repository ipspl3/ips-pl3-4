package logica;

public class Cliente {
	
	DNI dni;
	String name;
	String apellidos;
	String direccion;
	String telefono;
	String tarjeta;
	String correo;
	long nacimiento;
	String residencia;
	String libroFamilia = null;
	boolean acompaņante;
	
	@Override
	public String toString() {
		return name + " " + apellidos + ", con dni " + dni;
	}
	
	public void setAcompaņante(boolean bool) {
		acompaņante = bool;
	}
	
	public boolean getAcompaņante() {
		return acompaņante;
	}
	
	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String appelidos) {
		this.apellidos = appelidos;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public long getNacimiento() {
		return nacimiento;
	}

	public void setNacimiento(long nacimiento) {
		this.nacimiento = nacimiento;
	}

	public String getResidencia() {
		return residencia;
	}

	public void setResidencia(String residencia) {
		this.residencia = residencia;
	}

	boolean nuevo = false;
	
	public boolean isNuevo() {
		return nuevo;
	}

	public void setNuevo(boolean nuevo) {
		this.nuevo = nuevo;
	}

	public DNI getDni() {
		return dni;
	}

	public void setDni(DNI dni) {
		this.dni = dni;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public void setLibroFamilia(String libroFamilia) {
		this.libroFamilia = libroFamilia;
	}

	public String getLibroFamilia() {
		return libroFamilia;
	}
	
}
