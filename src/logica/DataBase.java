package logica;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.JOptionPane;

public class DataBase {

	static Connection db;
	private static boolean connected = false;

	public static void Connect() throws SQLException {
		DriverManager.registerDriver(new oracle.jdbc.OracleDriver());

		String url = "jdbc:oracle:thin:@156.35.94.99:1521:DESA";
		String username = "uo250972";
		String password = "uo250972";

		db = DriverManager.getConnection(url, username, password);
		setConnected(true);
	}

	public static boolean isConnected() {
		return connected;
	}

	public static void setConnected(boolean connected) {
		DataBase.connected = connected;
	}

	public static ArrayList<String> ComprobarFecha(long in, long out) throws SQLException 
	{
		String sentence = "SELECT pk_habitacion FROM habitacion "
				+ "MINUS (SELECT pk_habitacion FROM reserva NATURAL JOIN reserva_hab "
				+ "WHERE NOT fecha_inicio >= to_date(?, 'DD/MM/YYYY') AND NOT fecha_fin <= to_date(?, 'DD/MM/YYYY'))";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setDate(2,  new java.sql.Date(in));
		psQuery.setDate(1, new java.sql.Date(out));
		ResultSet rs = psQuery.executeQuery();
		ArrayList<String> sol = new ArrayList<String>();
		while (rs.next()){
			String id = rs.getString("pk_habitacion");
			sol.add(id);
		}

		psQuery.close();
		rs.close();

		return sol;
	}

	public static String  TipoHabitación(String f) throws SQLException 
	{
		String sentence = "SELECT DISTINCT tipo FROM habitacion  WHERE pk_habitacion = ? ";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		String cadena = f;
		psQuery.setString(1, String.valueOf(cadena));
		ResultSet rs = psQuery.executeQuery();
		String tipo = "";
		if (rs.next()){
			tipo = rs.getString("tipo");
		}

		psQuery.close();
		rs.close();

		return tipo;
	}

	public static ArrayList<String> Modalidades(String tipohab) throws SQLException 
	{
		String sentence = "SELECT nombre_suplemento "
				+ "FROM suplemento_modalidad "
				+ "WHERE nombre = ?";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setString(1, String.valueOf(tipohab));
		ResultSet rs = psQuery.executeQuery();
		ArrayList<String> tipo = new ArrayList<String>();
		while (rs.next()){
			tipo.add(rs.getString("nombre_suplemento"));
		}

		psQuery.close();
		rs.close();

		return tipo;
	}

	public static ArrayList<Integer> selecionReserva() throws SQLException 
	{
		String sentence = "SELECT id_reserva FROM reserva";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.executeQuery();
		ResultSet rs = psQuery.executeQuery();
		ArrayList<Integer> sel = new ArrayList<Integer>();
		if (rs.next()){
			sel.add(rs.getInt("id_reserva"));
		}

		psQuery.close();
		rs.close();

		return sel;
	}

	public static int numeroHabs() throws SQLException 
	{
		String sentence = "SELECT count(pk_habitacion) as cantidad " 
				+ "FROM habitacion";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.executeQuery();
		ResultSet rs = psQuery.executeQuery();
		int sel = 0;
		if (rs.next()){
			sel = rs.getInt("cantidad");
		}

		psQuery.close();
		rs.close();

		return sel;
	}

	public static void nuevoCliente(String dni, String nombre, String apellidos, 
			String direccion, String telefono, long tarjeta, String correo, long nacimiento, String pais) throws SQLException 
	{
		String sentence = "INSERT INTO CLIENTE (DNI, NOMBRE, APELLIDOS, DIRECCION, TELEFONO, TARJETA, CORREO, FECHA_NACIMIENTO, PAIS_RESIDENCIA) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setString(1, dni.toUpperCase());
		psQuery.setString(2, nombre);
		psQuery.setString(3, apellidos);
		psQuery.setString(4, direccion);
		psQuery.setString(5, telefono);
		psQuery.setLong(6, tarjeta);
		psQuery.setString(7, correo);
		psQuery.setDate(8, new java.sql.Date(nacimiento));
		psQuery.setString(9, pais);
		psQuery.executeQuery();

		psQuery.close();
	}

	public static int nuevaReserva1(String dni, long f_in, long f_fin) throws SQLException
	{
		String sentence  = "select SEQ_RESERVA.nextVal from dual";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		ResultSet rs = psQuery.executeQuery();
		int res_id = 0;
		if (rs.next()){
			res_id = rs.getInt(1);
		}
		sentence = "INSERT INTO RESERVA (DNI, ID_RESERVA, FECHA_INICIO, FECHA_FIN, TIPO) " +
				" VALUES (?, ?, ? , ?, 'reserva')";
		psQuery= db.prepareStatement(sentence);
		psQuery.setString(1, dni.toUpperCase());
		psQuery.setInt(2, res_id);
		psQuery.setDate(3, new java.sql.Date(f_in));
		psQuery.setDate(4, new java.sql.Date(f_fin));
		psQuery.executeQuery();

		psQuery.close();
		rs.close();

		return res_id;
	}

	public static void nuevaReserva_hab(int pk, String dni, String id, int mod_id, String modalidad) throws SQLException
	{	
		String sentence = "INSERT INTO RESERVA_HAB (ID_RESERVA, PK_HABITACION, ID_SUPL_MOD, NOMBRE) " +
				"VALUES (?, ?, ?, ?)";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setInt(1, pk);
		psQuery.setString(2, id);
		psQuery.setInt(3, mod_id);
		psQuery.setString(4, modalidad);
		System.out.println("Primero el id de la modalidad " +  mod_id);
		System.out.println("Luego el nombre de la modalidad " +  modalidad);
		
		psQuery.executeQuery();

		psQuery.close();
	}

	@SuppressWarnings("deprecation")
	public static Cliente buscarClientePorDni(String dni) throws SQLException
	{
		String sentence = "SELECT * FROM cliente WHERE DNI= ? ";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setString(1, dni);
		psQuery.executeQuery();
		ResultSet rs = psQuery.executeQuery();
		if (rs.next()){
			String name = rs.getString("nombre");
			String apellidos = rs.getString("apellidos");
			String direccion = rs.getString("direccion");
			String telefono = rs.getString("telefono");   	
			String tarjeta = rs.getString("tarjeta");
			String correo = rs.getString("correo");
			java.sql.Date nacimiento = rs.getDate("fecha_nacimiento");
			String residencia = rs.getString("pais_residencia");

			Cliente cliente = new Cliente();
			cliente.setDni(new DNI(dni));
			cliente.setName(name);
			cliente.setApellidos(apellidos);
			cliente.setDireccion(direccion);
			cliente.setTelefono(telefono);
			cliente.setTarjeta(tarjeta);
			cliente.setCorreo(correo);
			cliente.setNacimiento(nacimiento.getDate());
			cliente.setResidencia(residencia);

			psQuery.close();
			rs.close();

			return cliente;
		}

		psQuery.close();
		rs.close();

		return null;
	}
	
	@SuppressWarnings("deprecation")
	public static Cliente buscarAcomapañantePorDni(String dni) throws SQLException
	{
		String sentence = "SELECT * FROM acompañante WHERE DNI= ? ";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setString(1, dni);
		psQuery.executeQuery();
		ResultSet rs = psQuery.executeQuery();
		if (rs.next()){
			String name = rs.getString("nombre");
			String apellidos = rs.getString("apellidos");
			String direccion = rs.getString("direccion");
			String telefono = rs.getString("telefono");
			String correo = rs.getString("correo");
			java.sql.Date nacimiento = rs.getDate("fecha_nacimiento");
			String libro = rs.getString("libro_de_familia");
			String residencia = rs.getString("pais_residencia");

			Cliente cliente = new Cliente();
			cliente.setDni(new DNI(dni));
			cliente.setName(name);
			cliente.setApellidos(apellidos);
			cliente.setDireccion(direccion);
			cliente.setTelefono(telefono);
			cliente.setCorreo(correo);
			cliente.setNacimiento(nacimiento.getDate());
			cliente.setLibroFamilia(libro);
			System.out.println("libro: " + libro);
			cliente.setResidencia(residencia);

			psQuery.close();
			rs.close();

			return cliente;
		}

		psQuery.close();
		rs.close();

		return null;
	}

	public static Cliente buscarClientePorNombre(String nombre) throws SQLException
	{
		String sentence = "SELECT * FROM cliente WHERE NOMBRE= ? ";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setString(1, nombre);
		psQuery.executeQuery();
		ResultSet rs = psQuery.executeQuery();
		if (rs.next()){
			String dni = rs.getString("dni");
			String apellidos = rs.getString("apellidos");
			String direccion = rs.getString("direccion");
			String telefono = rs.getString("telefono");   	
			String tarjeta = rs.getString("tarjeta");
			String correo = rs.getString("correo");
			java.sql.Date nacimiento = rs.getDate("fecha_nacimiento");
			String residencia = rs.getString("pais_residencia");

			Cliente cliente = new Cliente();
			cliente.setDni(new DNI(dni));
			cliente.setName(nombre);
			cliente.setApellidos(apellidos);
			cliente.setDireccion(direccion);
			cliente.setTelefono(telefono);
			cliente.setTarjeta(tarjeta);
			cliente.setCorreo(correo);
			cliente.setNacimiento(nacimiento.getTime());
			cliente.setResidencia(residencia);

			psQuery.close();
			rs.close();

			return cliente;
		}

		psQuery.close();
		rs.close();

		return null;
	}

	public static Cliente buscarAcomapañantePorNombre(String nombre) throws SQLException
	{
		String sentence = "SELECT * FROM acompañante WHERE NOMBRE= ? ";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setString(1, nombre);
		psQuery.executeQuery();
		ResultSet rs = psQuery.executeQuery();
		if (rs.next()){
			String dni = rs.getString("dni");
			String apellidos = rs.getString("apellidos");
			String direccion = rs.getString("direccion");
			String telefono = rs.getString("telefono");
			String correo = rs.getString("correo");
			java.sql.Date nacimiento = rs.getDate("fecha_nacimiento");
			String libro = rs.getString("libro_de_familia");
			String residencia = rs.getString("pais_residencia");

			Cliente cliente = new Cliente();
			cliente.setDni(new DNI(dni));
			cliente.setName(nombre);
			cliente.setApellidos(apellidos);
			cliente.setDireccion(direccion);
			cliente.setTelefono(telefono);
			cliente.setCorreo(correo);
			cliente.setNacimiento(nacimiento.getTime());
			cliente.setLibroFamilia(libro);
			cliente.setResidencia(residencia);

			psQuery.close();
			rs.close();

			return cliente;
		}

		psQuery.close();
		rs.close();

		return null;
	}
	
	public static int pk_modalidad(String modalidad, Habitacion hab) throws SQLException
	{
		String sentence = "SELECT id_supl_mod " +
				"FROM suplemento_modalidad " + 		
				"WHERE nombre_suplemento = ? AND nombre = ?";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setString(1, modalidad);
		psQuery.setString(2, hab.tipo);
		ResultSet rs = psQuery.executeQuery();
		int p = 0;
		if (rs.next()){      
			p = rs.getInt("id_supl_mod");
		}

		psQuery.close();
		rs.close();

		return p;
	}

	public static ArrayList<Reserva> buscarReservas() throws SQLException
	{
		String sentence = "SELECT DISTINCT fecha_inicio, fecha_fin, pk_habitacion, id_supl_mod, nombre_suplemento, id_reserva, dni " + 
				"FROM reserva NATURAL JOIN reserva_hab NATURAL JOIN suplemento_modalidad "
				+ "WHERE tipo = 'reserva' ";
		PreparedStatement psQuery= db.prepareStatement(sentence);

		ArrayList<Reserva> reservas = new ArrayList<Reserva>();
		ResultSet rs = psQuery.executeQuery();
		while (rs.next()) {
			Habitacion hab = new Habitacion(rs.getString("pk_habitacion"));
			hab.setModalidad(rs.getString("nombre_suplemento"), rs.getInt("id_supl_mod"));
			Reserva res = new Reserva();
			res.setPk(rs.getInt("id_reserva"));
			res.add(hab);
			res.datos(rs.getString("dni"));
			res.fecha(rs.getDate("fecha_inicio").getTime(), rs.getDate("fecha_fin").getTime());
			reservas.add(res);
		}

		psQuery.close();
		rs.close();

		return reservas;
	}

	public static ArrayList<Reserva> buscarReservaPorDni(String dni) throws SQLException
	{
		String sentence = "SELECT DISTINCT fecha_inicio, fecha_fin, pk_habitacion, id_supl_mod, nombre_suplemento,  id_reserva "
				+ "FROM reserva NATURAL JOIN reserva_hab NATURAL JOIN suplemento_modalidad "
				+ "WHERE tipo = 'reserva' AND dni = ?";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setString(1, dni);
		ArrayList<Reserva> reservas = new ArrayList<Reserva>();
		ResultSet rs = psQuery.executeQuery();
		while (rs.next()) {
			Habitacion hab = new Habitacion(rs.getString("pk_habitacion"));
			hab.setModalidad(rs.getString("nombre_suplemento"), rs.getInt("id_supl_mod"));
			Reserva res = new Reserva();
			res.setPk(rs.getInt("id_reserva"));
			res.add(hab);
			res.datos(dni);
			res.fecha(rs.getDate("fecha_inicio").getTime(), rs.getDate("fecha_fin").getTime());
			reservas.add(res);
		}

		psQuery.close();
		rs.close();
		return reservas;
	}

	public static void modificarDatosCliente(Cliente cliente) throws SQLException 
	{
		String sentence = "UPDATE cliente " + 
				"SET nombre = ?, apellidos = ?, direccion = ?, telefono = ?, tarjeta = ?, correo = ?, fecha_nacimiento = ?, pais_residencia = ? " + 
				"WHERE dni = ? ";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setString(1, cliente.getName());
		psQuery.setString(2, cliente.getApellidos());
		psQuery.setString(3, cliente.getDireccion());
		psQuery.setString(4, cliente.getTelefono());
		psQuery.setString(5, cliente.getTarjeta());
		psQuery.setString(6, cliente.getCorreo());
		psQuery.setDate(7, new java.sql.Date(cliente.getNacimiento()));
		psQuery.setString(8, cliente.getResidencia());
		psQuery.setString(9, cliente.getDni().toString());

		int p = psQuery.executeUpdate();

		if (p==0) {
			JOptionPane.showMessageDialog(null, "No se guadaron...");
		}
	}

	public static void modificarDatosAcompañante(Cliente cliente) throws SQLException 
	{
		if (cliente.getLibroFamilia() == null) {
			String sentence = "UPDATE acompañante " + 
					"SET nombre = ?, apellidos = ?, direccion = ?, telefono = ?, correo = ?, fecha_nacimiento = ?, pais_residencia = ? " + 
					"WHERE dni = ? ";
			PreparedStatement psQuery= db.prepareStatement(sentence);
			psQuery.setString(1, cliente.getName());
			psQuery.setString(2, cliente.getApellidos());
			psQuery.setString(3, cliente.getDireccion());
			psQuery.setString(4, cliente.getTelefono());
			psQuery.setString(5, cliente.getCorreo());
			psQuery.setDate(6, new java.sql.Date(cliente.getNacimiento()));
			psQuery.setString(7, cliente.getResidencia());
			psQuery.setString(8, cliente.getDni().toString());

			psQuery.executeUpdate();

			psQuery.close();
		} 
		else {
			String sentence = "UPDATE acompañante " + 
					"SET nombre = ?, apellidos = ?, direccion = ?, telefono = ?, correo = ?, fecha_nacimiento = ?, pais_residencia = ?, libro_de_familia = ? " + 
					"WHERE dni = ? ";
			PreparedStatement psQuery= db.prepareStatement(sentence);
			psQuery.setString(1, cliente.getName());
			psQuery.setString(2, cliente.getApellidos());
			psQuery.setString(3, cliente.getDireccion());
			psQuery.setString(4, cliente.getTelefono());
			psQuery.setString(5, cliente.getCorreo());
			psQuery.setDate(6, new java.sql.Date(cliente.getNacimiento()));
			psQuery.setString(7, cliente.getResidencia());
			psQuery.setString(8, cliente.getLibroFamilia());
			psQuery.setString(9, cliente.getDni().toString());
			
			psQuery.executeQuery();

			psQuery.close();
		}
	}
	
	public static void eliminarRegistro(Reserva res) throws SQLException 
	{
		String sentence = "SELECT pk_habitacion "
				+ "FROM RESERVA_HAB "
				+ "WHERE id_reserva = ?";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setInt(1, res.getPk());
		
		psQuery.executeQuery();
		ResultSet rs = psQuery.executeQuery();
		while (rs.next()) {
			
			String pk_habitacion = rs.getString("pk_habitacion");
			sentence = "UPDATE habitacion "
					+ "SET estado = 'libre' "
					+ "WHERE pk_habitacion = ? ";
			psQuery = db.prepareStatement(sentence);
			psQuery.setString(1,pk_habitacion);
			
			psQuery.executeQuery();
			
		}

		sentence = "DELETE "
				+ "FROM RESERVA_HAB "
				+ "WHERE id_reserva = ?";
		psQuery= db.prepareStatement(sentence);
		psQuery.setInt(1, res.getPk());
		
		psQuery.executeQuery();
		
		sentence = "DELETE "
				+ "FROM ACOMPAÑA_A "
				+ "WHERE id_reserva = ?";
		psQuery= db.prepareStatement(sentence);
		psQuery.setInt(1, res.getPk());
		
		psQuery.executeQuery();
		
		sentence = "DELETE "
				+ "FROM RESERVA "
				+ "WHERE id_reserva = ?";
		psQuery= db.prepareStatement(sentence);
		psQuery.setInt(1, res.getPk());
		
		psQuery.executeQuery();

		psQuery.close();
	}

	public static void eliminarReserva(Reserva res) throws SQLException 
	{
		String sentence = "DELETE "
				+ "FROM RESERVA_HAB "
				+ "WHERE id_reserva = ? AND pk_habitacion = ?";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setInt(1, res.getPk());
		psQuery.setString(2, res.getHab().getId());
		
		psQuery.executeQuery();

		sentence = "UPDATE habitacion "
				+ "SET estado = 'libre' "
				+ "WHERE pk_habitacion = ? ";
		psQuery= db.prepareStatement(sentence);
		psQuery.setString(1,res.getHab().getId());
		
		psQuery.executeQuery();

		psQuery.close();
	}
	
	public static void añadirReserva(int pk, Habitacion habitacion) throws SQLException 
	{
		String sentence = "INSERT INTO reserva_hab (id_reserva, pk_habitacion, id_supl_mod, nombre) "
				+ "VALUES (?, ?, ?, ?)";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setInt(1, pk);
		psQuery.setString(2, habitacion.getId());
		psQuery.setInt(3, habitacion.modalidad_id);
		psQuery.setString(4, habitacion.tipo);
		
		psQuery.executeQuery();

		psQuery.close();
	}

	public static void eleiminarReserva1(int pk) throws SQLException 
	{	
		String sentence = "DELETE "
				+ "FROM RESERVA "
				+ "WHERE id_reserva = ?";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setInt(1, pk);

		psQuery.executeQuery();

		psQuery.close();
	}

	public static boolean existenReservas_hab(int pk_reserva) throws SQLException 
	{
		String sentence = "SELECT count(*) "
				+ "FROM reserva_hab "
				+ "WHERE id_reserva = ?";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setInt(1, pk_reserva);

		ResultSet rs = psQuery.executeQuery();
		while (rs.next()) {
			if (rs.getInt(1) >= 1) {
				return true;
			}
			else {
				return false;
			}
		}
		return false;
	}
	
	@SuppressWarnings("deprecation")
	//TODO
	public static int calcularPrecioFinal(Reserva res) throws SQLException
	{
		int aPagar = 0;
		Date today = new Date();
		today.setHours(0); 
		int diasReserva;
		if (today.after(res.f_fin_d) ) {
			diasReserva = res.f_fin_d.getDate() - res.f_in_d.getDate();
		}
		else {
			diasReserva = today.getDate() - res.f_in_d.getDate();
		}
		System.out.println("Dias de reserva " + diasReserva);
		System.out.println();
		
		String sentence = "SELECT pk_habitacion, nombre, id_supl_mod "
				+ "FROM reserva_hab "
				+ "WHERE id_reserva = ?";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setInt(1, res.pk);
		
		psQuery.executeQuery();
		ResultSet rs = psQuery.executeQuery();
		while(rs.next()) {
			
			String tipo_hab = rs.getString("nombre");
			System.out.println("Tipo de habitacion " + tipo_hab);
			int id_supl_mod = rs.getInt("id_supl_mod");
			System.out.println("Id del suplemento de modalidad " + id_supl_mod );
			
			sentence = "SELECT nombre, importe_dia "
					+ "FROM precio_tipo "
					+ "WHERE fecha_inicio <= ? AND fecha_fin >= ? AND nombre = ?";
			psQuery= db.prepareStatement(sentence);
			
			psQuery.setDate(1, new java.sql.Date(res.f_in));
			psQuery.setDate(2, new java.sql.Date(res.f_in));
			psQuery.setString(3, tipo_hab);

			psQuery.executeQuery();
			ResultSet rs2 = psQuery.executeQuery();
			if(rs2.next()) {
				System.out.println("Importe por día por habitacion " + rs2.getInt("importe_dia"));
				aPagar = aPagar + rs2.getInt("importe_dia") * (diasReserva);
			}
			System.out.println("Importe solo de habitacion " + aPagar);
			System.out.println();

			sentence = "SELECT importe " + 
					"FROM precio_suplemento p, suplemento_modalidad s " + 
					"WHERE p.ID_SUPL_MOD = s.ID_SUPL_MOD AND p.fecha_inicio <= ? AND p.fecha_fin >= ? AND p.nombre = ? AND s.id_supl_mod = ?";
			psQuery= db.prepareStatement(sentence);
			psQuery.setDate(1, new java.sql.Date(res.f_in));
			psQuery.setDate(2, new java.sql.Date(res.f_fin));
			psQuery.setString(3, tipo_hab);
			psQuery.setInt(4, id_supl_mod);

			psQuery.executeQuery();
			rs2 = psQuery.executeQuery();
			if(rs2.next()) {
				System.out.println("Importe por día por suplemento " + rs2.getInt("importe"));
				aPagar = aPagar + rs2.getInt("importe") * (diasReserva);
			}
			System.out.println("Importe de habitacion y suplemento " + aPagar);
			System.out.println();
			
		}
		
		psQuery.close();
		rs.close();

		return aPagar;
	}

	@SuppressWarnings("deprecation")
	public static int calcularPrecioCancelacion(Reserva res) throws SQLException
	{
		int aPagar = 0;

		String sentence = "SELECT nombre, importe_dia "
				+ "FROM precio_tipo "
				+ "WHERE fecha_inicio <= ? AND fecha_fin >= ? AND nombre = ?";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		Date today = new Date();
		today.setHours(0); 
		psQuery.setDate(1, new java.sql.Date(today.getTime()));
		psQuery.setDate(2, new java.sql.Date(today.getTime()));
		psQuery.setString(3, res.getHab().tipo);

		psQuery.executeQuery();
		ResultSet rs = psQuery.executeQuery();
		if(rs.next()) {
			aPagar = aPagar + rs.getInt("importe_dia");
		}
		psQuery.close();
		rs.close();

		return aPagar;
	}

	public static void cambiarModalidad(Reserva res, Object selectedItem, Habitacion habitacion) throws SQLException
	{
		String sentence = "SELECT id_supl_mod " 
				+ "FROM suplemento_modalidad " 
				+ "WHERE nombre_suplemento = ? and nombre = ?";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setString(1, String.valueOf(selectedItem));
		psQuery.setString(2, habitacion.tipo);

		int id_supl_nuevo = 0;
		psQuery.executeQuery();
		ResultSet rs = psQuery.executeQuery();
		if(rs.next()) {
			id_supl_nuevo = rs.getInt("id_supl_mod");
		}

		sentence = "SELECT id_supl_mod " 
				+ "FROM suplemento_modalidad " 
				+ "WHERE nombre_suplemento = ? and nombre = ?";
		psQuery= db.prepareStatement(sentence);
		psQuery.setString(1, habitacion.modalidad);
		psQuery.setString(2, habitacion.tipo);

		int id_supl_viejo = 0;
		psQuery.executeQuery();
		rs = psQuery.executeQuery();
		if(rs.next()) {
			id_supl_viejo = rs.getInt("id_supl_mod");
		}
		System.out.println("id_supl_mod antiguo " + id_supl_viejo);
		System.out.println("id_supl_mod nuevo " + id_supl_nuevo);
		System.out.println("el id de la reserva es "+  res.getPk());

		sentence = "UPDATE reserva_hab "
				+ "SET id_supl_mod = ? "
				+ "WHERE id_supl_mod = ? AND id_reserva = ?";
		psQuery= db.prepareStatement(sentence);
		psQuery.setInt(1, id_supl_nuevo);
		psQuery.setInt(2, id_supl_viejo);
		psQuery.setInt(3, res.getPk());

		psQuery.executeUpdate();

		psQuery.close();
		rs.close();
		System.out.println("no se peta aqui");
	}

	public static ArrayList<Habitacion> buscarHabitaciones() throws SQLException
	{
		String sentence = "SELECT * " 
				+ "FROM habitacion " 
				+ "ORDER BY pk_habitacion ";

		PreparedStatement psQuery= db.prepareStatement(sentence);
		ArrayList<Habitacion> habitaciones = new ArrayList<Habitacion>();
		ResultSet rs = psQuery.executeQuery();

		while (rs.next()) {
			Habitacion hab = new Habitacion(rs.getString("pk_habitacion"));
			hab.tipo = rs.getString("tipo");
			hab.setComentario(rs.getString("comentario"));
			hab.planta = rs.getString("planta");
			habitaciones.add(hab);
		}

		psQuery.close();
		rs.close();

		return habitaciones;
	}

	public static Habitacion buscarHabitacionNumero(int id) throws SQLException
	{
		String sentence = "SELECT * " 
				+ "FROM habitacion " 
				+ "WHERE pk_habitacion = ?";

		PreparedStatement psQuery= db.prepareStatement(sentence);
		Habitacion hab;
		psQuery.setInt(1, id);
		ResultSet rs = psQuery.executeQuery();

		if (rs.next()) {
			hab = new Habitacion(rs.getString("pk_habitacion"));
			hab.tipo = rs.getString("tipo");
			hab.setComentario(rs.getString("comentario"));
			hab.planta = rs.getString("planta");

			return hab;
		}

		psQuery.close();
		rs.close();

		return null;
	}

	@SuppressWarnings("deprecation")
	public static void nuevaTarea(String id, String comentario, Date fecha_hacer, String hora_hacer) throws SQLException 
	{
		String sentence  = "select SEC_TAREA.nextVal from dual";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		ResultSet rs = psQuery.executeQuery();
		int tarea_id = 0;
		if (rs.next()){
			tarea_id = rs.getInt(1);
		}

		Date fecha = new Date();
		fecha.setHours(0);
		Calendar calendario = new GregorianCalendar();
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		int minutos = calendario.get(Calendar.MINUTE);
		int segundos = calendario.get(Calendar.SECOND);
		String hoy = String.valueOf(hora) + ":" + String.valueOf(minutos) + ":" + String.valueOf(segundos);

		sentence = "INSERT INTO tarea (numero_tarea, pk_habitacion, fecha_registrada, hora_registrada, tarea_comentario, fecha_hacer, hora_hacer) "
				+ " VALUES (?, ?, ?, ?, ?, ?, ?)";

		psQuery= db.prepareStatement(sentence);
		psQuery.setInt(1, tarea_id);
		psQuery.setString(2, id);
		psQuery.setDate(3, new java.sql.Date(fecha.getTime()));
		psQuery.setString(4, hoy);
		psQuery.setString(5, comentario);
		psQuery.setDate(6, new java.sql.Date(fecha_hacer.getTime()));
		psQuery.setString(7, hora_hacer);

		psQuery.executeQuery();

		psQuery.close();
		rs.close();
	}

	public static ArrayList<Tarea> buscarTareas() throws SQLException
	{
		String sentence = "SELECT * "
				+ "FROM tarea";

		PreparedStatement psQuery= db.prepareStatement(sentence);
		ArrayList<Tarea> tareas = new ArrayList<Tarea>();

		ResultSet rs = psQuery.executeQuery();
		while (rs.next()) {
			Tarea tar = new Tarea();
			tar.setNumero_tarea(rs.getInt("numero_tarea"));
			tar.setPk_habitacion(rs.getInt("pk_habitacion"));
			tar.setFecha_registrada(rs.getDate("fecha_registrada").getTime());
			tar.setHora_registrada(rs.getString("hora_registrada"));
			tar.setFecha_hacer(rs.getDate("fecha_hacer").getTime());
			tar.setHora_hacer(rs.getString("hora_hacer"));
			tar.setTarea_comentario(rs.getString("tarea_comentario"));
			tareas.add(tar);
		}

		psQuery.close();
		rs.close();

		return tareas;
	}

	public static void borrarTarea(Tarea tar) throws SQLException 
	{
		String sentence = "DELETE " 
				+ "FROM TAREA "
				+ "WHERE numero_tarea = ?";

		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setInt(1, tar.getNumero_tarea());

		psQuery.executeUpdate();

		psQuery.close();
	}

	public static void nuevoAcompañante(Cliente cliente) throws SQLException
	{
		if (cliente.getLibroFamilia() == null) {
			String sentence = "INSERT INTO ACOMPAÑANTE (DNI, NOMBRE, APELLIDOS, DIRECCION, TELEFONO, CORREO, FECHA_NACIMIENTO, PAIS_RESIDENCIA) "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement psQuery= db.prepareStatement(sentence);
			psQuery.setString(1, cliente.getDni().toString().toUpperCase());
			psQuery.setString(2, cliente.getName());
			psQuery.setString(3, cliente.getApellidos());
			psQuery.setString(4, cliente.getDireccion());
			psQuery.setString(5, cliente.getTelefono());
			psQuery.setString(6, cliente.getCorreo());
			psQuery.setDate(7, new java.sql.Date(cliente.getNacimiento()));
			psQuery.setString(8, cliente.getResidencia());
			psQuery.executeQuery();

			psQuery.close();
		}
		else {
			String sentence = "INSERT INTO ACOMPAÑANTE (DNI, NOMBRE, APELLIDOS, DIRECCION, TELEFONO, CORREO, FECHA_NACIMIENTO, PAIS_RESIDENCIA, LIBRO_DE_FAMILIA) "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement psQuery= db.prepareStatement(sentence);
			psQuery.setString(1, cliente.getDni().toString().toUpperCase());
			psQuery.setString(2, cliente.getName());
			psQuery.setString(3, cliente.getApellidos());
			psQuery.setString(4, cliente.getDireccion());
			psQuery.setString(5, cliente.getTelefono());
			psQuery.setString(6, cliente.getCorreo());
			psQuery.setDate(7, new java.sql.Date(cliente.getNacimiento()));
			psQuery.setString(8, cliente.getResidencia());
			psQuery.setString(9, cliente.getLibroFamilia());
			psQuery.executeQuery();

			psQuery.close();
		}
	}

	public static void guardarCheckIn(Reserva reserva, ArrayList<Cliente> acompañantes, ArrayList<Reserva> habitacionesReservadas) throws SQLException
	{
		String sentence = "INSERT INTO ACOMPAÑA_A (ID_RESERVA, DNI_ACOMPAÑANTE) "
				+ "VALUES (?, ?)";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		for (Cliente acomp:acompañantes) {
			psQuery.setInt(1, reserva.getPk());
			psQuery.setString(2, acomp.getDni().toString());
			
			psQuery.executeQuery();
		}
		
		sentence = "UPDATE reserva " 
				+ "SET tipo = 'registro' "
				+ "WHERE id_reserva = ?";
		psQuery= db.prepareStatement(sentence);
		psQuery.setInt(1, reserva.getPk());
		System.out.println("pk de la reserva -> " + reserva.getPk());
		psQuery.executeQuery();
		
		for(Reserva res:habitacionesReservadas)
		{
			sentence = "UPDATE habitacion "
					+ "SET estado = 'reservada' "
					+ "WHERE pk_habitacion = ? ";
			psQuery= db.prepareStatement(sentence);
			psQuery.setString(1, res.getHab().getId());
			
			psQuery.executeQuery();
		}

		psQuery.close();
	}
	
	public static String estadoHabitacion(Habitacion hab) throws SQLException
	{
		String sentence = "SELECT estado "
				+ "FROM habitacion "
				+ "WHERE pk_habitacion = ? ";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setString(1, hab.getId());
		
		String estado = "";
		ResultSet rs = psQuery.executeQuery();
		while (rs.next()) {
			estado = rs.getString("estado");
		}
		
		rs.close();
		psQuery.close();
		
		return estado;
	}
	
	public static ArrayList<Reserva> buscarRegistros() throws SQLException
	{
//		String sentence = "SELECT DISTINCT fecha_inicio, fecha_fin, pk_habitacion, id_supl_mod, nombre_suplemento, id_reserva, dni " + 
//				"FROM reserva NATURAL JOIN reserva_hab NATURAL JOIN suplemento_modalidad "
//				+ "WHERE tipo = 'registro' ";
		String sentence = "SELECT DISTINCT fecha_inicio, fecha_fin, id_reserva, dni " + 
				"FROM reserva "
				+ "WHERE tipo = 'registro' ";
		PreparedStatement psQuery= db.prepareStatement(sentence);

		ArrayList<Reserva> reservas = new ArrayList<Reserva>();
		ResultSet rs = psQuery.executeQuery();
		while (rs.next()) {
			Reserva res = new Reserva();
			res.setPk(rs.getInt("id_reserva"));
			res.datos(rs.getString("dni"));
			res.fecha(rs.getDate("fecha_inicio").getTime(), rs.getDate("fecha_fin").getTime());
			reservas.add(res);
		}

		psQuery.close();
		rs.close();

		return reservas;
	}
	
	public static ArrayList<Reserva> buscarRegistrosPorDni(String dni) throws SQLException
	{
		String sentence = "SELECT DISTINCT fecha_inicio, fecha_fin, pk_habitacion, id_supl_mod, nombre_suplemento,  id_reserva "
				+ "FROM reserva NATURAL JOIN reserva_hab NATURAL JOIN suplemento_modalidad "
				+ "WHERE tipo = 'registro' AND dni = ?";
		PreparedStatement psQuery= db.prepareStatement(sentence);
		psQuery.setString(1, dni);

		ArrayList<Reserva> reservas = new ArrayList<Reserva>();
		ResultSet rs = psQuery.executeQuery();
		while (rs.next()) {
			Habitacion hab = new Habitacion(rs.getString("pk_habitacion"));
			hab.setModalidad(rs.getString("nombre_suplemento"), rs.getInt("id_supl_mod"));
			Reserva res = new Reserva();
			res.setPk(rs.getInt("id_reserva"));
			res.add(hab);
			res.datos(dni);
			res.fecha(rs.getDate("fecha_inicio").getTime(), rs.getDate("fecha_fin").getTime());
			reservas.add(res);
		}

		psQuery.close();
		rs.close();

		return reservas;
	}

}
