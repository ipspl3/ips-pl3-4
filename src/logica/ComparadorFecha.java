package logica;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class ComparadorFecha {
	
	private int dia;
	private int mes;
	private int a�o;
	
	private int diafinal;
	private int mesfinal;
	private int a�ofinal;
	
	private Calendar c;
	
	private int diaactual;
	private int mesactual;
	private int a�oactual;
	
	public ComparadorFecha(String fechainicio,String fechafinal)
	{
		String[] componentes = fechainicio.split("/");
		this.dia=Integer.parseInt(componentes[0]);
		this.mes=Integer.parseInt(componentes[1]);
		this.a�o=Integer.parseInt(componentes[2]);
		
		String[] componentes2 = fechafinal.split("/");
		this.diafinal=Integer.parseInt(componentes2[0]);
		this.mesfinal=Integer.parseInt(componentes2[1]);
		this.a�ofinal=Integer.parseInt(componentes2[2]);
		c = new GregorianCalendar();;
		diaactual = c.get(Calendar.DATE);
		mesactual = c.get(Calendar.MONTH)+1;
		a�oactual = c.get(Calendar.YEAR);
	}
	//Para solo comparar fecha con actual
	public ComparadorFecha(String fecha)
	{
		String[] componentes = fecha.split("/");
		
		this.dia=Integer.parseInt(componentes[0]);
		this.mes=Integer.parseInt(componentes[1]);
		this.a�o=Integer.parseInt(componentes[2]);	
		
		c = new GregorianCalendar();;
		diaactual = c.get(Calendar.DATE);
		mesactual = c.get(Calendar.MONTH)+1;
		a�oactual = c.get(Calendar.YEAR);
		
		
	}
	
	public String getFechaInicio()
	{
		String fechainicio = String.valueOf(dia) + "/" + String.valueOf(mes) + "/"+ String.valueOf(a�o);
		return fechainicio;
	}
	
	public String getFechaFinal()
	{
		String fechafinal = String.valueOf(diafinal) + "/" + String.valueOf(mesfinal) + "/" + String.valueOf(a�ofinal);
		return fechafinal;
	}
	
	//Compara la fecha introducida primero en el constructor con la fecha actual
	public boolean CheckFechaActual() {
		boolean fechacorrecta = false;

		if (a�o < a�oactual) {
			return false;
		} else if (a�o >= a�oactual) {
			if (a�oactual == a�o) {
				if (mes >= mesactual) {
					if (mesactual == mes) {
						if (dia < diaactual) {
							return false;
						} else {
							fechacorrecta = true;
						}

					} else {
						fechacorrecta = true;
					}
				} else {
					fechacorrecta = false;
				}
			} else {
				fechacorrecta = true;
			}
		}

		return fechacorrecta;
	}
	
	//Compara las dos fechas introducidas en el constructor
	public boolean CheckFechaFinal() {
		boolean fechacorrecta = false;

		if (a�ofinal < a�o ) {
			return false;
		} else if (a�ofinal >= a�o) {
			if (a�ofinal == a�o) {
				if ( mesfinal>= mes) {
					if (mesfinal == mes) {
						if (diafinal < dia ) {
							return false;
						} else {
							return true;
						}

					} else {
						fechacorrecta = true;
					}
				} else {
					fechacorrecta = false;
				}
			} else {
				fechacorrecta = true;
			}
		}

		return fechacorrecta;
	}
	
	

}
