package logica;

public class Tarjeta {

	String tarjeta;
	String tipo;
	
	public Tarjeta(String tarj) {
		tarjeta = tarj;
		tipo = buscarTipo();
	}
	
	public String buscarTipo(){
		String numeros = tarjeta.substring(0,1);
		int numero = Integer.parseInt(numeros);
		switch(numero) {
		case 3:
			return "American Express";
		case 4:
			return "VISA";
		case 5:
			return "MasterCard";
		}
		return "Desconocido";
	}
	
	public boolean comprobarTarjeta()
	  {
	    final int length = tarjeta.length();
	    int sum = 0;
	    boolean alternate = false;
	    for (int i = length - 1; i >= 0; i--)
	    {
	      int digit = Character.digit(tarjeta.charAt(i), 10);
	      if (alternate)
	      {
	        digit = digit * 2;
	        digit = digit > 9? digit - 9: digit;
	      }
	      sum = sum + digit;
	      alternate = !alternate;
	    }
	    final boolean passesLuhnCheck = sum % 10 == 0;
	    return passesLuhnCheck;
	}
	
//	public boolean comprobarTarjeta() {
//		char[] temp = tarjeta.toCharArray();
//		for (int i=0; i<16; i=i+2) {
//			int p = Integer.parseInt(tarjeta.substring(i,i+1)) * 2;
//			if (p > 9) {
//				p = p-9;
//			}
//			temp[i] = String.valueOf(p).charAt(0);
//		}
//
//		int fin = 0;
//		for (int i=0; i<temp.length; i++) {
//			fin = fin + Integer.parseInt(String.valueOf(temp[i]));
//		}
//		if (fin%10==0 && fin<=150) {
//			return true;
//		}
//		return false;
//	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}
