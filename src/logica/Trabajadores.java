package logica;

import java.util.HashMap;

public class Trabajadores {
	private HashMap<String,String> trabajadores = new HashMap<String, String>();
	
	/**
	 * Inserta los trabajadores por nombre y dni. El nombre lo convierte a minusculas.
	 * @param nombre
	 * @param dni
	 */
	public void aņadirTrabajador(String nombre, String dni){
		trabajadores.put(nombre.toLowerCase(), dni);
	}
	
	public String getDni(String nombre){
		return trabajadores.get(nombre.toLowerCase());
	}
	
	public String encontrarTrabajador(String nombre){
		for (String clave : trabajadores.keySet()) {
		    if(clave.contains(nombre.toLowerCase()))
		    		return capitalize(clave);
		}
		return null;
	}
	
	public String capitalize(String texto) {
		char[] caracteres = texto.toCharArray();
		for (int i = 0; i < caracteres.length-2; i++) {
			if (caracteres[i] == ' ')
		      caracteres[i + 1] = Character.toUpperCase(caracteres[i + 1]);
			if(i==0) 
				caracteres[0] = Character.toUpperCase(caracteres[0]);		
		}
		return String.valueOf(caracteres);
		
		
	}

}
