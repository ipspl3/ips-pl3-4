package logica;

import java.sql.SQLException;

public class Reserva {

	String dni;
	
	Habitacion hab;
	public java.sql.Date f_in_d;
	public java.sql.Date f_fin_d;
	long f_in;
	long f_fin;
	
	int pk = -1;
	
	double gastos;
	
	public Habitacion getHab() {
		return hab;
	}

	public void setHab(Habitacion hab) {
		this.hab = hab;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public double getGastos() {
		return gastos;
	}

	public void setGastos(double gastos) {
		this.gastos = gastos;
	}

	public void grabar() throws SQLException {
		DataBase.nuevaReserva_hab(pk, dni, hab.id, hab.modalidad_id, hab.tipo);
	}
	
	public void add(Habitacion e) {
		hab = e;
	}
	
	public void datos(String dni) throws SQLException{
		this.dni = dni;
	}
	
	public void fecha(long l, long m) {
		f_in = l;
		f_in_d = new java.sql.Date(f_in);
		f_fin = m;
		f_fin_d = new java.sql.Date(f_fin);
	}
	
	public void setPk(int pk) {
		this.pk = pk;
	}
	
	public int getPk() {
		return pk;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public String toString(){
		if (hab != null) {
			return "Habitacion: " + hab.id + " " + hab.tipo + " ------- " +  hab.modalidad + " "
					+ " ------- Fechas: " + f_in_d.getDate() +"/" + (f_in_d.getMonth()+1) + " -- " + f_fin_d.getDate() +"/" + (f_fin_d.getMonth()+1); 
		}
		else {
			return "Reserva con id " + pk + " a nombre del DNI/Pasaporte " + dni 
					+ " ------- Fechas: " + f_in_d.getDate() +"/" + (f_in_d.getMonth()+1) + " -- " + f_fin_d.getDate() +"/" + (f_fin_d.getMonth()+1);
		}
	}

	public double cargarGastosCancelacion() throws SQLException {
		gastos = DataBase.calcularPrecioCancelacion(this);
		System.out.println(gastos);
		return gastos;
	}
	
	public double cargarGastosFinales() throws SQLException {
		gastos = DataBase.calcularPrecioFinal(this);
		return gastos;
	}
}
