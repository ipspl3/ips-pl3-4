package logica;

public class Tarea {

	private int numero_tarea;
	private int pk_habitacion;
	private long fecha_registrada;
	private String tarea_comentario;
	private String hora_registrada;
	private long fecha_hacer;
	private String hora_hacer;
	
	public int getNumero_tarea() {
		return numero_tarea;
	}
	public void setNumero_tarea(int numero_tarea) {
		this.numero_tarea = numero_tarea;
	}
	public int getPk_habitacion() {
		return pk_habitacion;
	}
	public void setPk_habitacion(int pk_habitacion) {
		this.pk_habitacion = pk_habitacion;
	}
	public long getFecha_registrada() {
		return fecha_registrada;
	}
	public void setFecha_registrada(long fecha_registrada) {
		this.fecha_registrada = fecha_registrada;
	}
	public String getTarea_comentario() {
		return tarea_comentario;
	}
	public void setTarea_comentario(String tarea_comentario) {
		this.tarea_comentario = tarea_comentario;
	}
	public String getHora_registrada() {
		return hora_registrada;
	}
	public void setHora_registrada(String hora_registrada) {
		this.hora_registrada = hora_registrada;
	}
	public long getFecha_hacer() {
		return fecha_hacer;
	}
	public void setFecha_hacer(long fecha_hacer) {
		this.fecha_hacer = fecha_hacer;
	}
	public String getHora_hacer() {
		return hora_hacer;
	}
	public void setHora_hacer(String hora_hacer) {
		this.hora_hacer = hora_hacer;
	}
	
	@Override
	public String toString() {
		return "Tarea " + numero_tarea;
	}
}
