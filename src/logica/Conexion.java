package logica;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Conexion {
	
	private static final String USUARIO = "uo250972";
	private static final String CONTRASEŅA = "uo250972";
	private static final String URL = "jdbc:oracle:thin:@156.35.94.99:3389:DESA";
	private static Connection con;
	
	private static Connection getConnection() throws SQLException{
		DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
		return  DriverManager.getConnection(URL,USUARIO,CONTRASEŅA);
	}
	
	public void establecerConexion() throws SQLException{
		con = getConnection();	
	}
	
	public Connection getCon(){
		return con;
	}
	
	@SuppressWarnings("unused")
	private static void imprimirResultados(ResultSet rs) throws SQLException {
		int columnCount = rs.getMetaData().getColumnCount();
		StringBuilder headers = new StringBuilder();		
		for(int i = 1; i < columnCount ; i++)
			headers.append(rs.getMetaData().getColumnName(i) + "\t");
		headers.append(rs.getMetaData().getColumnName(columnCount));
		System.out.println(headers.toString());
		StringBuilder result = null;
		while (rs.next()) {
			result = new StringBuilder();	
			for(int i = 1; i < columnCount ; i++)
				result.append(rs.getObject(i) + "\t");
			result.append(rs.getObject(columnCount));
			System.out.println(result.toString());
		}
		if(result == null)
			System.out.println("Not data found!!");
	}

//	private static void ejercicioPrueba() throws SQLException {
//	Connection con = getConnection();
//	System.out.println("######### EJERCICIO DE PRUEBA #########");
//	
//	Statement s = con.createStatement(); 
//	ResultSet rs = s.executeQuery ("select * from habitacion where habitacion.estado = 'Libre'");
//	imprimirResultados(rs);
//	
//	con.close();
//}
	

//	public static void main(String[] args) throws SQLException {	
//		ejercicioPrueba();
//		
//	}

}
