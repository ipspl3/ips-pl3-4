package logica;

public class DNI {

	String dni;
	boolean es_dni = false;
	boolean es_pasaporte = false;

	public DNI (String dni){
		this.dni = dni;
		comprobarDNI();
		comprobarPasaporte();
	}

	public boolean comprobarDNI(){
		if (dni.length()<8) {
			return false;
		}
		String numeros = dni.substring(0,8);
		String letra = dni.substring(8);
		if (isNumeric(numeros) && !isNumeric(letra)){
			int num = Integer.parseInt(numeros);
			num = num%23;
			switch (num){
			case 0:
				if (letra.toUpperCase().equals("T")){
					return true;
				}
				return false;
			case 1:
				if (letra.toUpperCase().equals("R")){
					return true;
				}
				return false;
			case 2:
				if (letra.toUpperCase().equals("W")){
					return true;
				}
				return false;
			case 3:
				if (letra.toUpperCase().equals("A")){
					return true;
				}
				return false;
			case 4:
				if (letra.toUpperCase().equals("G")){
					return true;
				}
				return false;
			case 5:
				if (letra.toUpperCase().equals("M")){
					return true;
				}
				return false;
			case 6:
				if (letra.toUpperCase().equals("Y")){
					return true;
				}
				return false;
			case 7:
				if (letra.toUpperCase().equals("F")){
					return true;
				}
				return false;
			case 8:
				if (letra.toUpperCase().equals("P")){
					return true;
				}
				return false;
			case 9:
				if (letra.toUpperCase().equals("D")){
					return true;
				}
				return false;
			case 10:
				if (letra.toUpperCase().equals("X")){
					return true;
				}
				return false;
			case 11:
				if (letra.toUpperCase().equals("B")){
					return true;
				}
				return false;
			case 12:
				if (letra.toUpperCase().equals("N")){
					return true;
				}
				return false;
			case 13:
				if (letra.toUpperCase().equals("J")){
					return true;
				}
				return false;
			case 14:
				if (letra.toUpperCase().equals("Z")){
					return true;
				}
				return false;
			case 15:
				if (letra.toUpperCase().equals("S")){
					return true;
				}
				return false;
			case 16:
				if (letra.toUpperCase().equals("Q")){
					return true;
				}
				return false;
			case 17:
				if (letra.toUpperCase().equals("V")){
					return true;
				}
				return false;
			case 18:
				if (letra.toUpperCase().equals("H")){
					return true;
				}
				return false;
			case 19:
				if (letra.toUpperCase().equals("L")){
					return true;
				}
				return false;
			case 20:
				if (letra.toUpperCase().equals("C")){
					return true;
				}
				return false;
			case 21:
				if (letra.toUpperCase().equals("K")){
					return true;
				}
				return false;
			case 22:
				if (letra.toUpperCase().equals("E")){
					return true;
				}
				return false;
			}
		}
		es_pasaporte = false;
		es_dni = true;
		return false;
	}

	private boolean isNumeric(String cadena){
		try {
			Integer.parseInt(cadena);
			return true;
		} catch(NumberFormatException nfe){
			return false;
		}
	}

	public boolean comprobarPasaporte(){
		if (dni.length()<3) {
			return false;
		}
		String letras = dni.substring(0,3);
		String numeros = dni.substring(3);
		es_pasaporte = true;
		es_dni = false;
		if (isNumeric(numeros) && !isNumeric(letras)){
			return true;
		}
		else{
			return false;
		}
	}
	
	public String que_es() {
		if (es_pasaporte) {
			return "pasaporte";
		}
		else {
			return "dni";
		}
	}
	
	@Override
	public String toString() {
		return dni;
	}
	
}
