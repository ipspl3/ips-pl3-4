package igu;

import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import logica.DataBase;
import logica.Tarea;

import javax.swing.JScrollPane;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class CardTareas extends JPanel {

	private VentanaPersonalLimpieza papa;
	private JScrollPane scrollPane;
	private JList<Tarea> list;
	private DefaultListModel<Tarea> listModel = new DefaultListModel<Tarea>();
	private JPanel pnInfo;
	private JLabel lblHabitacion;
	private JLabel lblFechaRegistrada;
	private JLabel lblFechaHacer;
	private JLabel lblHoraHacer;
	private JLabel lblTarea;
	private JLabel lblTxtHabitacion;
	private JLabel lblTxtFregis;
	private JLabel lblTxtFhacer;
	private JLabel lblTxtHora;
	private JLabel lblTxtTarea;
	private JButton btnMarcar;
	private JButton btnCancelar;
	
	public CardTareas(VentanaPersonalLimpieza padre) {

		papa = padre;
		
		setLayout(null);
		add(getScrollPane());
		add(getPnInfo());
		add(getBtnMarcar());
		add(getBtnCancelar());
		
		setVisible(true);
		
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBorder(new TitledBorder(null, "Tareas", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			scrollPane.setBounds(10, 31, 279, 607);
			scrollPane.setViewportView(getList());
		}
		return scrollPane;
	}
	private JList<Tarea> getList() {
		if (list == null) {
			list = new JList<Tarea>(listModel);
			list.addMouseListener(new MouseAdapter() {
				@SuppressWarnings("deprecation")
				@Override
				public void mouseClicked(MouseEvent e) {
					pnInfo.setVisible(true);
					btnMarcar.setVisible(true);
					btnCancelar.setVisible(true);
					
					Tarea tar = list.getSelectedValue();
					lblTxtHabitacion.setText(String.valueOf(tar.getPk_habitacion()));
					Date dat = new Date(tar.getFecha_registrada());
					lblTxtFregis.setText(String.valueOf(dat.getDate() + "/" + dat.getMonth() + "/" + dat.getYear()));
					dat = new Date(tar.getFecha_hacer());
					lblTxtFhacer.setText(String.valueOf(dat.getDate() + "/" + dat.getMonth() + "/" + dat.getYear()));
					lblTxtHora.setText(String.valueOf(tar.getHora_hacer()));
					lblTxtTarea.setText(tar.getTarea_comentario());
				}
			});
		}
		return list;
	}
	
	public void todasTareas() {
		if (DataBase.isConnected() && listModel.isEmpty()) {
			try {
				ArrayList<Tarea> reservas = DataBase.buscarTareas();
				reservas.sort(new ComparadorPorFecha());
				for (Tarea res:reservas) 
				{
					listModel.addElement(res);
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private JPanel getPnInfo() {
		if (pnInfo == null) {
			pnInfo = new JPanel();
			pnInfo.setBorder(new TitledBorder(null, "Informaci\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnInfo.setBounds(299, 31, 337, 317);
			pnInfo.setLayout(null);
			pnInfo.add(getLblHabitacion());
			pnInfo.add(getLblFechaRegistrada());
			pnInfo.add(getLblFechaHacer());
			pnInfo.add(getLblHoraHacer());
			pnInfo.add(getLblTarea());
			pnInfo.add(getLblTxtHabitacion());
			pnInfo.add(getLblTxtFregis());
			pnInfo.add(getLblTxtFhacer());
			pnInfo.add(getLblTxtHora());
			pnInfo.add(getLblTxtTarea());
			pnInfo.setVisible(false);
		}
		return pnInfo;
	}
	private JLabel getLblHabitacion() {
		if (lblHabitacion == null) {
			lblHabitacion = new JLabel("Habitacion:");
			lblHabitacion.setBounds(10, 41, 74, 14);
		}
		return lblHabitacion;
	}
	private JLabel getLblFechaRegistrada() {
		if (lblFechaRegistrada == null) {
			lblFechaRegistrada = new JLabel("Fecha registrada:");
			lblFechaRegistrada.setBounds(10, 96, 104, 14);
		}
		return lblFechaRegistrada;
	}
	private JLabel getLblFechaHacer() {
		if (lblFechaHacer == null) {
			lblFechaHacer = new JLabel("Fecha hacer:");
			lblFechaHacer.setBounds(10, 151, 83, 14);
		}
		return lblFechaHacer;
	}
	private JLabel getLblHoraHacer() {
		if (lblHoraHacer == null) {
			lblHoraHacer = new JLabel("Hora hacer:");
			lblHoraHacer.setBounds(10, 206, 83, 14);
		}
		return lblHoraHacer;
	}
	private JLabel getLblTarea() {
		if (lblTarea == null) {
			lblTarea = new JLabel("Tarea:");
			lblTarea.setBounds(10, 261, 83, 14);
		}
		return lblTarea;
	}
	private JLabel getLblTxtHabitacion() {
		if (lblTxtHabitacion == null) {
			lblTxtHabitacion = new JLabel("");
			lblTxtHabitacion.setBounds(132, 41, 195, 14);
		}
		return lblTxtHabitacion;
	}
	private JLabel getLblTxtFregis() {
		if (lblTxtFregis == null) {
			lblTxtFregis = new JLabel("");
			lblTxtFregis.setBounds(132, 96, 195, 14);
		}
		return lblTxtFregis;
	}
	private JLabel getLblTxtFhacer() {
		if (lblTxtFhacer == null) {
			lblTxtFhacer = new JLabel("");
			lblTxtFhacer.setBounds(132, 151, 195, 14);
		}
		return lblTxtFhacer;
	}
	private JLabel getLblTxtHora() {
		if (lblTxtHora == null) {
			lblTxtHora = new JLabel("");
			lblTxtHora.setBounds(132, 206, 195, 14);
		}
		return lblTxtHora;
	}
	private JLabel getLblTxtTarea() {
		if (lblTxtTarea == null) {
			lblTxtTarea = new JLabel("");
			lblTxtTarea.setBounds(66, 261, 261, 14);
		}
		return lblTxtTarea;
	}
	private JButton getBtnMarcar() {
		if (btnMarcar == null) {
			btnMarcar = new JButton("Marcar como realizada");
			btnMarcar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						DataBase.borrarTarea(list.getSelectedValue());
						JOptionPane.showMessageDialog(null, "La tarea se ha marcado como realizada.");
						refrescar();
					}
					catch (SQLException e1) {
						JOptionPane.showMessageDialog(null, "No se ha podido marcar como realizada. Consulte con un t�cnico.");
						e1.printStackTrace();
					}
				}
			});
			btnMarcar.setBounds(309, 359, 176, 23);
			btnMarcar.setVisible(false);
		}
		return btnMarcar;
	}
	private void refrescar() {
		listModel.clear();
		todasTareas();
		pnInfo.setVisible(false);
		btnMarcar.setVisible(false);
		btnCancelar.setVisible(false);
	}
	private void iniciar() {
		refrescar();
		papa.iniciar();
	}
	
	public class ComparadorPorFecha implements Comparator<Tarea> {

		@Override
		public int compare(Tarea p1, Tarea p2) {
			if (p1.getFecha_hacer() == p2.getFecha_hacer()) {
				String[] temp = p1.getHora_hacer().split(":");
				String[] temp2 = p2.getHora_hacer().split(":");
				if (temp[0].equals(temp2[0])) {
					if (temp[1].equals(temp2[1])) {
						return Integer.parseInt(temp[2]) > Integer.parseInt(temp2[2]) ? 1 : 0;
					}
					else {
						return Integer.parseInt(temp[1]) > Integer.parseInt(temp2[1]) ? 1 : -1;
					}
				}
				else {
					return Integer.parseInt(temp[0]) > Integer.parseInt(temp2[0]) ? 1 : -1;
				}
			}
			return p1.getFecha_hacer() > p2.getFecha_hacer() ? 1 : -1;
		}
	}
	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					iniciar();
				}
			});
			btnCancelar.setBounds(495, 359, 141, 23);
			btnCancelar.setVisible(false);
		}
		return btnCancelar;
	}
}

