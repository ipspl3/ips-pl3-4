package igu;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import logica.Cliente;
import logica.DNI;
import logica.DataBase;
import logica.Reserva;
import javax.swing.UIManager;
import javax.swing.ListSelectionModel;

@SuppressWarnings("serial")
public class CardCheckOut extends JPanel {

	private VentanaRecepcionista padre;

	private Reserva res;
	private Cliente cliente;

	private JLabel lblDni;
	private JTextField txtDni;
	private JButton btnBuscar;
	private JScrollPane scrollPane;
	private JList<Reserva> listRegistros;
	private DefaultListModel<Reserva> listModelRegistros;

	private JPanel pnCliente;
	private JLabel lblTxtTelefono;
	private JLabel lblTarjeta;
	private JLabel lblTxtTarjeta;
	private JLabel lblTelefono;
	private JLabel lblTxtNombre;
	private JLabel lblNombre;

	private JPanel pnReserva;
	private JSeparator separator;
	//	private JLabel lblHabitacion;
	//	private JLabel lblTxtHabitacion;
	//	private JLabel lblModalidad;

	private JButton btnCancelar;
	private JButton btnFinalizarReserva;



	public CardCheckOut(VentanaRecepcionista papa) {

		padre = papa;
		setLayout(null);
		add(getLblDni());
		add(getTxtDni());
		add(getBtnBuscar());
		add(getScrollPane());

		add(getPnCliente());
		add(getSeparator());
		//		add(getPnReserva());

		add(getBtnCancelar());
		add(getBtnFinalizarReserva());

		todosRegistros();

		setVisible(true);
	}

	private JLabel getLblDni() {
		if (lblDni == null) {
			lblDni = new JLabel("DNI:");
			lblDni.setBounds(24, 22, 64, 14);
		}
		return lblDni;
	}

	private JTextField getTxtDni() {
		if (txtDni == null) {
			txtDni = new JTextField();
			txtDni.setBounds(93, 19, 141, 20);
			txtDni.setColumns(10);
			txtDni.addKeyListener(new KeyAdapter() {
				int limite = 9;
				@Override
				public void keyTyped(KeyEvent e) {
					if (txtDni.getText().length() == limite){
						DNI dni = new DNI(txtDni.getText().toUpperCase());
						boolean bien = dni.comprobarDNI();
						boolean bien_p = dni.comprobarPasaporte();
						if (bien || bien_p){
							txtDni.setBorder(new LineBorder(Color.BLACK));
							btnBuscar.setEnabled(true);
						}
						else{
							txtDni.setBorder(new LineBorder(Color.RED));
						}
					}
					if (txtDni.getText().length() >= limite){
						e.consume();
					}
				}
			});
		}
		return txtDni;
	}
	private JButton getBtnBuscar() {
		if (btnBuscar == null) {
			btnBuscar = new JButton("Buscar");
			btnBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (txtDni.getText().equals("")) {
						JOptionPane.showMessageDialog(null, "Introduce un DNI o pasaporte v�lido");
					}
					else {
						DNI dni = new DNI(txtDni.getText().toUpperCase());
						if (dni.comprobarDNI() || dni.comprobarPasaporte()) {
							listModelRegistros.clear();
							try {
								ArrayList<Reserva> reservas = DataBase.buscarRegistrosPorDni(txtDni.getText().toString().toUpperCase());
								for (Reserva res:reservas) 
								{
									listModelRegistros.addElement(res);
								}
							} catch (SQLException e1) {
								e1.printStackTrace();
							}
						}
						else {
							JOptionPane.showMessageDialog(null, "Introduce un DNI o pasaporte v�lido");
						}
					}
				}
			});
			btnBuscar.setBounds(266, 18, 89, 23);
		}
		return btnBuscar;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Registros", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			scrollPane.setBounds(10, 49, 622, 417);
			scrollPane.setViewportView(getListReservas());
		}
		return scrollPane;
	}

	private JList<Reserva> getListReservas() {
		if (listRegistros == null) {
			listModelRegistros = new DefaultListModel<Reserva>();
			listRegistros = new JList<Reserva>(listModelRegistros);
			listRegistros.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			listRegistros.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (listRegistros.getSelectedIndices().length<2) {
						res = listRegistros.getSelectedValue();
						try {
							cliente = DataBase.buscarClientePorDni(res.getDni());
							pnCliente.setVisible(true);
							rellenarCliente();
							btnFinalizarReserva.setEnabled(true);
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
					else {
						pnReserva.setVisible(false);
						comprobarClientes();
					}
				}
			});
		}
		return listRegistros;
	}

	@SuppressWarnings("deprecation")
	private void comprobarClientes(){
		Reserva res = (Reserva) listRegistros.getSelectedValues()[0];
		int pk = res.getPk();
		boolean perfe = true;
		for (Object resTemp: listRegistros.getSelectedValues()) {
			if (!(((Reserva) resTemp).getPk() == pk)) {
				JOptionPane.showMessageDialog(null, "Para una selecci�n m�ltiple, la reserva debe ser la misma.");
				perfe = false;
				btnFinalizarReserva.setEnabled(false);
			}
		}
		if (perfe) {
			btnFinalizarReserva.setEnabled(true);
		}
	}

	private void rellenarCliente() {
		lblTxtNombre.setText(cliente.getName() + ", " + cliente.getApellidos());
		lblTxtTelefono.setText(String.valueOf(cliente.getTelefono()));
		lblTxtTarjeta.setText(cliente.getTarjeta().toString());
	}

	//	private void rellenarHab() {
	//		lblTxtHabitacion.setText(res.getHab().id + " (" + res.getHab().tipo + ") ");
	//	}

	private JPanel getPnCliente() {
		if (pnCliente == null) {
			pnCliente = new JPanel();
			pnCliente.setBorder(new TitledBorder(null, "Cliente", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnCliente.setBounds(10, 490, 622, 101);
			pnCliente.setLayout(null);
			pnCliente.add(getLblNombre());
			pnCliente.add(getLblTxtNombre());
			pnCliente.add(getLblTelefono());
			pnCliente.add(getLblTxtTelefono());
			pnCliente.add(getLblTarjeta());
			pnCliente.add(getLblTxtTarjeta());
			pnCliente.setVisible(false);
		}
		return pnCliente;
	}
	private JLabel getLblNombre() {
		if (lblNombre == null) {
			lblNombre = new JLabel("Nombre:");
			lblNombre.setBounds(10, 26, 60, 14);
		}
		return lblNombre;
	}
	private JLabel getLblTxtNombre() {
		if (lblTxtNombre == null) {
			lblTxtNombre = new JLabel("");
			lblTxtNombre.setBounds(79, 26, 212, 14);
		}
		return lblTxtNombre;
	}
	private JLabel getLblTelefono() {
		if (lblTelefono == null) {
			lblTelefono = new JLabel("Telefono:");
			lblTelefono.setBounds(10, 51, 60, 14);
		}
		return lblTelefono;
	}
	private JLabel getLblTxtTelefono() {
		if (lblTxtTelefono == null) {
			lblTxtTelefono = new JLabel("");
			lblTxtTelefono.setBounds(79, 51, 212, 14);
		}
		return lblTxtTelefono;
	}
	private JLabel getLblTarjeta() {
		if (lblTarjeta == null) {
			lblTarjeta = new JLabel("Tarjeta:");
			lblTarjeta.setBounds(10, 76, 60, 14);
		}
		return lblTarjeta;
	}
	private JLabel getLblTxtTarjeta() {
		if (lblTxtTarjeta == null) {
			lblTxtTarjeta = new JLabel("");
			lblTxtTarjeta.setBounds(79, 76, 212, 14);
		}
		return lblTxtTarjeta;
	}

	private JSeparator getSeparator() {
		if (separator == null) {
			separator = new JSeparator();
			separator.setBounds(20, 477, 601, 2);
		}
		return separator;
	}
	//	private JPanel getPnReserva() {
	//		if (pnReserva == null) {
	//			pnReserva = new JPanel();
	//			pnReserva.setBounds(10, 439, 622, 150);
	//			pnReserva.setLayout(null);
	//			pnReserva.add(getLblHabitacin());
	//			pnReserva.add(getLblTxtHabitacion());
	//			pnReserva.add(getLblModalidad());
	//
	//			pnReserva.setVisible(false);
	//		}
	//		return pnReserva;
	//	}
	//	private JLabel getLblHabitacin() {
	//		if (lblHabitacion == null) {
	//			lblHabitacion = new JLabel("Habitaci\u00F3n:");
	//			lblHabitacion.setBounds(10, 11, 71, 14);
	//		}
	//		return lblHabitacion;
	//	}
	//	private JLabel getLblTxtHabitacion() {
	//		if (lblTxtHabitacion == null) {
	//			lblTxtHabitacion = new JLabel("");
	//			lblTxtHabitacion.setBounds(85, 11, 120, 14);
	//		}
	//		return lblTxtHabitacion;
	//	}
	//	private JLabel getLblModalidad() {
	//		if (lblModalidad == null) {
	//			lblModalidad = new JLabel("Modalidad:");
	//			lblModalidad.setBounds(10, 48, 71, 14);
	//		}
	//		return lblModalidad;
	//	}

	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					iniciar();
					padre.iniciar();
				}
			});
			btnCancelar.setBounds(514, 602, 118, 23);
		}
		return btnCancelar;
	}

	public void iniciar(){
		listModelRegistros.clear();
		txtDni.setText("");
		pnCliente.setVisible(false);

		pnReserva.setVisible(false);

		padre.iniciar();
	}

	private JButton getBtnFinalizarReserva() {
		if (btnFinalizarReserva == null) {
			btnFinalizarReserva = new JButton("Finalizar Reserva");
			btnFinalizarReserva.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						int opcion = JOptionPane.showConfirmDialog(null, "�Desea finalizar el alojamiento?", "Finalizar alojamiento", JOptionPane.YES_NO_OPTION);
						if(opcion == 0){//YES
							int pk_reserva = ((Reserva) listRegistros.getSelectedValue()).getPk();
							double totalGastos = 0;
							//for (Object ob:listRegistros.getSelectedValues()) {
							Reserva temp = (Reserva) listRegistros.getSelectedValue();

							totalGastos = totalGastos + temp.cargarGastosFinales();

							DataBase.eliminarRegistro(temp);
							//}
							listModelRegistros.removeElement((Reserva) listRegistros.getSelectedValue());
							JOptionPane.showMessageDialog(null,"Se ha cargado en su tarjeta un importe total de " + totalGastos + "�");
							if (!DataBase.existenReservas_hab(pk_reserva)) {
								JOptionPane.showMessageDialog(null,"No quedaban mas habitaciones reservadas, y el registro se ha finalizado completa.");
							}
							else {
								JOptionPane.showMessageDialog(null,"Se ha finalizado correctamente el registro de las habitaciones.");
							}
							refrescar();
						}
					}
					catch (SQLException e1) {
						JOptionPane.showMessageDialog(null, "La reserva no ha podido ser anulada, contacte con un t�cnico");
						e1.printStackTrace();
					}
				}
			});
			btnFinalizarReserva.setEnabled(false);
			btnFinalizarReserva.setBounds(346, 602, 158, 23);

		}
		return btnFinalizarReserva;
	}

	private void refrescar() {
		listModelRegistros.clear();
		todosRegistros();
	}

	public void todosRegistros() {
		listModelRegistros.clear();
		if (DataBase.isConnected() && listModelRegistros.isEmpty()) {
			try {
				ArrayList<Reserva> reservas = DataBase.buscarRegistros();
				for (Reserva res:reservas) 
				{
					listModelRegistros.addElement(res);
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
}
