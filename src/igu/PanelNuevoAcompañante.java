package igu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.toedter.calendar.JDateChooser;

import logica.Cliente;
import logica.DNI;
import logica.DataBase;

import java.awt.Toolkit;

@SuppressWarnings("serial")
public class PanelNuevoAcompañante extends JFrame{

	private CardCheckIn padre;
	private Cliente nuevoAcompañante;
	private boolean nuevo = true;
	private boolean modificar = false;
	
	private JLabel lblClienteDnipasaporte;
	private JTextField txtDni;
	private JPanel pnDatos;
	private JLabel lblNombre;
	private JLabel lblApellidos;
	private JLabel lblCorreo;
	private JLabel lblDireccion;
	private JLabel lblTelefono;
	private JLabel lblPaisDeResidencia;
	private JLabel lblFechaDeNacimiento;
	private JTextField txtNombre;
	private JTextField txtApellidos;
	private JTextField txtCorreo;
	private JTextField txtDireccion;
	private JTextField txtTelefono;
	private JTextField txtPaisDeResidencia;
	private JDateChooser nacimiento;
	private JButton btnGuardar;
	private JButton btnCancelar;
	private JLabel lblLibroDeFamilia;
	private JTextField txtLibro;
	private JButton btnBuscar;
	
	public PanelNuevoAcompañante(CardCheckIn papa) {
		setTitle("Nuevo acompa\u00F1ante");
		setIconImage(Toolkit.getDefaultToolkit().getImage(PanelNuevoAcompañante.class.getResource("/img/logo.jpg")));
		padre = papa;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 415, 428);

		setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		getContentPane().add(getPnDatos());
		getContentPane().add(getBtnGuardar());
		getContentPane().add(getBtnCancelar());
		setResizable(false);
		setVisible(true);
	}
	
	/**
	 * @wbp.parser.constructor
	 */
	public PanelNuevoAcompañante(CardCheckIn papa, Cliente acompañante) {
		setTitle("Nuevo acompa\u00F1ante");
		setIconImage(Toolkit.getDefaultToolkit().getImage(PanelNuevoAcompañante.class.getResource("/img/logo.jpg")));
		padre = papa;
		nuevoAcompañante = acompañante;
		modificar = true;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 415, 428);

		setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		getContentPane().add(getPnDatos());
		getContentPane().add(getBtnGuardar());
		getContentPane().add(getBtnCancelar());
		setResizable(false);
		setVisible(true);
		
		txtNombre.setText(acompañante.getName());
		txtApellidos.setText(acompañante.getApellidos());
		txtDni.setText(acompañante.getDni().toString());
		txtCorreo.setText(acompañante.getCorreo());
		txtTelefono.setText(acompañante.getTelefono());
		txtDireccion.setText(acompañante.getDireccion());
		txtPaisDeResidencia.setText(acompañante.getResidencia());
		nacimiento.setDate(new Date(acompañante.getNacimiento()));
		if (!(acompañante.getLibroFamilia() == null)) {
			txtLibro.setEditable(true);
			txtLibro.setText(acompañante.getLibroFamilia());
		}
	}
	
	private JPanel getPnDatos() {
		if (pnDatos == null) {
			pnDatos = new JPanel();
			pnDatos.setBorder(new TitledBorder(null, "Datos", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnDatos.setBounds(10, 21, 389, 333);
			pnDatos.setLayout(null);
			pnDatos.add(getLblClienteDnipasaporte());
			pnDatos.add(getLblNombre());
			pnDatos.add(getLblApellidos());
			pnDatos.add(getLblCorreo());
			pnDatos.add(getLblDireccion());
			pnDatos.add(getLblTelefono());
			pnDatos.add(getLblPaisDeResidencia());
			pnDatos.add(getLblFechaDeNacimiento());
			pnDatos.add(getTxtDni());
			pnDatos.add(getTxtNombre());
			pnDatos.add(getTxtApellidos());
			pnDatos.add(getTxtCorreo());
			pnDatos.add(getTxtDireccion());
			pnDatos.add(getTxtTelefono());
			pnDatos.add(getTxtPaisDeResidencia());
			pnDatos.add(getNacimiento());
			pnDatos.add(getLblLibroDeFamilia());
			pnDatos.add(getTxtLibro());
			pnDatos.add(getBtnBuscar());
		}
		return pnDatos;
	}
	private JLabel getLblClienteDnipasaporte() {
		if (lblClienteDnipasaporte == null) {
			lblClienteDnipasaporte = new JLabel("DNI/Pasaporte:");
			lblClienteDnipasaporte.setBounds(34, 20, 128, 14);
		}
		return lblClienteDnipasaporte;
	}
	private JTextField getTxtDni() {
		if (txtDni == null) {
			txtDni = new JTextField("");
			txtDni.setBounds(127, 20, 139, 20);
		}
		return txtDni;
	}
	private JLabel getLblNombre() {
		if (lblNombre == null) {
			lblNombre = new JLabel("Nombre:");
			lblNombre.setBounds(34, 54, 60, 14);
		}
		return lblNombre;
	}
	private JLabel getLblApellidos() {
		if (lblApellidos == null) {
			lblApellidos = new JLabel("Apellidos:");
			lblApellidos.setBounds(34, 88, 60, 14);
		}
		return lblApellidos;
	}
	private JLabel getLblCorreo() {
		if (lblCorreo == null) {
			lblCorreo = new JLabel("Correo:");
			lblCorreo.setBounds(34, 190, 60, 14);
		}
		return lblCorreo;
	}
	private JLabel getLblDireccion() {
		if (lblDireccion == null) {
			lblDireccion = new JLabel("Direccion:");
			lblDireccion.setBounds(34, 224, 60, 14);
		}
		return lblDireccion;
	}
	private JLabel getLblTelefono() {
		if (lblTelefono == null) {
			lblTelefono = new JLabel("Telefono:");
			lblTelefono.setBounds(34, 258, 60, 14);
		}
		return lblTelefono;
	}
	private JLabel getLblPaisDeResidencia() {
		if (lblPaisDeResidencia == null) {
			lblPaisDeResidencia = new JLabel("Pais de Residencia:");
			lblPaisDeResidencia.setBounds(34, 292, 131, 14);
		}
		return lblPaisDeResidencia;
	}
	private JLabel getLblFechaDeNacimiento() {
		if (lblFechaDeNacimiento == null) {
			lblFechaDeNacimiento = new JLabel("Fecha de nacimiento:");
			lblFechaDeNacimiento.setBounds(34, 122, 131, 14);
		}
		return lblFechaDeNacimiento;
	}
	private JTextField getTxtNombre() {
		if (txtNombre == null) {
			txtNombre = new JTextField();
			txtNombre.setBounds(127, 53, 139, 20);
			txtNombre.setColumns(10);
		}
		return txtNombre;
	}
	private JTextField getTxtApellidos() {
		if (txtApellidos == null) {
			txtApellidos = new JTextField();
			txtApellidos.setBounds(127, 86, 194, 20);
			txtApellidos.setColumns(10);
		}
		return txtApellidos;
	}
	private JTextField getTxtCorreo() {
		if (txtCorreo == null) {
			txtCorreo = new JTextField();
			txtCorreo.setBounds(127, 187, 194, 20);
			txtCorreo.setColumns(10);
		}
		return txtCorreo;
	}
	private JTextField getTxtDireccion() {
		if (txtDireccion == null) {
			txtDireccion = new JTextField();
			txtDireccion.setBounds(127, 220, 194, 20);
			txtDireccion.setColumns(10);
		}
		return txtDireccion;
	}
	private JTextField getTxtTelefono() {
		if (txtTelefono == null) {
			txtTelefono = new JTextField();
			txtTelefono.setBounds(127, 253, 194, 20);
			txtTelefono.setColumns(10);
		}
		return txtTelefono;
	}
	private JTextField getTxtPaisDeResidencia() {
		if (txtPaisDeResidencia == null) {
			txtPaisDeResidencia = new JTextField();
			txtPaisDeResidencia.setBounds(165, 287, 156, 20);
			txtPaisDeResidencia.setColumns(10);
		}
		return txtPaisDeResidencia;
	}
	
	@SuppressWarnings("deprecation")
	private JDateChooser getNacimiento() {
		if (nacimiento == null){
			nacimiento = new JDateChooser("dd/MM/yyyy", "##/##/####", '_');
			nacimiento.setVisible(true);
			nacimiento.setBounds(165, 119, 156, 23);
			Date mayorDe = new Date();
			mayorDe.setHours(0); 
			mayorDe.setYear(mayorDe.getYear());
			Date menorDe = new Date();
			menorDe.setHours(0); 
			menorDe.setYear(menorDe.getYear() - 150);

			nacimiento.setSelectableDateRange(menorDe, mayorDe);
		}
		return nacimiento;	
	}
	private JButton getBtnGuardar() {
		if (btnGuardar == null) {
			btnGuardar = new JButton("Guardar");
			btnGuardar.addActionListener(new ActionListener() {
				@SuppressWarnings("deprecation")
				public void actionPerformed(ActionEvent e) {
					if (nuevo && !modificar) {
						boolean condicionMenor = true;
						Date today = new Date();
						today.setHours(0); 
						today.setYear(today.getYear() - 18);
						if (nacimiento.getDate() != null) {
							if (nacimiento.getDate().getYear() > today.getYear() && txtLibro.getText().isEmpty()) {
								condicionMenor = false;
							}
							if (condicionMenor) {
								if (camposRellenados()) {
									DNI dni = new DNI(txtDni.getText());
									if ((dni.comprobarDNI() || dni.comprobarPasaporte())) {
										try {
											nuevoAcompañante = new Cliente();
											nuevoAcompañante.setAcompañante(true);
											nuevoAcompañante.setDni(new DNI(txtDni.getText()));
											nuevoAcompañante.setName(txtNombre.getText());
											nuevoAcompañante.setApellidos(txtApellidos.getText());
											nuevoAcompañante.setDireccion(txtDireccion.getText());
											nuevoAcompañante.setTelefono(txtTelefono.getText());
											nuevoAcompañante.setCorreo(txtCorreo.getText());
											nuevoAcompañante.setNacimiento(nacimiento.getDate().getTime());
											nuevoAcompañante.setResidencia(txtPaisDeResidencia.getText());
											if (!txtLibro.getText().isEmpty()) {
												nuevoAcompañante.setLibroFamilia(txtLibro.getText());
											}
											int opcion = JOptionPane.showConfirmDialog(null, "¿Desea confirmar los cambios?", "Confirmar cambios", JOptionPane.YES_NO_OPTION);
											if(opcion == 0){//YES
												DataBase.nuevoAcompañante(nuevoAcompañante);
												JOptionPane.showMessageDialog(null, "Se ha añadido al acompañante correctamente");
												padre.refrescarAcomp(nuevoAcompañante);
												dispose();
											}
										} 
										catch (SQLException e1) {
											e1.printStackTrace();
										}
									}
									else {
										JOptionPane.showMessageDialog(null, "Compruebe el DNI y vuelva a intentarlo.");
									}
								}
							}
							else {
								JOptionPane.showMessageDialog(null, "Rellene todos los campos (Libro de familia por ser menor)");
								txtLibro.setEditable(true);
							}
						} 
						else {
							JOptionPane.showMessageDialog(null, "Rellene todos los campos.");
						}
					}
					if (modificar) {
						try {
							nuevoAcompañante.setDni(new DNI(txtDni.getText()));
							nuevoAcompañante.setName(txtNombre.getText());
							nuevoAcompañante.setApellidos(txtApellidos.getText());
							nuevoAcompañante.setDireccion(txtDireccion.getText());
							nuevoAcompañante.setTelefono(txtTelefono.getText());
							nuevoAcompañante.setCorreo(txtCorreo.getText());
							nuevoAcompañante.setNacimiento(nacimiento.getDate().getTime());
							nuevoAcompañante.setResidencia(txtPaisDeResidencia.getText());
							if (!txtLibro.getText().isEmpty()) {
								nuevoAcompañante.setLibroFamilia(txtLibro.getText());
							}
							DataBase.modificarDatosAcompañante(nuevoAcompañante);
							JOptionPane.showMessageDialog(null, "Los datos se han guardado correctamente.");

							padre.refrescarAcomp(nuevoAcompañante);
							dispose();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
					else if (!nuevo){
						JOptionPane.showMessageDialog(null, "Se ha añadido al acompañante correctamente");

						padre.refrescarAcomp(nuevoAcompañante);
						dispose();
					}
				}
			});
			btnGuardar.setBounds(165, 365, 89, 23);
		}
		return btnGuardar;
	}
	private boolean camposRellenados() {
		boolean condicion = !txtDni.getText().isEmpty() && !txtNombre.getText().isEmpty() && !txtApellidos.getText().isEmpty() 
				&& !txtCorreo.getText().isEmpty() && !txtDireccion.getText().isEmpty() && !txtTelefono.getText().isEmpty() 
				&& !txtPaisDeResidencia.getText().isEmpty();
		return condicion;
	}
	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cerrar();
				}
			});
			btnCancelar.setBounds(264, 365, 89, 23);
		}
		return btnCancelar;
	}

	private void cerrar() {
		dispose();
	}
	private JLabel getLblLibroDeFamilia() {
		if (lblLibroDeFamilia == null) {
			lblLibroDeFamilia = new JLabel("Libro de familia:");
			lblLibroDeFamilia.setBounds(34, 156, 101, 14);
		}
		return lblLibroDeFamilia;
	}
	private JTextField getTxtLibro() {
		if (txtLibro == null) {
			txtLibro = new JTextField();
			txtLibro.setEditable(false);
			txtLibro.setBounds(127, 156, 194, 20);
			txtLibro.setColumns(10);
		}
		return txtLibro;
	}
	private JButton getBtnBuscar() {
		if (btnBuscar == null) {
			btnBuscar = new JButton("Buscar");
			btnBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (!txtDni.getText().isEmpty()){
						try {
							nuevoAcompañante = DataBase.buscarAcomapañantePorDni(txtDni.getText().toUpperCase());
							if (nuevoAcompañante == null){
								JOptionPane.showMessageDialog(null, "El cliente aun no ha sido registrado");
								nuevoAcompañante = new Cliente();
								nuevoAcompañante.setNuevo(true);
							}
							else{
								nuevo = false;
								JOptionPane.showMessageDialog(null, "Compruebe los datos guardados");
								txtNombre.setText(nuevoAcompañante.getName());
								txtApellidos.setText(nuevoAcompañante.getApellidos());
								txtTelefono.setText(String.valueOf(nuevoAcompañante.getTelefono()));
								txtDireccion.setText(nuevoAcompañante.getDireccion());
								txtCorreo.setText(nuevoAcompañante.getCorreo());
								nacimiento.setDate(new Date(nuevoAcompañante.getNacimiento()));
								txtPaisDeResidencia.setText(nuevoAcompañante.getResidencia());
								if (nuevoAcompañante.getLibroFamilia() != null) {
									txtLibro.setEditable(true);
									txtLibro.setText(nuevoAcompañante.getLibroFamilia());
								}
							}
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
					else if (!txtNombre.getText().isEmpty()){
						try {
							nuevoAcompañante = DataBase.buscarAcomapañantePorNombre(txtNombre.getText());
							if (nuevoAcompañante == null){
								JOptionPane.showMessageDialog(null, "El cliente aun no ha sido registrado");
								nuevoAcompañante = new Cliente();
								nuevoAcompañante.setNuevo(true);
							}
							else{
								nuevo = false;
								JOptionPane.showMessageDialog(null, "Compruebe los datos guardados");
								txtDni.setText(nuevoAcompañante.getDni().toString());
								txtApellidos.setText(nuevoAcompañante.getApellidos());
								txtTelefono.setText(String.valueOf(nuevoAcompañante.getTelefono()));
								txtDireccion.setText(nuevoAcompañante.getDireccion());
								txtCorreo.setText(nuevoAcompañante.getCorreo());
								nacimiento.setDate(new Date(nuevoAcompañante.getNacimiento()));
								txtPaisDeResidencia.setText(nuevoAcompañante.getResidencia());
								if (nuevoAcompañante.getLibroFamilia() != null) {
									txtLibro.setEditable(true);
									txtLibro.setText(nuevoAcompañante.getLibroFamilia());
								}
							}
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
					else {
						JOptionPane.showMessageDialog(null, "Introducca un DNI, pasaporte o nombre");
					}
				}
			});
			btnBuscar.setBounds(290, 34, 89, 23);
		}
		return btnBuscar;
	}
}
