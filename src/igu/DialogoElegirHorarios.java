package igu;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DialogoElegirHorarios extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextPane txtpnatencinSeAsignar;
	private JLabel lblImagen;
	private JCheckBox boxD2;
	private JCheckBox boxD3;
	private JCheckBox boxD4;
	private JCheckBox boxD5;
	private JCheckBox boxD6;
	private JCheckBox boxD7;
	List<String> dias;
	private JButton btnAceptar;
	List<Integer> boxSeleccionados = new ArrayList<Integer>();

	public DialogoElegirHorarios(List<String> dias) {
		this.dias = dias;
		setTitle("Escoger d\u00EDas");
		setBounds(100, 100, 425, 308);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		contentPanel.add(getTxtpnatencinSeAsignar());
		contentPanel.add(getLblImagen());
		contentPanel.add(getBoxD2());
		contentPanel.add(getBoxD3());
		contentPanel.add(getBoxD4());
		contentPanel.add(getBoxD5());
		contentPanel.add(getBoxD6());
		contentPanel.add(getBoxD7());
		contentPanel.add(getBtnAceptar());
		setModal(true);
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	private JTextPane getTxtpnatencinSeAsignar() {
		if (txtpnatencinSeAsignar == null) {
			txtpnatencinSeAsignar = new JTextPane();
			txtpnatencinSeAsignar.setOpaque(false);
			txtpnatencinSeAsignar.setEditable(false);
			txtpnatencinSeAsignar.setText("\u00A1Atenci\u00F3n! Se asignar\u00E1 el horario del primer d\u00EDa de la semana a los d\u00EDas escogidos");
			txtpnatencinSeAsignar.setBounds(72, 12, 336, 32);
		}
		return txtpnatencinSeAsignar;
	}
	private JLabel getLblImagen() {
		if (lblImagen == null) {
			lblImagen = new JLabel("");
			lblImagen.setIcon(new ImageIcon(DialogoElegirHorarios.class.getResource("/img/iconoInfo.png")));
			lblImagen.setBounds(23, 13, 30, 30);
		}
		return lblImagen;
	}
	private JCheckBox getBoxD2() {
		if (boxD2 == null) {
			boxD2 = new JCheckBox(dias.get(0));
			boxD2.setName("box2");
			boxD2.setBounds(54, 81, 164, 23);
		}
		return boxD2;
	}
	private JCheckBox getBoxD3() {
		if (boxD3 == null) {
			boxD3 = new JCheckBox(dias.get(1));
			boxD3.setName("box3");
			boxD3.setBounds(54, 122, 164, 23);
		}
		return boxD3;
	}
	private JCheckBox getBoxD4() {
		if (boxD4 == null) {
			boxD4 = new JCheckBox(dias.get(2));
			boxD4.setName("box4");
			boxD4.setBounds(54, 163, 164, 23);
		}
		return boxD4;
	}
	private JCheckBox getBoxD5() {
		if (boxD5 == null) {
			boxD5 = new JCheckBox(dias.get(3));
			boxD5.setName("box5");
			boxD5.setBounds(230, 81, 164, 23);
		}
		return boxD5;
	}
	private JCheckBox getBoxD6() {
		if (boxD6 == null) {
			boxD6 = new JCheckBox(dias.get(4));
			boxD6.setName("box6");
			boxD6.setBounds(230, 122, 164, 23);
		}
		return boxD6;
	}
	private JCheckBox getBoxD7() {
		if (boxD7 == null) {
			boxD7 = new JCheckBox(dias.get(5));
			boxD7.setName("box7");
			boxD7.setBounds(230, 163, 164, 23);
		}
		return boxD7;
	}
	private JButton getBtnAceptar() {
		if (btnAceptar == null) {
			btnAceptar = new JButton("Aceptar");
			btnAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(boxD2.isSelected())
						boxSeleccionados.add(2);
					if(boxD3.isSelected())
						boxSeleccionados.add(3);
					if(boxD4.isSelected())
						boxSeleccionados.add(4);
					if(boxD5.isSelected())
						boxSeleccionados.add(5);
					if(boxD6.isSelected())
						boxSeleccionados.add(6);
					if(boxD7.isSelected())
						boxSeleccionados.add(7);
					dispose();
				}
			});
			btnAceptar.setBounds(321, 251, 98, 29);
		}
		return btnAceptar;
	}
	
}
