package igu;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JPanel;

import logica.Conexion;
import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;

import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.FlowLayout;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.awt.Dimension;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

@SuppressWarnings("serial")
public class CardModificarHabitacion extends JPanel {

	/**
	 * Create the panel.
	 */
	private static Conexion conn;
	private static PreparedStatement stmt;
	private CardGerenteHabitaciones padre;
	private JLabel lblDarDeAlta;
	private JPanel pnCentral;
	private JPanel pnSouth;
	private JButton btnModificar;
	private JPanel pn1;
	private JPanel pn2;
	private JPanel pn3;
	private JLabel lblNumero;
	private JLabel lblPlanta;
	private JLabel lblTipo;
	private JComboBox<Object> cbtipo;
	private JLabel lblComentario;
	private JButton btnSeleccionarImagen;
	private VentanaGerentePrincipal vGp;

	private String pathImagen;
	private JScrollPane scrollPane;
	private JPanel panel;
	private JPanel panel_1;
	private JTextArea textArea;
	private JComboBox<Object> cbNumero;
	private JLabel lblEstado;
	private JComboBox<Object> cbEstado;
	private JTextField txtPlanta;

	private ArrayList<JLabel> imagenes = new ArrayList<JLabel>();
	private JPanel panel_2;
	private JButton btnActualizar;
	private JButton btnAtras;

	public CardModificarHabitacion(Conexion con,VentanaGerentePrincipal vgp, CardGerenteHabitaciones papa) {
		padre = papa;
		pathImagen ="";
		conn = con;
		vGp = vgp;
		setLayout(new BorderLayout(0, 0));
		add(getLblDarDeAlta(), BorderLayout.NORTH);
		add(getPnCentral(), BorderLayout.CENTER);
		add(getPnSouth(), BorderLayout.SOUTH);

		reiniciarPantalla((String)getCbNumero().getSelectedItem());
		setVisible(true);
	}

	private JLabel getLblDarDeAlta() {
		if (lblDarDeAlta == null) {
			lblDarDeAlta = new JLabel("Modificar habitacion");
			lblDarDeAlta.setFont(new Font("Lucida Grande", Font.PLAIN, 19));
		}
		return lblDarDeAlta;
	}
	private JPanel getPnCentral() {
		if (pnCentral == null) {
			pnCentral = new JPanel();
			pnCentral.setLayout(new GridLayout(0, 1, 0, 0));
			pnCentral.add(getPn1());
			pnCentral.add(getPn2());
			pnCentral.add(getPanel_2());
			pnCentral.add(getPn3());
		}
		return pnCentral;
	}
	private JPanel getPnSouth() {
		if (pnSouth == null) {
			pnSouth = new JPanel();
			pnSouth.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			pnSouth.add(getBtnModficar());
			pnSouth.add(getBtnAtras());
		}
		return pnSouth;
	}
	private JButton getBtnModficar() {
		if (btnModificar == null) {
			btnModificar = new JButton("Modficar");
			btnModificar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						PreparedStatement stmt = conn.getCon().prepareStatement("UPDATE HABITACION "
								+ "SET PLANTA = ?,TIPO = ?,ESTADO = ?, COMENTARIO = ?,IMAGEN=? "
								+ "WHERE PK_HABITACION = ?");
						stmt.setString(1, getTxtPlanta().getText().toString());
						stmt.setString(2, getCbtipo().getSelectedItem().toString());
						stmt.setString(3, getComboBox_1().getSelectedItem().toString());
						stmt.setString(4, getTextArea().getText().toString());
						stmt.setString(5,pathImagen);

						stmt.setString(6,getCbNumero().getSelectedItem().toString());
						stmt.executeUpdate();

						JOptionPane.showMessageDialog(null, "Habitación actualziada correctamente");
						getCbNumero().setSelectedIndex(0);
						reiniciarPantalla(getCbNumero().getSelectedItem().toString());


						stmt.close();

					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
			});





		}
		return btnModificar;
	}

	private JPanel getPn1() {
		if (pn1 == null) {
			pn1 = new JPanel();
			pn1.setLayout(new BorderLayout(0, 0));
			pn1.add(getPanel(), BorderLayout.NORTH);
			pn1.add(getPanel_1(), BorderLayout.SOUTH);
		}
		return pn1;
	}
	private JPanel getPn2() {
		if (pn2 == null) {
			pn2 = new JPanel();
			pn2.setLayout(null);
			pn2.add(getLblComentario());
			pn2.add(getScrollPane());
		}
		return pn2;
	}
	private JPanel getPn3() {
		if (pn3 == null) {
			pn3 = new JPanel();
		}
		return pn3;
	}
	private JLabel getLblNumero() {
		if (lblNumero == null) {
			lblNumero = new JLabel("Numero:");
		}
		return lblNumero;
	}
	private JLabel getLblPlanta() {
		if (lblPlanta == null) {
			lblPlanta = new JLabel("Planta:");
		}
		return lblPlanta;
	}
	private JLabel getLblTipo() {
		if (lblTipo == null) {
			lblTipo = new JLabel("Tipo:");
		}
		return lblTipo;
	}
	private JComboBox<Object> getCbtipo() {
		if (cbtipo == null) {
			cbtipo = new JComboBox<Object>();
			String[] tipohabita = null ;
			try{
				Statement tipohabitacion = conn.getCon().createStatement();
				ResultSet numhabitacion = tipohabitacion.executeQuery("SELECT COUNT(*) FROM TIPO_HABITACION");
				while(numhabitacion.next())
				{
					tipohabita = new String[numhabitacion.getInt(1)];

				}
				int count = 0;	
				ResultSet rs = tipohabitacion.executeQuery("SELECT NOMBRE FROM TIPO_HABITACION");
				while(rs.next())
				{
					tipohabita[count] = rs.getString(1);
					count++;
				}

				tipohabitacion.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

			cbtipo.setModel(new DefaultComboBoxModel(tipohabita));
		}
		return cbtipo;
	}
	private JLabel getLblComentario() {
		if (lblComentario == null) {
			lblComentario = new JLabel("Comentario:");
			lblComentario.setBounds(22, 33, 77, 16);
		}
		return lblComentario;
	}
	private JButton getBtnSeleccionarImagen() {
		if (btnSeleccionarImagen == null) {
			btnSeleccionarImagen = new JButton("Seleccionar imagen");
			btnSeleccionarImagen.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(getPn3().getComponentCount()>0)
					{
						for(JLabel l:imagenes)
						{
							l.setVisible(false);
							getPn3().remove(l);
						}
						imagenes.clear();
						getPn3().removeAll();
					}

					FileNameExtensionFilter filter = new FileNameExtensionFilter("Imagenes","jpg","jpeg","png");
					File dir = new File("./src/img/habitaciones");					
					JFileChooser filechoose = new JFileChooser();
					filechoose.setMultiSelectionEnabled(true);
					filechoose.setCurrentDirectory(dir);
					filechoose.setFileFilter(filter);

					int opcion = filechoose.showOpenDialog((Component)null);
					if(opcion == JFileChooser.APPROVE_OPTION)
					{
						String ruta = "";
						File[] files = filechoose.getSelectedFiles();
						for(int i = 0;i<files.length;i++)
						{
							JLabel lblI = new JLabel("");
							getPn3().add(lblI);
							getPn3().setVisible(true);
							String fotoruta = "./src/img/habitaciones/"+ files[i].getName();
							ruta = ruta + "./src/img/habitaciones/"+ files[i].getName()+"@";
							ImageIcon iconhabitacion = new ImageIcon(fotoruta);
							lblI.setSize(new Dimension(150,100));
							Icon icono = new ImageIcon(iconhabitacion.getImage().getScaledInstance(lblI.getWidth(), lblI.getHeight(), Image.SCALE_SMOOTH));
							lblI.setIcon(icono);
							lblI.setVisible(true);
							imagenes.add(lblI);

						}
						pathImagen = ruta;
					}

				}
			});
		}
		return btnSeleccionarImagen;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(111, 6, 318, 70);
			scrollPane.setViewportView(getTextArea());
		}
		return scrollPane;
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.add(getLblNumero());
			panel.add(getCbNumero());
			panel.add(getLblPlanta());
			panel.add(getTxtPlanta());
		}
		return panel;
	}
	private JPanel getPanel_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.add(getLblTipo());
			panel_1.add(getCbtipo());
			panel_1.add(getLblEstado());
			panel_1.add(getComboBox_1());
		}
		return panel_1;
	}
	private JTextArea getTextArea() {
		if (textArea == null) {
			textArea = new JTextArea();
		}
		return textArea;
	}
	private JComboBox<Object> getCbNumero() {
		if (cbNumero == null) {
			cbNumero = new JComboBox<Object>();
			cbNumero.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					reiniciarPantalla((String) cbNumero.getSelectedItem());
				}
			});

			String[] numeros = null;

			try {
				Statement num = conn.getCon().createStatement();
				ResultSet numrs = num.executeQuery("SELECT count(*) FROM HABITACION");
				while(numrs.next())
				{
					numeros = new String[numrs.getInt(1)];
				}
				numrs.close();

				ResultSet habitaciones = num.executeQuery("SELECT PK_HABITACION FROM HABITACION ");
				int count = 0;
				while(habitaciones.next())
				{
					numeros[count] = habitaciones.getString(1);
					count++;
				}
				habitaciones.close();
				num.close();
				cbNumero.setModel(new DefaultComboBoxModel(numeros));
			} catch (SQLException e) {
				e.printStackTrace();
			}
			//cbNumero.setSelectedIndex(0);



		}
		return cbNumero;
	}
	private JLabel getLblEstado() {
		if (lblEstado == null) {
			lblEstado = new JLabel("Estado:");
		}
		return lblEstado;
	}
	private JComboBox<Object> getComboBox_1() {
		if (cbEstado == null) {
			cbEstado = new JComboBox<Object>();
			String[] estados = {"libre","ocupada","reservada"};
			cbEstado.setModel(new DefaultComboBoxModel(estados));
		}
		return cbEstado;
	}
	private void reiniciarPantalla(String number) {
		if (getPn3().getComponentCount() > 0) {
			eliminarImagenes();
		}
		try {
			PreparedStatement stmt = conn.getCon().prepareStatement("SELECT PLANTA,TIPO,ESTADO,COMENTARIO,IMAGEN FROM HABITACION "
					+ "WHERE PK_HABITACION =?");
			stmt.setString(1, number);
			ResultSet rs = stmt.executeQuery();
			while(rs.next())
			{
				getTxtPlanta().setText(rs.getString(1));
				getCbtipo().setSelectedItem(rs.getString(2));
				getComboBox_1().setSelectedItem(rs.getString(3));
				getTextArea().setText(rs.getString(4));

				pathImagen = rs.getString(5);
				
					if(pathImagen!=null)
					{
						String[] imagenes = pathImagen.split("@");
						//System.out.println(imagenes[0]);
						for(int i = 0;i<imagenes.length;i++)
						{
							JLabel lblI = new JLabel("");
							getPn3().add(lblI);
							getPn3().setVisible(true);
							ImageIcon iconhabitacion = new ImageIcon(imagenes[i]);
							lblI.setSize(new Dimension(150,100));
							Icon icono = new ImageIcon(iconhabitacion.getImage().getScaledInstance(lblI.getWidth(), lblI.getHeight(), Image.SCALE_SMOOTH));
							lblI.setIcon(icono);
							lblI.setVisible(true);
							this.imagenes.add(lblI);
					}
					
					}
	

			}
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}


	private void eliminarImagenes()
	{
		for (JLabel l : imagenes) {
			l.setVisible(false);
			getPn3().remove(l);
		}
		imagenes.clear();
		getPn3().removeAll();
		pathImagen="";
	}
	private JTextField getTxtPlanta() {
		if (txtPlanta == null) {
			txtPlanta = new JTextField();
			txtPlanta.setColumns(10);
		}
		return txtPlanta;
	}
	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.add(getBtnSeleccionarImagen());
			panel_2.add(getBtnActualizar());
		}
		return panel_2;
	}
	private JButton getBtnActualizar() {
		if (btnActualizar == null) {
			btnActualizar = new JButton("Actualizar");
			btnActualizar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					getCbNumero().setSelectedIndex(0);
					reiniciarPantalla((String)getCbNumero().getSelectedItem());
					actualizarHabitaciones();

				}
			});
		}
		return btnActualizar;
	}
	private JButton getBtnAtras() {
		if (btnAtras == null) {
			btnAtras = new JButton("Atras");
			btnAtras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					padre.getSuperCard().show(padre.getPanelSuperCard(), "panelElegirAccion");
					actualizarHabitaciones();
					reiniciarPantalla((String)getCbNumero().getSelectedItem());
				}
			});
		}
		return btnAtras;
	}
	
	private void actualizarHabitaciones()
	{
		String[] numeros = null;

		try {
			Statement num = conn.getCon().createStatement();
			ResultSet numrs = num.executeQuery("SELECT count(*) FROM HABITACION");
			while(numrs.next())
			{
				numeros = new String[numrs.getInt(1)];
			}
			numrs.close();

			ResultSet habitaciones = num.executeQuery("SELECT PK_HABITACION FROM HABITACION ");
			int count = 0;
			while(habitaciones.next())
			{
				numeros[count] = habitaciones.getString(1);
				count++;
			}
			habitaciones.close();
			num.close();
			cbNumero.setModel(new DefaultComboBoxModel(numeros));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
