package igu;

import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JPanel;

import logica.Cliente;
import logica.DNI;
import logica.DataBase;
import logica.Tarjeta;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.border.TitledBorder;

import com.toedter.calendar.JDateChooser;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

@SuppressWarnings("serial")
public class VentanaCambiarDatosCliente extends JFrame{

	private CardNuevaReserva padre_NuevaReserva;
	private CardCheckIn padre_CheckIn;
	private Cliente cliente;
	private JLabel lblClienteDnipasaporte;
	private JTextField txtDni;
	private JPanel pnDatos;
	private JLabel lblNombre;
	private JLabel lblApellidos;
	private JLabel lblCorreo;
	private JLabel lblDireccion;
	private JLabel lblTelefono;
	private JLabel lblTarjeta;
	private JLabel lblPaisDeResidencia;
	private JLabel lblFechaDeNacimiento;
	private JTextField txtNombre;
	private JTextField txtApellidos;
	private JTextField txtCorreo;
	private JTextField txtDireccion;
	private JTextField txtTelefono;
	private JTextField txtTarjeta;
	private JTextField txtPaisDeResidencia;
	private JDateChooser nacimiento;
	private JButton btnGuardar;
	private JButton btnCancelar;

	/**
	 * @wbp.parser.constructor
	 */
	public VentanaCambiarDatosCliente(CardNuevaReserva papa, Cliente cliente) {
		setTitle("Modificar Datos Cliente");

		padre_NuevaReserva = papa;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 415, 428);

		setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		getContentPane().add(getPnDatos());
		getContentPane().add(getBtnGuardar());
		getContentPane().add(getBtnCancelar());
		setResizable(false);
		setVisible(true);

		txtDni.setText(cliente.getDni().toString());
		txtNombre.setText(cliente.getName());
		txtApellidos.setText(cliente.getApellidos());
		txtDireccion.setText(cliente.getDireccion());
		nacimiento.setDate(new Date(cliente.getNacimiento()));
		txtTelefono.setText(String.valueOf(cliente.getTelefono()));
		txtCorreo.setText(cliente.getCorreo());
		txtTarjeta.setText(cliente.getTarjeta());
		txtPaisDeResidencia.setText(cliente.getResidencia());
	}
	public VentanaCambiarDatosCliente(CardCheckIn papa, Cliente cliente) {
		setTitle("Modificar Datos Cliente");

		padre_CheckIn = papa;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 415, 428);

		setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		getContentPane().add(getPnDatos());
		getContentPane().add(getBtnGuardar());
		getContentPane().add(getBtnCancelar());
		setResizable(false);
		setVisible(true);

		txtDni.setText(cliente.getDni().toString());
		txtNombre.setText(cliente.getName());
		txtApellidos.setText(cliente.getApellidos());
		txtDireccion.setText(cliente.getDireccion());
		nacimiento.setDate(new Date(cliente.getNacimiento()));
		txtTelefono.setText(String.valueOf(cliente.getTelefono()));
		txtCorreo.setText(cliente.getCorreo());
		txtTarjeta.setText(cliente.getTarjeta());
		txtPaisDeResidencia.setText(cliente.getResidencia());
	}
	
	private JLabel getLblClienteDnipasaporte() {
		if (lblClienteDnipasaporte == null) {
			lblClienteDnipasaporte = new JLabel("DNI/Pasaporte:");
			lblClienteDnipasaporte.setBounds(35, 26, 128, 14);
		}
		return lblClienteDnipasaporte;
	}
	private JTextField getTxtDni() {
		if (txtDni == null) {
			txtDni = new JTextField("");
			txtDni.setBounds(127, 20, 139, 20);
		}
		return txtDni;
	}
	private JPanel getPnDatos() {
		if (pnDatos == null) {
			pnDatos = new JPanel();
			pnDatos.setBorder(new TitledBorder(null, "Datos", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnDatos.setBounds(10, 21, 351, 333);
			pnDatos.setLayout(null);
			pnDatos.add(getLblNombre());
			pnDatos.add(getLblApellidos());
			pnDatos.add(getLblCorreo());
			pnDatos.add(getLblDireccion());
			pnDatos.add(getLblTelefono());
			pnDatos.add(getLblTarjeta());
			pnDatos.add(getLblPaisDeResidencia());
			pnDatos.add(getLblFechaDeNacimiento());
			pnDatos.add(getTxtNombre());
			pnDatos.add(getTxtApellidos());
			pnDatos.add(getTxtCorreo());
			pnDatos.add(getTxtDireccion());
			pnDatos.add(getTxtTelefono());
			pnDatos.add(getTxtTarjeta());
			pnDatos.add(getTxtPaisDeResidencia());
			pnDatos.add(getNacimiento());
			pnDatos.add(getLblClienteDnipasaporte());
			pnDatos.add(getTxtDni());
		}
		return pnDatos;
	}
	private JLabel getLblNombre() {
		if (lblNombre == null) {
			lblNombre = new JLabel("Nombre:");
			lblNombre.setBounds(35, 59, 60, 14);
		}
		return lblNombre;
	}
	private JLabel getLblApellidos() {
		if (lblApellidos == null) {
			lblApellidos = new JLabel("Apellidos:");
			lblApellidos.setBounds(35, 92, 60, 14);
		}
		return lblApellidos;
	}
	private JLabel getLblCorreo() {
		if (lblCorreo == null) {
			lblCorreo = new JLabel("Correo:");
			lblCorreo.setBounds(35, 161, 60, 14);
		}
		return lblCorreo;
	}
	private JLabel getLblDireccion() {
		if (lblDireccion == null) {
			lblDireccion = new JLabel("Direccion:");
			lblDireccion.setBounds(35, 194, 60, 14);
		}
		return lblDireccion;
	}
	private JLabel getLblTelefono() {
		if (lblTelefono == null) {
			lblTelefono = new JLabel("Telefono:");
			lblTelefono.setBounds(35, 227, 60, 14);
		}
		return lblTelefono;
	}
	private JLabel getLblTarjeta() {
		if (lblTarjeta == null) {
			lblTarjeta = new JLabel("Tarjeta:");
			lblTarjeta.setBounds(35, 260, 46, 14);
		}
		return lblTarjeta;
	}
	private JLabel getLblPaisDeResidencia() {
		if (lblPaisDeResidencia == null) {
			lblPaisDeResidencia = new JLabel("Pais de Residencia:");
			lblPaisDeResidencia.setBounds(35, 293, 131, 14);
		}
		return lblPaisDeResidencia;
	}
	private JLabel getLblFechaDeNacimiento() {
		if (lblFechaDeNacimiento == null) {
			lblFechaDeNacimiento = new JLabel("Fecha de nacimiento:");
			lblFechaDeNacimiento.setBounds(35, 128, 131, 14);
		}
		return lblFechaDeNacimiento;
	}
	private JTextField getTxtNombre() {
		if (txtNombre == null) {
			txtNombre = new JTextField();
			txtNombre.setBounds(127, 53, 194, 20);
			txtNombre.setColumns(10);
		}
		return txtNombre;
	}
	private JTextField getTxtApellidos() {
		if (txtApellidos == null) {
			txtApellidos = new JTextField();
			txtApellidos.setBounds(127, 86, 194, 20);
			txtApellidos.setColumns(10);
		}
		return txtApellidos;
	}
	private JTextField getTxtCorreo() {
		if (txtCorreo == null) {
			txtCorreo = new JTextField();
			txtCorreo.setBounds(127, 155, 194, 20);
			txtCorreo.setColumns(10);
		}
		return txtCorreo;
	}
	private JTextField getTxtDireccion() {
		if (txtDireccion == null) {
			txtDireccion = new JTextField();
			txtDireccion.setBounds(127, 188, 194, 20);
			txtDireccion.setColumns(10);
		}
		return txtDireccion;
	}
	private JTextField getTxtTelefono() {
		if (txtTelefono == null) {
			txtTelefono = new JTextField();
			txtTelefono.setBounds(127, 221, 194, 20);
			txtTelefono.setColumns(10);
		}
		return txtTelefono;
	}
	private JTextField getTxtTarjeta() {
		if (txtTarjeta == null) {
			txtTarjeta = new JTextField();
			txtTarjeta.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					try {
						Tarjeta tarjeta = new Tarjeta(txtTarjeta.getText());
						if (tarjeta.comprobarTarjeta()) {
							txtTarjeta.setBackground(Color.WHITE);
						}
						else {
							txtTarjeta.setBackground(Color.RED);
						}
					}
					catch (NumberFormatException et) {
						JOptionPane.showMessageDialog(null, "El campo tarjeta debe contener solo n�meros");
					}
				}
			});
			txtTarjeta.setBounds(127, 254, 194, 20);
			txtTarjeta.setColumns(10);
		}
		return txtTarjeta;
	}
	private JTextField getTxtPaisDeResidencia() {
		if (txtPaisDeResidencia == null) {
			txtPaisDeResidencia = new JTextField();
			txtPaisDeResidencia.setBounds(165, 287, 156, 20);
			txtPaisDeResidencia.setColumns(10);
		}
		return txtPaisDeResidencia;
	}
	
	@SuppressWarnings("deprecation")
	private JDateChooser getNacimiento() {
		if (nacimiento == null){
			nacimiento = new JDateChooser("dd/MM/yyyy", "##/##/####", '_');
			nacimiento.setVisible(true);
			nacimiento.setBounds(165, 119, 156, 23);
			Date mayorDe = new Date();
			mayorDe.setHours(0); 
			mayorDe.setYear(mayorDe.getYear() - 16);
			Date menorDe = new Date();
			menorDe.setHours(0); 
			menorDe.setYear(menorDe.getYear() - 150);

			nacimiento.setSelectableDateRange(menorDe, mayorDe);
		}
		return nacimiento;	
	}
	private JButton getBtnGuardar() {
		if (btnGuardar == null) {
			btnGuardar = new JButton("Guardar");
			btnGuardar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					DNI dni = new DNI(txtDni.getText());
					Tarjeta tarjeta = new Tarjeta(txtTarjeta.getText());
					if ((dni.comprobarDNI() || dni.comprobarPasaporte()) && tarjeta.comprobarTarjeta()) {
						try {
							cliente = new Cliente();
							cliente.setDni(new DNI(txtDni.getText()));
							cliente.setName(txtNombre.getText());
							cliente.setApellidos(txtApellidos.getText());
							cliente.setDireccion(txtDireccion.getText());
							cliente.setTelefono(txtTelefono.getText());
							cliente.setCorreo(txtCorreo.getText());
							cliente.setTarjeta(txtTarjeta.getText());
							cliente.setNacimiento(nacimiento.getDate().getTime());
							cliente.setResidencia(txtPaisDeResidencia.getText());
							int opcion = JOptionPane.showConfirmDialog(null, "�Desea confirmar los cambios?", "Confirmar cambios", JOptionPane.YES_NO_OPTION);
							if(opcion == 0){//YES
								DataBase.modificarDatosCliente(cliente);
								JOptionPane.showMessageDialog(null, "Los datos se han guardado correctamente.");
								if (padre_NuevaReserva != null) {
									padre_NuevaReserva.refrescar(cliente);
								}
								else {
									padre_CheckIn.refrescar(cliente);
								}
								cerrar();
							}
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
					else {
						JOptionPane.showMessageDialog(null, "Compruebe el DNI, o la tarjeta y vuelva a intentarlo.");
					}
				}
			});
			btnGuardar.setBounds(165, 365, 89, 23);
		}
		return btnGuardar;
	}
	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cerrar();
				}
			});
			btnCancelar.setBounds(264, 365, 89, 23);
		}
		return btnCancelar;
	}

	private void cerrar() {
		dispose();
	}
}
