package igu;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import logica.Conexion;

import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;
import java.awt.Rectangle;
import java.awt.Toolkit;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import java.util.Date;

import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.CardLayout;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class VentanaSuplementos extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Conexion con;
	private JPanel contentPane;
	private JPanel panelSuplementosActuales;
	private JLabel lblSuplementosActuales;
	private JScrollPane spTablaSuplementos;
	private JTable tablaSuplementos;
	private String[] cabecera = {"SUPLEMENTO", "TIPO DE HABITACI�N", "IMPORTE POR D�A", "FECHA DE ALTA", "FECHA DE BAJA"};
	private JPanel panelBotones;
	private JButton btnModificar;
	private JButton btnCrearNuevoSuplemento;
	private JButton btnBorrar;
	private JPanel panelCentral;
	private JPanel panelCard;
	private JPanel panelTabla;
	private JPanel panelContenedorCard;
	private JPanel panelBotones2;
	private JButton btnA�adir;
	private JSeparator separator;
	private JPanel panelNuevoSuplemento;
	private JLabel lblSuplemento;
	private JLabel lblImporte;
	private JTextField tfNuevoImporte;
	private JDateChooser dateChFechaInicio;
	private JDateChooser dateChFechaFin;
	private JLabel lblFechaInicio;
	private JLabel lblFechaFin;
	private String suplementoSeleccionado;
	private CardLayout card = new CardLayout();
	private JPanel panelModificacion;
	private JLabel lblSuplementoMod;
	private JTextField txfSuplementoMod;
	private JLabel lblImporteMod;
	private JTextField txfImporteMod;
	private JLabel lblFechaInicioMod;
	private JLabel lblFechaFinMod;
	private JDateChooser dtChFechaInicioMod;
	private JDateChooser dtChFechaFinMod;
	private boolean enModificacion;
	private JLabel lblTipoDeHabitacin;
	private JComboBox<String> cbTipoHab;
	private String tipoSeleccionado;
	private int id_supl;
	private JComboBox<String> cbSuplemento;
	private String suplementoSeleccionadoCombo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//VentanaAltaTarifasTipo frame = new VentanaAltaTarifasTipo();
					Conexion conexion = new Conexion();
					conexion.establecerConexion();	
					VentanaSuplementos ventana = new VentanaSuplementos(conexion);
			
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public class MiModeloTabla extends DefaultTableModel{
		private static final long serialVersionUID = 1L;
		public MiModeloTabla(Object[] columnNames, int numFilas) {
			super(columnNames,numFilas);
		}
		
		public boolean isCellEditable(int numFila, int numColumna){
			return false;
		}
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public VentanaSuplementos(Conexion conexion) throws SQLException {
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaSuplementos.class.getResource("/img/logo.jpg")));
		this.con = conexion;
		setTitle("Suplementos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 800);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(getPanelTarifasActuales(), BorderLayout.NORTH);
		contentPane.add(getPanelCentral(), BorderLayout.CENTER);
		setLocationRelativeTo(null);
		rellenarTablaSuplementos();
		setVisible(true);
	}

	private JPanel getPanelTarifasActuales() {
		if (panelSuplementosActuales == null) {
			panelSuplementosActuales = new JPanel();
			panelSuplementosActuales.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			panelSuplementosActuales.add(getLblTarifasActuales());
		}
		return panelSuplementosActuales;
	}
	private JLabel getLblTarifasActuales() {
		if (lblSuplementosActuales == null) {
			lblSuplementosActuales = new JLabel("SUPLEMENTOS ACTUALES:");
			lblSuplementosActuales.setFont(new Font("Tahoma", Font.BOLD, 24));
			lblSuplementosActuales.setHorizontalTextPosition(SwingConstants.LEFT);
			lblSuplementosActuales.setHorizontalAlignment(SwingConstants.LEFT);
		}
		return lblSuplementosActuales;
	}
	private JScrollPane getSpTablaTarifasTipo() {
		if (spTablaSuplementos == null) {
			spTablaSuplementos = new JScrollPane();
			spTablaSuplementos.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
			spTablaSuplementos.setViewportView(getTablaTarifasTipo());
		}
		return spTablaSuplementos;
	}
	private JTable getTablaTarifasTipo() {
		if (tablaSuplementos == null) {
			tablaSuplementos = new JTable();
			tablaSuplementos.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					if(enModificacion){
						int opcion = JOptionPane.showConfirmDialog(null, "Atenci�n est� modificando una tarifa, �desea abandonar la operaci�n?\n"
								+ "Los cambios no ser�n guardados", "Abandonar modificaci�n.", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
						if(opcion==0){
							tablaSuplementos.setEnabled(true);
							suplementoSeleccionadoCombo = (String) tablaSuplementos.getValueAt(tablaSuplementos.getSelectedRow(), 0);
							System.out.println(suplementoSeleccionadoCombo);
							enModificacion = false;
							panelCard.setVisible(false);
							panelBotones2.setVisible(false);
						}
					
					}
					else{
						suplementoSeleccionadoCombo = (String) tablaSuplementos.getValueAt(tablaSuplementos.getSelectedRow(), 0);
						System.out.println(suplementoSeleccionadoCombo);
					}
				}
			});
			tablaSuplementos.setBorder(null);
			tablaSuplementos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tablaSuplementos;
	}
	
	private void rellenarTablaSuplementos() throws SQLException{
		
		MiModeloTabla miModelo = new MiModeloTabla(cabecera, 0);
		
		Statement s = con.getCon().createStatement();
		
		ResultSet rs = s.executeQuery("select s.nombre_suplemento, s.pk_tipo_habitacion, ps.importe, ps.fecha_inicio, ps.fecha_fin "
				+ "from suplemento_modalidad s, precio_suplemento ps "
				+ "where s.id_supl_mod = ps.pk_supl");
		
		SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");//Formatea el tipo Date en java
		//imprimirResultados(rs);
		
		while(rs.next()){
			Object[] fila = new Object[5];
			fila[0] = rs.getString("NOMBRE_SUPLEMENTO");
			fila[1] = rs.getString("PK_TIPO_HABITACION");
			fila[2] = rs.getFloat("IMPORTE");
			fila[3] = formato.format(rs.getDate("FECHA_INICIO"));
			fila[4] = formato.format(rs.getDate("FECHA_FIN"));
			
			miModelo.addRow(fila);
		}
		
		tablaSuplementos.setModel(miModelo);
		modificarTablaSuplementos();
		
		rs.close();
		s.close();
	}
	
	private static void imprimirResultados(ResultSet rs) throws SQLException {
		int columnCount = rs.getMetaData().getColumnCount();
		StringBuilder headers = new StringBuilder();		
		for(int i = 1; i < columnCount ; i++)
			headers.append(rs.getMetaData().getColumnName(i) + "\t");
		headers.append(rs.getMetaData().getColumnName(columnCount));
		System.out.println(headers.toString());
		StringBuilder result = null;
		while (rs.next()) {
			result = new StringBuilder();	
			for(int i = 1; i < columnCount ; i++)
				result.append(rs.getObject(i) + "\t");
			result.append(rs.getObject(columnCount));
			System.out.println(result.toString());
		}
		if(result == null)
			System.out.println("Not data found!!");
	}
	
	private void modificarTablaSuplementos(){
		tablaSuplementos.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
		tablaSuplementos.setFont(new Font("Tahoma", Font.PLAIN, 12));
		TableColumn columna = tablaSuplementos.getColumn(cabecera[0]);//MODIFICO EL ANCHO DE LA PRIMERA COLUMNA
		columna.setMinWidth(200);
		columna.setMaxWidth(200);
		
		columna = tablaSuplementos.getColumn(cabecera[2]);
		columna.setMinWidth(160);
		columna.setMaxWidth(160);
		
		columna = tablaSuplementos.getColumn(cabecera[3]);
		columna.setMinWidth(130);
		columna.setMaxWidth(130);
		
		columna = tablaSuplementos.getColumn(cabecera[4]);
		columna.setMinWidth(130);
		columna.setMaxWidth(130);
		tablaSuplementos.setRowHeight(25);
	}

	private JPanel getPanelBotones() {
		if (panelBotones == null) {
			panelBotones = new JPanel();
			panelBotones.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
			panelBotones.setBounds(new Rectangle(0, 0, 50, 41));
			GridBagLayout gbl_panelBotones = new GridBagLayout();
			gbl_panelBotones.columnWidths = new int[] {140, 0};
			gbl_panelBotones.rowHeights = new int[] {41, 41, 41, 0};
			gbl_panelBotones.columnWeights = new double[]{0.0, Double.MIN_VALUE};
			gbl_panelBotones.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
			panelBotones.setLayout(gbl_panelBotones);
			GridBagConstraints gbc_btnCrearNuevaTarifa = new GridBagConstraints();
			gbc_btnCrearNuevaTarifa.fill = GridBagConstraints.BOTH;
			gbc_btnCrearNuevaTarifa.insets = new Insets(0, 0, 5, 0);
			gbc_btnCrearNuevaTarifa.gridx = 0;
			gbc_btnCrearNuevaTarifa.gridy = 0;
			panelBotones.add(getBtnCrearNuevoSuplemento(), gbc_btnCrearNuevaTarifa);
			GridBagConstraints gbc_btnModificar = new GridBagConstraints();
			gbc_btnModificar.fill = GridBagConstraints.BOTH;
			gbc_btnModificar.insets = new Insets(0, 0, 5, 0);
			gbc_btnModificar.gridx = 0;
			gbc_btnModificar.gridy = 1;
			panelBotones.add(getBtnModificar(), gbc_btnModificar);
			GridBagConstraints gbc_btnBorrar = new GridBagConstraints();
			gbc_btnBorrar.fill = GridBagConstraints.BOTH;
			gbc_btnBorrar.gridx = 0;
			gbc_btnBorrar.gridy = 2;
			panelBotones.add(getBtnBorrar(), gbc_btnBorrar);
		}
		return panelBotones;
	}
	private JButton getBtnModificar() {
		if (btnModificar == null) {
			btnModificar = new JButton("Modificar");
			btnModificar.setVisible(false);
			btnModificar.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
			btnModificar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String tipo = (String) tablaSuplementos.getValueAt(tablaSuplementos.getSelectedRow(), 0);
					if(tipo != null) {
						panelCard.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
						panelBotones2.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
						panelBotones2.setVisible(true);
						panelCard.setVisible(true);
						enModificacion = true;
						panelModificacion.setBorder(new TitledBorder(null, "Modificar tarifa", TitledBorder.LEADING, TitledBorder.TOP, null, null));
						card.show(panelCard, "panelModificacion");
						tablaSuplementos.setEnabled(false);
						rellenarDatosModificacion(tipo);
									
					}
									
				}
			});
			//btnModificar.setVisible(false);
			btnModificar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		}
		return btnModificar;
	}
	
	private void modificar(String nuevoNombre, float nuevoImporte, Date nuevaFechaInicio, Date nuevaFechaFin) throws SQLException{
		PreparedStatement psUpdatePrecio = con.getCon().prepareStatement("update precio_tipo"
				+ " set importe_dia = ?,  fecha_inicio = ?, fecha_fin = ?"
				+ " where (ID_TIPOHAB in"
				+ " (select id"
				+ " from tipo_habitacion"
				+ " where nombre = ?))");
		
		psUpdatePrecio.setFloat(1, nuevoImporte);
		psUpdatePrecio.setDate(2, new java.sql.Date(dateChFechaInicio.getDate().getTime()));
		psUpdatePrecio.setDate(3, new java.sql.Date(dateChFechaFin.getDate().getTime()));
		psUpdatePrecio.setString(4, suplementoSeleccionadoCombo);
		
		psUpdatePrecio.executeUpdate();
		
		psUpdatePrecio.close();
		dispose();
	}
	
	private void rellenarDatosModificacion(String tipo) {
		
		float importe = (float) tablaSuplementos.getValueAt(tablaSuplementos.getSelectedRow(), 1);
		try {
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			Date fecha_ini = formato.parse((String) tablaSuplementos.getValueAt(tablaSuplementos.getSelectedRow(), 2));
			Date fecha_final = formato.parse((String)tablaSuplementos.getValueAt(tablaSuplementos.getSelectedRow(), 3));
			
			txfSuplementoMod.setText(tipo);
			txfImporteMod.setText(importe+"");
			dtChFechaInicioMod.setDate(fecha_ini);
			dtChFechaFinMod.setDate(fecha_final);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	private JButton getBtnCrearNuevoSuplemento() {
		if (btnCrearNuevoSuplemento == null) {
			btnCrearNuevoSuplemento = new JButton("Nuevo suplemento");
			btnCrearNuevoSuplemento.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
			btnCrearNuevoSuplemento.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					panelCard.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
					panelBotones2.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
					card.first(panelCard);
					lblFechaFin.setVisible(true);
					lblFechaInicio.setVisible(true);
					lblImporte.setVisible(true);
					lblSuplemento.setVisible(true);
					tfNuevoImporte.setVisible(true);
					dateChFechaFin.setVisible(true);
					dateChFechaInicio.setVisible(true);
					cbSuplemento.setVisible(true);
					btnA�adir.setVisible(true);
					cbTipoHab.setVisible(true);
					lblTipoDeHabitacin.setVisible(true);
					panelNuevoSuplemento.setBorder(new TitledBorder(null, "Crear nuevo suplemento", TitledBorder.LEADING, TitledBorder.TOP, null, null));
					try {
						rellenarTablaSuplementos();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			});
			btnCrearNuevoSuplemento.setFont(new Font("Tahoma", Font.PLAIN, 15));
		}
		return btnCrearNuevoSuplemento;
	}
	private JButton getBtnBorrar() {
		if (btnBorrar == null) {
			btnBorrar = new JButton("Borrar");
			btnBorrar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String tipo = (String) tablaSuplementos.getValueAt(tablaSuplementos.getSelectedRow(), 0);
					if(tipo != null) {
						int opcion = JOptionPane.showConfirmDialog(null, "�Desea borrar esta tarifa?");
						if(opcion == 0) { //YES_OPTION
							//PreparedStatement ps =
						}
					}
				}
			});
			btnBorrar.setVisible(false);
			btnBorrar.setFont(new Font("Tahoma", Font.PLAIN, 15));
			btnBorrar.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		}
		return btnBorrar;
	}
	private JPanel getPanelCentral() {
		if (panelCentral == null) {
			panelCentral = new JPanel();
			GridBagLayout gbl_panelCentral = new GridBagLayout();
			gbl_panelCentral.columnWidths = new int[]{983, 0};
			gbl_panelCentral.rowHeights = new int[]{340, 0, 360, 0};
			gbl_panelCentral.columnWeights = new double[]{0.0, Double.MIN_VALUE};
			gbl_panelCentral.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
			panelCentral.setLayout(gbl_panelCentral);
			GridBagConstraints gbc_panelTabla = new GridBagConstraints();
			gbc_panelTabla.fill = GridBagConstraints.BOTH;
			gbc_panelTabla.insets = new Insets(0, 0, 5, 0);
			gbc_panelTabla.gridx = 0;
			gbc_panelTabla.gridy = 0;
			panelCentral.add(getPanelTabla(), gbc_panelTabla);
			GridBagConstraints gbc_separator_1 = new GridBagConstraints();
			gbc_separator_1.insets = new Insets(0, 0, 5, 0);
			gbc_separator_1.gridx = 0;
			gbc_separator_1.gridy = 1;
			panelCentral.add(getSeparator(), gbc_separator_1);
			GridBagConstraints gbc_panelContenedorCard = new GridBagConstraints();
			gbc_panelContenedorCard.fill = GridBagConstraints.BOTH;
			gbc_panelContenedorCard.gridx = 0;
			gbc_panelContenedorCard.gridy = 2;
			panelCentral.add(getPanelContenedorCard(), gbc_panelContenedorCard);
		}
		return panelCentral;
	}
	private JPanel getPanelCard() {
		if (panelCard == null) {
			panelCard = new JPanel();
			panelCard.setBorder(null);
			panelCard.setLayout(card);
			panelCard.add(getPanelNuevoTipo(), "panelNuevoTipo");
			panelCard.add(getPanelModificacion(), "panelModificacion");
		}
		return panelCard;
	}
	private JPanel getPanelTabla() {
		if (panelTabla == null) {
			panelTabla = new JPanel();
			panelTabla.setLayout(new BorderLayout(0, 0));
			panelTabla.add(getSpTablaTarifasTipo());
			panelTabla.add(getPanelBotones(), BorderLayout.EAST);
		}
		return panelTabla;
	}
	private JPanel getPanelContenedorCard() {
		if (panelContenedorCard == null) {
			panelContenedorCard = new JPanel();
			panelContenedorCard.setLayout(new BorderLayout(0, 0));
			panelContenedorCard.add(getPanelCard());
			panelContenedorCard.add(getPanelBotones2(), BorderLayout.EAST);
		}
		return panelContenedorCard;
	}
	private JPanel getPanelBotones2() {
		if (panelBotones2 == null) {
			panelBotones2 = new JPanel();
			panelBotones2.setBounds(new Rectangle(0, 0, 50, 41));
			panelBotones2.setBorder(null);
			GridBagLayout gbl_panelBotones2 = new GridBagLayout();
			gbl_panelBotones2.columnWidths = new int[]{140, 0};
			gbl_panelBotones2.rowHeights = new int[]{41, 41, 41, 0};
			gbl_panelBotones2.columnWeights = new double[]{0.0, Double.MIN_VALUE};
			gbl_panelBotones2.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
			panelBotones2.setLayout(gbl_panelBotones2);
			GridBagConstraints gbc_btnRealizarCambios = new GridBagConstraints();
			gbc_btnRealizarCambios.fill = GridBagConstraints.BOTH;
			gbc_btnRealizarCambios.insets = new Insets(0, 0, 5, 0);
			gbc_btnRealizarCambios.gridx = 0;
			gbc_btnRealizarCambios.gridy = 0;
			panelBotones2.add(getBtnA�adir(), gbc_btnRealizarCambios);
		}
		return panelBotones2;
	}
	private JButton getBtnA�adir() {
		if (btnA�adir == null) {
			btnA�adir = new JButton("A\u00F1adir");
			btnA�adir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(tipoSeleccionado != null && suplementoSeleccionadoCombo!= null){
						if(comprobarFechas() && comprobarImporte()){
							System.out.println("TODO OK");
							tfNuevoImporte.setBorder(new LineBorder(Color.GREEN));
							int opcion = JOptionPane.showConfirmDialog(null, "�Desea confirmar los cambios?", "Confirmar cambios", JOptionPane.YES_NO_OPTION);
							if(opcion == 0){//YES
								try{
									//String nuevoSuplemento = suplemento.toLowerCase();
									float nuevoImporte = Float.parseFloat(tfNuevoImporte.getText());	
									
									Statement s1 = con.getCon().createStatement();
									
									if(!existeSuplemento(s1/*, nuevoSuplemento*/)){
										insertar(s1/*, nuevoSuplemento, */,nuevoImporte);
										panelContenedorCard.setVisible(false);
									}
									else{
										JOptionPane.showMessageDialog(null, "El error puede deberse a:\n-Tipo de suplemento ya existente"
												+ "\n-No pueden existir dos nombres de modalidades de alojamiento iguales para \n"
												+ "un mismo tipo de habitaci�n cuyas fechas sean conflictivas.", "Error", JOptionPane.ERROR_MESSAGE);
									}
									
									
								}
								catch(Exception e){
									e.printStackTrace();
									System.out.println("Error");
								}
							}
						}
					}
					else{
						JOptionPane.showMessageDialog(null, "�Faltan datos!", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			});
			btnA�adir.setFont(new Font("Tahoma", Font.PLAIN, 15));
			btnA�adir.setVisible(false);
			btnA�adir.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		}
		return btnA�adir;
	}

	
	private JSeparator getSeparator() {
		if (separator == null) {
			separator = new JSeparator();
		}
		return separator;
	}
	private JPanel getPanelNuevoTipo() {
		if (panelNuevoSuplemento == null) {
			panelNuevoSuplemento = new JPanel();
			panelNuevoSuplemento.setName("panelNuevoTipo");
			panelNuevoSuplemento.setLayout(null);
			panelNuevoSuplemento.add(getLblNuevaTarifa());
			panelNuevoSuplemento.add(getLblImporte());
			panelNuevoSuplemento.add(getTxfImporte());
			panelNuevoSuplemento.add(getDateChFechaInicio());
			panelNuevoSuplemento.add(getLblFechaInicio());
			panelNuevoSuplemento.add(getDateChFechaFin());
			panelNuevoSuplemento.add(getLblFechaFin());
			panelNuevoSuplemento.add(getLblTipoDeHabitacin());
			panelNuevoSuplemento.add(getCbTipoHab());
			panelNuevoSuplemento.add(getCbSuplemento());
		}
		return panelNuevoSuplemento;
	}
	private JLabel getLblNuevaTarifa() {
		if (lblSuplemento == null) {
			lblSuplemento = new JLabel("Suplemento:");
			lblSuplemento.setBounds(364, 33, 192, 29);
			lblSuplemento.setVisible(false);
		}
		return lblSuplemento;
	}
	private JLabel getLblImporte() {
		if (lblImporte == null) {
			lblImporte = new JLabel("Importe:");
			lblImporte.setBounds(630, 34, 127, 26);
			lblImporte.setVisible(false);
		}
		return lblImporte;
	}
	private JTextField getTxfImporte() {
		if (tfNuevoImporte == null) {
			tfNuevoImporte = new JTextField();
			tfNuevoImporte.setBounds(630, 62, 109, 32);
			tfNuevoImporte.setColumns(10);
			tfNuevoImporte.setVisible(false);
		}
		return tfNuevoImporte;
	}
	private JDateChooser getDateChFechaInicio() {
		if (dateChFechaInicio == null) {
			dateChFechaInicio = new JDateChooser();
			dateChFechaInicio.setBounds(85, 142, 155, 32);
			dateChFechaInicio.setVisible(false);
			dateChFechaInicio.setMinSelectableDate(new Date());
		}
		return dateChFechaInicio;
	}
	private JLabel getLblFechaInicio() {
		if (lblFechaInicio == null) {
			lblFechaInicio = new JLabel("Fecha inicio:");
			lblFechaInicio.setBounds(85, 116, 155, 26);
			lblFechaInicio.setVisible(false);
		}
		return lblFechaInicio;
	}
	private JDateChooser getDateChFechaFin() {
		if (dateChFechaFin == null) {
			dateChFechaFin = new JDateChooser();
			dateChFechaFin.setBounds(364, 142, 155, 32);
			dateChFechaFin.setVisible(false);
			dateChFechaFin.setMinSelectableDate(new Date());
		}
		return dateChFechaFin;
	}
	private JLabel getLblFechaFin() {
		if (lblFechaFin == null) {
			lblFechaFin = new JLabel("Fecha fin:");
			lblFechaFin.setBounds(364, 116, 135, 26);
			lblFechaFin.setVisible(false);
		}
		return lblFechaFin;
	}
	
	private boolean existeSuplemento(Statement s1/*, String nuevoSuplemento*/) throws SQLException{		
//		ResultSet rsCheck = s1.executeQuery ("select count(s.nombre_suplemento) "
//				+ "from suplemento_modalidad s "
//				+ "where s.pk_tipohab = '" + tipoSeleccionado +"' and s.nombre_suplemento = '"
//						+nuevoSuplemento+ "'");
		System.out.println("nombre suplemento: " +  suplementoSeleccionadoCombo);
		System.out.println("tipo seleccionado: " + tipoSeleccionado);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
		String fecha_ini= sdf.format(new java.sql.Date(dateChFechaInicio.getDate().getTime()));
		System.out.println(new java.sql.Date(dateChFechaInicio.getDate().getTime()).toString());
		System.out.println(fecha_ini);
		String fecha_fin= sdf.format(new java.sql.Date(dateChFechaFin.getDate().getTime()));
		
		
		ResultSet rsCheck = s1.executeQuery ("select count(s.nombre_suplemento) "
				+ "from suplemento_modalidad s, precio_suplemento ps "
				+ "where ((to_date('" +fecha_ini + "','dd-MM-YYYY') between ps.fecha_inicio  "
				+ "and ps.fecha_fin) or (to_date('"+ fecha_fin +"','dd-MM-YYYY') between ps.fecha_inicio "
				+ "and ps.fecha_fin)) and s.nombre_suplemento= '"+ suplementoSeleccionadoCombo +"' and "
				+ "ps.pk_supl = s.id_supl_mod and s.pk_tipo_habitacion = '"+ tipoSeleccionado +"'");
		
		rsCheck.next();
		int existeSuplemento = rsCheck.getInt("COUNT(S.NOMBRE_SUPLEMENTO)");
		System.out.println("suplemento existe: "+ existeSuplemento);
		return existeSuplemento>0;
	}
	
	private boolean nombreSuplementoExiste(){
		try {
			Statement s1 = con.getCon().createStatement();
			ResultSet rsCheck = s1.executeQuery ("select count(distinct(s.id_supl_mod)) "
					+ "from suplemento_modalidad s, precio_suplemento ps "
					+ "where ps.pk_supl = s.id_supl_mod and "
					+ "s.nombre_suplemento = '" + suplementoSeleccionadoCombo +"' and "
					+ "s.pk_tipo_habitacion='"+ tipoSeleccionado + "'");
			
			rsCheck.next();
			int existeNombreSuplemento = rsCheck.getInt("COUNT(DISTINCT(S.ID_SUPL_MOD))");
			if(existeNombreSuplemento>0)
				id_supl = existeNombreSuplemento;
			return existeNombreSuplemento>0;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private void insertar(Statement s1/*, String nuevoSuplemento*/, float nuevoImporte) throws SQLException{
			
		if(!nombreSuplementoExiste()){
			Statement st = con.getCon().createStatement();
			ResultSet rs = st.executeQuery("select SECUENCIA_SUPLMOD.nextVal from dual");
			rs.next();
			int id_Acc = rs.getInt("NEXTVAL");
			id_supl = id_Acc;
			
			PreparedStatement psSuplemento = con.getCon().prepareStatement(""
					+ "insert into suplemento_modalidad values(?, ?, ?)");
			psSuplemento.setString(1, suplementoSeleccionadoCombo);
			psSuplemento.setString(2, tipoSeleccionado);
			psSuplemento.setInt(3, id_Acc);
			psSuplemento.executeUpdate();
			psSuplemento.close();
		}
//		ResultSet rsIdSupl = s1.executeQuery("select distinct(id_supl_mod)  "
//				+ "from suplemento_modalidad "
//				+ "where nombre_suplemento = '"+suplementoSeleccionadoCombo +"' "
//				+ "and pk_tipohab = '"+ tipoSeleccionado + "'");
//		rsIdSupl.next();
//		id_supl = rsIdSupl.getInt("ID_SUPL_MOD");
		
			
		PreparedStatement psPrecio = con.getCon().
				prepareStatement("insert into precio_suplemento values(SECUENCIA_PRECIOSUPL.nextval, to_date(?), to_date(?), ?, ?)");

		psPrecio.setDate(1, new java.sql.Date(dateChFechaInicio.getDate().getTime()));
		psPrecio.setDate(2, new java.sql.Date(dateChFechaFin.getDate().getTime()));
		psPrecio.setFloat(3, nuevoImporte);
		psPrecio.setInt(4, id_supl);
		
		psPrecio.executeUpdate();
		System.out.println("Insertado");
		psPrecio.close();
		s1.close();
		rellenarTablaSuplementos();
		tablaSuplementos.repaint();
	}

	
	private boolean comprobarImporte(){
		String importe = tfNuevoImporte.getText();
		try{
			Double.parseDouble(importe);
			return true;
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "Formato de importe incorrecto");
			tfNuevoImporte.setBorder(new LineBorder(Color.RED));
			return false;
		}
	}
	
	private boolean comprobarFechas(){
		try{
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			Date fecha_inicio = formato.parse(formato.format(dateChFechaInicio.getDate()));
			Date fecha_fin = formato.parse(formato.format(dateChFechaFin.getDate()));

			Date fecha_acc = formato.parse(formato.format(new Date()));
			
			if(fecha_inicio.compareTo(fecha_acc) >= 0){
				if(fecha_inicio.compareTo(fecha_fin) > 0){
					JOptionPane.showMessageDialog(null, "La fecha de fin debe de ser mayor que la fecha de inicio");
					return false;
				}
				else 
					return true;
			}
			else{
				JOptionPane.showMessageDialog(null, "La fecha de inicio debe ser mayor que la fecha actual del sistema");
				return false;
			}
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "Fechas incorrectas");
			return false;
		}
	}
	private JPanel getPanelModificacion() {
		if (panelModificacion == null) {
			panelModificacion = new JPanel();
			panelModificacion.setName("panelModificacion");
			panelModificacion.setLayout(null);
			panelModificacion.add(getLblTarifaMod());
			panelModificacion.add(getTxfTarifaMod());
			panelModificacion.add(getLblImporteMod());
			panelModificacion.add(getTxfImporteMod());
			panelModificacion.add(getLblFechaInicioMod());
			panelModificacion.add(getLblFechaFinMod());
			panelModificacion.add(getDtChFechaInicioMod());
			panelModificacion.add(getDtChFechaFinMod());
		}
		return panelModificacion;
	}
	private JLabel getLblTarifaMod() {
		if (lblSuplementoMod == null) {
			lblSuplementoMod = new JLabel("Tarifa:");
			lblSuplementoMod.setBounds(69, 47, 92, 26);
		}
		return lblSuplementoMod;
	}
	private JTextField getTxfTarifaMod() {
		if (txfSuplementoMod == null) {
			txfSuplementoMod = new JTextField();
			txfSuplementoMod.setBounds(69, 72, 186, 32);
			txfSuplementoMod.setColumns(10);
		}
		return txfSuplementoMod;
	}
	private JLabel getLblImporteMod() {
		if (lblImporteMod == null) {
			lblImporteMod = new JLabel("Importe:");
			lblImporteMod.setBounds(69, 143, 92, 26);
		}
		return lblImporteMod;
	}
	private JTextField getTxfImporteMod() {
		if (txfImporteMod == null) {
			txfImporteMod = new JTextField();
			txfImporteMod.setBounds(69, 170, 186, 32);
			txfImporteMod.setColumns(10);
		}
		return txfImporteMod;
	}
	private JLabel getLblFechaInicioMod() {
		if (lblFechaInicioMod == null) {
			lblFechaInicioMod = new JLabel("Fecha inicio:");
			lblFechaInicioMod.setBounds(344, 47, 147, 26);
		}
		return lblFechaInicioMod;
	}
	private JLabel getLblFechaFinMod() {
		if (lblFechaFinMod == null) {
			lblFechaFinMod = new JLabel("Fecha fin:");
			lblFechaFinMod.setBounds(599, 47, 115, 26);
		}
		return lblFechaFinMod;
	}
	private JDateChooser getDtChFechaInicioMod() {
		if (dtChFechaInicioMod == null) {
			dtChFechaInicioMod = new JDateChooser();
			dtChFechaInicioMod.setBounds(344, 72, 155, 32);
		}
		return dtChFechaInicioMod;
	}
	private JDateChooser getDtChFechaFinMod() {
		if (dtChFechaFinMod == null) {
			dtChFechaFinMod = new JDateChooser();
			dtChFechaFinMod.setBounds(599, 72, 155, 32);
		}
		return dtChFechaFinMod;
	}
	private JLabel getLblTipoDeHabitacin() {
		if (lblTipoDeHabitacin == null) {
			lblTipoDeHabitacin = new JLabel("Tipo de habitaci\u00F3n:");
			lblTipoDeHabitacin.setBounds(85, 34, 211, 26);
			lblTipoDeHabitacin.setVisible(false);
		}
		return lblTipoDeHabitacin;
	}
	private JComboBox<String> getCbTipoHab() {
		if (cbTipoHab == null) {
			cbTipoHab = new JComboBox<String>();
			cbTipoHab.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					tipoSeleccionado = (String)cbTipoHab.getSelectedItem();
					System.out.println(tipoSeleccionado);
				}
			});
			cbTipoHab.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					String tipo = tipoSeleccionado;
					cbTipoHab.removeAllItems();
					rellenarTiposHab();
					if(tipo!= null)
						cbTipoHab.setSelectedItem(tipo);
					else
						cbTipoHab.setSelectedItem(null);
					cbTipoHab.repaint();
				}
			});
			cbTipoHab.setBounds(85, 62, 186, 32);
			cbTipoHab.setVisible(false);
		}
		return cbTipoHab;
	}
	
	private void rellenarTiposHab(){
		try{
			Statement s = con.getCon().createStatement();	
			ResultSet rs = s.executeQuery("select t.nombre from tipo_habitacion t");
			
			while(rs.next()){
				cbTipoHab.addItem(rs.getString("NOMBRE"));
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void rellenarComboSuplementos(){
		try{
			Statement s = con.getCon().createStatement();	
			ResultSet rs = s.executeQuery("select distinct(nombre_suplemento) from suplemento_modalidad");
			
			while(rs.next()){
				cbSuplemento.addItem(rs.getString("NOMBRE_SUPLEMENTO"));
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	private JComboBox<String> getCbSuplemento() {
		if (cbSuplemento == null) {
			cbSuplemento = new JComboBox<String>();
			cbSuplemento.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					suplementoSeleccionadoCombo = (String)cbSuplemento.getSelectedItem();
					System.out.println(suplementoSeleccionadoCombo);
				}
			});
			cbSuplemento.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					String suplemento = suplementoSeleccionadoCombo;
					cbSuplemento.removeAllItems();
					rellenarComboSuplementos();
					if(suplemento!= null)
						cbSuplemento.setSelectedItem(suplemento);
					else
						cbSuplemento.setSelectedItem(null);
					cbSuplemento.repaint();
				}
			});
			cbSuplemento.setBounds(364, 62, 186, 32);
			cbSuplemento.setVisible(false);
		}
		return cbSuplemento;
	}
	
	
}
