package igu;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;


import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import java.util.Date;
import java.awt.Color;
import javax.swing.JTextField;

import com.toedter.calendar.JDateChooser;

import javax.swing.border.EtchedBorder;

public class CardTiposNuevoTipo extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Connection con;
	private String[] cabecera = {"TIPO DE HABITACIÓN", "IMPORTE", "FECHA DE ALTA", "FECHA DE BAJA"};
	private JButton btnCrear;
	private JPanel panelAsignarTarifa;
	private JLabel lblNuevoTipo;
	private JLabel lblImporte;
	private JTextField tfImporte;
	private JDateChooser dateChFechaInicio;
	private JDateChooser dateChFechaFin;
	private JLabel lblFechaInicio;
	private JLabel lblFechaFin;
	private JButton btnAtras;
	private JScrollPane scTablaTarifasError;
	private JTable tablaTarifas;
	private JLabel lblEstasSonLa;
	private JLabel lblTipoError;
	private JTextField tfNuevoTipo;
	private CardGerenteHabitaciones padre;

	
	public class MiModeloTabla extends DefaultTableModel{
		private static final long serialVersionUID = 1L;
		public MiModeloTabla(Object[] columnNames, int numFilas) {
			super(columnNames,numFilas);
		}
		
		public boolean isCellEditable(int numFila, int numColumna){
			return false;
		}
	}
	

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public CardTiposNuevoTipo(Connection conexion, CardGerenteHabitaciones padre) {
		this.con = conexion;
		this.padre = padre;
		setBounds(100, 100, 755, 626);
		setLayout(new BorderLayout(0, 0));
		add(getPanelAsignarTarifa(), BorderLayout.CENTER);
		rellenarTablaTarifas();
		setVisible(true);
		
	}
	
	private void rellenarTablaTarifas(){
		try {
			MiModeloTabla miModelo = new MiModeloTabla(cabecera, 0);
			
			Statement s = con.createStatement();
			
			ResultSet rs = s.executeQuery("select distinct nombre, importe_dia, fecha_inicio, fecha_fin " + 
					"from tipo_habitacion natural join precio_tipo pt " + 
					"order by pt.fecha_inicio desc");
	
			
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			
			while(rs.next()){
				Object[] fila = new Object[4];
				fila[0] = rs.getString("nombre");
				fila[1] = rs.getFloat("importe_dia");
				fila[2] = formato.format(rs.getDate("fecha_inicio"));
				fila[3] = formato.format(rs.getDate("fecha_fin"));
				
				miModelo.addRow(fila);
			}
			tablaTarifas.setModel(miModelo);
			modificarAspectoTabla();
			rs.close();
			s.close();
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void modificarAspectoTabla() {
		tablaTarifas.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 13));
		tablaTarifas.setFont(new Font("Tahoma", Font.PLAIN, 12));
		TableColumn columna = tablaTarifas.getColumn(cabecera[0]);	
		columna.setMinWidth(175);
		columna.setMaxWidth(175);
		tablaTarifas.setRowHeight(20);
	}
	
	private void resetCampos(){
		tfNuevoTipo.setText(null);
		tfImporte.setText(null);
		tfImporte.setBorder(new LineBorder(Color.BLACK));
		dateChFechaInicio.setDate(null);
		dateChFechaFin.setDate(null);
	}
	private JButton getBtnCrear() {
		if (btnCrear == null) {
			btnCrear = new JButton("Crear");
			btnCrear.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(comprobarTodosLosCamposEstanCorrectos()) {
						if(!existeTipo()) {
							if(crearTipo()) {
								JOptionPane.showMessageDialog(null, "Tarifa insertada correctamente", "OK", JOptionPane.INFORMATION_MESSAGE);
								rellenarTablaTarifas();
								tablaTarifas.repaint();
								resetCampos();
							}
						}	
						else {
							JOptionPane.showMessageDialog(null, "Tipo de habitación ya existente.\n"
									+ "Si desea asignar una tarifa a un tipo de habitación vaya a \"Asignar tarifa\"", "Error", JOptionPane.ERROR_MESSAGE);
						}
					}
					else {
						JOptionPane.showMessageDialog(null, "Escriba un nombre para el tipo de dato", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			});
			btnCrear.setBounds(604, 214, 100, 36);
			btnCrear.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return btnCrear;
	}
	private boolean crearTipo() {
		try {
			String consulta = "insert into tipo_habitacion values(?)";
			
			PreparedStatement ps = con.prepareStatement(consulta);
			ps.setString(1, tfNuevoTipo.getText());
			
			int insertado = ps.executeUpdate();
			if(insertado >0 ) {
				if(insertarTarifa())
					return true;
				return false;
			}
			ps.close();
			return false;
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private boolean insertarTarifa() {
		try {
			PreparedStatement ps = con.
					prepareStatement("insert into precio_tipo values(sec_preciotipo.nextVal, to_date(?), to_date(?), ?, ?)");
			ps.setDate(1, new java.sql.Date(dateChFechaInicio.getDate().getTime()));
			ps.setDate(2, new java.sql.Date(dateChFechaFin.getDate().getTime()));
			ps.setFloat(3, Float.parseFloat(tfImporte.getText()));
			ps.setString(4, tfNuevoTipo.getText());
			
			int insertado = ps.executeUpdate();
			ps.close();
			if(insertado == 1) { 
				return true;
			}
			return false;
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private boolean existeTipo() {
		try {
			String consulta = "select count(nombre) from tipo_habitacion where nombre = ?";
			PreparedStatement ps = con.prepareStatement(consulta);
			ps.setString(1, tfNuevoTipo.getText());
			
			ResultSet rs = ps.executeQuery();
			int tiposExistentes = 0;
			while(rs.next()) {
				tiposExistentes = rs.getInt("count(nombre)");
			}
			
			rs.close();
			ps.close();
			if(tiposExistentes > 0)
				return true;
			return false;
		}
		catch(Exception e) {
			e.printStackTrace();
			return true;
		}
	}
	
	private boolean comprobarTodosLosCamposEstanCorrectos() {
		String nombre = tfNuevoTipo.getText();
		return comprobarImporte() &&
				comprobarFechas(dateChFechaInicio, dateChFechaFin) && nombre != null;
	}
	private JPanel getPanelAsignarTarifa() {
		if (panelAsignarTarifa == null) {
			panelAsignarTarifa = new JPanel();
			panelAsignarTarifa.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Crear nuevo tipo de habitaci\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			panelAsignarTarifa.setName("panelNuevoTipo");
			panelAsignarTarifa.setLayout(null);
			panelAsignarTarifa.add(getLblNuevaTarifa());
			panelAsignarTarifa.add(getLblImporte());
			panelAsignarTarifa.add(getTxfImporte());
			panelAsignarTarifa.add(getDateChFechaInicio());
			panelAsignarTarifa.add(getLblFechaInicio());
			panelAsignarTarifa.add(getDateChFechaFin());
			panelAsignarTarifa.add(getLblFechaFin());
			panelAsignarTarifa.add(getBtnCrear());
			panelAsignarTarifa.add(getBtnAtras());
			panelAsignarTarifa.add(getScTablaTarifasError());
			panelAsignarTarifa.add(getLblEstasSonLa());
			panelAsignarTarifa.add(getLblTipoError());
			panelAsignarTarifa.add(getTfNuevoTipo());
		}
		return panelAsignarTarifa;
	}
	
	private JLabel getLblNuevaTarifa() {
		if (lblNuevoTipo == null) {
			lblNuevoTipo = new JLabel("Nombre del tipo de habitaci\u00F3n:");
			lblNuevoTipo.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblNuevoTipo.setBounds(154, 36, 186, 26);
		}
		return lblNuevoTipo;
	}
	private JLabel getLblImporte() {
		if (lblImporte == null) {
			lblImporte = new JLabel("Importe:");
			lblImporte.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblImporte.setBounds(399, 35, 127, 26);
		}
		return lblImporte;
	}
	private JTextField getTxfImporte() {
		if (tfImporte == null) {
			tfImporte = new JTextField();
			tfImporte.setFont(new Font("Tahoma", Font.PLAIN, 14));
			tfImporte.setBounds(399, 62, 100, 32);
			tfImporte.setColumns(10);
		}
		return tfImporte;
	}
	private JDateChooser getDateChFechaInicio() {
		if (dateChFechaInicio == null) {
			dateChFechaInicio = new JDateChooser();
			dateChFechaInicio.getCalendarButton().setFont(new Font("Tahoma", Font.PLAIN, 12));
			dateChFechaInicio.setFont(new Font("Tahoma", Font.PLAIN, 12));
			dateChFechaInicio.setBounds(154, 140, 138, 32);
			dateChFechaInicio.setVisible(true);
			dateChFechaInicio.setMinSelectableDate(new Date());
		}
		return dateChFechaInicio;
	}
	private JLabel getLblFechaInicio() {
		if (lblFechaInicio == null) {
			lblFechaInicio = new JLabel("Fecha inicio:");
			lblFechaInicio.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblFechaInicio.setBounds(154, 114, 155, 26);
		}
		return lblFechaInicio;
	}
	private JDateChooser getDateChFechaFin() {
		if (dateChFechaFin == null) {
			dateChFechaFin = new JDateChooser();
			dateChFechaFin.getCalendarButton().setFont(new Font("Tahoma", Font.PLAIN, 12));
			dateChFechaFin.setFont(new Font("Tahoma", Font.PLAIN, 12));
			dateChFechaFin.setBounds(399, 140, 132, 32);
			dateChFechaFin.setVisible(true);
			dateChFechaFin.setMinSelectableDate(new Date());
		}
		return dateChFechaFin;
	}
	private JLabel getLblFechaFin() {
		if (lblFechaFin == null) {
			lblFechaFin = new JLabel("Fecha fin:");
			lblFechaFin.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblFechaFin.setBounds(399, 114, 135, 26);
		}
		return lblFechaFin;
	}
	

	
	private boolean comprobarImporte(){
		String importe = tfImporte.getText();
		try{
			Double.parseDouble(importe);
			return true;
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "Formato de importe incorrecto", "Error", JOptionPane.ERROR_MESSAGE);
			tfImporte.setBorder(new LineBorder(Color.RED));
			return false;
		}
	}
	
	
	private boolean comprobarFechas(JDateChooser fechaInicio, JDateChooser fechaFin){
		try{
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			Date fecha_inicio = formato.parse(formato.format(fechaInicio.getDate()));
			Date fecha_fin = formato.parse(formato.format(fechaFin.getDate()));
			
			if(fecha_inicio.compareTo(fecha_fin) > 0){
				JOptionPane.showMessageDialog(null, "La fecha de fin debe de ser mayor que la fecha de inicio");
				return false;
			}
			else 
				return true;
			
			
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "Fechas incorrectas");
			return false;
		}
	}
	
	
	
	private JButton getBtnAtras() {
		if (btnAtras == null) {
			btnAtras = new JButton("Atr\u00E1s");
			btnAtras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					resetCampos();
					padre.mostrarOpciones();
				}
			});
			btnAtras.setBounds(506, 214, 86, 36);
		}
		return btnAtras;
	}

	
	
	
	private JScrollPane getScTablaTarifasError() {
		if (scTablaTarifasError == null) {
			scTablaTarifasError = new JScrollPane();
			scTablaTarifasError.setBorder(new LineBorder(Color.BLACK, 3));
			scTablaTarifasError.setBounds(17, 297, 697, 287);
			scTablaTarifasError.setViewportView(getTablaTarifasError());
		}
		return scTablaTarifasError;
	}
	private JTable getTablaTarifasError() {
		if (tablaTarifas == null) {
			tablaTarifas = new JTable();
		}
		return tablaTarifas;
	}
	private JLabel getLblEstasSonLa() {
		if (lblEstasSonLa == null) {
			lblEstasSonLa = new JLabel("Tipos de habitaci\u00F3n existentes:");
			lblEstasSonLa.setForeground(Color.BLACK);
			lblEstasSonLa.setBounds(17, 269, 388, 26);
		}
		return lblEstasSonLa;
	}
	private JLabel getLblTipoError() {
		if (lblTipoError == null) {
			lblTipoError = new JLabel("");
			lblTipoError.setForeground(Color.RED);
			lblTipoError.setFont(new Font("Lucida Grande", Font.ITALIC, 13));
			lblTipoError.setBounds(399, 269, 100, 26);
			lblTipoError.setVisible(false);
		}
		return lblTipoError;
	}
	private JTextField getTfNuevoTipo() {
		if (tfNuevoTipo == null) {
			tfNuevoTipo = new JTextField();
			tfNuevoTipo.setBounds(154, 62, 170, 32);
			tfNuevoTipo.setColumns(10);
		}
		return tfNuevoTipo;
	}
}
