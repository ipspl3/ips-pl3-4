package igu;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import logica.Cliente;
import logica.DNI;
import logica.DataBase;
import logica.Reserva;
import javax.swing.JSeparator;
import javax.swing.JComboBox;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

@SuppressWarnings("serial")
public class CardGestionarReserva extends JPanel {

	private VentanaRecepcionista padre;

	private JButton btnBorrar;
	private JButton btnCancelar;
	private JLabel lblDni;
	private JTextField txtDni;
	private JButton btnBuscar;

	private JScrollPane scrollPane;
	private JList<Reserva> listReservas;
	private DefaultListModel<Reserva> list4Model = null;
	private DefaultComboBoxModel<String> combo1Model = null;
	private Reserva res;
	private JPanel pnCliente;
	private JLabel lblNombre;
	private JLabel lblTxtNombre;
	private JLabel lblTelefono;
	private JLabel lblTxtTelefono;
	private JLabel lblTarjeta;
	private JLabel lblTxtTarjeta;
	private JSeparator separator;

	private Cliente cliente;
	private JPanel pnReserva;
	private JLabel lblHabitacion;
	private JLabel lblTxtHabitacion;
	private JLabel lblModalidad;
	private JComboBox<String> comboBox;
	private JButton btnCambiar;

	public CardGestionarReserva(VentanaRecepcionista papa) {

		padre = papa;

		setLayout(null);
		add(getScrollPane());
		add(getLblDni());
		add(getTxtDni());
		add(getBtnBuscar());
		add(getBtnBorrar());
		add(getBtnCancelar());
		add(getPnCliente());
		add(getSeparator_1());
		add(getPnReserva());
		todasReservas();

		setVisible(true);
	}
	private JLabel getLblDni() {
		if (lblDni == null) {
			lblDni = new JLabel("DNI:");
			lblDni.setBounds(24, 22, 64, 14);
		}
		return lblDni;
	}
	private JTextField getTxtDni() {
		if (txtDni == null) {
			txtDni = new JTextField();
			txtDni.setBounds(93, 19, 141, 20);
			txtDni.setColumns(10);
			txtDni.addKeyListener(new KeyAdapter() {
				int limite = 9;
				@Override
				public void keyTyped(KeyEvent e) {
					if (txtDni.getText().length() == limite){
						DNI dni = new DNI(txtDni.getText().toUpperCase());
						boolean bien = dni.comprobarDNI();
						boolean bien_p = dni.comprobarPasaporte();
						if (bien || bien_p){
							txtDni.setBorder(new LineBorder(Color.BLACK));
							btnBuscar.setEnabled(true);
						}
						else{
							txtDni.setBorder(new LineBorder(Color.RED));
						}
					}
					if (txtDni.getText().length() >= limite){
						e.consume();
					}
				}
			});
		}
		return txtDni;
	}
	private JButton getBtnBuscar() {
		if (btnBuscar == null) {
			btnBuscar = new JButton("Buscar");
			btnBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (txtDni.getText().equals("")) {
						JOptionPane.showMessageDialog(null, "Introduce un DNI o pasaporte v�lido");
					}
					else {
						DNI dni = new DNI(txtDni.getText().toUpperCase());
						if (dni.comprobarDNI() || dni.comprobarPasaporte()) {
							list4Model.clear();
							try {
								ArrayList<Reserva> reservas = DataBase.buscarReservaPorDni(txtDni.getText().toString().toUpperCase());
								for (Reserva res:reservas) 
								{
									list4Model.addElement(res);
								}
							} catch (SQLException e1) {
								e1.printStackTrace();
							}
						}
						else {
							JOptionPane.showMessageDialog(null, "Introduce un DNI o pasaporte v�lido");
						}
					}
				}
			});
			btnBuscar.setBounds(266, 18, 89, 23);
		}
		return btnBuscar;
	}

	private JButton getBtnBorrar() {
		if (btnBorrar == null) {
			btnBorrar = new JButton("Borrar Reserva");
			btnBorrar.addActionListener(new ActionListener() {
				@SuppressWarnings("deprecation")
				public void actionPerformed(ActionEvent e) {
					try {
						int opcion = JOptionPane.showConfirmDialog(null, "�Desea elimnar la reserva?", "Confirmar cambios", JOptionPane.YES_NO_OPTION);
						if(opcion == 0){//YES
							int pk_reserva = ((Reserva) listReservas.getSelectedValues()[0]).getPk();
							double totalGastos = 0;
							for (Object ob:listReservas.getSelectedValues()) {
								Reserva temp = (Reserva) ob;
								Date today = new Date();
								today.setHours(0); 
								java.sql.Date fecha = new java.sql.Date(today.getTime());
								if (temp.f_in_d.getYear() == fecha.getYear() && temp.f_in_d.getMonth() == fecha.getMonth() && (temp.f_in_d.getDate()-1) <= fecha.getDate()) {
									totalGastos = totalGastos + temp.cargarGastosCancelacion();
								}
								DataBase.eliminarReserva(temp);
								list4Model.removeElement(temp);
							}
							totalGastos = totalGastos*0.5;
							if (totalGastos != 0) {
								JOptionPane.showMessageDialog(null, "Esta cancelando una reserva con menos de 24h de antelaci�n."
										+ "\nSe le cargar�n en la tarjeta los gastos de cancelaci�n");
								JOptionPane.showMessageDialog(null,"Se ha cargado en su tarjeta un importe total de " + totalGastos + "�");
							}
							if (!DataBase.existenReservas_hab(pk_reserva)) {
								DataBase.eleiminarReserva1(pk_reserva);
								JOptionPane.showMessageDialog(null,"No quedaban mas habitaciones reservadas, y se ha cancelado la reserva completa.");
							}
							else {
								JOptionPane.showMessageDialog(null,"Se han cancelado correctamente las habitaciones.");
							}
							refrescar();
						}
					}
					catch (SQLException e1) {
						JOptionPane.showMessageDialog(null, "La reserva no ha podido ser anulada, contacte con un t�cnico");
						e1.printStackTrace();
					}
				}
			});
			btnBorrar.setEnabled(false);
			btnBorrar.setBounds(346, 602, 158, 23);

		}
		return btnBorrar;
	}
	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					iniciar();
					padre.iniciar();
				}
			});
			btnCancelar.setBounds(514, 602, 118, 23);
		}
		return btnCancelar;
	}


	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBorder(new TitledBorder(null, "Reservas", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			scrollPane.setBounds(10, 49, 622, 256);
			scrollPane.setViewportView(getListReservas());
		}
		return scrollPane;
	}
	private JList<Reserva> getListReservas() {
		if (listReservas == null) {
			list4Model = new DefaultListModel<Reserva>();
			listReservas = new JList<Reserva>(list4Model);
			listReservas.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (listReservas.getSelectedIndices().length<2) {
						res = listReservas.getSelectedValue();
						try {
							cliente = DataBase.buscarClientePorDni(res.getDni());
							pnCliente.setVisible(true);
							pnReserva.setVisible(true);
							llenarCombo();
							rellenarCliente();
							rellenarHab();
							btnBorrar.setEnabled(true);
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
					else {
						pnReserva.setVisible(false);
						comprobarClientes();
					}
				}
			});
		}
		return listReservas;
	}

	@SuppressWarnings("deprecation")
	private void comprobarClientes(){
		Reserva res = (Reserva) listReservas.getSelectedValues()[0];
		int pk = res.getPk();
		boolean perfe = true;
		for (Object resTemp: listReservas.getSelectedValues()) {
			if (!(((Reserva) resTemp).getPk() == pk)) {
				JOptionPane.showMessageDialog(null, "Para una selecci�n m�ltiple, la reserva debe ser la misma.");
				perfe = false;
				btnBorrar.setEnabled(false);
			}
		}
		if (perfe) {
			btnBorrar.setEnabled(true);
		}
	}

	private void rellenarCliente() {
		lblTxtNombre.setText(cliente.getName() + ", " + cliente.getApellidos());
		lblTxtTelefono.setText(String.valueOf(cliente.getTelefono()));
		lblTxtTarjeta.setText(cliente.getTarjeta().toString());
	}

	private void rellenarHab() {
		lblTxtHabitacion.setText(res.getHab().id + " (" + res.getHab().tipo + ") ");
	}

	public void iniciar(){
		list4Model.clear();
		txtDni.setText("");
		pnCliente.setVisible(false);

		pnReserva.setVisible(false);

		btnCambiar.setEnabled(false);

		padre.iniciar();
	}

	public void todasReservas() {
		if (DataBase.isConnected() && list4Model.isEmpty()) {
			try {
				ArrayList<Reserva> reservas = DataBase.buscarReservas();
				for (Reserva res:reservas) 
				{
					list4Model.addElement(res);
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
	private JPanel getPnCliente() {
		if (pnCliente == null) {
			pnCliente = new JPanel();
			pnCliente.setBorder(new TitledBorder(null, "Cliente", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnCliente.setBounds(10, 327, 622, 101);
			pnCliente.setLayout(null);
			pnCliente.add(getLblNombre());
			pnCliente.add(getLblTxtNombre());
			pnCliente.add(getLblTelefono());
			pnCliente.add(getLblTxtTelefono());
			pnCliente.add(getLblTarjeta());
			pnCliente.add(getLblTxtTarjeta());
			pnCliente.setVisible(false);
		}
		return pnCliente;
	}
	private JLabel getLblNombre() {
		if (lblNombre == null) {
			lblNombre = new JLabel("Nombre:");
			lblNombre.setBounds(10, 26, 60, 14);
		}
		return lblNombre;
	}
	private JLabel getLblTxtNombre() {
		if (lblTxtNombre == null) {
			lblTxtNombre = new JLabel("");
			lblTxtNombre.setBounds(79, 26, 212, 14);
		}
		return lblTxtNombre;
	}
	private JLabel getLblTelefono() {
		if (lblTelefono == null) {
			lblTelefono = new JLabel("Telefono:");
			lblTelefono.setBounds(10, 51, 60, 14);
		}
		return lblTelefono;
	}
	private JLabel getLblTxtTelefono() {
		if (lblTxtTelefono == null) {
			lblTxtTelefono = new JLabel("");
			lblTxtTelefono.setBounds(79, 51, 212, 14);
		}
		return lblTxtTelefono;
	}
	private JLabel getLblTarjeta() {
		if (lblTarjeta == null) {
			lblTarjeta = new JLabel("Tarjeta:");
			lblTarjeta.setBounds(10, 76, 60, 14);
		}
		return lblTarjeta;
	}
	private JLabel getLblTxtTarjeta() {
		if (lblTxtTarjeta == null) {
			lblTxtTarjeta = new JLabel("");
			lblTxtTarjeta.setBounds(79, 76, 212, 14);
		}
		return lblTxtTarjeta;
	}
	private JSeparator getSeparator_1() {
		if (separator == null) {
			separator = new JSeparator();
			separator.setBounds(20, 314, 601, 2);
		}
		return separator;
	}
	private JPanel getPnReserva() {
		if (pnReserva == null) {
			pnReserva = new JPanel();
			pnReserva.setBounds(10, 439, 622, 150);
			pnReserva.setLayout(null);
			pnReserva.add(getLblHabitacin());
			pnReserva.add(getLblTxtHabitacion());
			pnReserva.add(getLblModalidad());
			pnReserva.add(getComboBox());
			pnReserva.add(getBtnCambiar());

			pnReserva.setVisible(false);
		}
		return pnReserva;
	}
	private JLabel getLblHabitacin() {
		if (lblHabitacion == null) {
			lblHabitacion = new JLabel("Habitaci\u00F3n:");
			lblHabitacion.setBounds(10, 11, 71, 14);
		}
		return lblHabitacion;
	}
	private JLabel getLblTxtHabitacion() {
		if (lblTxtHabitacion == null) {
			lblTxtHabitacion = new JLabel("");
			lblTxtHabitacion.setBounds(85, 11, 120, 14);
		}
		return lblTxtHabitacion;
	}
	private JLabel getLblModalidad() {
		if (lblModalidad == null) {
			lblModalidad = new JLabel("Modalidad:");
			lblModalidad.setBounds(10, 48, 71, 14);
		}
		return lblModalidad;
	}
	private JComboBox<String> getComboBox() {
		if (comboBox == null) {
			combo1Model = new DefaultComboBoxModel<String>();
			comboBox = new JComboBox<String>();
			comboBox.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent e) {
					btnCambiar.setEnabled(true);
				}
			});
			comboBox.setBounds(85, 45, 120, 20);
			comboBox.setModel(combo1Model);
		}
		return comboBox;
	}

	private void llenarCombo() {
		if(combo1Model.getSize()==0) {
			ArrayList<String> temp;
			try {
				temp = DataBase.Modalidades(res.getHab().tipo);
				for (String e:temp) {
					combo1Model.addElement(e);
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		combo1Model.setSelectedItem(res.getHab().modalidad);
	}
	private JButton getBtnCambiar() {
		if (btnCambiar == null) {
			btnCambiar = new JButton("Cambiar");
			btnCambiar.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					try {
						DataBase.cambiarModalidad(res, comboBox.getSelectedItem(), res.getHab());
						JOptionPane.showMessageDialog(null, "Modalidad cambiada correctamente");

						llenarCombo();
						refrescar();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
			});
			btnCambiar.setBounds(215, 44, 89, 23);
			btnCambiar.setEnabled(false);
		}
		return btnCambiar;
	}

	private void refrescar() {
		list4Model.clear();
		todasReservas();
		btnCambiar.setEnabled(false);
	}
}
