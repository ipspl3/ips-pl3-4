package igu;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import java.awt.Font;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.DefaultListModel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;


import javax.swing.border.TitledBorder;

import java.util.List;


import java.awt.Dimension;
import javax.swing.JList;

public class CardTiposConsultarTarifas extends JPanel {


	private static final long serialVersionUID = 1L;
	private Connection con;
	private JScrollPane spTablaTarifasTipo;
	private JTable tablaTarifasTipo;
	private String[] cabecera = {"TIPO DE HABITACIÓN", "IMPORTE", "FECHA DE ALTA", "FECHA DE BAJA", "SUPLEMENTO", "IMPORTE", "FECHA DE ALTA", "FECHA DE BAJA"};
	private JPanel panelConsultarTarifas;
	private JLabel lblTarifasPorTipo;
	private JPanel panelFiltro;
	private JLabel lblSeleccioneUnunosTipos;
	private JScrollPane spListaFiltro;
	private JList<String> listaTiposHab;
	private JButton btnFiltrar;
	private JButton btnNuevaConsulta;
	private JButton btnAtrs;
	private CardGerenteHabitaciones padre;

	//PARA QUE SE VEAN LOS CARD EN EL DESIGN HAY QUE COMENTAR LA LINEA superCard.show(panelSuperCard, "panelElegirAccion"); DEL METODO GETPANELSUPERCARD();

	
	public class MiModeloTabla extends DefaultTableModel{
		private static final long serialVersionUID = 1L;
		public MiModeloTabla(Object[] columnNames, int numFilas) {
			super(columnNames,numFilas);
		}
		
		public boolean isCellEditable(int numFila, int numColumna){
			return false;
		}
	}
	

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public CardTiposConsultarTarifas(Connection conexion, CardGerenteHabitaciones padre) {
		this.con = conexion;
		this.padre = padre;
		setBounds(100, 100, 755, 626);
		setLayout(new BorderLayout(0, 0));
		add(getPanelConsultarTarifas(), BorderLayout.CENTER);
		rellenarTablaTarifas();
		setVisible(true);
		
	}
	private JScrollPane getSpTablaTarifasTipo() {
		if (spTablaTarifasTipo == null) {
			spTablaTarifasTipo = new JScrollPane();
			spTablaTarifasTipo.setBounds(6, 217, 733, 349);
			spTablaTarifasTipo.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
			spTablaTarifasTipo.setViewportView(getTablaTarifasTipo());
		}
		return spTablaTarifasTipo;
	}
	private JTable getTablaTarifasTipo() {
		if (tablaTarifasTipo == null) {
			tablaTarifasTipo = new JTable();
			tablaTarifasTipo.setBorder(null);
			tablaTarifasTipo.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tablaTarifasTipo;
	}
	
	public void rellenarTablaTarifas(){
		try {
			MiModeloTabla miModelo = new MiModeloTabla(cabecera, 0);
			
			Statement s = con.createStatement();
			
			ResultSet rs = s.executeQuery("select distinct t.nombre, pt.importe_dia, pt.fecha_inicio, pt.fecha_fin, s.nombre_suplemento, "
					+ "ps.importe, ps.fecha_inicio as s_fecha_incio, ps.fecha_fin as s_fecha_fin " + 
					"from tipo_habitacion t, precio_tipo pt, precio_suplemento ps, suplemento_modalidad s " + 
					"where t.nombre = pt.nombre and s.nombre = t.nombre and t.nombre = pt.nombre and "
					+ "s.id_supl_mod = ps.id_supl_mod order by pt.fecha_inicio desc");
	
			
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			
			while(rs.next()){
				Object[] fila = new Object[8];
				fila[0] = rs.getString("nombre");
				fila[1] = rs.getFloat("importe_dia");
				fila[2] = formato.format(rs.getDate("fecha_inicio"));
				fila[3] = formato.format(rs.getDate("fecha_fin"));
				fila[4] = rs.getString("nombre_suplemento");
				fila[5] = rs.getFloat("importe");
				fila[6] = formato.format(rs.getDate("s_fecha_incio"));
				fila[7] = formato.format(rs.getDate("s_fecha_fin"));
				miModelo.addRow(fila);
			}
			
			tablaTarifasTipo.setModel(miModelo);
			modificarTablaTipos(tablaTarifasTipo);
			añadirTiposSinSuplementos();
			rs.close();
			s.close();
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void añadirTiposSinSuplementos() {
		try {
			String consulta = "select distinct nombre, importe_dia, fecha_inicio, fecha_fin "
					+ "from tipo_habitacion natural join precio_tipo "
					+ "where nombre not in (select distinct nombre "
					+ "from precio_suplemento)";
			
			MiModeloTabla miModelo = (MiModeloTabla) tablaTarifasTipo.getModel();
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			
			PreparedStatement ps = con.prepareStatement(consulta);
			ResultSet rs = null;
				
			rs = ps.executeQuery();
			
			while(rs.next()){
				Object[] fila = new Object[8];
				fila[0] = rs.getString("nombre");
				fila[1] = rs.getFloat("importe_dia");
				fila[2] = formato.format(rs.getDate("fecha_inicio"));
				fila[3] = formato.format(rs.getDate("fecha_fin"));
				fila[4] = null;
				fila[5] = null;
				fila[6] = null;
				fila[7] = null;
				miModelo.addRow(fila);
			}
			
			
			tablaTarifasTipo.setModel(miModelo);
			modificarTablaTipos(tablaTarifasTipo);
			rs.close();
			ps.close();
	
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private void modificarTablaTipos(JTable tabla){
		tabla.getTableHeader().setFont(new Font("Tahoma", Font.PLAIN, 12));
		tabla.getTableHeader().setReorderingAllowed(false);
		TableColumn columna = tabla.getColumnModel().getColumn(1);
		columna.setMinWidth(50);
		columna.setMaxWidth(50);
		
		columna = tabla.getColumnModel().getColumn(5);
		columna.setMinWidth(50);
		columna.setMaxWidth(50);
		
		
		tabla.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tabla.setRowHeight(25);
	}

	private JPanel getPanelConsultarTarifas() {
		if (panelConsultarTarifas == null) {
			panelConsultarTarifas = new JPanel();
			panelConsultarTarifas.setName("panelTarifasTipo");
			panelConsultarTarifas.setMaximumSize(new Dimension(50, 50));
			panelConsultarTarifas.setLayout(null);
			panelConsultarTarifas.add(getSpTablaTarifasTipo());
			panelConsultarTarifas.add(getLblTarifasPorTipo());
			panelConsultarTarifas.add(getPanelFiltro());
			panelConsultarTarifas.add(getBtnNuevaConsulta());
			panelConsultarTarifas.add(getBtnAtrs());
		}
		return panelConsultarTarifas;
	}
	private JLabel getLblTarifasPorTipo() {
		if (lblTarifasPorTipo == null) {
			lblTarifasPorTipo = new JLabel("TARIFAS POR TIPO DE HABITACI\u00D3N");
			lblTarifasPorTipo.setBounds(0, 189, 745, 25);
			lblTarifasPorTipo.setFont(new Font("Lucida Grande", Font.BOLD, 20));
			lblTarifasPorTipo.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblTarifasPorTipo;
	}
	private JPanel getPanelFiltro() {
		if (panelFiltro == null) {
			panelFiltro = new JPanel();
			panelFiltro.setBorder(new TitledBorder(null, "Filtro", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelFiltro.setBounds(17, 6, 709, 171);
			panelFiltro.setLayout(null);
			panelFiltro.add(getLblSeleccioneUnunosTipos());
			panelFiltro.add(getSpListaFiltro());
			panelFiltro.add(getBtnFiltrar());
		}
		return panelFiltro;
	}
	private JLabel getLblSeleccioneUnunosTipos() {
		if (lblSeleccioneUnunosTipos == null) {
			lblSeleccioneUnunosTipos = new JLabel("Seleccione un/unos tipo/s de habitaci\u00F3n:");
			lblSeleccioneUnunosTipos.setBounds(240, 23, 270, 16);
		}
		return lblSeleccioneUnunosTipos;
	}
	private JScrollPane getSpListaFiltro() {
		if (spListaFiltro == null) {
			spListaFiltro = new JScrollPane();
			spListaFiltro.setBounds(240, 51, 261, 109);
			spListaFiltro.setViewportView(getListaTiposHab());
		}
		return spListaFiltro;
	}
	private JList<String> getListaTiposHab() {
		if (listaTiposHab == null) {
			listaTiposHab = new JList<String>();
			rellenarListaTiposHabPanelVerTarifas();
		}
		return listaTiposHab;
	}
	private void rellenarListaTiposHabPanelVerTarifas() {
		try {
			DefaultListModel<String> modelo = new DefaultListModel<String>();
			String consulta = "select distinct t.nombre from tipo_habitacion t";
			PreparedStatement ps = con.prepareStatement(consulta);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				modelo.addElement(rs.getString("NOMBRE"));
			}
			listaTiposHab.setModel(modelo);
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

	private JButton getBtnFiltrar() {
		if (btnFiltrar == null) {
			btnFiltrar = new JButton("Filtrar");
			btnFiltrar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					List<String> lista = listaTiposHab.getSelectedValuesList();
					if(lista.size()>0) {
						filtrarTarifas(lista);
						añadirTiposSinSuplementos(lista);
					}
					else {
						JOptionPane.showMessageDialog(null, "No ha sido seleccionado ningún tipo de habitación", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			});
			btnFiltrar.setBounds(611, 126, 81, 34);
		}
		return btnFiltrar;
	}
	
	private void filtrarTarifas(List<String> lista) {
		try {
			String consulta = "select distinct t.nombre, pt.importe_dia, pt.fecha_inicio, pt.fecha_fin, s.nombre_suplemento, "
					+ "ps.importe, ps.fecha_inicio as s_fecha_incio, ps.fecha_fin as s_fecha_fin " + 
					"from tipo_habitacion t, precio_tipo pt, precio_suplemento ps, suplemento_modalidad s " + 
					"where t.nombre = ? and t.nombre = pt.nombre and s.nombre = t.nombre and t.nombre = pt.nombre and "
					+ "s.id_supl_mod = ps.id_supl_mod order by pt.fecha_inicio desc ";
			
			MiModeloTabla miModelo = new MiModeloTabla(cabecera, 0);
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			
			PreparedStatement ps = con.prepareStatement(consulta);
			ResultSet rs = null;
			
			for (String tipo : lista) {	
				ps.setString(1, tipo);
				
				rs = ps.executeQuery();
				
				while(rs.next()){
					Object[] fila = new Object[8];
					fila[0] = rs.getString("nombre");
					fila[1] = rs.getFloat("importe_dia");
					fila[2] = formato.format(rs.getDate("fecha_inicio"));
					fila[3] = formato.format(rs.getDate("fecha_fin"));
					fila[4] = rs.getString("nombre_suplemento");
					fila[5] = rs.getFloat("importe");
					fila[6] = formato.format(rs.getDate("s_fecha_incio"));
					fila[7] = formato.format(rs.getDate("s_fecha_fin"));
					miModelo.addRow(fila);
				}
			}
			
			tablaTarifasTipo.setModel(miModelo);
			modificarTablaTipos(tablaTarifasTipo);
			rs.close();
			ps.close();
	
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	private void añadirTiposSinSuplementos(List<String> lista) {
		try {
			String consulta = "select distinct nombre, importe_dia, fecha_inicio, fecha_fin "
					+ "from tipo_habitacion natural join precio_tipo "
					+ "where nombre = ? and nombre not in (select distinct nombre "
					+ "from precio_suplemento)";
			
			MiModeloTabla miModelo = (MiModeloTabla) tablaTarifasTipo.getModel();
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			
			PreparedStatement ps = con.prepareStatement(consulta);
			ResultSet rs = null;
			
			for (String tipo : lista) {	
				ps.setString(1, tipo);
				
				rs = ps.executeQuery();
				
				while(rs.next()){
					Object[] fila = new Object[8];
					fila[0] = rs.getString("nombre");
					fila[1] = rs.getFloat("importe_dia");
					fila[2] = formato.format(rs.getDate("fecha_inicio"));
					fila[3] = formato.format(rs.getDate("fecha_fin"));
					fila[4] = null;
					fila[5] = null;
					fila[6] = null;
					fila[7] = null;
					miModelo.addRow(fila);
				}
			}
			
			tablaTarifasTipo.setModel(miModelo);
			modificarTablaTipos(tablaTarifasTipo);
			rs.close();
			ps.close();
	
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	private JButton getBtnNuevaConsulta() {
		if (btnNuevaConsulta == null) {
			btnNuevaConsulta = new JButton("Nueva consulta");
			btnNuevaConsulta.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					rellenarTablaTarifas();
					limpiarDatosFiltrado();
				}
			});
			btnNuevaConsulta.setBounds(598, 578, 141, 29);
		}
		return btnNuevaConsulta;
	}
	
	private void limpiarDatosFiltrado() {
		rellenarListaTiposHabPanelVerTarifas();
	}
	private JButton getBtnAtrs() {
		if (btnAtrs == null) {
			btnAtrs = new JButton("Atr\u00E1s");
			btnAtrs.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					limpiarDatosFiltrado();		
					padre.mostrarOpciones();
				}
			});
			btnAtrs.setBounds(497, 578, 89, 29);
		}
		return btnAtrs;
	}
}
