package igu;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.GridLayout;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.FlowLayout;
import javax.swing.JTextField;

import igu.CardModificarTurnosTrabajo.Trabajador;
import logica.Conexion;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CardModificarEstadoTrabajador extends JPanel {
	private JPanel pnNorth;
	private JPanel pnCentral;
	private JLabel lblModificarEstadoTrabajadores;
	private JPanel panel;
	private JPanel panel_1;
	private JScrollPane scrollPane;
	private JList listaTrabajadores;
	private JLabel lblModificarEstado;
	private JComboBox cBEstados;
	private JButton btnModificarEstado;
	private JPanel panel_3;
	private JPanel panel_4;
	private JScrollPane spmodiciaciones;
	private JList listaTurnos;
	private Connection conn;

	private List<Trabajador> trabajadores = new ArrayList<Trabajador>();
	
	/**
	 * Create the panel.
	 * @param con 
	 */
	public CardModificarEstadoTrabajador(Conexion con) {
		conn = con.getCon();
		setLayout(new BorderLayout(0, 0));
		add(getPnNorth(), BorderLayout.NORTH);
		add(getPnCentral(), BorderLayout.CENTER);
		
		rellenarTablaTurnosTrabajadores();

	}

	private void rellenarTablaTurnosTrabajadores() {
		PreparedStatement workers;
		try {
			workers = conn
					.prepareStatement("SELECT t.NOMBRE,tu.ID,tu.FECHA_INICIO,tu.FECHA_FIN,tu.TIPO "
							+ "FROM TRABAJADOR t,TURNO tu "
							+ "WHERE t.DNI = tu.DNI "
							+ "ORDER BY tu.ID");
			ResultSet rs = workers.executeQuery();
			DefaultListModel<String> modeloLista = new DefaultListModel<String>();
			while(rs.next())
			{
				Trabajador t = new Trabajador(rs.getString(1),rs.getString(2),rs.getDate(3),rs.getDate(4),rs.getString(5));
				trabajadores.add(t);
				modeloLista.addElement(t.toString());
			}
			getListaTrabajadores().setModel(modeloLista);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		
	}

	private JPanel getPnNorth() {
		if (pnNorth == null) {
			pnNorth = new JPanel();
			pnNorth.add(getLblModificarEstadoTrabajadores());
		}
		return pnNorth;
	}
	private JPanel getPnCentral() {
		if (pnCentral == null) {
			pnCentral = new JPanel();
			pnCentral.setLayout(new GridLayout(0, 1, 0, 0));
			pnCentral.add(getPanel());
			pnCentral.add(getPanel_1());
			pnCentral.add(getSpmodiciaciones());
		}
		return pnCentral;
	}
	private JLabel getLblModificarEstadoTrabajadores() {
		if (lblModificarEstadoTrabajadores == null) {
			lblModificarEstadoTrabajadores = new JLabel("Modificar estado trabajadores");
			lblModificarEstadoTrabajadores.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 20));
		}
		return lblModificarEstadoTrabajadores;
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setLayout(new GridLayout(0, 1, 0, 0));
			panel.add(getScrollPane());
		}
		return panel;
	}
	private JPanel getPanel_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setLayout(new BorderLayout(0, 0));
			panel_1.add(getPanel_3(), BorderLayout.NORTH);
			panel_1.add(getPanel_4(), BorderLayout.CENTER);
		}
		return panel_1;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getListaTrabajadores());
		}
		return scrollPane;
	}
	private JList getListaTrabajadores() {
		if (listaTrabajadores == null) {
			listaTrabajadores = new JList();
		}
		return listaTrabajadores;
	}
	private JLabel getLblModificarEstado() {
		if (lblModificarEstado == null) {
			lblModificarEstado = new JLabel("Modificar estado:");
		}
		return lblModificarEstado;
	}
	private JComboBox getCBEstados() {
		if (cBEstados == null) {
			cBEstados = new JComboBox();
			String[] estados = new String[]{"activo","baja laboral","vacaciones","permiso"};
			cBEstados.setModel(new DefaultComboBoxModel(estados));
			cBEstados.setSelectedIndex(0);
		}
		return cBEstados;
	}
	private JButton getBtnModificarEstado() {
		if (btnModificarEstado == null) {
			btnModificarEstado = new JButton("Modificar estado");
			btnModificarEstado.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					List workersmodify = getListaTrabajadores().getSelectedValuesList();
					List<Trabajador> mostrarT = new ArrayList<Trabajador>();
					String estado = getCBEstados().getSelectedItem().toString();
					for(int i = 0;i<workersmodify.size();i++)
					{
						String[] worker = ((String) workersmodify.get(i)).split(", ");
						String nombre = worker[0];
						String[] worker1 = worker[1].split(": ");
						String turnoid = worker1[1];
						Trabajador t = buscarTrabajador(nombre,turnoid);
						if(!estado.equals("activo"))
						{
							t.setEstado(estado);
							mostrarT.add(t);

						}
						//Actualizar tabla Turno
						try {
							PreparedStatement ps = conn.prepareStatement("UPDATE TURNO "
					+ "SET TIPO= ? "
					+ "WHERE ID = ?");
							ps.setString(1,estado);
							ps.setString(2, turnoid);
							ps.executeUpdate();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
					DefaultListModel<String> modeloLista = new DefaultListModel<String>();
					for(Trabajador t: mostrarT)
					{
						modeloLista.addElement(t.toString());
					}
						
					rellenarTablaTurnosTrabajadores();
					getListaTurnos().setModel(modeloLista);
					getSpmodiciaciones().setVisible(true);
					getCBEstados().setSelectedIndex(0);
					JOptionPane.showMessageDialog(null, "Estado modificado correctamente.");

				}

				
			});
		}
		return btnModificarEstado;
	}
	private Trabajador buscarTrabajador(String nombre, String turnoid) {
		Trabajador tr = null;
		for(Trabajador t:trabajadores)
		{
			if(t.getName().equals(nombre) && t.getTurnoid().equals(turnoid))
			{
				tr = t;
				break;
			}
		}
		return tr;
	}
	private JPanel getPanel_3() {
		if (panel_3 == null) {
			panel_3 = new JPanel();
			panel_3.add(getLblModificarEstado());
			panel_3.add(getCBEstados());
		}
		return panel_3;
	}
	private JPanel getPanel_4() {
		if (panel_4 == null) {
			panel_4 = new JPanel();
			panel_4.add(getBtnModificarEstado());
		}
		return panel_4;
	}
	private JScrollPane getSpmodiciaciones() {
		if (spmodiciaciones == null) {
			spmodiciaciones = new JScrollPane();
			spmodiciaciones.setViewportView(getListaTurnos());
			spmodiciaciones.setVisible(false);
		}
		return spmodiciaciones;
	}
	private JList getListaTurnos() {
		if (listaTurnos == null) {
			listaTurnos = new JList();
		}
		return listaTurnos;
	}
	public class Trabajador {
		private String name;
		private String turnoid;
		private Date fechainicio;
		private Date fechafinal;
		private String estado;
		
		public Trabajador(String name, String turnoid, Date fechainicio, Date fechafinal, String estado) {
			this.name = name;
			this.turnoid = turnoid;
			this.fechainicio = fechainicio;
			this.fechafinal = fechafinal;
			this.estado = estado;
		}

		public void setEstado(String estado) {
			this.estado = estado;
			
		}

		public String getName() {
			return name;
		}

		public String getTurnoid() {
			return turnoid;
		}

		public Date getFechainicio() {
			return fechainicio;
		}

		public Date getFechafinal() {
			return fechafinal;
		}
		public String getEstado()
		{
			return estado;
		}

		@Override
		public String toString() {
			return name + ", TURNO ID: " + turnoid + ", FECHA INICIO: " + fechainicio
					+ ", FECHA FINAL: " + fechafinal + ", estado: " + estado;
		}

	}
}
