package igu;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.border.TitledBorder;

import logica.Conexion;

import javax.swing.JButton;
import java.awt.CardLayout;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

@SuppressWarnings("serial")
public class VentanaAdministrador extends JFrame {

	private JPanel contentPane;
	private JPanel pnUsuario;
	private JLabel label;
	private JLabel lblComo;
	private JLabel lblRecepcionista;
	private JPanel pnAccBotones;
	private JPanel pnCards;
	private JPanel panelUnoCard;
	public CardLayout card = new CardLayout(0,0);
	private static Conexion con;
	private JButton btnConsultarEstad;
	private JPanel panelElegirOpcionesTrabajadores;
	private JLabel lblOperacionesTrabajadores;
	private JButton btnConsultarEstadoDe;
	private JButton btnAsignarTurnos;
	private JButton btnRegistrarTrabajador;
	private JButton btnModificarturnotrabajo;
	private JButton btnModificarEstadoTrabajador;
	private CardTrabajadores cardTrabajadores;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaAdministrador frame = new VentanaAdministrador();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public VentanaAdministrador() throws SQLException {
		con = new Conexion();
		con.establecerConexion();
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaAdministrador.class.getResource("/img/logo.jpg")));
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getPnUsuario());
		contentPane.add(getPnAccBotones());
		contentPane.add(getPanelCards());
		setLocationRelativeTo(null);
	}
	private JPanel getPnUsuario() {
		if (pnUsuario == null) {
			pnUsuario = new JPanel();
			pnUsuario.setBorder(new TitledBorder(null, "Usuario", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnUsuario.setForeground(Color.LIGHT_GRAY);
			pnUsuario.setBounds(10, 11, 212, 82);
			pnUsuario.setLayout(null);
			pnUsuario.add(getLabel());
			pnUsuario.add(getLblComo());
			pnUsuario.add(getLblRecepcionista());
		}
		return pnUsuario;
	}
	private JLabel getLabel() {
		if (label == null) {
			label = new JLabel("---");
			label.setBounds(22, 25, 168, 14);
		}
		return label;
	}
	private JLabel getLblComo() {
		if (lblComo == null) {
			lblComo = new JLabel("Como: ");
			lblComo.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblComo.setBounds(10, 60, 46, 14);
		}
		return lblComo;
	}
	private JLabel getLblRecepcionista() {
		if (lblRecepcionista == null) {
			lblRecepcionista = new JLabel("Gerente");
			lblRecepcionista.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblRecepcionista.setBounds(92, 57, 130, 23);
		}
		return lblRecepcionista;
	}
	private JPanel getPnAccBotones() {
		if (pnAccBotones == null) {
			pnAccBotones = new JPanel();
			pnAccBotones.setBounds(10, 95, 212, 542);
			pnAccBotones.setBorder(new TitledBorder(null, "Acciones", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnAccBotones.setLayout(new GridLayout(14, 1, 0, 0));
			pnAccBotones.add(getBtnConsultarEstad());
			pnAccBotones.add(getBtnRegistrarTrabajador());
			pnAccBotones.add(getBtnModificarEstadoTrabajador());

		}
		return pnAccBotones;
	}
	public JPanel getPanelCards() throws SQLException {
		if (pnCards == null) {
			pnCards = new JPanel();
			pnCards.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnCards.setBounds(229, 11, 754, 626);
			pnCards.setLayout(card);
			pnCards.add(getUnoCard(), "panelUno");
		
			card.show(pnCards, "Inicial");
			cardTrabajadores = new CardTrabajadores(con);
			pnCards.add(cardTrabajadores, "cardTrabajadores");
			pnCards.add(getPanelElegirOpcionesTrabajadores(), "panelElegirOpcionesTrabajadores");
			pnCards.add(new CardRegistrarTrabajador(con),"panelRegistrarTrabajador");
			pnCards.add(new CardModificarTurnosTrabajo(con,this),"panelModificarTurnoTrabajo");
			pnCards.add(new CardModificarEstadoTrabajador(con),"panelModificarEstadoTrabajador");

		}
		return pnCards;
	}

	private JPanel getUnoCard() {
		if (panelUnoCard == null) {
			panelUnoCard = new JPanel();
			panelUnoCard.setName("panelUno");
			panelUnoCard.setLayout(null);
		}
		return panelUnoCard;
	}

	void iniciar(){
		card.show(pnCards, "panelUno");
	}
	private JButton getBtnConsultarEstad() {
		if (btnConsultarEstad == null) {
			btnConsultarEstad = new JButton("Gestionar trabajadores");
			btnConsultarEstad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					card.show(pnCards, "panelElegirOpcionesTrabajadores");
					
				}
			});
		}
		return btnConsultarEstad;
	}
	private JPanel getPanelElegirOpcionesTrabajadores() {
		if (panelElegirOpcionesTrabajadores == null) {
			panelElegirOpcionesTrabajadores = new JPanel();
			panelElegirOpcionesTrabajadores.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Gesti\u00F3n de trabajadores", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			panelElegirOpcionesTrabajadores.setLayout(null);
			panelElegirOpcionesTrabajadores.add(getLblOperacionesTrabajadores());
			panelElegirOpcionesTrabajadores.add(getBtnConsultarEstadoDe());
			panelElegirOpcionesTrabajadores.add(getBtnAsignarTurnos());
			panelElegirOpcionesTrabajadores.add(getBtnModificarturnotrabajo());

		}
		return panelElegirOpcionesTrabajadores;
	}
	private JLabel getLblOperacionesTrabajadores() {
		if (lblOperacionesTrabajadores == null) {
			lblOperacionesTrabajadores = new JLabel("\u00BFQu\u00E9 operaci\u00F3n desea realizar?");
			lblOperacionesTrabajadores.setHorizontalAlignment(SwingConstants.CENTER);
			lblOperacionesTrabajadores.setFont(new Font("Dialog", Font.PLAIN, 25));
			lblOperacionesTrabajadores.setBounds(152, 96, 446, 68);
		}
		return lblOperacionesTrabajadores;
	}
	private JButton getBtnConsultarEstadoDe() {
		if (btnConsultarEstadoDe == null) {
			btnConsultarEstadoDe = new JButton("Consultar estado de los trabajadores");
			btnConsultarEstadoDe.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cardTrabajadores.mostrarConsultarEstado();
					card.show(pnCards, "cardTrabajadores");
				}
			});
			btnConsultarEstadoDe.setBounds(213, 176, 323, 42);
		}
		return btnConsultarEstadoDe;
	}
	private JButton getBtnAsignarTurnos() {
		if (btnAsignarTurnos == null) {
			btnAsignarTurnos = new JButton("Asignar turnos de trabajo");
			btnAsignarTurnos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cardTrabajadores.mostrarTurnos();
					card.show(pnCards, "cardTrabajadores");
				}
			});
			btnAsignarTurnos.setBounds(213, 230, 323, 42);
		}
		return btnAsignarTurnos;
	}

	private JButton getBtnRegistrarTrabajador() {
		if (btnRegistrarTrabajador == null) {
			btnRegistrarTrabajador = new JButton("Registrar trabajador");
			btnRegistrarTrabajador.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					card.show(pnCards, "panelRegistrarTrabajador");
				}
			});
		}
		return btnRegistrarTrabajador;
	}
	public CardLayout getCard()
	{
		return card;
	}
	
	;
	private JButton getBtnModificarturnotrabajo() {
		if (btnModificarturnotrabajo == null) {
			btnModificarturnotrabajo = new JButton("Modificar turno de trabajo");
			btnModificarturnotrabajo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					card.show(pnCards, "panelModificarTurnoTrabajo");

				}
			});
			btnModificarturnotrabajo.setBounds(213, 284, 323, 42);
			
		}
		return btnModificarturnotrabajo;
	}

	public void mostrarPanelOpciones() {
		card.show(pnCards, "Inicial");
		
	}
	private JButton getBtnModificarEstadoTrabajador() {
		if (btnModificarEstadoTrabajador == null) {
			btnModificarEstadoTrabajador = new JButton("Modificar estado trabajador");
			btnModificarEstadoTrabajador.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					card.show(pnCards, "panelModificarEstadoTrabajador");

				}
			});
		}
		return btnModificarEstadoTrabajador;
	}
	}

