package igu;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import logica.DataBase;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class VentanaPersonalLimpieza extends JFrame {
	
	private CardTareas tareas = new CardTareas(this);
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaPersonalLimpieza frame = new VentanaPersonalLimpieza();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private JPanel pnUsuario;
	private JPanel contentPane;
	private JLabel label;
	private JLabel lblComo;
	private JLabel lblRecepcionista;
	private JPanel pnAccBotones;
	private JPanel pnAccCards;
	private CardLayout c1;
	private JPanel cardLogo;
	private JButton btnTareas;

	/**
	 * Create the frame.
	 */
	public VentanaPersonalLimpieza() {
		setTitle("Ventana Recepcionista");
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaRecepcionista.class.getResource("/img/logo.jpg")));
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getPnUsuario());
		contentPane.add(getPnAccBotones());
		contentPane.add(getAccCards());
		setLocationRelativeTo(null);
		try {
			DataBase.Connect();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Base de datos no disponible.\nConsulte con un t�cnico.");
		}
	}
	
	private JPanel getPnUsuario() {
		if (pnUsuario == null) {
			pnUsuario = new JPanel();
			pnUsuario.setBorder(new TitledBorder(null, "Usuario", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnUsuario.setForeground(Color.LIGHT_GRAY);
			pnUsuario.setBounds(10, 11, 222, 74);
			pnUsuario.setLayout(null);
			pnUsuario.add(getLabel());
			pnUsuario.add(getLblComo());
			pnUsuario.add(getLblRecepcionista());
		}
		return pnUsuario;
	}
	private JLabel getLabel() {
		if (label == null) {
			label = new JLabel("---");
			label.setBounds(22, 25, 168, 14);
		}
		return label;
	}
	private JLabel getLblComo() {
		if (lblComo == null) {
			lblComo = new JLabel("Como: ");
			lblComo.setBounds(14, 46, 46, 14);
		}
		return lblComo;
	}
	private JLabel getLblRecepcionista() {
		if (lblRecepcionista == null) {
			lblRecepcionista = new JLabel("Limpieza");
			lblRecepcionista.setBounds(92, 46, 108, 14);
		}
		return lblRecepcionista;
	}
	private JPanel getPnAccBotones() {
		if (pnAccBotones == null) {
			pnAccBotones = new JPanel();
			pnAccBotones.setBounds(10, 95, 222, 565);
			pnAccBotones.setBorder(new TitledBorder(null, "Acciones", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnAccBotones.setLayout(null);
			pnAccBotones.add(getBtnTareas());
		}
		return pnAccBotones;
	}
	
	private JPanel getAccCards() {
		if (pnAccCards == null) {
			pnAccCards = new JPanel();
			pnAccCards.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnAccCards.setBounds(238, 11, 646, 649);
			pnAccCards.setLayout(new CardLayout(0, 0));
			pnAccCards.add(getCardLogo(), "Inicial");
			pnAccCards.add(tareas, "Tareas");
			c1 = (CardLayout)(pnAccCards.getLayout());
			c1.show(pnAccCards, "Inicial");
		}
		return pnAccCards;
	}
	

	private JPanel getCardLogo() {
		if (cardLogo == null) {
			cardLogo = new JPanel();
			cardLogo.setLayout(null);
		}
		return cardLogo;
	}
	private JButton getBtnTareas() {
		if (btnTareas == null) {
			btnTareas = new JButton("Tareas");
			btnTareas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					c1.show(pnAccCards, "Tareas");
					tareas.todasTareas();
				}
			});
			btnTareas.setBounds(10, 22, 202, 23);
		}
		return btnTareas;
	}

	public void iniciar() {
		c1.show(pnAccCards, "Inicial");
	}
}
