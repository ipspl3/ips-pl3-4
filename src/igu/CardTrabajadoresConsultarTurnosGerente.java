package igu;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import logica.Conexion;
import logica.Trabajadores;


import javax.swing.JLabel;

import java.awt.Font;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.DefaultListModel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;



import javax.swing.border.TitledBorder;


import java.awt.Color;


import javax.swing.JTextField;


import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JOptionPane;
import com.toedter.calendar.JDateChooser;
import javax.swing.JCheckBox;
import javax.swing.UIManager;
import javax.swing.JTextPane;
public class CardTrabajadoresConsultarTurnosGerente extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Connection con;
	private JPanel contentPane;
	private JScrollPane spTablaTurnos;
	private JTable tablaEstados;
	private String[] cabecera = {"TRABAJADOR", "ESTADO", "FECHA INICIO", "FECHA FIN", "HORA ENTRADA", "HORA SALIDA"};
	private JPanel panelEstadoTrabajadores;
	private JPanel panelTabla;
	private JPanel panelFiltros;
	private JLabel lblNombreEmpleado;
	private JTextField tfEmpleado;
	private JButton btnLupa;
	private JScrollPane scrollPaneListaEmpleados;
	private JList<String> listaEmpleadosConsultarEstado;
	private DefaultListModel<String> modeloLista = new DefaultListModel<String>();
	private JDateChooser dcFechaInicio;
	private JLabel lblFechaInicio;
	private JLabel lblFechaFin;
	private JDateChooser dcFechaFin;
	private ArrayList<String> empleadosSeleccionados = new ArrayList<String>();
	private Trabajadores trabajadores;
	private boolean primero = true;
	private JButton btnFiltrar;
	private JCheckBox chckbxRecepcin;
	private JPanel panelAreas;
	private JCheckBox chckbxLimpieza;
	private JCheckBox chckbxMantenimiento;
	private JCheckBox chckbxRestauracin;
	private MiModeloTabla modeloTabla;
	private JPanel panelFiltrarPorEmpleados;
	private JButton btnAtrs;
	private JTextPane txtpnSeleccioneEllosEmpleados;
	private VentanaGerentePrincipal padre;
	private JButton btnNuevaConsulta;
	
	
	
	private class MiModeloTabla extends DefaultTableModel{
		private static final long serialVersionUID = 1L;
		public MiModeloTabla(Object[] columnNames, int numFilas) {
			super(columnNames,numFilas);
		}
		
		public boolean isCellEditable(int numFila, int numColumna){
			return false;
		}
	}


	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public CardTrabajadoresConsultarTurnosGerente(Conexion conexion, VentanaGerentePrincipal padre) {
		this.con = conexion.getCon();
		this.padre = padre;
		setBounds(100, 100, 755, 626);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setLayout(new BorderLayout(0, 0));
		add(getPanelEstadoTrabajadores(), BorderLayout.CENTER);
		rellenarTablaTurnos();
		setVisible(true);
	}
	private JScrollPane getSpTablaTurnos() {
		if (spTablaTurnos == null) {
			spTablaTurnos = new JScrollPane();
			spTablaTurnos.setBounds(0, 0, 731, 301);
			spTablaTurnos.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
			spTablaTurnos.setViewportView(getTablaTurnos());
		}
		return spTablaTurnos;
	}
	private JTable getTablaTurnos() {
		if (tablaEstados == null) {
			tablaEstados = new JTable();
			tablaEstados.setBorder(null);
			tablaEstados.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tablaEstados;
	}
	
	private void rellenarTablaTurnos(){
		try {
			modeloTabla = new MiModeloTabla(cabecera, 0);
			
			Statement s = con.createStatement();
			
			ResultSet rs = s.executeQuery("select tr.nombre, t.tipo, t.fecha_inicio, "
					+ "t.fecha_fin, t.hora_entrada, t.hora_salida "
					+ "from trabajador tr, turno t "
					+ "where tr.dni = t.dni and tipo = 'activo' order by t.fecha_inicio desc");
	
			
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			
			while(rs.next()){
				Object[] fila = new Object[6];
				fila[0] = rs.getString("nombre");
				fila[1] = rs.getString("tipo");
				fila[2] = formato.format(rs.getDate("fecha_inicio"));
				fila[3] = formato.format(rs.getDate("fecha_fin"));
				fila[4] = rs.getString("hora_entrada");
				fila[5] = rs.getString("hora_salida");
				
				modeloTabla.addRow(fila);
			}
			
			tablaEstados.setModel(modeloTabla);
			modificarTablaTipos(tablaEstados);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private void modificarTablaTipos(JTable tabla){
		tabla.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 12));
		tabla.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tabla.setRowHeight(25);
		
		tabla.getTableHeader().setReorderingAllowed(false);
		
		TableColumn columna;
		for(int i=0; i<6; i++){
			columna = tabla.getColumn(cabecera[i]);
			if(i==0){
				columna.setMinWidth(175);
				columna.setMaxWidth(175);
			}
			if(i== 2 || i==4 || i==5){
				columna.setMinWidth(103);
				columna.setMaxWidth(103);
			}
			if(i==3){
				columna.setMinWidth(90);
				columna.setMaxWidth(90);
			}
			columna.setResizable(false);
		}
		
		
	}

	
	private JPanel getPanelEstadoTrabajadores() {
		if (panelEstadoTrabajadores == null) {
			panelEstadoTrabajadores = new JPanel();
			panelEstadoTrabajadores.setLayout(null);
			panelEstadoTrabajadores.add(getPanelTabla());
			panelEstadoTrabajadores.add(getPanelFiltros());
			panelEstadoTrabajadores.add(getBtnAtrs());
			panelEstadoTrabajadores.add(getBtnNuevaConsulta());
		}
		return panelEstadoTrabajadores;
	}
	private JPanel getPanelTabla() {
		if (panelTabla == null) {
			panelTabla = new JPanel();
			panelTabla.setBounds(10, 284, 731, 301);
			panelTabla.setLayout(null);
			panelTabla.add(getSpTablaTurnos());
		}
		return panelTabla;
	}
	
	private JPanel getPanelFiltros() {
		if (panelFiltros == null) {
			panelFiltros = new JPanel();
			panelFiltros.setBorder(new TitledBorder(null, "Consultar estado de los trabajadores", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelFiltros.setBounds(10, 6, 731, 266);
			panelFiltros.setName("panelNuevoTipo");
			panelFiltros.setLayout(null);
			panelFiltros.add(getDcFechaInicio());
			panelFiltros.add(getLblFechaInicio());
			panelFiltros.add(getLblFechaFin());
			panelFiltros.add(getDcFechaFin());
			panelFiltros.add(getBtnFiltrar());
			panelFiltros.add(getPanelAreas());
			panelFiltros.add(getPanelFiltrarPorEmpleados());
		}
		return panelFiltros;
	}
	

	private JLabel getLblNombreEmpleado() {
		if (lblNombreEmpleado == null) {
			lblNombreEmpleado = new JLabel("Nombre empleado:");
			lblNombreEmpleado.setBounds(22, 16, 143, 26);
			lblNombreEmpleado.setFont(new Font("Tahoma", Font.PLAIN, 13));
		}
		return lblNombreEmpleado;
	}
	private JTextField getTfEmpleado() {
		if (tfEmpleado == null) {
			tfEmpleado = new JTextField();
			tfEmpleado.setBounds(20, 41, 165, 32);
			tfEmpleado.setFont(new Font("Tahoma", Font.PLAIN, 12));
			tfEmpleado.setColumns(10);
		}
		return tfEmpleado;
	}
	
	private JButton getBtnLupa() {
		if (btnLupa == null) {
			btnLupa = new JButton("");
			btnLupa.setBounds(197, 41, 32, 32);
			btnLupa.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(tfEmpleado.getText()!= null){
						try{
							String trabajador = trabajadores.encontrarTrabajador(tfEmpleado.getText());

							int pos = 0;
							for(int i=0; i<listaEmpleadosConsultarEstado.getModel().getSize(); i++) {
								String empleado = listaEmpleadosConsultarEstado.getModel().getElementAt(i);
								if(empleado.equals(trabajador)) {
									pos= i;
									break;
								}
							}
							
							listaEmpleadosConsultarEstado.addSelectionInterval(pos, pos);
						}
						catch(Exception exc){
							exc.printStackTrace();
						}
					}
				}
			});
			btnLupa.setIcon(new ImageIcon(CardTrabajadoresConsultarTurnosGerente.class.getResource("/img/lupa3.png")));
		}
		return btnLupa;
	}
	
	
	private JScrollPane getScrollPaneListaEmpleados() {
		if (scrollPaneListaEmpleados == null) {
			scrollPaneListaEmpleados = new JScrollPane();
			scrollPaneListaEmpleados.setBounds(20, 115, 218, 104);
			scrollPaneListaEmpleados.setViewportView(getListaEmpleados());
		}
		return scrollPaneListaEmpleados;
	}
	private JList<String> getListaEmpleados() {
		if (listaEmpleadosConsultarEstado == null) {
			listaEmpleadosConsultarEstado = new JList<String>();
			listaEmpleadosConsultarEstado.setFont(new Font("Tahoma", Font.PLAIN, 13));
			listaEmpleadosConsultarEstado.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					int indices[] = listaEmpleadosConsultarEstado.getSelectedIndices();
					empleadosSeleccionados = new ArrayList<String>();

					for(int i=0; i<indices.length; i++){
						empleadosSeleccionados.add(modeloLista.get(indices[i]));
					}
//					System.out.println("Numero de seleccionados: " +indices.length);
//					System.out.println("El empleado seleccionado es: " +  empleadosSeleccionados.get(0));
					if(indices.length==1)
						tfEmpleado.setText(empleadosSeleccionados.get(0));
					else
						tfEmpleado.setText(null);
					
				}
			});
			iniciarLista(listaEmpleadosConsultarEstado);
		}
		return listaEmpleadosConsultarEstado;
	}
	
	private void iniciarLista(JList<String> lista){
		try{
			
			modeloLista = new DefaultListModel<String>();
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("select distinct(nombre), dni from trabajador order by nombre asc");
			if(trabajadores== null)
				trabajadores = new Trabajadores();
			while(rs.next()){
				String nombre = rs.getString("NOMBRE");
				String dni = rs.getString("DNI");
				modeloLista.addElement(nombre);
				if(primero)
					trabajadores.añadirTrabajador(nombre, dni);
			}
			lista.setModel(modeloLista);
			primero = false;
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	private JDateChooser getDcFechaInicio() {
		if (dcFechaInicio == null) {
			dcFechaInicio = new JDateChooser();
			dcFechaInicio.setFont(new Font("Tahoma", Font.PLAIN, 11));
			dcFechaInicio.getCalendarButton().setFont(new Font("Tahoma", Font.PLAIN, 12));
			dcFechaInicio.setBounds(284, 142, 155, 32);
			dcFechaInicio.setVisible(true);
			//dcFechaInicio.setMinSelectableDate(new Date());
		}
		return dcFechaInicio;
	}
	private JLabel getLblFechaInicio() {
		if (lblFechaInicio == null) {
			lblFechaInicio = new JLabel("Fecha inicio:");
			lblFechaInicio.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblFechaInicio.setBounds(284, 116, 93, 26);
		}
		return lblFechaInicio;
	}
	private JLabel getLblFechaFin() {
		if (lblFechaFin == null) {
			lblFechaFin = new JLabel("Fecha fin:");
			lblFechaFin.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblFechaFin.setBounds(471, 116, 104, 26);
		}
		return lblFechaFin;
	}
	private JDateChooser getDcFechaFin() {
		if (dcFechaFin == null) {
			dcFechaFin = new JDateChooser();
			dcFechaFin.getCalendarButton().setFont(new Font("Tahoma", Font.PLAIN, 12));
			dcFechaFin.setFont(new Font("Tahoma", Font.PLAIN, 11));
			dcFechaFin.setBounds(471, 142, 155, 32);
			dcFechaFin.setVisible(true);
			//dcFechaFin.setMinSelectableDate(new Date());
			
		}
		return dcFechaFin;
	}
	
	
	
	private boolean comprobarFechas(JDateChooser inicio, JDateChooser fin){
		try{
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			Date fecha_inicio = formato.parse(formato.format(inicio.getDate()));
			Date fecha_fin = formato.parse(formato.format(fin.getDate()));
			
			if(fecha_inicio.compareTo(fecha_fin) > 0){
				JOptionPane.showMessageDialog(null, "La fecha de fin debe de ser mayor que la fecha de inicio");
				return false;
			}
			return true;
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "Por favor, inserte un rango de fechas", "Faltan fechas", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}


	private JButton getBtnFiltrar() {
		if (btnFiltrar == null) {
			btnFiltrar = new JButton("Filtrar");
			btnFiltrar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(comprobarFechas(dcFechaInicio, dcFechaFin)){
						if(hayAlgunCheckActivado()){
							if(!noHaySeleccionesEnLaLista())
								listaEmpleadosConsultarEstado.clearSelection();
							filtrarPorChecks();
						}
						else if(!noHaySeleccionesEnLaLista()){
							filtrarPorLista();
						}
						else
							JOptionPane.showMessageDialog(null, "No se ha aplicado ningún criterio de filtrado", "Error", JOptionPane.ERROR_MESSAGE);
					}
					
				}
			});
			btnFiltrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnFiltrar.setBounds(616, 205, 93, 35);
		}
		return btnFiltrar;
	}
	
	private void filtrarPorLista(){
		try{
			modeloTabla = new MiModeloTabla(cabecera, 0);
			StringBuilder sb = new StringBuilder();
			PreparedStatement ps;
			
			for(int i=0;i<empleadosSeleccionados.size(); i++){
				sb = new StringBuilder();
				sb.append("select tr.nombre, t.tipo, t.fecha_inicio, t.fecha_fin, t.hora_entrada, t.hora_salida ");
				sb.append("from turno t natural join trabajador tr ");
				sb.append("where to_date(?, 'dd-MM-YYYY') <= t.fecha_fin and ");
				sb.append("t.fecha_inicio <= to_date(?,'dd-MM-YYYY') and tr.nombre = ? and tipo = 'activo'");
				ps = con.prepareStatement(sb.toString());
				ps.setDate(1, new java.sql.Date(dcFechaInicio.getDate().getTime()));
				ps.setDate(2, new java.sql.Date(dcFechaFin.getDate().getTime()));
				ps.setString(3, empleadosSeleccionados.get(i));
				ResultSet rs = ps.executeQuery();
				rellenarTabla(rs);	
			}
			tablaEstados.setModel(modeloTabla);
			modificarTablaTipos(tablaEstados);
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void filtrarPorChecks(){
		try{
			
			modeloTabla = new MiModeloTabla(cabecera, 0);
			if(chckbxLimpieza.isSelected()){
				filtroCheck("limpieza");
			}
			if(chckbxRecepcin.isSelected()){
				filtroCheck("recepción");
			}
			if(chckbxMantenimiento.isSelected()){
				filtroCheck("mantenimiento");
			}
			if(chckbxRestauracin.isSelected()){
				filtroCheck("restauración");
			}
		}
		catch(Exception e){
			
		}
	}
	
	private boolean noHaySeleccionesEnLaLista(){
		return listaEmpleadosConsultarEstado.isSelectionEmpty();
	}
	
	private void rellenarTabla(ResultSet rs){
		try{
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			while(rs.next()){
				Object[] fila = new Object[6];
				fila[0] = rs.getString("nombre");
				fila[1] = rs.getString("tipo");
				fila[2] = formato.format(rs.getDate("fecha_inicio"));
				fila[3] = formato.format(rs.getDate("fecha_fin"));
				fila[4] = rs.getString("hora_entrada");
				fila[5] = rs.getString("hora_salida");
				
				modeloTabla.addRow(fila);
			}
			
		}
		catch(Exception e){
			
		}
	}
	
	private void filtroCheck(String area){
		try{
			StringBuilder sb = new StringBuilder();
			
			sb.append("select tr.nombre, t.tipo, t.fecha_inicio, t.fecha_fin, t.hora_entrada, t.hora_salida ");
			sb.append("from trabajador tr natural join turno t ");
			sb.append("where tr.area=? and to_date(?, 'dd-MM-YYYY') <= t.fecha_fin and ");
			sb.append("t.fecha_inicio <= to_date(?,'dd-MM-YYYY') and tipo= 'activo'");
			
			PreparedStatement ps = con.prepareStatement(sb.toString());
			ps.setString(1, area);
			ps.setDate(2, new java.sql.Date(dcFechaInicio.getDate().getTime()));	
			ps.setDate(3, new java.sql.Date(dcFechaFin.getDate().getTime()));
			ResultSet rs = ps.executeQuery();
			rellenarTabla(rs);	
			tablaEstados.setModel(modeloTabla);
			modificarTablaTipos(tablaEstados);
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private boolean hayAlgunCheckActivado(){
		return chckbxLimpieza.isSelected() || chckbxMantenimiento.isSelected() || chckbxRecepcin.isSelected() || chckbxRestauracin.isSelected();
	}
	private JCheckBox getChckbxRecepcin() {
		if (chckbxRecepcin == null) {
			chckbxRecepcin = new JCheckBox("Recepci\u00F3n");
			chckbxRecepcin.setBounds(9, 25, 95, 35);
			chckbxRecepcin.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return chckbxRecepcin;
	}
	private JPanel getPanelAreas() {
		if (panelAreas == null) {
			panelAreas = new JPanel();
			panelAreas.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Filtrar por \u00E1reas", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			panelAreas.setFont(new Font("Tahoma", Font.PLAIN, 12));
			panelAreas.setBounds(274, 27, 435, 69);
			panelAreas.setLayout(null);
			panelAreas.add(getChckbxRecepcin());
			panelAreas.add(getChckbxLimpieza());
			panelAreas.add(getChckbxMantenimiento());
			panelAreas.add(getChckbxRestauracin());
		}
		return panelAreas;
	}
	private JCheckBox getChckbxLimpieza() {
		if (chckbxLimpieza == null) {
			chckbxLimpieza = new JCheckBox("Limpieza");
			chckbxLimpieza.setBounds(110, 25, 83, 35);
			chckbxLimpieza.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return chckbxLimpieza;
	}
	private JCheckBox getChckbxMantenimiento() {
		if (chckbxMantenimiento == null) {
			chckbxMantenimiento = new JCheckBox("Mantenimiento");
			chckbxMantenimiento.setBounds(200, 25, 119, 35);
			chckbxMantenimiento.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return chckbxMantenimiento;
	}
	private JCheckBox getChckbxRestauracin() {
		if (chckbxRestauracin == null) {
			chckbxRestauracin = new JCheckBox("Restauraci\u00F3n");
			chckbxRestauracin.setBounds(323, 25, 107, 35);
			chckbxRestauracin.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return chckbxRestauracin;
	}
	private JPanel getPanelFiltrarPorEmpleados() {
		if (panelFiltrarPorEmpleados == null) {
			panelFiltrarPorEmpleados = new JPanel();
			panelFiltrarPorEmpleados.setBorder(new TitledBorder(null, "Filtrar por empleados", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelFiltrarPorEmpleados.setBounds(6, 27, 260, 229);
			panelFiltrarPorEmpleados.setLayout(null);
			panelFiltrarPorEmpleados.add(getLblNombreEmpleado());
			panelFiltrarPorEmpleados.add(getTfEmpleado());
			panelFiltrarPorEmpleados.add(getBtnLupa());
			panelFiltrarPorEmpleados.add(getScrollPaneListaEmpleados());
			panelFiltrarPorEmpleados.add(getTxtpnSeleccioneEllosEmpleados());
		}
		return panelFiltrarPorEmpleados;
	}
	
	private JButton getBtnAtrs() {
		if (btnAtrs == null) {
			btnAtrs = new JButton("Atr\u00E1s");
			btnAtrs.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					limpiarDatos();
					padre.mostrarPanelInicial();
				}
			});
			btnAtrs.setBounds(520, 591, 77, 29);
		}
		return btnAtrs;
	}
	private JTextPane getTxtpnSeleccioneEllosEmpleados() {
		if (txtpnSeleccioneEllosEmpleados == null) {
			txtpnSeleccioneEllosEmpleados = new JTextPane();
			txtpnSeleccioneEllosEmpleados.setOpaque(false);
			txtpnSeleccioneEllosEmpleados.setEditable(false);
			txtpnSeleccioneEllosEmpleados.setText("Seleccione el/los empleados:");
			txtpnSeleccioneEllosEmpleados.setBounds(22, 95, 218, 16);
		}
		return txtpnSeleccioneEllosEmpleados;
	}
	private void limpiarDatos() {
		iniciarLista(listaEmpleadosConsultarEstado);
		chckbxLimpieza.setSelected(false);
		chckbxMantenimiento.setSelected(false);
		chckbxRecepcin.setSelected(false);
		chckbxRestauracin.setSelected(false);
		dcFechaInicio.setDate(null);
		dcFechaFin.setDate(null);
		rellenarTablaTurnos();
	}
	private JButton getBtnNuevaConsulta() {
		if (btnNuevaConsulta == null) {
			btnNuevaConsulta = new JButton("Nueva consulta");
			btnNuevaConsulta.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					limpiarDatos();
				}
			});
			btnNuevaConsulta.setBounds(609, 591, 132, 29);
		}
		return btnNuevaConsulta;
	}
}



