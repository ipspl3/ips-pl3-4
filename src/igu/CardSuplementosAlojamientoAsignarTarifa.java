package igu;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import logica.Conexion;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import java.util.Date;

import java.awt.Color;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.border.EtchedBorder;

public class CardSuplementosAlojamientoAsignarTarifa extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Conexion con;
	private JPanel contentPane;
	private JScrollPane spTablaSuplementos;
	private JTable tablaSuplementos;
	private String[] cabecera = {"SUPLEMENTO", "TIPO DE HABITACIÓN", "IMPORTE POR DÍA", "FECHA DE ALTA", "FECHA DE BAJA"};
	private JPanel panelCentral;
	private JPanel panelTabla;
	private JButton btnAsignar;
	private JPanel panelAsignarSuplemento;
	private JLabel lblSuplemento;
	private JLabel lblImporte;
	private JTextField tfImporteAsignarTarifa;
	private JDateChooser dcFechaInicioAsignarTarifa;
	private JDateChooser dcFechaFinAsignarTarifa;
	private JLabel lblFechaInicio;
	private JLabel lblFechaFin;
	private boolean enModificacion;
	private JLabel lblTipoDeHabitacin;
	private JComboBox<String> cbTipoHab;
	private String tipoSeleccionado;
	private int id_supl;
	private JComboBox<String> cbSuplemento;
	private String suplementoSeleccionadoCombo;
	private JLabel lblEstosSonLas;

	
	public class MiModeloTabla extends DefaultTableModel{
		private static final long serialVersionUID = 1L;
		public MiModeloTabla(Object[] columnNames, int numFilas) {
			super(columnNames,numFilas);
		}
		
		public boolean isCellEditable(int numFila, int numColumna){
			return false;
		}
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public CardSuplementosAlojamientoAsignarTarifa(Conexion conexion) throws SQLException {
		this.con = conexion;
		setBounds(100, 100,  755, 626);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setLayout(new BorderLayout(0, 0));
		add(getPanelCentral(), BorderLayout.CENTER);
		rellenarTablaSuplementos();
		setVisible(true);
	}
	private JScrollPane getSpTablaTarifasTipo() {
		if (spTablaSuplementos == null) {
			spTablaSuplementos = new JScrollPane();
			spTablaSuplementos.setBounds(0, 0, 718, 276);
			spTablaSuplementos.setBorder(new LineBorder(new Color(0, 0, 0), 2));
			spTablaSuplementos.setViewportView(getTablaTarifasTipo());
		}
		return spTablaSuplementos;
	}
	private JTable getTablaTarifasTipo() {
		if (tablaSuplementos == null) {
			tablaSuplementos = new JTable();
			tablaSuplementos.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					if(enModificacion){
						int opcion = JOptionPane.showConfirmDialog(null, "Atención está modificando una tarifa, ¿desea abandonar la operación?\n"
								+ "Los cambios no serán guardados", "Abandonar modificación.", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
						if(opcion==0){
							tablaSuplementos.setEnabled(true);
							suplementoSeleccionadoCombo = (String) tablaSuplementos.getValueAt(tablaSuplementos.getSelectedRow(), 0);
							enModificacion = false;
						}
					
					}
					else{
						suplementoSeleccionadoCombo = (String) tablaSuplementos.getValueAt(tablaSuplementos.getSelectedRow(), 0);
					}
				}
			});
			tablaSuplementos.setBorder(null);
			tablaSuplementos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		rellenarTablaSuplementos();
		return tablaSuplementos;
	}
	
	public void rellenarTablaSuplementos(){
		try {
			MiModeloTabla miModelo = new MiModeloTabla(cabecera, 0);
			SimpleDateFormat formato = new SimpleDateFormat("dd-MMM-yyyy");//Formatea el tipo Date en java
			
			String consulta = "select s.nombre_suplemento, s.nombre, ps.importe, ps.fecha_inicio, ps.fecha_fin "
					+ "from suplemento_modalidad s, precio_suplemento ps "
					+ "where s.id_supl_mod = ps.id_supl_mod order by ps.fecha_fin desc";
			PreparedStatement ps = con.getCon().prepareStatement(consulta);
			ResultSet rs = ps.executeQuery();
				
			while(rs.next()){
				Object[] fila = new Object[5];
				fila[0] = rs.getString("NOMBRE_SUPLEMENTO");
				fila[1] = rs.getString("NOMBRE");
				fila[2] = rs.getFloat("IMPORTE");
				fila[3] = formato.format(rs.getDate("FECHA_INICIO"));
				fila[4] = formato.format(rs.getDate("FECHA_FIN"));
				
				miModelo.addRow(fila);
			}
			
			tablaSuplementos.setModel(miModelo);
			modificarTablaSuplementos(tablaSuplementos);
			
			rs.close();
			ps.close();
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	private void modificarTablaSuplementos(JTable tabla){
		tabla.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 12));
		tabla.setFont(new Font("Tahoma", Font.PLAIN, 12));
		TableColumn columna = tabla.getColumn(cabecera[0]);	
		columna.setMinWidth(150);
		columna.setMaxWidth(150);
		tabla.setRowHeight(25);
	}
	private JPanel getPanelCentral() {
		if (panelCentral == null) {
			panelCentral = new JPanel();
			panelCentral.setBorder(new TitledBorder(null, "Asignar tarifa a un suplemento por modalidad de alojamiento", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelCentral.setLayout(null);
			panelCentral.add(getPanelTabla());
			panelCentral.add(getPanelNuevaTarifa());
			panelCentral.add(getLblEstosSonLas());
		}
		return panelCentral;
	}
	private JPanel getPanelTabla() {
		if (panelTabla == null) {
			panelTabla = new JPanel();
			panelTabla.setBounds(20, 333, 718, 276);
			panelTabla.setLayout(null);
			panelTabla.add(getSpTablaTarifasTipo());
		}
		return panelTabla;
	}
	private JButton getBtnAsignar() {
		if (btnAsignar == null) {
			btnAsignar = new JButton("Asignar");
			btnAsignar.setBounds(625, 219, 87, 35);
			btnAsignar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(tipoSeleccionado != null && suplementoSeleccionadoCombo!= null){
						if(comprobarFechas(dcFechaInicioAsignarTarifa, dcFechaFinAsignarTarifa) && comprobarImporte(tfImporteAsignarTarifa)){
							tfImporteAsignarTarifa.setBorder(new LineBorder(Color.GREEN));	
							
							float importeAsignacion = Float.parseFloat(tfImporteAsignarTarifa.getText());	
							
							if(!existeTarifa(suplementoSeleccionadoCombo,tipoSeleccionado, dcFechaInicioAsignarTarifa, dcFechaFinAsignarTarifa)){
								int opcionDeConfirmacion = JOptionPane.showConfirmDialog(null, "¿Desea confirmar los cambios?", "Confirmar cambios", JOptionPane.YES_NO_OPTION);
								if(opcionDeConfirmacion == 0){//YES
									getIdSuplemento(suplementoSeleccionadoCombo, tipoSeleccionado);
									if(insertarTarifa(importeAsignacion, dcFechaInicioAsignarTarifa, dcFechaFinAsignarTarifa)) {
										rellenarTablaSuplementos();
										tablaSuplementos.repaint();
										resetCampos();
										spTablaSuplementos.setBorder(new LineBorder(Color.GREEN, 3));
										JOptionPane.showMessageDialog(null, "Tarifa insertada correctamente", "OK", JOptionPane.INFORMATION_MESSAGE);
									}
									else {
										JOptionPane.showMessageDialog(null, "Ese nombre de suplemento no está asociado a ese tipo\n"
												+ "de habitación.Vaya a \"Crear suplemento\"", "Error", JOptionPane.ERROR_MESSAGE);
									}	
								}
								//OPCION NO
							}
							else{
								spTablaSuplementos.setBorder(new LineBorder(Color.RED, 3));
								JOptionPane.showMessageDialog(null, "No pueden existir dos nombres de modalidades de alojamiento iguales para \n"
										+ "un mismo tipo de habitación cuyas fechas sean conflictivas.", "Error", JOptionPane.ERROR_MESSAGE);
							}		
						}
						//No hace falta else ya lanzan los JOption los metodos
					}
					else{
						JOptionPane.showMessageDialog(null, "Faltan datos", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			});
			btnAsignar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return btnAsignar;
	}
	private JPanel getPanelNuevaTarifa() {
		if (panelAsignarSuplemento == null) {
			panelAsignarSuplemento = new JPanel();
			panelAsignarSuplemento.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panelAsignarSuplemento.setBounds(20, 25, 718, 260);
			panelAsignarSuplemento.setName("panelNuevoTipo");
			panelAsignarSuplemento.setLayout(null);
			panelAsignarSuplemento.add(getLblNuevaTarifa());
			panelAsignarSuplemento.add(getLblImporte());
			panelAsignarSuplemento.add(getTxfImporte());
			panelAsignarSuplemento.add(getDateChFechaInicio());
			panelAsignarSuplemento.add(getLblFechaInicio());
			panelAsignarSuplemento.add(getDateChFechaFin());
			panelAsignarSuplemento.add(getLblFechaFin());
			panelAsignarSuplemento.add(getLblTipoDeHabitacin());
			panelAsignarSuplemento.add(getCbTipoHab());
			panelAsignarSuplemento.add(getCbSuplemento());
			panelAsignarSuplemento.add(getBtnAsignar());
		}
		return panelAsignarSuplemento;
	}
	private JLabel getLblNuevaTarifa() {
		if (lblSuplemento == null) {
			lblSuplemento = new JLabel("Suplemento:");
			lblSuplemento.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblSuplemento.setBounds(312, 33, 109, 29);
		}
		return lblSuplemento;
	}
	private JLabel getLblImporte() {
		if (lblImporte == null) {
			lblImporte = new JLabel("Importe:");
			lblImporte.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblImporte.setBounds(542, 34, 127, 26);
		}
		return lblImporte;
	}
	private JTextField getTxfImporte() {
		if (tfImporteAsignarTarifa == null) {
			tfImporteAsignarTarifa = new JTextField();
			tfImporteAsignarTarifa.setBounds(542, 65, 109, 32);
			tfImporteAsignarTarifa.setColumns(10);
		}
		return tfImporteAsignarTarifa;
	}
	private JDateChooser getDateChFechaInicio() {
		if (dcFechaInicioAsignarTarifa == null) {
			dcFechaInicioAsignarTarifa = new JDateChooser();
			dcFechaInicioAsignarTarifa.getCalendarButton().setFont(new Font("Tahoma", Font.PLAIN, 12));
			dcFechaInicioAsignarTarifa.setBounds(75, 144, 155, 32);
			dcFechaInicioAsignarTarifa.setMinSelectableDate(new Date());
		}
		return dcFechaInicioAsignarTarifa;
	}
	private JLabel getLblFechaInicio() {
		if (lblFechaInicio == null) {
			lblFechaInicio = new JLabel("Fecha inicio:");
			lblFechaInicio.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblFechaInicio.setBounds(75, 118, 155, 26);
		}
		return lblFechaInicio;
	}
	private JDateChooser getDateChFechaFin() {
		if (dcFechaFinAsignarTarifa == null) {
			dcFechaFinAsignarTarifa = new JDateChooser();
			dcFechaFinAsignarTarifa.getCalendarButton().setFont(new Font("Tahoma", Font.PLAIN, 12));
			dcFechaFinAsignarTarifa.setBounds(312, 144, 155, 32);
			dcFechaFinAsignarTarifa.setMinSelectableDate(new Date());
		}
		return dcFechaFinAsignarTarifa;
	}
	private JLabel getLblFechaFin() {
		if (lblFechaFin == null) {
			lblFechaFin = new JLabel("Fecha fin:");
			lblFechaFin.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblFechaFin.setBounds(312, 118, 135, 26);
		}
		return lblFechaFin;
	}
	
	private boolean existeTarifa(String nombreSupl, String nombreTipo, JDateChooser fi, JDateChooser ff){		
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
			String fecha_ini= sdf.format(new java.sql.Date(fi.getDate().getTime()));
			String fecha_fin= sdf.format(new java.sql.Date(ff.getDate().getTime()));
					
			String consulta = "select count(s.nombre_suplemento) "
					+ "from suplemento_modalidad s, precio_suplemento ps "
					+ "where ((to_date(?,'dd-MM-YYYY') between ps.fecha_inicio  "
					+ "and ps.fecha_fin) or (to_date(?,'dd-MM-YYYY') between ps.fecha_inicio "
					+ "and ps.fecha_fin)) and s.nombre_suplemento= ? and "
					+ "ps.id_supl_mod = s.id_supl_mod and s.nombre = ?";
			PreparedStatement ps = con.getCon().prepareStatement(consulta);
			
			ps.setString(1, fecha_ini);
			ps.setString(2, fecha_fin);
			ps.setString(3, nombreSupl);
			ps.setString(4, nombreTipo);
			ResultSet rs = ps.executeQuery();
			
			rs.next();
			int existeSuplemento = rs.getInt("COUNT(S.NOMBRE_SUPLEMENTO)");
			rs.close();
			ps.close();
			return existeSuplemento>0;
		}
		catch(SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private int getIdSuplemento(String nombreSuplemento, String nombreTipo){
		try {
			
			String consulta = "select s.id_supl_mod "
					+ "from suplemento_modalidad s "
					+ "where s.nombre_suplemento =? and s.nombre=?";
			PreparedStatement ps = con.getCon().prepareStatement(consulta);
			
			ps.setString(1, nombreSuplemento);
			ps.setString(2, nombreTipo);
			ResultSet rs = ps.executeQuery();
			
			
			rs.next();
			int idSuplemento = rs.getInt("ID_SUPL_MOD");
			id_supl = idSuplemento;
			rs.close();
			ps.close();
			return idSuplemento;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	
	private boolean comprobarImporte(JTextField tfImporte){
		String importe = tfImporte.getText();
		try{
			Double.parseDouble(importe);
			return true;
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "Formato de importe incorrecto");
			tfImporte.setBorder(new LineBorder(Color.RED));
			return false;
		}
	}
	
	private boolean comprobarFechas(JDateChooser fechaInicio, JDateChooser fechaFin){
		try{
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			Date fecha_inicio = formato.parse(formato.format(fechaInicio.getDate()));
			Date fecha_fin = formato.parse(formato.format(fechaFin.getDate()));

			Date fecha_acc = formato.parse(formato.format(new Date()));
			
			if(fecha_inicio.compareTo(fecha_acc) >= 0){
				if(fecha_inicio.compareTo(fecha_fin) > 0){
					JOptionPane.showMessageDialog(null, "La fecha de fin debe de ser mayor que la fecha de inicio");
					return false;
				}
				else 
					return true;
			}
			else{
				JOptionPane.showMessageDialog(null, "La fecha de inicio debe ser mayor que la fecha actual del sistema");
				return false;
			}
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "Fechas incorrectas");
			return false;
		}
	}
	private JLabel getLblTipoDeHabitacin() {
		if (lblTipoDeHabitacin == null) {
			lblTipoDeHabitacin = new JLabel("Tipo de habitaci\u00F3n:");
			lblTipoDeHabitacin.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblTipoDeHabitacin.setBounds(75, 33, 135, 26);
		}
		return lblTipoDeHabitacin;
	}
	private JComboBox<String> getCbTipoHab() {
		if (cbTipoHab == null) {
			cbTipoHab = new JComboBox<String>();
			cbTipoHab.setFont(new Font("Tahoma", Font.PLAIN, 12));
			cbTipoHab.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					tipoSeleccionado = (String)cbTipoHab.getSelectedItem();
				}
			});
			cbTipoHab.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					String tipo = tipoSeleccionado;
					cbTipoHab.removeAllItems();
					rellenarTiposHab(cbTipoHab);
					if(tipo!= null)
						cbTipoHab.setSelectedItem(tipo);
					else
						cbTipoHab.setSelectedItem(null);
					cbTipoHab.repaint();
				}
			});
			cbTipoHab.setBounds(75, 64, 135, 32);
		}
		return cbTipoHab;
	}
	
	private void rellenarTiposHab(JComboBox<String> cb){
		try{
			Statement s = con.getCon().createStatement();	
			ResultSet rs = s.executeQuery("select t.nombre from tipo_habitacion t");
			
			while(rs.next()){
				cb.addItem(rs.getString("NOMBRE"));
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void rellenarComboSuplementos(){
		try{
			Statement s = con.getCon().createStatement();	
			ResultSet rs = s.executeQuery("select distinct(nombre_suplemento) from suplemento_modalidad");
			
			while(rs.next()){
				cbSuplemento.addItem(rs.getString("NOMBRE_SUPLEMENTO"));
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	private JComboBox<String> getCbSuplemento() {
		if (cbSuplemento == null) {
			cbSuplemento = new JComboBox<String>();
			cbSuplemento.setFont(new Font("Tahoma", Font.PLAIN, 12));
			cbSuplemento.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					suplementoSeleccionadoCombo = (String)cbSuplemento.getSelectedItem();
				}
			});
			cbSuplemento.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					String suplemento = suplementoSeleccionadoCombo;
					cbSuplemento.removeAllItems();
					rellenarComboSuplementos();
					if(suplemento!= null)
						cbSuplemento.setSelectedItem(suplemento);
					else
						cbSuplemento.setSelectedItem(null);
					cbSuplemento.repaint();
				}
			});
			cbSuplemento.setBounds(306, 66, 155, 32);
		}
		return cbSuplemento;
	}
	
	

	
	private boolean insertarTarifa(float nuevoImporte, JDateChooser fechaInicio, JDateChooser fechaFin){
		try{
			String consulta = "insert into precio_suplemento values(secuencia_preciosupl.nextVal,?,?,?,?,?)";
			
			PreparedStatement ps = con.getCon().prepareStatement(consulta);
			ps.setDate(1, new java.sql.Date(fechaInicio.getDate().getTime()));
			ps.setDate(2, new java.sql.Date(fechaFin.getDate().getTime()));
			ps.setFloat(3, nuevoImporte);
			ps.setInt(4, id_supl);
			ps.setString(5, tipoSeleccionado);	
			
			int actualizacion= ps.executeUpdate();
			
			ps.close();
			if(actualizacion >= 1) 
				return true;
			return false;		
		}
		catch(SQLException e){
			e.printStackTrace();
			return false;
		}
	}
	
	private void resetCampos(){
		tfImporteAsignarTarifa.setText(null);
		tfImporteAsignarTarifa.setBorder(new LineBorder(Color.BLACK));
		cbSuplemento.setSelectedItem(null);
		dcFechaInicioAsignarTarifa.setDate(null);
		dcFechaFinAsignarTarifa.setDate(null);
		id_supl = -1;
		tipoSeleccionado = null;
		suplementoSeleccionadoCombo = null;
	}
	
	private JLabel getLblEstosSonLas() {
		if (lblEstosSonLas == null) {
			lblEstosSonLas = new JLabel("Estos son las tarifas por suplementos de alojamiento existentes:");
			lblEstosSonLas.setBounds(20, 305, 446, 25);
		}
		return lblEstosSonLas;
	}
}
