package igu;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import logica.ComparadorFecha;
import logica.Conexion;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;

import java.awt.Dimension;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import com.toedter.calendar.JDateChooser;

public class CardRegistrarProducto extends JPanel {

	private JPanel contentPane;

	private JLabel lblRegistrarProducto;
	private JPanel pnCenter;
	private JPanel pn1;
	private JPanel pn3;
	private JPanel pn4;
	private JRadioButton rdbtnAlimentos;
	private JRadioButton rdbtnBebidas;
	private JRadioButton rdbtnSnacks;
	private JPanel pnSouth;
	private JLabel lblLocalizacion;
	private JComboBox cbLocalizacion;
	private JLabel lblPrecio;
	private JTextField txtPrecio;
	private JLabel lblNewLabel;
	private JLabel lblDescripcion;
	private JPanel pn5;
	private JTextField txtDescripcion;
	private JButton btnRegistrar;
	private JPanel pn2;
	private JLabel lblNombreProducto;
	private JTextField txtnombreproducto;
	private JTextArea textArea;
	private ButtonGroup productos = new ButtonGroup();
	private static Conexion conn;
	private JDateChooser dCfecha;

	/**
	 * Create the frame.
	 */
	public CardRegistrarProducto(Conexion con) {

		conn = con;

		setBounds(100, 100, 579, 380);
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(new BorderLayout(0, 0));
		add(getLblRegistrarProducto(), BorderLayout.NORTH);
		add(getPnCenter(), BorderLayout.CENTER);
		add(getPnSouth(), BorderLayout.SOUTH);


		setVisible(true);
	}

	private JLabel getLblRegistrarProducto() {
		if (lblRegistrarProducto == null) {
			lblRegistrarProducto = new JLabel("Registrar producto");
			lblRegistrarProducto.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		}
		return lblRegistrarProducto;
	}

	private JPanel getPnCenter() {
		if (pnCenter == null) {
			pnCenter = new JPanel();
			pnCenter.setLayout(new GridLayout(0, 1, 0, 0));
			pnCenter.add(getPn2());
			pnCenter.add(getPn1());
			pnCenter.add(getPn3());
			pnCenter.add(getPn4());
			pnCenter.add(getPn5());
		}
		return pnCenter;
	}

	private JPanel getPn1() {
		if (pn1 == null) {
			pn1 = new JPanel();
			pn1.add(getRdbtnAlimentos());
			pn1.add(getRdbtnBebidas());
			pn1.add(getRdbtnSnacks());
		}
		return pn1;
	}

	private JPanel getPn3() {
		if (pn3 == null) {
			pn3 = new JPanel();
			pn3.add(getLblLocalizacion());
			pn3.add(getCbLocalizacion());
			pn3.add(getLblPrecio());
			pn3.add(getTxtPrecio());
		}
		return pn3;
	}

	private JPanel getPn4() {
		if (pn4 == null) {
			pn4 = new JPanel();
			pn4.add(getLblNewLabel());
			pn4.add(getDCfecha());
		}
		return pn4;
	}

	private JRadioButton getRdbtnAlimentos() {
		if (rdbtnAlimentos == null) {
			rdbtnAlimentos = new JRadioButton("Alimentos");
			productos.add(rdbtnAlimentos);
			rdbtnAlimentos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (rdbtnAlimentos.isSelected()) {
						String[] alimentos = new String[] { "restaurante", "cafeteria" };
						getCbLocalizacion().setModel(new DefaultComboBoxModel(alimentos));
						getCbLocalizacion().setSelectedIndex(0);
					}
				}
			});
			rdbtnAlimentos.setSelected(true);
		}
		return rdbtnAlimentos;
	}

	private JRadioButton getRdbtnBebidas() {
		if (rdbtnBebidas == null) {
			rdbtnBebidas = new JRadioButton("Bebidas");
			rdbtnBebidas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (rdbtnBebidas.isSelected()) {
						String[] bebidas = new String[] { "restaurante", "cafeteria", "minibar" };
						getCbLocalizacion().setModel(new DefaultComboBoxModel(bebidas));
						getCbLocalizacion().setSelectedIndex(0);
					}
				}
			});
			productos.add(rdbtnBebidas);

		}
		return rdbtnBebidas;
	}

	private JRadioButton getRdbtnSnacks() {
		if (rdbtnSnacks == null) {
			rdbtnSnacks = new JRadioButton("Snacks");
			rdbtnSnacks.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (rdbtnSnacks.isSelected()) {
						String[] minibar = new String[] { "minibar" };
						getCbLocalizacion().setModel(new DefaultComboBoxModel(minibar));
						getCbLocalizacion().setSelectedIndex(0);
					}
				}
			});
			productos.add(rdbtnSnacks);

		}
		return rdbtnSnacks;
	}

	private JPanel getPnSouth() {
		if (pnSouth == null) {
			pnSouth = new JPanel();
			pnSouth.add(getBtnRegistrar());
		}
		return pnSouth;
	}

	private JLabel getLblLocalizacion() {
		if (lblLocalizacion == null) {
			lblLocalizacion = new JLabel("Localizacion:");
		}
		return lblLocalizacion;
	}

	private JComboBox getCbLocalizacion() {
		if (cbLocalizacion == null) {
			cbLocalizacion = new JComboBox();
			String[] alimentos = new String[] { "restaurante", "cafeteria" };
			cbLocalizacion.setModel(new DefaultComboBoxModel(alimentos));
			cbLocalizacion.setPreferredSize(new Dimension(140, 30));
			cbLocalizacion.setSelectedIndex(0);

		}
		return cbLocalizacion;
	}

	private JLabel getLblPrecio() {
		if (lblPrecio == null) {
			lblPrecio = new JLabel("Precio:");
		}
		return lblPrecio;
	}

	private JTextField getTxtPrecio() {
		if (txtPrecio == null) {
			txtPrecio = new JTextField();
			txtPrecio.setColumns(5);
		}
		return txtPrecio;
	}

	private JLabel getLblNewLabel() {
		if (lblNewLabel == null) {
			lblNewLabel = new JLabel("Fecha:");
		}
		return lblNewLabel;
	}

	private JLabel getLblDescripcion() {
		if (lblDescripcion == null) {
			lblDescripcion = new JLabel("Descripcion:");
		}
		return lblDescripcion;
	}

	private JPanel getPn5() {
		if (pn5 == null) {
			pn5 = new JPanel();
			pn5.add(getLblDescripcion());
			pn5.add(getTextArea());
			pn5.add(getTxtDescripcion());
		}
		return pn5;
	}

	private JTextField getTxtDescripcion() {
		if (txtDescripcion == null) {
			txtDescripcion = new JTextField();
			txtDescripcion.setColumns(25);
		}
		return txtDescripcion;
	}

	private JButton getBtnRegistrar() {
		if (btnRegistrar == null) {
			btnRegistrar = new JButton("Registrar");
			btnRegistrar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					if (getTxtDescripcion().getText().equals("") || getTxtnombreproducto().getText().equals("")
							|| getTxtPrecio().getText().equals("")) {
						JOptionPane.showMessageDialog(null, "Se han encontrado campos vaci�os.");

					}
					else
					{
						SimpleDateFormat Formato = new SimpleDateFormat("dd/MM/yyyy");
						double precio = Double.parseDouble(getTxtPrecio().getText());
						ComparadorFecha cf = new ComparadorFecha(Formato.format(getDCfecha().getDate()));


						if (precio <= 0) {
							JOptionPane.showMessageDialog(null, "Precio del producto erroneo.");
						} else if (!cf.CheckFechaActual()) {
							JOptionPane.showMessageDialog(null, "La fecha introcucida es inferior a la actual");
						} else {


							try {
								PreparedStatement comprobar = conn.getCon().prepareStatement("SELECT count(*) "
										+ "FROM PRODUCTOS_DISPONIBLES "
										+ "WHERE (TIPO = 'bebida' OR TIPO = 'alimento') "
										+ "AND FECHA =? AND PRODUCTO_NOMBRE =? AND LOCALIZACION=?");
								String nombreproducto = getTxtnombreproducto().getText().toString();
								String localizacion = getCbLocalizacion().getSelectedItem().toString();
								String descripcion = getTxtDescripcion().getText().toString();
								//COMPROBAR SI SE REPTIEN LA FECHA,NOMBRE Y LOCALIZACION
								java.sql.Date fecha = new java.sql.Date(Formato.parse(cf.getFechaInicio()).getTime());


								comprobar.setDate(1, (java.sql.Date) fecha);
								comprobar.setString(2, nombreproducto);
								comprobar.setString(3, localizacion);
								ResultSet rs = comprobar.executeQuery();
								int repeateddates = 0;
								while(rs.next())
								{
									repeateddates = rs.getInt(1);
								}
								comprobar.close();
								if(repeateddates>0)
								{
									JOptionPane.showMessageDialog(null, "Producto existente con ese nombre en "+localizacion+" para esa fecha");
								}
								else
								{

									try {
										Statement statement = conn.getCon().createStatement();
										ResultSet id = statement.executeQuery("SELECT count(*) FROM PRODUCTOS_DISPONIBLES");
										int idnumber = 0;
										id.next();
										idnumber = id.getInt(1) + 1;

										PreparedStatement producto = conn.getCon()
												.prepareStatement("INSERT INTO PRODUCTOS_DISPONIBLES VALUES (?,?,?,?,?,?,?)");

										if (getRdbtnAlimentos().isSelected()) {
											String alimento = "alimento";
											producto.setInt(1, idnumber);
											producto.setString(2, nombreproducto);
											producto.setString(3, alimento);
											producto.setDouble(4, precio);
											producto.setString(5, localizacion);

											producto.setDate(6, (java.sql.Date) fecha);

											producto.setString(7, descripcion);
											producto.executeUpdate();
											producto.close();
											clearScreen();
											JOptionPane.showMessageDialog(null, "El nuevo " + alimento + " " + nombreproducto
													+ " se ha a�adido correctamente.");
										} else if (getRdbtnBebidas().isSelected()) {
											String bebida = "bebida";

											producto.setInt(1, idnumber);
											producto.setString(2, nombreproducto);
											producto.setString(3, bebida);
											producto.setDouble(4, precio);
											producto.setString(5, localizacion);

											producto.setDate(6, (java.sql.Date) fecha);

											producto.setString(7, descripcion);

											producto.executeUpdate();
											producto.close();
											clearScreen();
											JOptionPane.showMessageDialog(null,
													"La nueva " + bebida + " " + nombreproducto + " se ha a�adido correctamente.");
										} else {
											String snack = "snack";

											producto.setInt(1, idnumber);
											producto.setString(2, nombreproducto);
											producto.setString(3, snack);
											producto.setDouble(4, precio);
											producto.setString(5, localizacion);

											producto.setDate(6, (java.sql.Date) fecha);

											producto.setString(7, descripcion);

											producto.executeUpdate();
											producto.close();
											clearScreen();
											JOptionPane.showMessageDialog(null,
													"El nuevo " + snack + " " + nombreproducto + " se ha a�adido correctamente.");

										}

									} catch (SQLException e2) {
										e2.printStackTrace();
									} 
								}
							}
							catch (ParseException | SQLException e1) {
								e1.printStackTrace();
							}
						}
					}
				}
			});
		}
		return btnRegistrar;
	}

	private void clearScreen()
	{
		getRdbtnAlimentos().setSelected(true);
		getTxtnombreproducto().setText("");
		getTxtDescripcion().setText("");
		getTxtPrecio().setText("");
		String[] alimentos = new String[] { "restaurante", "cafeteria" };
		cbLocalizacion.setModel(new DefaultComboBoxModel(alimentos));
		cbLocalizacion.setPreferredSize(new Dimension(140, 30));
		cbLocalizacion.setSelectedIndex(0);

	}
	private JPanel getPn2() {
		if (pn2 == null) {
			pn2 = new JPanel();
			pn2.add(getLblNombreProducto());
			pn2.add(getTxtnombreproducto());
		}
		return pn2;
	}

	private JLabel getLblNombreProducto() {
		if (lblNombreProducto == null) {
			lblNombreProducto = new JLabel("Nombre producto:");
		}
		return lblNombreProducto;
	}

	private JTextField getTxtnombreproducto() {
		if (txtnombreproducto == null) {
			txtnombreproducto = new JTextField();
			txtnombreproducto.setColumns(20);
		}
		return txtnombreproducto;
	}

	private JTextArea getTextArea() {
		if (textArea == null) {
			textArea = new JTextArea();
		}
		return textArea;
	}

	private JDateChooser getDCfecha() {
		if (dCfecha == null) {
			dCfecha = new JDateChooser();
		}
		return dCfecha;
	}
}
