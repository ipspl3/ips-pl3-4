package igu;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import logica.Conexion;
import logica.Trabajadores;

import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.DefaultListModel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;

import java.awt.Rectangle;
import java.awt.Toolkit;

import javax.swing.border.TitledBorder;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.CardLayout;
import java.awt.Color;

import javax.swing.JSeparator;
import javax.swing.JTextField;


import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JOptionPane;
import com.toedter.calendar.JDateChooser;

public class VentanaTurnoTrabajadores extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Conexion con;
	private JPanel contentPane;
	private JPanel panelTurnos;
	private JLabel lblTurnos;
	private JScrollPane spTablaTurnos;
	private JTable tablaTurnos;
	private String[] cabecera = {"TRABAJADOR", "�REA", "FECHA INICIO", "FECHA FIN", "HORA ENTRADA", "HORA SALIDA"};
	private JPanel panelBotones;
	private JButton btnCrearNuevoTurno;
	private JPanel panelCentral;
	private JPanel panelCard;
	private JPanel panelTabla;
	private JPanel panelContenedorCard;
	private JPanel panelBotones2;
	private JButton btnAsignar;
	private JSeparator separator;
	private JPanel panelNuevoTurno;
	private String areaSeleccionada;
	private CardLayout card = new CardLayout();
	private JLabel lblAreaDeTrabajo;
	private JLabel lblNombreEmpleado;
	private JTextField tfEmpleado;
	private JButton btnLupa;
	private JScrollPane scrollPaneListaEmpleados;
	private JList<String> listaEmpleados;
	private JLabel lblSeleccioneElEmpleado;
	private DefaultListModel<String> modeloLista = new DefaultListModel<String>();
	private JComboBox<String> cbAreaTrabajo;
	private JLabel lblHoraDeEntrada;
	private JTextField tfHoraEntrada;
	private JLabel lblHoraDeSalida;
	private JTextField tfHoraSalida;
	private JDateChooser dcFechaInicio;
	private JLabel lblFechaInicio;
	private JLabel lblFechaFin;
	private JDateChooser dcFechaFin;
	private ArrayList<String> empleadosSeleccionados = new ArrayList<String>();
	private ArrayList<String> areasPorEmpleados = new ArrayList<String>();
	private boolean areasIguales = true;
	private Trabajadores trabajadores;
	private boolean primero = true;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Conexion conexion = new Conexion();
					conexion.establecerConexion();	
					VentanaTurnoTrabajadores ventana = new VentanaTurnoTrabajadores(conexion);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public class MiModeloTabla extends DefaultTableModel{
		private static final long serialVersionUID = 1L;
		public MiModeloTabla(Object[] columnNames, int numFilas) {
			super(columnNames,numFilas);
		}
		
		public boolean isCellEditable(int numFila, int numColumna){
			return false;
		}
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public VentanaTurnoTrabajadores(Conexion conexion) throws SQLException {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaTurnoTrabajadores.class.getResource("/img/logo.jpg")));
		this.con = conexion;
		setTitle("Trabajadores");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1050, 800);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(getPanelTurnos(), BorderLayout.NORTH);
		contentPane.add(getPanelCentral(), BorderLayout.CENTER);
		setLocationRelativeTo(null);
		rellenarTablaTurnos();
		setVisible(true);
		setResizable(false);
	}

	private JPanel getPanelTurnos() {
		if (panelTurnos == null) {
			panelTurnos = new JPanel();
			panelTurnos.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			panelTurnos.add(getLblTurnos());
		}
		return panelTurnos;
	}
	private JLabel getLblTurnos() {
		if (lblTurnos == null) {
			lblTurnos = new JLabel("TURNOS:");
			lblTurnos.setFont(new Font("Tahoma", Font.BOLD, 24));
			lblTurnos.setHorizontalTextPosition(SwingConstants.LEFT);
			lblTurnos.setHorizontalAlignment(SwingConstants.LEFT);
		}
		return lblTurnos;
	}
	private JScrollPane getSpTablaTurnos() {
		if (spTablaTurnos == null) {
			spTablaTurnos = new JScrollPane();
			spTablaTurnos.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
			spTablaTurnos.setViewportView(getTablaTurnos());
		}
		return spTablaTurnos;
	}
	private JTable getTablaTurnos() {
		if (tablaTurnos == null) {
			tablaTurnos = new JTable();
			tablaTurnos.setBorder(null);
			tablaTurnos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tablaTurnos;
	}
	
	private void rellenarTablaTurnos() throws SQLException{
		
		MiModeloTabla miModelo = new MiModeloTabla(cabecera, 0);
		
		Statement s = con.getCon().createStatement();
		
		ResultSet rs = s.executeQuery("select tr.nombre, tr.area, t.fecha_inicio, "
				+ "t.fecha_fin, t.hora_entrada, t.hora_salida "
				+ "from trabajador tr, turno t "
				+ "where tr.dni = t.fk_trabajador");

		
		SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
		
		while(rs.next()){
			Object[] fila = new Object[6];
			fila[0] = rs.getString("nombre");
			fila[1] = rs.getString("area");
			fila[2] = formato.format(rs.getDate("fecha_inicio"));
			fila[3] = formato.format(rs.getDate("fecha_fin"));
			fila[4] = rs.getString("hora_entrada");
			fila[5] = rs.getString("hora_salida");
			
			miModelo.addRow(fila);
		}
		
		tablaTurnos.setModel(miModelo);
		modificarTablaTipos();
	}
	
	private void modificarTablaTipos(){
		tablaTurnos.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
		tablaTurnos.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tablaTurnos.setRowHeight(25);
		
		tablaTurnos.getTableHeader().setReorderingAllowed(false);
		
		TableColumn columna;
		for(int i=0; i<6; i++){
			columna = tablaTurnos.getColumn(cabecera[i]);
			if(i==0){
				columna.setMinWidth(250);
				columna.setMaxWidth(250);
			}
			if(i== 2 || i==3 || i==4 || i==5){
				columna.setMinWidth(120);
				columna.setMaxWidth(120);
			}
			columna.setResizable(false);
		}
		
		
	}

	private JPanel getPanelBotones() {
		if (panelBotones == null) {
			panelBotones = new JPanel();
			panelBotones.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
			panelBotones.setBounds(new Rectangle(0, 0, 50, 41));
			GridBagLayout gbl_panelBotones = new GridBagLayout();
			gbl_panelBotones.columnWidths = new int[] {140, 0};
			gbl_panelBotones.rowHeights = new int[] {41, 41, 41, 0};
			gbl_panelBotones.columnWeights = new double[]{0.0, Double.MIN_VALUE};
			gbl_panelBotones.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
			panelBotones.setLayout(gbl_panelBotones);
			GridBagConstraints gbc_btnCrearNuevoTurno = new GridBagConstraints();
			gbc_btnCrearNuevoTurno.fill = GridBagConstraints.BOTH;
			gbc_btnCrearNuevoTurno.insets = new Insets(0, 0, 5, 0);
			gbc_btnCrearNuevoTurno.gridx = 0;
			gbc_btnCrearNuevoTurno.gridy = 0;
			panelBotones.add(getBtnCrearNuevoTurno(), gbc_btnCrearNuevoTurno);
		}
		return panelBotones;
	}
	
	private JButton getBtnCrearNuevoTurno() {
		if (btnCrearNuevoTurno == null) {
			btnCrearNuevoTurno = new JButton("Nuevo turno");
			btnCrearNuevoTurno.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
			btnCrearNuevoTurno.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					resetCampos();
					panelCard.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
					panelBotones2.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
					panelContenedorCard.setVisible(true);
					card.first(panelCard);
					lblAreaDeTrabajo.setVisible(true);
					lblNombreEmpleado.setVisible(true);
					listaEmpleados.setVisible(true);
					lblSeleccioneElEmpleado.setVisible(true);
					scrollPaneListaEmpleados.setVisible(true);
					cbAreaTrabajo.setVisible(true);
					lblHoraDeEntrada.setVisible(true);
					tfHoraEntrada.setVisible(true);
					lblHoraDeSalida.setVisible(true);
					tfHoraSalida.setVisible(true);
					lblFechaInicio.setVisible(true);
					dcFechaInicio.setVisible(true);
					lblFechaFin.setVisible(true);
					dcFechaFin.setVisible(true);
					tfEmpleado.setVisible(true);
					btnLupa.setVisible(true);
					btnAsignar.setVisible(true);
					panelNuevoTurno.setBorder(new TitledBorder(null, "Asignar turno", TitledBorder.LEADING, TitledBorder.TOP, null, null));
				}
			});
			btnCrearNuevoTurno.setFont(new Font("Tahoma", Font.PLAIN, 15));
		}
		return btnCrearNuevoTurno;
	}
	
	private void resetCampos(){
		cbAreaTrabajo.setSelectedIndex(-1);
		tfHoraEntrada.setText("");
		tfHoraSalida.setText("");
		dcFechaInicio.setDate(null);
		dcFechaFin.setDate(null);
		tfEmpleado.setText(null);
		tfHoraEntrada.setBorder(new LineBorder(Color.BLACK));
		tfHoraSalida.setBorder(new LineBorder(Color.BLACK));
		empleadosSeleccionados = new ArrayList<String>();
		areaSeleccionada = null;
		iniciarLista();
		
	}
	private JPanel getPanelCentral() {
		if (panelCentral == null) {
			panelCentral = new JPanel();
			GridBagLayout gbl_panelCentral = new GridBagLayout();
			gbl_panelCentral.columnWidths = new int[]{1033, 0};
			gbl_panelCentral.rowHeights = new int[]{300, 0, 415, 0};
			gbl_panelCentral.columnWeights = new double[]{0.0, Double.MIN_VALUE};
			gbl_panelCentral.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
			panelCentral.setLayout(gbl_panelCentral);
			GridBagConstraints gbc_panelTabla = new GridBagConstraints();
			gbc_panelTabla.fill = GridBagConstraints.BOTH;
			gbc_panelTabla.insets = new Insets(0, 0, 5, 0);
			gbc_panelTabla.gridx = 0;
			gbc_panelTabla.gridy = 0;
			panelCentral.add(getPanelTabla(), gbc_panelTabla);
			GridBagConstraints gbc_separator_1 = new GridBagConstraints();
			gbc_separator_1.insets = new Insets(0, 0, 5, 0);
			gbc_separator_1.gridx = 0;
			gbc_separator_1.gridy = 1;
			panelCentral.add(getSeparator(), gbc_separator_1);
			GridBagConstraints gbc_panelContenedorCard = new GridBagConstraints();
			gbc_panelContenedorCard.fill = GridBagConstraints.BOTH;
			gbc_panelContenedorCard.gridx = 0;
			gbc_panelContenedorCard.gridy = 2;
			panelCentral.add(getPanelContenedorCard(), gbc_panelContenedorCard);
		}
		return panelCentral;
	}
	private JPanel getPanelCard() {
		if (panelCard == null) {
			panelCard = new JPanel();
			panelCard.setBorder(null);
			panelCard.setLayout(card);
			panelCard.add(getPanelNuevoTurno(), "panelNuevoTipo");
		}
		return panelCard;
	}
	private JPanel getPanelTabla() {
		if (panelTabla == null) {
			panelTabla = new JPanel();
			panelTabla.setLayout(new BorderLayout(0, 0));
			panelTabla.add(getSpTablaTurnos());
			panelTabla.add(getPanelBotones(), BorderLayout.EAST);
		}
		return panelTabla;
	}
	private JPanel getPanelContenedorCard() {
		if (panelContenedorCard == null) {
			panelContenedorCard = new JPanel();
			panelContenedorCard.setLayout(new BorderLayout(0, 0));
			panelContenedorCard.add(getPanelCard());
			panelContenedorCard.add(getPanelBotones2(), BorderLayout.EAST);
		}
		return panelContenedorCard;
	}
	private JPanel getPanelBotones2() {
		if (panelBotones2 == null) {
			panelBotones2 = new JPanel();
			panelBotones2.setBounds(new Rectangle(0, 0, 50, 41));
			panelBotones2.setBorder(null);
			GridBagLayout gbl_panelBotones2 = new GridBagLayout();
			gbl_panelBotones2.columnWidths = new int[]{140, 0};
			gbl_panelBotones2.rowHeights = new int[]{41, 41, 41, 0};
			gbl_panelBotones2.columnWeights = new double[]{0.0, Double.MIN_VALUE};
			gbl_panelBotones2.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
			panelBotones2.setLayout(gbl_panelBotones2);
			GridBagConstraints gbc_btnAsignar = new GridBagConstraints();
			gbc_btnAsignar.fill = GridBagConstraints.BOTH;
			gbc_btnAsignar.insets = new Insets(0, 0, 5, 0);
			gbc_btnAsignar.gridx = 0;
			gbc_btnAsignar.gridy = 0;
			panelBotones2.add(getBtnAsignar(), gbc_btnAsignar);
		}
		return panelBotones2;
	}
	private JButton getBtnAsignar() {
		if (btnAsignar == null) {
			btnAsignar = new JButton("Asignar");
			btnAsignar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(comprobar8hDiferencia() && comprobarFechas()){
						if(!horarioConflictivo()){
							asignarTurnos();
							System.out.println("CORRECCCCCCCCCCTOOOOOOOOOOOOOOO");
						}
						else{
							JOptionPane.showMessageDialog(null, "-Un trabajador no puede tener asociados dos\n"
									+ "turnos distintos en una semana", "Error",JOptionPane.ERROR_MESSAGE);
						}
					}
					else{
						System.out.println("FALLO HORAS");
					}
				}
			});
			btnAsignar.setFont(new Font("Tahoma", Font.PLAIN, 15));
			btnAsignar.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
			btnAsignar.setVisible(false);
		}
		return btnAsignar;
	}
	
	private void asignarTurnos(){
		try{
			
			for(int i=0; i<empleadosSeleccionados.size(); i++){
				PreparedStatement ps = con.getCon().prepareStatement("insert into turno"
						+ " values(SEC_TURNO.nextval, 'activo', to_date(?), to_date(?), ?, ?, ?)");
				ps.setDate(1, new java.sql.Date(dcFechaInicio.getDate().getTime()));
				ps.setDate(2, new java.sql.Date(dcFechaFin.getDate().getTime()));
				ps.setString(3, tfHoraEntrada.getText());
				ps.setString(4, tfHoraSalida.getText());
				String dni = trabajadores.getDni(empleadosSeleccionados.get(i));
				System.out.println("El dni es:" + dni);
				ps.setString(5, dni);
				ps.executeUpdate();
			}
			panelContenedorCard.setVisible(false);
			rellenarTablaTurnos();
			tablaTurnos.repaint();
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	private JSeparator getSeparator() {
		if (separator == null) {
			separator = new JSeparator();
		}
		return separator;
	}
	private JPanel getPanelNuevoTurno() {
		if (panelNuevoTurno == null) {
			panelNuevoTurno = new JPanel();
			panelNuevoTurno.setName("panelNuevoTipo");
			panelNuevoTurno.setLayout(null);
			panelNuevoTurno.add(getLblAreaDeTrabajo());
			panelNuevoTurno.add(getLblNombreEmpleado());
			panelNuevoTurno.add(getTfEmpleado());
			panelNuevoTurno.add(getBtnLupa());
			panelNuevoTurno.add(getScrollPaneListaEmpleados());
			panelNuevoTurno.add(getLblSeleccioneElEmpleado());
			panelNuevoTurno.add(getCbAreaTrabajo());
			panelNuevoTurno.add(getLblHoraDeEntrada());
			panelNuevoTurno.add(getTfHoraEntrada());
			panelNuevoTurno.add(getLblHoraDeSalida());
			panelNuevoTurno.add(getTfHoraSalida());
			panelNuevoTurno.add(getDcFechaInicio());
			panelNuevoTurno.add(getLblFechaInicio());
			panelNuevoTurno.add(getLblFechaFin());
			panelNuevoTurno.add(getDcFechaFin());
		}
		return panelNuevoTurno;
	}
	
	private JLabel getLblAreaDeTrabajo() {
		if (lblAreaDeTrabajo == null) {
			lblAreaDeTrabajo = new JLabel("\u00C1rea de trabajo:");
			lblAreaDeTrabajo.setBounds(359, 21, 229, 39);
			lblAreaDeTrabajo.setVisible(false);
		}
		return lblAreaDeTrabajo;
	}
	

	private JLabel getLblNombreEmpleado() {
		if (lblNombreEmpleado == null) {
			lblNombreEmpleado = new JLabel("Nombre empleado:");
			lblNombreEmpleado.setBounds(50, 27, 207, 26);
			lblNombreEmpleado.setVisible(false);
		}
		return lblNombreEmpleado;
	}
	private JTextField getTfEmpleado() {
		if (tfEmpleado == null) {
			tfEmpleado = new JTextField();
			tfEmpleado.setBounds(50, 56, 229, 32);
			tfEmpleado.setColumns(10);
			tfEmpleado.setVisible(false);
		}
		return tfEmpleado;
	}
	
	private JButton getBtnLupa() {
		if (btnLupa == null) {
			btnLupa = new JButton("");
			btnLupa.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(tfEmpleado.getText()!= null){
						try{
							
							rellenarListaEmpleados();
						}
						catch(Exception exc){
							exc.printStackTrace();
						}
					}
				}
			});
			btnLupa.setIcon(new ImageIcon(VentanaTurnoTrabajadores.class.getResource("/img/lupa3.png")));
			btnLupa.setBounds(288, 56, 32, 32);
			btnLupa.setVisible(false);
		}
		return btnLupa;
	}
	
	private void rellenarListaEmpleados(){
		try{
			modeloLista = new DefaultListModel<String>();
			Statement st = con.getCon().createStatement();
			ResultSet rs = st.executeQuery("select nombre from trabajador where nombre like '" +tfEmpleado.getText() + "%'");
			while(rs.next()){
				modeloLista.addElement(rs.getString("NOMBRE"));
			}
			
			listaEmpleados.setModel(modeloLista);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	private JScrollPane getScrollPaneListaEmpleados() {
		if (scrollPaneListaEmpleados == null) {
			scrollPaneListaEmpleados = new JScrollPane();
			scrollPaneListaEmpleados.setBounds(50, 121, 270, 273);
			scrollPaneListaEmpleados.setViewportView(getListaEmpleados());
			scrollPaneListaEmpleados.setVisible(false);
		}
		return scrollPaneListaEmpleados;
	}
	private JList<String> getListaEmpleados() {
		if (listaEmpleados == null) {
			listaEmpleados = new JList<String>();
			listaEmpleados.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					int indices[] = listaEmpleados.getSelectedIndices();
					empleadosSeleccionados = new ArrayList<String>();
					areasIguales = true;
					for(int i=0; i<indices.length; i++){
						empleadosSeleccionados.add(modeloLista.get(indices[i]));
					}
					System.out.println("Numero de seleccionados: " +indices.length);
					System.out.println("El empleado seleccionado es: " +  empleadosSeleccionados.get(0));
					if(indices.length==1)
						tfEmpleado.setText(empleadosSeleccionados.get(0));
					else
						tfEmpleado.setText(null);
					if(areaSeleccionada== null)
						rellenarComboAreaTrabajoPorTrabajador();
				}
			});
			iniciarLista();
			listaEmpleados.setVisible(false);
		}
		return listaEmpleados;
	}
	
	private void rellenarComboAreaTrabajoPorTrabajador(){
		try{
			Statement st = con.getCon().createStatement();
			areasPorEmpleados = new ArrayList<String>();
			String area = null;
			for(int i=0; i<empleadosSeleccionados.size(); i++){
				ResultSet rsArea = st.executeQuery("select area from trabajador where nombre= '" + empleadosSeleccionados.get(i) + "'");
				rsArea.next();
				area = rsArea.getString("AREA");
				System.out.println("El area es: " + area);
				if(!areasPorEmpleados.contains(area) && i!=0){
					System.out.println("Entro por aqui");
					areasIguales = false;
				}
				areasPorEmpleados.add(area);
				
			}
			if(areasIguales){
				cbAreaTrabajo.setSelectedItem(area);
				areaSeleccionada = area;
				cbAreaTrabajo.repaint();
			}
			else{
				cbAreaTrabajo.setSelectedIndex(-1);
				areaSeleccionada = null;
				JOptionPane.showMessageDialog(null, "Los trabajadores tienen que pertenecer a la misma area de trabajo", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void iniciarComboAreaTrabajo(){
		try{
			
			cbAreaTrabajo.removeAllItems();
			Statement st = con.getCon().createStatement();
			ResultSet rs = st.executeQuery("select distinct(area) from trabajador");
			while(rs.next()){
				cbAreaTrabajo.addItem(rs.getString("AREA"));
			}
			cbAreaTrabajo.setSelectedItem(null);
			cbAreaTrabajo.repaint();
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	
	private void iniciarLista(){
		try{
			
			modeloLista = new DefaultListModel<String>();
			Statement st = con.getCon().createStatement();
			ResultSet rs = st.executeQuery("select distinct(nombre), dni from trabajador");
			if(trabajadores== null)
				trabajadores = new Trabajadores();
			while(rs.next()){
				String nombre = rs.getString("NOMBRE");
				String dni = rs.getString("DNI");
				modeloLista.addElement(nombre);
				if(primero)
					trabajadores.a�adirTrabajador(nombre, dni);
			}
			primero = false;
			listaEmpleados.setModel(modeloLista);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	private JLabel getLblSeleccioneElEmpleado() {
		if (lblSeleccioneElEmpleado == null) {
			lblSeleccioneElEmpleado = new JLabel("Seleccione el empleado de la siguiente lista:");
			lblSeleccioneElEmpleado.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblSeleccioneElEmpleado.setBounds(50, 98, 270, 20);
			lblSeleccioneElEmpleado.setVisible(false);
		}
		return lblSeleccioneElEmpleado;
	}
	
	private void filtrarListaEmpleados(){
		//System.out.println("EmpleadoFiltrar: " + empleadoSeleccionado );
		
		try{
			if(!checkEmpleadoCorrecto()){
				modeloLista.removeAllElements();
				Statement st = con.getCon().createStatement();
				ResultSet rs = st.executeQuery("select nombre from trabajador where area= '" +areaSeleccionada + "'");
				while(rs.next()){
					modeloLista.addElement(rs.getString("NOMBRE"));
				}
				listaEmpleados.repaint();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}
	private boolean checkEmpleadoCorrecto(){
		try{
			Statement st = con.getCon().createStatement();
			ResultSet rs = st.executeQuery("select count(nombre) from trabajador where area= '" + areaSeleccionada + "' and "
					+ "nombre = '" + empleadosSeleccionados.get(0)+ "'");
			rs.next();
			int numero = rs.getInt("COUNT(NOMBRE)");
			if(numero>0)
				return true;
			return false;
		}
		catch (Exception e){
			e.printStackTrace();
			return false;
		}
	}
	private JComboBox<String> getCbAreaTrabajo() {
		if (cbAreaTrabajo == null) {
			cbAreaTrabajo = new JComboBox<String>();
			iniciarComboAreaTrabajo();
			cbAreaTrabajo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					areaSeleccionada = (String) cbAreaTrabajo.getSelectedItem();
					if(areaSeleccionada != null)
						filtrarListaEmpleados();
					
				}
			});
			cbAreaTrabajo.setVisible(false);
			cbAreaTrabajo.setBounds(359, 56, 169, 32);
		}
		return cbAreaTrabajo;
	}
	private JLabel getLblHoraDeEntrada() {
		if (lblHoraDeEntrada == null) {
			lblHoraDeEntrada = new JLabel("Hora de entrada:");
			lblHoraDeEntrada.setBounds(563, 27, 160, 26);
			lblHoraDeEntrada.setVisible(false);
		}
		return lblHoraDeEntrada;
	}
	private JTextField getTfHoraEntrada() {
		if (tfHoraEntrada == null) {
			tfHoraEntrada = new JTextField();
			tfHoraEntrada.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					//System.out.println(tfHoraEntrada.getText().length());
					if(!formatoHoraCorrecta(tfHoraEntrada)){
						JOptionPane.showMessageDialog(null, "Formato de hora incorrecta.\nEl formato de hora debe ser hh:mm", 
								"Fecha incorrecta" , JOptionPane.ERROR_MESSAGE);
						tfHoraEntrada.setBorder(new LineBorder(Color.RED));
					}
					else
						tfHoraEntrada.setBorder(new LineBorder(Color.GREEN));
				}
			});
			tfHoraEntrada.setBounds(563, 56, 103, 32);
			tfHoraEntrada.setColumns(10);
			tfHoraEntrada.setVisible(false);
		}
		return tfHoraEntrada;
	}
	private JLabel getLblHoraDeSalida() {
		if (lblHoraDeSalida == null) {
			lblHoraDeSalida = new JLabel("Hora de salida:");
			lblHoraDeSalida.setBounds(718, 27, 154, 26);
			lblHoraDeSalida.setVisible(false);
		}
		return lblHoraDeSalida;
	}
	private JTextField getTfHoraSalida() {
		if (tfHoraSalida == null) {
			tfHoraSalida = new JTextField();
			tfHoraSalida.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					if(!formatoHoraCorrecta(tfHoraSalida)){
						JOptionPane.showMessageDialog(null, "Formato de hora incorrecta.\nEl formato de hora debe ser hh:mm", 
								"Fecha incorrecta" , JOptionPane.ERROR_MESSAGE);
						tfHoraSalida.setBorder(new LineBorder(Color.RED));
					}
					else
						tfHoraSalida.setBorder(new LineBorder(Color.GREEN));
				}
			});
			tfHoraSalida.setColumns(10);
			tfHoraSalida.setBounds(718, 56, 103, 32);
			tfHoraSalida.setVisible(false);
		}
		return tfHoraSalida;
	}
	private JDateChooser getDcFechaInicio() {
		if (dcFechaInicio == null) {
			dcFechaInicio = new JDateChooser();
			dcFechaInicio.setBounds(359, 149, 169, 32);
			dcFechaInicio.setVisible(false);
			dcFechaInicio.setMinSelectableDate(new Date());
		}
		return dcFechaInicio;
	}
	private JLabel getLblFechaInicio() {
		if (lblFechaInicio == null) {
			lblFechaInicio = new JLabel("Fecha inicio:");
			lblFechaInicio.setBounds(359, 123, 169, 26);
			lblFechaInicio.setVisible(false);
		}
		return lblFechaInicio;
	}
	private JLabel getLblFechaFin() {
		if (lblFechaFin == null) {
			lblFechaFin = new JLabel("Fecha fin:");
			lblFechaFin.setBounds(563, 123, 118, 26);
			lblFechaFin.setVisible(false);
		}
		return lblFechaFin;
	}
	private JDateChooser getDcFechaFin() {
		if (dcFechaFin == null) {
			dcFechaFin = new JDateChooser();
			dcFechaFin.setBounds(563, 149, 169, 32);
			dcFechaFin.setVisible(false);
			dcFechaFin.setMinSelectableDate(new Date());
			
		}
		return dcFechaFin;
	}
	
	private boolean horasCorrectas(String dato){
		try{
			int horas = Integer.parseInt(dato);
			if(horas>=0 && horas<=23)
				return true;
			return false;
		}
		catch(NumberFormatException e){
			e.printStackTrace();
			return false;
		}
	}
	private boolean minutosCorrectos(String dato){
		try{
			int minutos = Integer.parseInt(dato);
			if(minutos>=0 && minutos<=59)
				return true;
			return false;
		}
		catch(NumberFormatException e){
			e.printStackTrace();
			return false;
		}
	}

	
	private boolean formatoHoraCorrecta(JTextField campoHora){
		String hora = campoHora.getText();
		if(hora != null){
			if(hora.length() == 5){
				String horas = hora.substring(0, 2);
				String minutos = hora.substring(3, 5);
				return (horasCorrectas(horas) && hora.substring(2, 3).equals(":") && minutosCorrectos(minutos));
				
			}
			return false;
		}
		return false;
	}
	
	private boolean comprobar8hDiferencia(){
		try{
			int horasSalida = Integer.parseInt(tfHoraSalida.getText().substring(0,2));
			int horasEntrada= Integer.parseInt(tfHoraEntrada.getText().substring(0,2));
			if(horasEntrada > horasSalida){
				if(horasSalida>=0 && horasSalida <=8){
					int resta24hMenosHorasEntrada = 24-horasEntrada;
					int sumaHoras = horasSalida+resta24hMenosHorasEntrada;
					if(sumaHoras<=8)
						return true;
					return false;
				}
				return false;
			}
			else if(horasSalida == horasEntrada)
				return false;
			else{
				return horasSalida-horasEntrada <=8;
			}
		}
		catch (Exception e){
			return false;
		}
	}
	
	private boolean comprobarFechas(){
		try{
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			Date fecha_inicio = formato.parse(formato.format(dcFechaInicio.getDate()));
			Date fecha_fin = formato.parse(formato.format(dcFechaFin.getDate()));

			Date fecha_acc = formato.parse(formato.format(new Date()));
			
			if(fecha_inicio.compareTo(fecha_acc) >= 0){
				if(fecha_inicio.compareTo(fecha_fin) > 0){
					JOptionPane.showMessageDialog(null, "La fecha de fin debe de ser mayor que la fecha de inicio");
					return false;
				}
				else{ 
					long diferenciaEn_ms = fecha_fin.getTime() - fecha_inicio.getTime();
					long dias = diferenciaEn_ms / (1000 * 60 * 60 * 24);
					if(multiploDe7((int) dias))
						return true;
					else{
						JOptionPane.showMessageDialog(null, "Debe asignar los turnos por semanas", "Error", JOptionPane.ERROR_MESSAGE);
						return false;
					}
				}
			}
			else{
				JOptionPane.showMessageDialog(null, "La fecha de inicio debe ser mayor que la fecha actual del sistema");
				return false;
			}
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "Fechas incorrectas");
			return false;
		}
	}
	
	private boolean multiploDe7(int dias){
		if(dias%7==0)
			return true;
		return false;
	}
	
//	private boolean fechasEnConflicto(){
//		try{
//			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
//			String fecha_ini= sdf.format(new java.sql.Date(dcFechaInicio.getDate().getTime()));
//			String fecha_fin= sdf.format(new java.sql.Date(dcFechaFin.getDate().getTime()));
//			
//			Statement st = con.getCon().createStatement();
//			for(int i=0; i<empleadosSeleccionados.size(); i++){
//				ResultSet rs = st.executeQuery("select count(nombre) from trabajador tr, turno t "
//						+ "where ((to_date('"+ fecha_ini + "','dd-MM-YYYY') between t.fecha_inicio "
//					+ "and t.fecha_fin) or (to_date('" + fecha_fin + "','dd-MM-YYYY') between "
//					+ "t.fecha_inicio and t.fecha_fin)) and dni= '" + trabajadores.getDni(empleadosSeleccionados.get(i)) + "' "
//					+ "and t.fk_trabajador='"+ trabajadores.getDni(empleadosSeleccionados.get(i)) + "'");
//				
//				rs.next();
//				int existeTarifa = rs.getInt("COUNT(NOMBRE)");
//				System.out.println("Existe: " + existeTarifa);
//				if(existeTarifa>0)
//					return true;
//			}
//			
//			return false;
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			return true;
//		}
//	}
//	
//	private boolean comprobar12h(){
//		try {
//			PreparedStatement ps = con.getCon().prepareStatement("");
//			
//			return false;
//		}
//		catch(Exception e) {
//			return false;
//		}
//		
//	}
	
	private boolean horarioConflictivo(){
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
			String fecha_ini= sdf.format(new java.sql.Date(dcFechaInicio.getDate().getTime()));
			String fecha_fin= sdf.format(new java.sql.Date(dcFechaFin.getDate().getTime()));
			
			StringBuilder sb1 = new StringBuilder();
			for(String e: empleadosSeleccionados){
				sb1.append("select fecha_inicio, fecha_fin, hora_salida ");
				sb1.append("from turno t , trabajador tr ");
				sb1.append("where fk_trabajador = tr.dni and tr.nombre = ? ");
				PreparedStatement ps = con.getCon().prepareStatement(sb1.toString());
				ps.setString(1, e);
				
				ResultSet rs = ps.executeQuery();
				while(rs.next()){
					Date fechaFinTurno = rs.getDate("FECHA_FIN");
					Date fechaInicioTurno = rs.getDate("FECHA_INICIO");
					String horaSalidaTurno = rs.getString("HORA_SALIDA");
					String fechaFinTurnoMenos1Dia = sdf.format(fechaFinTurno.getTime() - 86400000);
					
					System.out.println("FECHA INICIO TURNO: " + fechaInicioTurno);
					System.out.println("FECHA FIN TURNO MENOS 1 DIA: " + fechaFinTurnoMenos1Dia);
					System.out.println("FECHA ASIGNADA INICIO: " + fecha_ini);
					System.out.println("FECHA ASIGNADA FIN: " + fecha_fin);
					System.out.println("EL TRABAJADOR ES: " +  e);
					
					StringBuilder sb2 = new StringBuilder();
					sb2.append("select count(distinct(tr.nombre)) from trabajador tr, turno t ");
					sb2.append("where ((to_date(?, 'dd-MM-YYYY') between t.fecha_inicio and to_date(?,'dd-MM-YYYY')) ");
					sb2.append("or (to_date(?,'dd-MM-YYYY') between t.fecha_inicio and to_date(?,'dd-MM-YYYY'))) and ");
					sb2.append("tr.nombre = ? ");
					
					ps = con.getCon().prepareStatement(sb2.toString());
					ps.setString(1, fecha_ini);
					ps.setString(2, fechaFinTurnoMenos1Dia);
					ps.setString(3, fecha_fin);
					ps.setString(4, fechaFinTurnoMenos1Dia);
					ps.setString(5, e);
					
					ResultSet rs2 = ps.executeQuery();
					rs2.next();
					int existeTarifa = rs2.getInt("COUNT(DISTINCT(TR.NOMBRE))");
					System.out.println("�EXISTE FILA????????????:" + existeTarifa);
					if(!verificar12hDistancia(fechaFinTurno,horaSalidaTurno) || existeTarifa>0)
						return true;
				}
			}
			return false;
		}
		catch(Exception e){
			e.printStackTrace();
			return true;
		}
	}

	private boolean verificar12hDistancia(Date fecha_fin, String hora_salida) {
		Calendar diaUltimoTurnoConHora = Calendar.getInstance();
		diaUltimoTurnoConHora.setTime(fecha_fin);
		diaUltimoTurnoConHora.set(Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH, 
				Integer.parseInt(hora_salida.substring(0,2)), Integer.parseInt(hora_salida.substring(3,5)));
		diaUltimoTurnoConHora.add(Calendar.HOUR, 12);
		
		long tiempoUltimoTurno = diaUltimoTurnoConHora.getTimeInMillis();
		
		//CALENDARIO NUEVO-HORA ENTRADA
		Calendar diaQueSeQuiereAsignar = Calendar.getInstance();
		diaQueSeQuiereAsignar.setTime(dcFechaInicio.getDate());
		diaQueSeQuiereAsignar.set(Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH,
				Integer.parseInt(tfHoraEntrada.getText().substring(0,2)), Integer.parseInt(tfHoraEntrada.getText().substring(3,5)));
		
		long tiempoQueSeQuiereAsignar = diaQueSeQuiereAsignar.getTimeInMillis();
		System.out.println("Diferencia 12 horas: " + Math.abs((Math.abs(tiempoQueSeQuiereAsignar)-Math.abs(tiempoUltimoTurno))));
		long diferencia = Math.abs(tiempoQueSeQuiereAsignar)-Math.abs(tiempoUltimoTurno);
		if(diferencia<0 || diferencia>=43200000)
			return true;
		else 
			return false;
		
		//return Math.abs(tiempoQueSeQuiereAsignar)-Math.abs(tiempoUltimoTurno) >= 43200000;
		
		
		
	}
}






























