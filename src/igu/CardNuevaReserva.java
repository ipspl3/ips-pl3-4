package igu;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.toedter.calendar.JDateChooser;

import logica.Cliente;
import logica.DNI;
import logica.DataBase;
import logica.Habitacion;
import logica.Reserva;
import logica.Tarjeta;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

@SuppressWarnings("serial")
public class CardNuevaReserva extends JPanel {

	private VentanaRecepcionista padre;
	private boolean registro = false;
	private Cliente cliente;
	private Tarjeta tarjeta;
	private ArrayList<Reserva> reservas;

	private JDateChooser calendarIn;
	private JDateChooser calendarFin;
	private JLabel lblFechaInicio;
	private JLabel lblFechaFin;
	private JButton btnBuscar;
	private JButton btnA�adir;
	private JList<Habitacion> listHabs;
	private JList<String> listModalidad;
	private DefaultListModel<Habitacion> list3Model = null;
	private JButton btnEliminar;
	private JList<Habitacion> listA�adido;
	private JSeparator separator;
	private JSeparator separator_1;
	private JButton btnConfirmarReserva;
	private JButton btnCancelar;

	private JPanel panel;
	private JTextField txtDni;
	private JLabel lblNombre;
	private JTextField txtNombre;
	private JLabel lblTelefono;
	private JTextField txtTelefono;
	private JLabel lblDireccin;
	private JTextField txtDireccion;
	private JLabel lblTarjeta;
	private JTextField txtTarjeta;
	private JButton btnBuscarCliente;
	private JRadioButton rdbtnDni;
	private JRadioButton rdbtnPasaporte;
	private JLabel lblTipoTarjeta;
	private JButton btnModificarDatos;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	private JScrollPane pnModalidad;

	private JScrollPane pnHabs;

	private JScrollPane pnA�adido;

	private DefaultListModel<Habitacion> list1Model = null;
	private DefaultListModel<String> list2Model = null;
	private JLabel lblApellidos;
	private JTextField txtApellidos;
	private JLabel lblCorreo;
	private JTextField txtCorreo;
	private JPanel pnDatos;
	private JLabel lblPaisDeResidencia;
	private JTextField txtPaisDeResidencia;
	private JDateChooser nacimiento;
	private JLabel lblFnacimiento;
	private JButton btnComprobar;

	public CardNuevaReserva(VentanaRecepcionista papa) {

		padre = papa;
		reservas = new ArrayList<Reserva>();
		setLayout(null);
		add(getCalendarIn());
		add(getCalendarFin());
		add(getLblFechaInicio());
		add(getLblFechaFin());
		add(getBtnBuscar());
		add(getPanel());
		add(getPnModalidad());
		add(getPnHabs());
		add(getBtnAadir());
		add(getPnA�adido());
		add(getBtnEliminar());
		add(getSeparator());
		add(getSeparator_1());
		add(getBtnConfirmarReserva());
		add(getBtnCancelar());

		setVisible(true);

	}

	@SuppressWarnings("deprecation")
	private JDateChooser getCalendarIn() {
		if (calendarIn == null){
			calendarIn = new JDateChooser("dd/MM/yyyy", "##/##/####", '_');
			calendarIn.setVisible(true);
			calendarIn.setBounds(59, 309, 154, 23);
			Date today = new Date();
			today.setHours(0); 
			calendarIn.setMinSelectableDate(today);
			calendarIn.setDate(today);
		}
		return calendarIn;	
	}
	@SuppressWarnings("deprecation")
	private JDateChooser getCalendarFin() {
		if (calendarFin == null){
			calendarFin = new JDateChooser("dd/MM/yyyy", "##/##/####", '_');
			calendarFin.setVisible(true);
			calendarFin.setBounds(295, 309, 154, 23);
			Date ma�ana = new Date();
			ma�ana.setHours(0); 
			ma�ana.setDate(ma�ana.getDate() + 1);
			calendarFin.setMinSelectableDate(ma�ana);
			calendarFin.setDate(ma�ana);
		}
		return calendarFin;	
	}
	private JLabel getLblFechaInicio() {
		if (lblFechaInicio == null) {
			lblFechaInicio = new JLabel("Fecha inicio:");
			lblFechaInicio.setBounds(49, 284, 103, 14);
		}
		return lblFechaInicio;
	}
	private JLabel getLblFechaFin() {
		if (lblFechaFin == null) {
			lblFechaFin = new JLabel("Fecha fin:");
			lblFechaFin.setBounds(285, 284, 103, 14);
		}
		return lblFechaFin;
	}
	private JButton getBtnBuscar() {
		if (btnBuscar == null) {
			btnBuscar = new JButton("Buscar");
			btnBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (calendarIn.getDate().before(calendarFin.getDate())){
						ArrayList<String> habs = new ArrayList<String>();
						try {
							list1Model.clear();
							list2Model.clear();
							list2Model.clear();
							habs = DataBase.ComprobarFecha(calendarIn.getDate().getTime(), calendarFin.getDate().getTime());
							if (habs.isEmpty()) {
								JOptionPane.showMessageDialog(null, "No hay ninguna habitaci�n disponible para la fecha indicada");
							}
							else {
								for (String f:habs){	
									Habitacion hab = new Habitacion(f); 
									list1Model.addElement(hab);
								}
							}
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
					else{
						JOptionPane.showMessageDialog(null, "La fecha de salida no puede ser anterior o igual a la de entrada");
					}
				}
			});
			btnBuscar.setBounds(517, 309, 89, 23);
		}
		return btnBuscar;
	}
	private JButton getBtnAadir() {
		if (btnA�adir == null) {
			btnA�adir = new JButton("A\u00F1adir");
			btnA�adir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (listHabs.isSelectionEmpty() ||listModalidad.isSelectionEmpty()) {
						JOptionPane.showMessageDialog(null, "Faltan datos en la seleccion");
					}
					else {
						Reserva reserva;
						try {
							reserva = new Reserva();
							Habitacion este = ((Habitacion) list1Model.getElementAt(listHabs.getSelectedIndex()));
							String nombre_suplemento = list2Model.getElementAt(listModalidad.getSelectedIndex()).toString();
							int ip = DataBase.pk_modalidad(nombre_suplemento, este);
							este.setModalidad(nombre_suplemento, ip);
							reserva.add(este);
							reserva.datos(txtDni.getText().toString());
							reservas.add(reserva);
							list3Model.addElement(este);
							list1Model.remove(listHabs.getSelectedIndex());
							list2Model.clear();
							listModalidad.clearSelection();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}
			});
			btnA�adir.setBounds(517, 405, 89, 23);
		}
		return btnA�adir;
	}
	private JList<String> getListModalidad() {
		if (listModalidad == null) {
			list2Model = new DefaultListModel<String>();
			listModalidad = new JList<String>(list2Model);
			listModalidad.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return listModalidad;
	}
	private JButton getBtnEliminar() {
		if (btnEliminar == null) {
			btnEliminar = new JButton("Eliminar");
			btnEliminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Habitacion esta = (Habitacion) list3Model.getElementAt(listA�adido.getSelectedIndex());
					esta.modalidad = "ninguna";
					reservas.remove(listA�adido.getSelectedIndex());
					list3Model.remove(listA�adido.getSelectedIndex());
					list1Model.addElement(esta);
				}
			});
			btnEliminar.setBounds(517, 529, 89, 23);
		}
		return btnEliminar;
	}
	private JSeparator getSeparator() {
		if (separator == null) {
			separator = new JSeparator();
			separator.setBounds(10, 271, 601, 2);
		}
		return separator;
	}
	private JSeparator getSeparator_1() {
		if (separator_1 == null) {
			separator_1 = new JSeparator();
			separator_1.setBounds(20, 480, 601, 2);
		}
		return separator_1;
	}
	private JButton getBtnConfirmarReserva() {
		if (btnConfirmarReserva == null) {
			btnConfirmarReserva = new JButton("Confirmar reserva");
			btnConfirmarReserva.setBounds(355, 616, 152, 23);
			btnConfirmarReserva.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (!list3Model.isEmpty() && !txtDni.getText().equals("") && !txtNombre.getText().equals("") && !txtTelefono.getText().equals("") 
							&& !txtDireccion.getText().equals("") && !txtTarjeta.getText().equals(""))
					{
						try {
							int opcion = JOptionPane.showConfirmDialog(null, "�Desea confirmar la reserva?", "Confirmar cambios", JOptionPane.YES_NO_OPTION);
							if(opcion == 0){//YES
								if (cliente.isNuevo()) {
									DataBase.nuevoCliente(txtDni.getText(), txtNombre.getText(), txtApellidos.getText(), txtDireccion.getText(),
											txtTelefono.getText(), Long.parseLong(txtTarjeta.getText()), txtCorreo.getText(), 
											nacimiento.getDate().getTime(), txtPaisDeResidencia.getText());
									cliente.setDni(new DNI(txtDni.getText()));
									cliente.setName(txtNombre.getText());
									cliente.setApellidos(txtApellidos.getText());
									cliente.setDireccion(txtDireccion.getText());
									cliente.setTelefono(txtTelefono.getText());
									cliente.setTarjeta(txtTarjeta.getText());
									cliente.setCorreo(txtCorreo.getText());
									cliente.setNacimiento(nacimiento.getDate().getTime());
									cliente.setResidencia(txtPaisDeResidencia.getText());
								}
								int pk_res = DataBase.nuevaReserva1(txtDni.getText(), calendarIn.getDate().getTime(), calendarFin.getDate().getTime());
								for (Reserva res:reservas){
									res.setPk(pk_res);
									res.fecha(calendarIn.getDate().getTime(), calendarFin.getDate().getTime());
									res.grabar();
								}
								JOptionPane.showMessageDialog(null, "La reserva se ha registrado correctamente");
								if (!registro) {
									iniciar();
								}
								else {
									registro = false;
									padre.registro(cliente);
								}
							}
							
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Rellene todos los campos");
					}
				}
			});
		}
		return btnConfirmarReserva;
	}
	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.setBounds(510, 616, 101, 23);
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					iniciar();
				}
			});
		}
		return btnCancelar;
	}


	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBounds(10, 11, 622, 249);
			panel.setLayout(null);
			panel.add(getTxtDni());
			panel.add(getLblTarjeta());
			panel.add(getTxtTarjeta());
			panel.add(getRdbtnDni());
			panel.add(getRdbtnPasaporte());
			panel.add(getBtnBuscarCliente());
			panel.add(getLblTipoTarjeta());
			panel.add(getBtnModificarDatos());
			panel.add(getPnDatos());
			panel.add(getBtnComprobar());
		}
		return panel;
	}
	private JTextField getTxtDni() {
		if (txtDni == null) {
			txtDni = new JTextField();
			txtDni.setBackground(Color.WHITE);
			txtDni.addKeyListener(new KeyAdapter() {
				int limite = 9;
				@Override
				public void keyTyped(KeyEvent e) {
					if (txtDni.getText().length() == limite){
						DNI dni = new DNI(txtDni.getText());
						boolean bien;
						if (rdbtnDni.isSelected()){
							bien = dni.comprobarDNI();
							if (bien){
								txtDni.setBorder(new LineBorder(Color.BLACK));
								btnBuscarCliente.setEnabled(true);
							}
							else{
								txtDni.setBorder(new LineBorder(Color.RED));
							}
						}
						else{
							bien = dni.comprobarPasaporte();
							if (bien){
								txtDni.setBorder(new LineBorder(Color.BLACK));
								btnBuscarCliente.setEnabled(true);
							}
							else{
								txtDni.setBorder(new LineBorder(Color.RED));
							}
						}
					}
					if (txtDni.getText().length() >= limite){
						e.consume();
					}
				}
			});
			txtDni.setBounds(10, 37, 204, 20);
			txtDni.setColumns(10);
		}
		return txtDni;
	}

	private JLabel getLblTarjeta() {
		if (lblTarjeta == null) {
			lblTarjeta = new JLabel("Tarjeta: ");
			lblTarjeta.setBounds(10, 86, 69, 14);
		}
		return lblTarjeta;
	}
	private JTextField getTxtTarjeta() {
		if (txtTarjeta == null) {
			txtTarjeta = new JTextField();
			txtTarjeta.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					btnComprobar.setVisible(true);
					btnComprobar.setEnabled(true);
				}
			});
			txtTarjeta.setBounds(58, 83, 202, 20);
			txtTarjeta.setColumns(10);
			txtTarjeta.setEditable(false);
		}
		return txtTarjeta;
	}
	
	private JRadioButton getRdbtnDni() {
		if (rdbtnDni == null) {
			rdbtnDni = new JRadioButton("DNI");
			rdbtnDni.setSelected(true);
			buttonGroup.add(rdbtnDni);
			rdbtnDni.setBounds(10, 7, 69, 23);
		}
		return rdbtnDni;
	}
	private JRadioButton getRdbtnPasaporte() {
		if (rdbtnPasaporte == null) {
			rdbtnPasaporte = new JRadioButton("Pasaporte");
			buttonGroup.add(rdbtnPasaporte);
			rdbtnPasaporte.setBounds(75, 7, 109, 23);
		}
		return rdbtnPasaporte;
	}
	private JButton getBtnBuscarCliente() {
		if (btnBuscarCliente == null) {
			btnBuscarCliente = new JButton("Buscar");
			btnBuscarCliente.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setNoEditable();
					if (!txtDni.getText().isEmpty()){
						try {
							cliente = DataBase.buscarClientePorDni(txtDni.getText().toUpperCase());
							if (cliente == null){
								JOptionPane.showMessageDialog(null, "El cliente aun no ha sido registrado");
								cliente = new Cliente();
								cliente.setNuevo(true);
								setEditable();
							}
							else{
								JOptionPane.showMessageDialog(null, "Compruebe los datos guardados");
								txtNombre.setText(cliente.getName());
								txtApellidos.setText(cliente.getApellidos());
								txtTelefono.setText(String.valueOf(cliente.getTelefono()));
								txtDireccion.setText(cliente.getDireccion());
								txtCorreo.setText(cliente.getCorreo());
								nacimiento.setDate(new Date(cliente.getNacimiento()));
								txtPaisDeResidencia.setText(cliente.getResidencia());
								txtTarjeta.setText(cliente.getTarjeta().toString());
								btnModificarDatos.setVisible(true);
							}
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
					else if (!txtNombre.getText().isEmpty()){
						try {
							cliente = DataBase.buscarClientePorNombre(txtNombre.getText());
							if (cliente == null){
								JOptionPane.showMessageDialog(null, "El cliente aun no ha sido registrado");
								cliente = new Cliente();
								cliente.setNuevo(true);
								setEditable();
							}
							else{
								JOptionPane.showMessageDialog(null, "Compruebe los datos guardados");
								txtDni.setText(cliente.getDni().toString());
								if (cliente.getDni().que_es().equals("dni")) {
									rdbtnDni.setSelected(true);
								}
								else {
									rdbtnPasaporte.setSelected(true);
								}
								txtApellidos.setText(cliente.getApellidos());
								txtTelefono.setText(String.valueOf(cliente.getTelefono()));
								txtDireccion.setText(cliente.getDireccion());
								txtCorreo.setText(cliente.getCorreo());
								nacimiento.setDate(new Date(cliente.getNacimiento()));
								txtPaisDeResidencia.setText(cliente.getResidencia());
								txtTarjeta.setText(cliente.getTarjeta().toString());
								tarjeta = new Tarjeta(cliente.getTarjeta());
								lblTipoTarjeta.setText(tarjeta.getTipo());
								btnModificarDatos.setVisible(true);
							}
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
					else {
						JOptionPane.showMessageDialog(null, "Introducca un DNI, pasaporte o nombre");
					}
				}
			});
			btnBuscarCliente.setEnabled(false);
			btnBuscarCliente.setBounds(224, 36, 89, 23);
		}
		return btnBuscarCliente;
	}
	
	private void setEditable() {
		txtTelefono.setEditable(true);
		txtDireccion.setEditable(true);
		txtApellidos.setEditable(true);
		txtCorreo.setEditable(true);
		txtPaisDeResidencia.setEditable(true);
		nacimiento.setEnabled(true);
		txtTarjeta.setEditable(true);
	}
	
	private void setNoEditable() {
		txtTelefono.setEditable(false);
		txtDireccion.setEditable(false);
		txtApellidos.setEditable(false);
		txtCorreo.setEditable(false);
		txtPaisDeResidencia.setEditable(false);
		nacimiento.setEnabled(false);
		txtTarjeta.setEditable(false);
		
	}
	
	private JLabel getLblTipoTarjeta() {
		if (lblTipoTarjeta == null) {
			lblTipoTarjeta = new JLabel("");
			lblTipoTarjeta.setBounds(151, 130, 109, 14);
		}
		return lblTipoTarjeta;
	}
	private JButton getBtnModificarDatos() {
		if (btnModificarDatos == null) {
			btnModificarDatos = new JButton("Modificar Datos");
			btnModificarDatos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					modificar();
				}
			});
			btnModificarDatos.setBounds(68, 215, 165, 23);
			btnModificarDatos.setVisible(false);
		}
		return btnModificarDatos;
	}
	
	public void modificar() {
		new VentanaCambiarDatosCliente(this, cliente);
	}

	private JScrollPane getPnModalidad() {
		if (pnModalidad == null) {
			pnModalidad = new JScrollPane();
			pnModalidad.setBorder(new TitledBorder(null, "Modalidad", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnModalidad.setBounds(285, 358, 184, 111);
			pnModalidad.setViewportView(getListModalidad());
		}
		return pnModalidad;
	}


	private JScrollPane getPnHabs() {
		if (pnHabs == null) {
			pnHabs = new JScrollPane();
			pnHabs.setBorder(new TitledBorder(null, "Habitaciones", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnHabs.setBounds(10, 358, 260, 111);
			pnHabs.setViewportView(getListHabs());
		}
		return pnHabs;
	}
	private JList<Habitacion> getListHabs() {
		if (listHabs == null) {
			list1Model = new DefaultListModel<Habitacion>();
			listHabs = new JList<Habitacion>(list1Model);
			listHabs.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (!listHabs.isSelectionEmpty()) {
						list2Model.clear();
						Habitacion este = (Habitacion) list1Model.getElementAt(listHabs.getSelectedIndex());
						try {
							ArrayList<String> modos = DataBase.Modalidades(este.tipo);
							for (String mod:modos){
								list2Model.addElement(mod);
							}
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
				}
			});
			listHabs.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return listHabs;
	}


	private JScrollPane getPnA�adido() {
		if (pnA�adido == null) {
			pnA�adido = new JScrollPane();
			pnA�adido.setBorder(new TitledBorder(null, "A\u00F1adido", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnA�adido.setBounds(10, 493, 458, 102);
			pnA�adido.setViewportView(getListA�adido());
		}
		return pnA�adido;
	}
	private JList<Habitacion> getListA�adido() {
		if (listA�adido == null) {
			list3Model = new DefaultListModel<Habitacion>();
			listA�adido = new JList<Habitacion>(list3Model);
		}
		return listA�adido;
	}

	public void iniciar(){
		calendarIn.setDate(null);
		calendarIn.setMaxSelectableDate(null);
		calendarFin.setDate(null);
		list1Model.clear();
		list2Model.clear();
		list3Model.clear();

		txtNombre.setText("");
		txtApellidos.setText("");
		txtDni.setText("");
		txtTelefono.setText("");
		txtDireccion.setText("");
		txtCorreo.setText("");
		nacimiento.setDate(null);
		txtPaisDeResidencia.setText("");
		lblTipoTarjeta.setText("");
		
		btnBuscarCliente.setEnabled(false);
		btnComprobar.setEnabled(false);
		btnComprobar.setVisible(false);
		
		txtTarjeta.setText("");
		
		reservas.clear();
		
		setNoEditable();

		padre.iniciar();
	}
	
	private JPanel getPnDatos() {
		if (pnDatos == null) {
			pnDatos = new JPanel();
			pnDatos.setBounds(323, 0, 289, 238);
			pnDatos.setLayout(null);
			pnDatos.add(getLblNombre());
			pnDatos.add(getTxtNombre());
			pnDatos.add(getLblTelefono());
			pnDatos.add(getTxtTelefono());
			pnDatos.add(getLblDireccin());
			pnDatos.add(getTxtDireccion());
			pnDatos.add(getLblApellidos());
			pnDatos.add(getTxtApellidos());
			pnDatos.add(getLblCorreo());
			pnDatos.add(getTxtCorreo());
			pnDatos.add(getLblPaisDeResidencia());
			pnDatos.add(getTxtPaisDeResidencia());
			pnDatos.add(getNacimiento());
			pnDatos.add(getLblFnacimiento());
		}
		return pnDatos;
	}
	private JLabel getLblNombre() {
		if (lblNombre == null) {
			lblNombre = new JLabel("Nombre:");
			lblNombre.setBounds(0, 17, 59, 14);
		}
		return lblNombre;
	}
	private JTextField getTxtNombre() {
		if (txtNombre == null) {
			txtNombre = new JTextField();
			txtNombre.setBounds(68, 11, 194, 20);
			txtNombre.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (!btnBuscarCliente.isEnabled()){
						btnBuscarCliente.setEnabled(true);
					}
				}
			});
			txtNombre.setColumns(10);
		}
		return txtNombre;
	}
	private JLabel getLblTelefono() {
		if (lblTelefono == null) {
			lblTelefono = new JLabel("Telefono:");
			lblTelefono.setBounds(0, 110, 59, 14);
		}
		return lblTelefono;
	}
	private JTextField getTxtTelefono() {
		if (txtTelefono == null) {
			txtTelefono = new JTextField();
			txtTelefono.setBounds(68, 104, 194, 20);
			txtTelefono.setColumns(10);
			txtTelefono.setEditable(false);
		}
		return txtTelefono;
	}
	private JLabel getLblDireccin() {
		if (lblDireccin == null) {
			lblDireccin = new JLabel("Direcci\u00F3n:");
			lblDireccin.setBounds(0, 79, 59, 14);
		}
		return lblDireccin;
	}
	private JTextField getTxtDireccion() {
		if (txtDireccion == null) {
			txtDireccion = new JTextField();
			txtDireccion.setBounds(68, 73, 194, 20);
			txtDireccion.setColumns(10);
			txtDireccion.setEditable(false);
		}
		return txtDireccion;
	}
	private JLabel getLblApellidos() {
		if (lblApellidos == null) {
			lblApellidos = new JLabel("Apellidos:");
			lblApellidos.setBounds(0, 48, 74, 14);
		}
		return lblApellidos;
	}
	private JTextField getTxtApellidos() {
		if (txtApellidos == null) {
			txtApellidos = new JTextField();
			txtApellidos.setBounds(68, 42, 194, 20);
			txtApellidos.setEditable(false);
			txtApellidos.setColumns(10);
		}
		return txtApellidos;
	}
	private JLabel getLblCorreo() {
		if (lblCorreo == null) {
			lblCorreo = new JLabel("Correo:");
			lblCorreo.setBounds(0, 141, 59, 14);
		}
		return lblCorreo;
	}
	private JTextField getTxtCorreo() {
		if (txtCorreo == null) {
			txtCorreo = new JTextField();
			txtCorreo.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					if (!(txtCorreo.getText().contains("@gmail.com") || 
							txtCorreo.getText().contains("@gmail.es") ||
							txtCorreo.getText().contains("@hotmail.com") ||
							txtCorreo.getText().contains("@hotmail.es") ||
							txtCorreo.getText().contains("@uniovi.es"))){
						txtCorreo.setBorder(new LineBorder(Color.RED));
					}
					else {
						txtCorreo.setBorder(new LineBorder(Color.BLACK));
					}
				}
			});
			txtCorreo.setBounds(68, 135, 194, 20);
			txtCorreo.setEditable(false);
			txtCorreo.setColumns(10);
		}
		return txtCorreo;
	}
	private JLabel getLblPaisDeResidencia() {
		if (lblPaisDeResidencia == null) {
			lblPaisDeResidencia = new JLabel("Pais de residencia:");
			lblPaisDeResidencia.setBounds(0, 206, 147, 14);
		}
		return lblPaisDeResidencia;
	}
	private JTextField getTxtPaisDeResidencia() {
		if (txtPaisDeResidencia == null) {
			txtPaisDeResidencia = new JTextField();
			txtPaisDeResidencia.setEditable(false);
			txtPaisDeResidencia.setBounds(116, 200, 146, 20);
			txtPaisDeResidencia.setColumns(10);
		}
		return txtPaisDeResidencia;
	}
	@SuppressWarnings("deprecation")
	private JDateChooser getNacimiento() {
		if (nacimiento == null){
			nacimiento = new JDateChooser("dd/MM/yyyy", "##/##/####", '_');
			nacimiento.setVisible(true);
			nacimiento.setBounds(115, 166, 147, 23);
			Date mayorDe = new Date();
			mayorDe.setHours(0); 
			mayorDe.setYear(mayorDe.getYear() - 16);
			Date menorDe = new Date();
			menorDe.setHours(0); 
			menorDe.setYear(menorDe.getYear() - 150);

			nacimiento.setSelectableDateRange(menorDe, mayorDe);
		}
		return nacimiento;	
	}
	private JLabel getLblFnacimiento() {
		if (lblFnacimiento == null) {
			lblFnacimiento = new JLabel("F.Nacimiento:");
			lblFnacimiento.setBounds(0, 175, 96, 14);
		}
		return lblFnacimiento;
	}
	
	public void refrescar(Cliente cliente) {
		this.cliente = cliente;
		txtNombre.setText(cliente.getName());
		txtApellidos.setText(cliente.getApellidos());
		txtTelefono.setText(String.valueOf(cliente.getTelefono()));
		txtDireccion.setText(cliente.getDireccion());
		txtCorreo.setText(cliente.getCorreo());
		nacimiento.setDate(new Date(cliente.getNacimiento()));
		txtPaisDeResidencia.setText(cliente.getResidencia());
		txtTarjeta.setText(cliente.getTarjeta().toString());
	}
	
	private JButton getBtnComprobar() {
		if (btnComprobar == null) {
			btnComprobar = new JButton("Comprobar");
			btnComprobar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						Tarjeta tarjeta = new Tarjeta(txtTarjeta.getText());
						if (tarjeta.comprobarTarjeta()) {
							txtTarjeta.setBorder(new LineBorder(Color.BLACK));
							lblTipoTarjeta.setText(tarjeta.buscarTipo());
						}
						else {
							txtTarjeta.setBorder(new LineBorder(Color.RED));
							lblTipoTarjeta.setText("");
						}
					}
					catch (NumberFormatException et) {
						JOptionPane.showMessageDialog(null, "El campo tarjeta debe contener solo n�meros");
					}
				}
			});
			btnComprobar.setVisible(false);
			btnComprobar.setEnabled(false);
			btnComprobar.setBounds(10, 121, 109, 23);
		}
		return btnComprobar;
	}

	@SuppressWarnings("deprecation")
	public void registro(String dato, String tipo, Cliente cliente) {
		registro = true;
		this.cliente = new Cliente();
		if (tipo.equals("dni")) {
			txtDni.setText(dato);
			this.cliente.setDni(new DNI(dato));
		}
		else {
			txtNombre.setText(dato);
			this.cliente.setName(dato);
		}
		this.cliente.setNuevo(true);
		Date today = new Date();
		today.setHours(0);
		calendarIn.setSelectableDateRange(today, today);
		setEditable();
		
		if (cliente != null) {
			this.cliente = cliente;
			JOptionPane.showMessageDialog(null, "Compruebe los datos guardados");
			txtNombre.setText(cliente.getName());
			txtDni.setText(cliente.getDni().toString());
			if (cliente.getDni().que_es().equals("dni")) {
				rdbtnDni.setSelected(true);
			}
			else {
				rdbtnPasaporte.setSelected(true);
			}
			txtApellidos.setText(cliente.getApellidos());
			txtTelefono.setText(String.valueOf(cliente.getTelefono()));
			txtDireccion.setText(cliente.getDireccion());
			txtCorreo.setText(cliente.getCorreo());
			nacimiento.setDate(new Date(cliente.getNacimiento()));
			txtPaisDeResidencia.setText(cliente.getResidencia());
			txtTarjeta.setText(cliente.getTarjeta().toString());
			btnModificarDatos.setVisible(true);
			setNoEditable();
		}
	}
}
