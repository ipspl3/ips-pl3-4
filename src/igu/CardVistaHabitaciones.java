package igu;

import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import logica.DataBase;
import logica.Habitacion;

import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JFrame;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

@SuppressWarnings("serial")
public class CardVistaHabitaciones extends JPanel{

	private VentanaRecepcionista padreRecepcionista = null;
	private CardGerenteHabitaciones padreGerente = null;
	private JScrollPane scrollHabs;
	private JList<Habitacion> listHabs;
	private DefaultListModel<Habitacion> listModel = null;
	private JPanel pnInfoHab;
	private JPanel pnHistorialHab;

	private Habitacion hab;
	private JLabel lblHabitacin;
	private JLabel lblTxtHab;
	private JLabel lblPlanta;
	private JLabel lblTxtPlanta;
	private JLabel lblComentarios;
	private JLabel lblTxtComentarios;
	private JLabel lblImagenes;
	private JPanel panel;
	private JTextField txtNumeroHab;
	private JButton btnBuscar;
	private JButton btnAadirTarea;
	private JLabel lblImagen;
	private JLabel lblEstado;
	private JLabel lblTxtEstado;
	private JButton btnAtras;

	public CardVistaHabitaciones(Object papa) {

		if (papa instanceof JFrame) {
			padreRecepcionista = (VentanaRecepcionista) papa;
			System.out.println("no deberia entrar aqui");
		}
		else {
			padreGerente = (CardGerenteHabitaciones) papa;
			System.out.println("entró bien!!");
		}
		System.out.println(padreRecepcionista);
		System.out.println(padreGerente);

		setLayout(null);
		add(getScrollHabs());
		add(getPnInfoHab());
		add(getPnHistorialHab());
		add(getTxtNumeroHab());
		add(getBtnBuscar());
		add(getBtnAtras());

		setVisible(true);
	}

	private JScrollPane getScrollHabs() {
		if (scrollHabs == null) {
			scrollHabs = new JScrollPane();
			scrollHabs.setBorder(new TitledBorder(null, "Habitaciones", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			if (padreRecepcionista != null) {
				scrollHabs.setBounds(10, 53, 241, 585);
			}
			else {
				scrollHabs.setBounds(10, 53, 241, 551);
			}
			scrollHabs.setViewportView(getListHabs());
		}
		return scrollHabs;
	}
	private JList<Habitacion> getListHabs() {
		if (listHabs == null) {
			listModel = new DefaultListModel<Habitacion>();
			listHabs = new JList<Habitacion>(listModel);
			listHabs.setForeground(Color.BLACK);
			listHabs.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (!listHabs.isSelectionEmpty()) {
						hab = listHabs.getSelectedValue();
						pnInfoHab.setVisible(true);
						pnHistorialHab.setVisible(true);
						lblImagen.setVisible(true);
						lblTxtHab.setText(hab.toString());
						lblTxtPlanta.setText(String.valueOf(hab.planta));
						lblTxtComentarios.setText(hab.comentario);
						if (hab.tipo.equals("doble")){
							imagenHab(new ImageIcon(CardVistaHabitaciones.class.getResource("/img/habitaciones/doble.jpeg")));
						}
						if (hab.tipo.equals("individual")){
							imagenHab(new ImageIcon(CardVistaHabitaciones.class.getResource("/img/habitaciones/individual.jpg")));
						}
						if (hab.tipo.equals("triple")){
							imagenHab(new ImageIcon(CardVistaHabitaciones.class.getResource("/img/habitaciones/triple.jpeg")));
						}
						String estado_actual = "";
						try {
							estado_actual = DataBase.estadoHabitacion(hab);
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
						lblTxtEstado.setText(estado_actual);
					}
				}
			});
			listHabs.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return listHabs;
	}

	public void todasHabitaciones() {
		System.out.println("La base es " + DataBase.isConnected());
		System.out.println("La lista esta " + listModel.isEmpty());
		if (DataBase.isConnected() && listModel.isEmpty()) {
			try {
				ArrayList<Habitacion> habis = DataBase.buscarHabitaciones();
				for (Habitacion hab:habis) 
				{
					listModel.addElement(hab);
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		System.out.println(listModel.size());
	}
	private JPanel getPnInfoHab() {
		if (pnInfoHab == null) {
			pnInfoHab = new JPanel();
			pnInfoHab.setBorder(new TitledBorder(null, "Habitacion", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnInfoHab.setVisible(false);
			pnInfoHab.setBounds(261, 11, 375, 283);
			pnInfoHab.setLayout(null);
			pnInfoHab.add(getLblHabitacin());
			pnInfoHab.add(getLblTxtHab());
			pnInfoHab.add(getLblPlanta());
			pnInfoHab.add(getLblTxtPlanta());
			pnInfoHab.add(getLblComentarios());
			pnInfoHab.add(getLblTxtComentarios());
			pnInfoHab.add(getLblImagenes());
			pnInfoHab.add(getPanel());
			pnInfoHab.add(getBtnAñadirTarea());
		}
		return pnInfoHab;
	}
	private JPanel getPnHistorialHab() {
		if (pnHistorialHab == null) {
			pnHistorialHab = new JPanel();
			pnHistorialHab.setBorder(new TitledBorder(null, "Historial", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnHistorialHab.setVisible(false);
			if (padreRecepcionista != null) {
				pnHistorialHab.setBounds(261, 305, 375, 333);
			}
			else {
				pnHistorialHab.setBounds(261, 305, 375, 299);
			}
			pnHistorialHab.setLayout(null);
			pnHistorialHab.add(getLblEstado());
			pnHistorialHab.add(getLblTxtEstado());
		}
		return pnHistorialHab;
	}
	private JLabel getLblHabitacin() {
		if (lblHabitacin == null) {
			lblHabitacin = new JLabel("Habitacion:");
			lblHabitacin.setBounds(10, 22, 75, 14);
		}
		return lblHabitacin;
	}
	private JLabel getLblTxtHab() {
		if (lblTxtHab == null) {
			lblTxtHab = new JLabel("");
			lblTxtHab.setBounds(95, 22, 270, 14);
		}
		return lblTxtHab;
	}
	private JLabel getLblPlanta() {
		if (lblPlanta == null) {
			lblPlanta = new JLabel("Planta:");
			lblPlanta.setBounds(10, 47, 75, 14);
		}
		return lblPlanta;
	}
	private JLabel getLblTxtPlanta() {
		if (lblTxtPlanta == null) {
			lblTxtPlanta = new JLabel("");
			lblTxtPlanta.setBounds(95, 47, 166, 14);
		}
		return lblTxtPlanta;
	}
	private JLabel getLblComentarios() {
		if (lblComentarios == null) {
			lblComentarios = new JLabel("Comentarios: ");
			lblComentarios.setBounds(10, 73, 86, 14);
		}
		return lblComentarios;
	}
	private JLabel getLblTxtComentarios() {
		if (lblTxtComentarios == null) {
			lblTxtComentarios = new JLabel("");
			lblTxtComentarios.setBounds(95, 73, 270, 14);
		}
		return lblTxtComentarios;
	}
	private JLabel getLblImagenes() {
		if (lblImagenes == null) {
			lblImagenes = new JLabel("Imagenes:");
			lblImagenes.setBounds(10, 155, 75, 14);
		}
		return lblImagenes;
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBounds(75, 98, 290, 133);
			panel.setLayout(null);
			panel.add(getLblImagen());
		}
		return panel;
	}
	private JTextField getTxtNumeroHab() {
		if (txtNumeroHab == null) {
			txtNumeroHab = new JTextField();
			txtNumeroHab.setBounds(29, 22, 86, 20);
			txtNumeroHab.setColumns(10);
		}
		return txtNumeroHab;
	}
	private JButton getBtnBuscar() {
		if (btnBuscar == null) {
			btnBuscar = new JButton("Buscar");
			btnBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (!isNumeric(txtNumeroHab.getText())) {
						listModel.clear();
						todasHabitaciones();
					}else if (txtNumeroHab.getText().equals("") ) {
						JOptionPane.showMessageDialog(null, "Introduce un número de habitacion");
					}else {
						try {
							hab = DataBase.buscarHabitacionNumero(Integer.parseInt(txtNumeroHab.getText()));
							listModel.clear();
							listModel.addElement(hab);
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
				}
			});
			btnBuscar.setBounds(132, 19, 89, 23);
		}
		return btnBuscar;
	}

	private boolean isNumeric(String cadena){
		try {
			Integer.parseInt(cadena);
			return true;
		} catch(NumberFormatException nfe){
			return false;
		}
	}

	public void iniciar() {
		pnInfoHab.setVisible(false);
		pnHistorialHab.setVisible(false);
		if (padreRecepcionista != null) {
			padreRecepcionista.iniciar();
			padreRecepcionista = null;
		}
		else {
			padreGerente = null;
		}
	}
	private JButton getBtnAñadirTarea() {
		if (btnAadirTarea == null) {
			btnAadirTarea = new JButton("A\u00F1adir tarea");
			btnAadirTarea.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					tarea(listHabs.getSelectedValue());
				}
			});
			btnAadirTarea.setBounds(95, 249, 160, 23);
			if (padreRecepcionista == null) {
				btnAadirTarea.setVisible(false);
			}
		}
		return btnAadirTarea;
	}

	private void tarea(Habitacion hab) {
		new VentanaTareas(this, hab);
	}

	private JLabel getLblImagen() {
		if (lblImagen == null) {
			lblImagen = new JLabel("");
			lblImagen.setIcon(new ImageIcon(CardVistaHabitaciones.class.getResource("/img/habitaciones/doble2.jpeg")));
			lblImagen.setBounds(10, 11, 270, 111);
		}
		return lblImagen;
	}

	private void imagenHab(ImageIcon img){
		Icon icono = new ImageIcon(img.getImage().getScaledInstance(getLblImagen().getWidth(), getLblImagen().getHeight(), Image.SCALE_SMOOTH));
		getLblImagen().setIcon(icono);
	}

	private JLabel getLblEstado() {
		if (lblEstado == null) {
			lblEstado = new JLabel("Estado:");
			lblEstado.setBounds(25, 34, 61, 14);
		}
		return lblEstado;
	}
	private JLabel getLblTxtEstado() {
		if (lblTxtEstado == null) {
			lblTxtEstado = new JLabel("");
			lblTxtEstado.setBounds(96, 34, 217, 14);
		}
		return lblTxtEstado;
	}
	private JButton getBtnAtras() {
		if (btnAtras == null) {
			btnAtras = new JButton("Atras");
			btnAtras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					padreGerente.getSuperCard().show(padreGerente.getPanelSuperCard(), "panelElegirAccion");
					iniciar();
				}
			});
			btnAtras.setBounds(644, 581, 89, 23);
			if (padreGerente == null) {
				btnAtras.setVisible(false);
			}
		}
		return btnAtras;
	}
}
