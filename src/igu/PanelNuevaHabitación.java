package igu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;

import logica.DataBase;
import logica.Habitacion;
import logica.Reserva;

import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import java.awt.Toolkit;

@SuppressWarnings("serial")
public class PanelNuevaHabitaci�n extends JFrame{

	private CardCheckIn padre;
	private int id_reserva;
	private DefaultListModel<Habitacion> listModelReservas;
	private ArrayList<Reserva> reservas;
	private JScrollPane pnHabs;
	private JScrollPane pnModalidad;
	private JList<Habitacion> listHabs;
	private DefaultListModel<Habitacion> list1Model;
	private DefaultListModel<String> list2Model;
	private JScrollPane pnA�adido;
	private JList<Habitacion> listA�adido;
	private DefaultListModel<Habitacion> list3Model;
	private JList<String> listModalidad;
	private JButton btnA�adir;
	private JButton btnEliminar;
	private JButton btnCancelar;
	private JButton btnConfirmar;
	
	@SuppressWarnings("deprecation")
	public PanelNuevaHabitaci�n(CardCheckIn papa, DefaultListModel<Habitacion> listModelReservas, ArrayList<Reserva> reservas, int id_reserva) {
		setTitle("Nueva habitaci\u00F3n");
		setIconImage(Toolkit.getDefaultToolkit().getImage(PanelNuevaHabitaci�n.class.getResource("/img/logo.jpg")));
		padre = papa;
		this.id_reserva = id_reserva;
		getContentPane().setLayout(null);
		getContentPane().add(getPnModalidad());
		getContentPane().add(getPnHabs());
		getContentPane().add(getPnA�adido());
		getContentPane().add(getBtnA�adir());
		getContentPane().add(getBtnEliminar());
		getContentPane().add(getBtnCancelar());
		getContentPane().add(getBtnConfirmar());
		this.listModelReservas = listModelReservas;
		this.reservas = reservas;
		setResizable(false);
		setVisible(true);
		setBounds(100,100,629,328);
		setLocationRelativeTo(null);
		
		ArrayList<String> habs = new ArrayList<String>();
		try {
			list1Model.clear();
			list2Model.clear();
			list2Model.clear();
			if (reservas.size()!=0) {
				habs = DataBase.ComprobarFecha(reservas.get(0).f_in_d.getTime(), reservas.get(0).f_fin_d.getTime());
			}
			else {
				Date today = new Date();
				today.setHours(0);
				habs = DataBase.ComprobarFecha(today.getDate(), today.getDate());
			}
			if (habs.isEmpty()) {
				JOptionPane.showMessageDialog(null, "No hay ninguna habitaci�n disponible para hoy");
				dispose();
			}
			else {
				for (String f:habs){	
					Habitacion hab = new Habitacion(f); 
					list1Model.addElement(hab);
				}
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
	private JScrollPane getPnModalidad() {
		if (pnModalidad == null) {
			pnModalidad = new JScrollPane();
			pnModalidad.setBorder(new TitledBorder(null, "Modalidad", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnModalidad.setBounds(292, 11, 184, 111);
			pnModalidad.setViewportView(getListModalidad());
		}
		return pnModalidad;
	}
	private JList<String> getListModalidad() {
		if (listModalidad == null) {
			list2Model = new DefaultListModel<String>();
			listModalidad = new JList<String>(list2Model);
			listModalidad.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return listModalidad;
	}

	private JScrollPane getPnHabs() {
		if (pnHabs == null) {
			pnHabs = new JScrollPane();
			pnHabs.setBorder(new TitledBorder(null, "Habitaciones", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnHabs.setBounds(10, 11, 260, 111);
			pnHabs.setViewportView(getListHabs());
		}
		return pnHabs;
	}
	private JList<Habitacion> getListHabs() {
		if (listHabs == null) {
			list1Model = new DefaultListModel<Habitacion>();
			listHabs = new JList<Habitacion>(list1Model);
			listHabs.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					list2Model.clear();
					Habitacion este = (Habitacion) list1Model.getElementAt(listHabs.getSelectedIndex());
					try {
						ArrayList<String> modos = DataBase.Modalidades(este.tipo);
						for (String mod:modos){
							list2Model.addElement(mod);
						}
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
			});
			listHabs.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return listHabs;
	}


	private JScrollPane getPnA�adido() {
		if (pnA�adido == null) {
			pnA�adido = new JScrollPane();
			pnA�adido.setBorder(new TitledBorder(null, "A\u00F1adido", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnA�adido.setBounds(10, 152, 466, 88);
			pnA�adido.setViewportView(getListA�adido());
		}
		return pnA�adido;
	}
	private JList<Habitacion> getListA�adido() {
		if (listA�adido == null) {
			list3Model = new DefaultListModel<Habitacion>();
			listA�adido = new JList<Habitacion>(list3Model);
		}
		return listA�adido;
	}

	
	private JButton getBtnA�adir() {
		if (btnA�adir == null) {
			btnA�adir = new JButton("A\u00F1adir");
			btnA�adir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (listHabs.isSelectionEmpty() ||listModalidad.isSelectionEmpty()) {
						JOptionPane.showMessageDialog(null, "Faltan datos en la seleccion");
					}
					else {
						Reserva reserva;
						try {
							reserva = new Reserva();
							Habitacion este = ((Habitacion) list1Model.getElementAt(listHabs.getSelectedIndex()));
							String mod = list2Model.getElementAt(listModalidad.getSelectedIndex()).toString();
							int ip = DataBase.pk_modalidad(mod, este);
							este.setModalidad(mod, ip);
							reserva.add(este);
							reserva.datos(padre.getCliente().getDni().toString());
							reservas.add(reserva);
							listModelReservas.addElement(este);
							list3Model.addElement(este);
							list1Model.remove(listHabs.getSelectedIndex());
							list2Model.clear();
							listModalidad.clearSelection();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}
			});
			btnA�adir.setBounds(516, 59, 89, 23);
		}
		return btnA�adir;
	}
	private JButton getBtnEliminar() {
		if (btnEliminar == null) {
			btnEliminar = new JButton("Eliminar");
			btnEliminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Habitacion esta = (Habitacion) list3Model.getElementAt(listA�adido.getSelectedIndex());
					esta.modalidad = "ninguna";
					reservas.remove(listA�adido.getSelectedIndex());
					list3Model.remove(listA�adido.getSelectedIndex());
					list1Model.addElement(esta);
				}
			});
			btnEliminar.setBounds(516, 190, 89, 23);
		}
		return btnEliminar;
	}
	private JButton getBtnCancelar() {
		if (btnCancelar == null){
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					int opcion = JOptionPane.showConfirmDialog(null, "Los cambios efectuados no se guardaran.\n�Desea continuar?", "Cambios no confirmados", JOptionPane.YES_NO_OPTION);
					if(opcion == 0){//YES
						dispose();
					}
				}
			});
			btnCancelar.setBounds(498, 265, 101, 23);
		}
		return btnCancelar;
	}
	private JButton getBtnConfirmar() {
		if (btnConfirmar == null) {
			btnConfirmar = new JButton("Confirmar");
			btnConfirmar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					padre.refrescar();
					for (int i=0; i<list3Model.getSize(); i++) {
						try {
							DataBase.a�adirReserva(id_reserva, list3Model.getElementAt(i));
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
					JOptionPane.showMessageDialog(null, "Cambios realizados correcamente");
					dispose();
				}
			});
			btnConfirmar.setBounds(387, 265, 101, 23);
		}
		return btnConfirmar;
	}
}
