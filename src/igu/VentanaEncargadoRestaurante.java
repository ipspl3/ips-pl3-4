package igu;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import logica.Conexion;

import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.border.TitledBorder;
import java.awt.CardLayout;

@SuppressWarnings("serial")
public class VentanaEncargadoRestaurante extends JFrame {

	private JPanel contentPane;
	private JPanel pnCards;
	private JButton btnAltaProducto;
	
	private CardRegistrarProducto vRP;
	
	private static Conexion conn;
	private JPanel pnUsuario;
	private JPanel pnAccBotones;
	private JPanel pnW;
	private JLabel lblNewLabel;
	private JLabel lblComo;
	private JLabel lblEncargadoRestaurante;
	private JPanel panel;
	private JPanel panelUnoCard;
	
	public CardLayout c1 = new CardLayout(0,0);



	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaEncargadoRestaurante frame = new VentanaEncargadoRestaurante();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public VentanaEncargadoRestaurante() throws SQLException {
		conn = new Conexion();
		conn.establecerConexion();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.add(getPnCards(), BorderLayout.CENTER);
		contentPane.add(getPnW(), BorderLayout.WEST);
		
		
		
	}
	private JPanel getPnCards() throws SQLException{
		if (pnCards == null) {
			pnCards = new JPanel();
			pnCards.setLayout(c1);
			pnCards.add(getPanelUnoCard(), "paneUno");
			pnCards.add(new CardRegistrarProducto(conn),"panelRegistrarProducto");
			
			c1.show(pnCards,"paneUno");
		}
		return pnCards;
	}
	private JButton getBtnAltaProducto() {
		if (btnAltaProducto == null) {
			btnAltaProducto = new JButton("Dar de alta producto");
			btnAltaProducto.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					c1.show(pnCards, "panelRegistrarProducto");
				}
			});
		}
		return btnAltaProducto;
	}


	private JPanel getPnUsuario() {
		if (pnUsuario == null) {
			pnUsuario = new JPanel();
			pnUsuario.setBorder(new TitledBorder(null, "Usuario", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnUsuario.setLayout(new BorderLayout(0, 0));
			pnUsuario.add(getLblNewLabel(), BorderLayout.NORTH);
			pnUsuario.add(getPanel(), BorderLayout.SOUTH);
		}
		return pnUsuario;
	}
	private JPanel getPnAccBotones() {
		if (pnAccBotones == null) {
			pnAccBotones = new JPanel();
			pnAccBotones.setBorder(new TitledBorder(null, "Acciones", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnAccBotones.add(getBtnAltaProducto());
		}
		return pnAccBotones;
	}
	private JPanel getPnW() {
		if (pnW == null) {
			pnW = new JPanel();
			pnW.setLayout(new BorderLayout(0, 0));
			pnW.add(getPnAccBotones(), BorderLayout.CENTER);
			pnW.add(getPnUsuario(), BorderLayout.NORTH);
		}
		return pnW;
	}
	private JLabel getLblNewLabel() {
		if (lblNewLabel == null) {
			lblNewLabel = new JLabel("---");
		}
		return lblNewLabel;
	}
	private JLabel getLblComo() {
		if (lblComo == null) {
			lblComo = new JLabel("Como:");
		}
		return lblComo;
	}
	private JLabel getLblEncargadoRestaurante() {
		if (lblEncargadoRestaurante == null) {
			lblEncargadoRestaurante = new JLabel("Encargado Restaurante");
		}
		return lblEncargadoRestaurante;
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.add(getLblComo());
			panel.add(getLblEncargadoRestaurante());
		}
		return panel;
	}
	private JPanel getPanelUnoCard() {
		if (panelUnoCard == null) {
			panelUnoCard = new JPanel();
			panelUnoCard.setLayout(null);
			panelUnoCard.setName("panelUno");
		}
		return panelUnoCard;
	}
}

/*
 * CREATE TABLE PRODUCTOS
(
    producto_nombre VARCHAR(20) not null,
    producto_tipo VARCHAR(9) not null,
    CONSTRAINT productos_pk PRIMARY KEY(producto_nombre)
);

CREATE TABLE PRODUCTOS_DISPONIBLES
(
    producto_id NUMERIC not null,
    producto_nombre  VARCHAR(20) not null,
    tipo VARCHAR(9) not null,
    precio DECIMAL not null,
    localizacion VARCHAR(12) not null,
    fecha DATE not null,
    descripcion VARCHAR(50) not null,
    CONSTRAINT productos_disponibles_pk PRIMARY KEY(producto_id),
    CONSTRAINT fk_productos
            FOREIGN KEY (producto_nombre)
            REFERENCES PRODUCTOS(producto_nombre)
);
 */
