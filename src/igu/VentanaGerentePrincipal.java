package igu;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import javax.swing.border.TitledBorder;

import logica.Conexion;
import logica.DataBase;

import javax.swing.JButton;
import java.awt.CardLayout;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.SwingConstants;

public class VentanaGerentePrincipal extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel pnUsuario;
	private JLabel label;
	private JLabel lblComo;
	private JLabel lblRecepcionista;
	private JPanel pnAccBotones;
	private JPanel pnCards;
	private JPanel panelUnoCard;
	public CardLayout card = new CardLayout(0,0);
	private static Conexion con;
	private JButton btnSuplementosDeAlojamiento;
	private JButton btnCrearPromocion;
	private JButton btnCrearSuplemento;
	private JButton btnConsultarPromocionO;
	private JButton btnTrabajadores;
	private JPanel panelElegirOpcionSuplementosModalidad;
	private JLabel lblquOperacinSuplementoModalidad;
	private JButton btnCrearNuevoSuplemento;
	private JButton btnModificarSuplemento;
	private JButton btnAsignarTarifaA;
	private CardSuplementosAlojamientoAsignarTarifa asignarTarifaSuplementosAlojamiento;

	private JButton btnHabitaciones;
	private CardGerenteHabitaciones cardTiposHab;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaGerentePrincipal frame = new VentanaGerentePrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public VentanaGerentePrincipal() throws SQLException {
		con = new Conexion();
		try {
			DataBase.Connect();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Base de datos no disponible.\nConsulte con un t�cnico.");
		}
		con.establecerConexion();
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaGerentePrincipal.class.getResource("/img/logo.jpg")));
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getPnUsuario());
		contentPane.add(getPnAccBotones());
		contentPane.add(getPanelCards());
		setLocationRelativeTo(null);
	}
	public Conexion getCon() {
		return con;
	}
	private JPanel getPnUsuario() {
		if (pnUsuario == null) {
			pnUsuario = new JPanel();
			pnUsuario.setBorder(new TitledBorder(null, "Usuario", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnUsuario.setForeground(Color.LIGHT_GRAY);
			pnUsuario.setBounds(10, 11, 212, 82);
			pnUsuario.setLayout(null);
			pnUsuario.add(getLabel());
			pnUsuario.add(getLblComo());
			pnUsuario.add(getLblRecepcionista());
		}
		return pnUsuario;
	}
	private JLabel getLabel() {
		if (label == null) {
			label = new JLabel("---");
			label.setBounds(22, 25, 168, 14);
		}
		return label;
	}
	private JLabel getLblComo() {
		if (lblComo == null) {
			lblComo = new JLabel("Como: ");
			lblComo.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblComo.setBounds(10, 60, 46, 14);
		}
		return lblComo;
	}
	private JLabel getLblRecepcionista() {
		if (lblRecepcionista == null) {
			lblRecepcionista = new JLabel("Gerente");
			lblRecepcionista.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblRecepcionista.setBounds(92, 57, 130, 23);
		}
		return lblRecepcionista;
	}
	private JPanel getPnAccBotones() {
		if (pnAccBotones == null) {
			pnAccBotones = new JPanel();
			pnAccBotones.setBounds(10, 95, 212, 542);
			pnAccBotones.setBorder(new TitledBorder(null, "Acciones", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnAccBotones.setLayout(new GridLayout(14, 1, 0, 0));
			pnAccBotones.add(getBtnHabitaciones());
//			pnAccBotones.add(getBtnTarifasTipo());
			pnAccBotones.add(getBtnSuplementosDeAlojamiento());
			pnAccBotones.add(getBtnCrearPromocion());
			pnAccBotones.add(getBtnCrearSuplemento());
			pnAccBotones.add(getBtnConsultarPromocionO());
//			pnAccBotones.add(getButton_1());
//			pnAccBotones.add(getBtnModificarHabitacion());
			pnAccBotones.add(getBtnTrabajadores());
		}
		return pnAccBotones;
	}

	private JPanel getPanelCards() throws SQLException {
		if (pnCards == null) {
			pnCards = new JPanel();
			pnCards.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnCards.setBounds(229, 11, 754, 626);
			pnCards.setLayout(card);
			pnCards.add(getUnoCard(), "panelUno");
			pnCards.add(new CardConsultarPromocionoSuplemento(con),"panelConsultaPoS");
			pnCards.add(new CardNuevaPromocion(con),"panelNuevaPromocion");
			pnCards.add(new CardNuevoSuplemento(con),"panelNuevoSuplemento");
			//pnCards.add(new CardRegistrarHabitacion(con,this),"panelNuevaHabitacion");
			//pnCards.add(new CardModificarHabitacion(con,this),"panelModificarHabitacion");
			
			pnCards.add(new CardSuplementosAlojamientoNuevoSuplemento(con), "panelNuevoSuplementoAlojamiento");
			pnCards.add(new CardTrabajadoresConsultarTurnosGerente(con, this), "panelEstadoTrabajadores");
			cardTiposHab = new CardGerenteHabitaciones(this);
			pnCards.add(cardTiposHab, "panelTarifasTipo");
		
			card.show(pnCards, "Inicial");
			asignarTarifaSuplementosAlojamiento = new CardSuplementosAlojamientoAsignarTarifa(con);
			pnCards.add(getPanelElegirOpcionSuplementosModalidad(), "panelElegirOpcionSuplementosModalidad");
			pnCards.add(new CardSuplementosAlojamientoModificacion(con), "panelModificarSuplementosModalidad");
			pnCards.add(asignarTarifaSuplementosAlojamiento, "panelAsignarTarifaSuplementosModalidad");
			
			
		}
		return pnCards;
	}
	
	public void mostrarPanelInicial() {
		card.show(pnCards, "panelUno");
	}

	private JPanel getUnoCard() {
		if (panelUnoCard == null) {
			panelUnoCard = new JPanel();
			panelUnoCard.setName("panelUno");
			panelUnoCard.setLayout(null);
		}
		return panelUnoCard;
	}

	void iniciar(){
		card.show(pnCards, "panelUno");
	}
	private JButton getBtnSuplementosDeAlojamiento() {
		if (btnSuplementosDeAlojamiento == null) {
			btnSuplementosDeAlojamiento = new JButton("Suplementos de alojamiento");
			btnSuplementosDeAlojamiento.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					card.show(pnCards, "panelElegirOpcionSuplementosModalidad");
				}
			});
			btnSuplementosDeAlojamiento.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return btnSuplementosDeAlojamiento;
	}
	private JButton getBtnCrearPromocion() {
		if (btnCrearPromocion == null) {
			btnCrearPromocion = new JButton("Crear promocion");
			btnCrearPromocion.setFont(new Font("Tahoma", Font.PLAIN, 12));
			btnCrearPromocion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					card.show(pnCards, "panelNuevaPromocion");
					
				}
			});
		}
		return btnCrearPromocion;
	}
	private JButton getBtnCrearSuplemento() {
		if (btnCrearSuplemento == null) {
			btnCrearSuplemento = new JButton("Crear suplemento");
			btnCrearSuplemento.setFont(new Font("Tahoma", Font.PLAIN, 12));
			btnCrearSuplemento.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					card.show(pnCards, "panelNuevoSuplemento");

				}
			});
		}
		return btnCrearSuplemento;
	}
	private JButton getBtnConsultarPromocionO() {
		if (btnConsultarPromocionO == null) {
			btnConsultarPromocionO = new JButton("Consultar promocion\n"+"o suplemento");
			btnConsultarPromocionO.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					card.show(pnCards, "panelConsultaPoS");

				}
			});
			btnConsultarPromocionO.setPreferredSize(new Dimension(215, 29));
			btnConsultarPromocionO.setFont(new Font("Tahoma", Font.PLAIN, 11));
		}
		return btnConsultarPromocionO;
	}
//	private JButton getButton_1() {
//		if (btnDarDeAlta == null) {
//			btnDarDeAlta = new JButton("Dar de alta habitacion");
//			btnDarDeAlta.addActionListener(new ActionListener() {
//				public void actionPerformed(ActionEvent e) {
//					card.show(pnCards, "panelNuevaHabitacion");
//				}
//			});
//		}
//		return btnDarDeAlta;
//	}
//	private JButton getBtnModificarHabitacion() {
//		if (btnModificarHabitacion == null) {
//			btnModificarHabitacion = new JButton("Modificar habitacion");
//			btnModificarHabitacion.addActionListener(new ActionListener() {
//				public void actionPerformed(ActionEvent e) {
//					card.show(pnCards, "panelModificarHabitacion");
//				}
//			});
//		}
//		return btnModificarHabitacion;
//	}
	private JButton getBtnTrabajadores() {
		if (btnTrabajadores == null) {
			btnTrabajadores = new JButton("Gesti\u00F3n trabajadores");
			btnTrabajadores.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					card.show(pnCards, "panelEstadoTrabajadores");
					
				}
			});
		}
		return btnTrabajadores;
	}
	private JPanel getPanelElegirOpcionSuplementosModalidad() {
		if (panelElegirOpcionSuplementosModalidad == null) {
			panelElegirOpcionSuplementosModalidad = new JPanel();
			panelElegirOpcionSuplementosModalidad.setBorder(new TitledBorder(null, "Suplementos por modalidad de alojamiento", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelElegirOpcionSuplementosModalidad.setLayout(null);
			panelElegirOpcionSuplementosModalidad.add(getLblquOperacinSuplementoModalidad());
			panelElegirOpcionSuplementosModalidad.add(getBtnCrearNuevoSuplemento());
			panelElegirOpcionSuplementosModalidad.add(getBtnModificarSuplemento());
			panelElegirOpcionSuplementosModalidad.add(getBtnAsignarTarifaA());
		}
		return panelElegirOpcionSuplementosModalidad;
	}
	private JLabel getLblquOperacinSuplementoModalidad() {
		if (lblquOperacinSuplementoModalidad == null) {
			lblquOperacinSuplementoModalidad = new JLabel("\u00BFQu\u00E9 operaci\u00F3n desea realizar?");
			lblquOperacinSuplementoModalidad.setHorizontalAlignment(SwingConstants.CENTER);
			lblquOperacinSuplementoModalidad.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
			lblquOperacinSuplementoModalidad.setBounds(166, 100, 418, 83);
		}
		return lblquOperacinSuplementoModalidad;
	}
	private JButton getBtnCrearNuevoSuplemento() {
		if (btnCrearNuevoSuplemento == null) {
			btnCrearNuevoSuplemento = new JButton("Crear nuevo suplemento");
			btnCrearNuevoSuplemento.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					card.show(pnCards, "panelNuevoSuplementoAlojamiento");
				}
			});
			btnCrearNuevoSuplemento.setBounds(209, 190, 331, 42);
		}
		return btnCrearNuevoSuplemento;
	}
	private JButton getBtnModificarSuplemento() {
		if (btnModificarSuplemento == null) {
			btnModificarSuplemento = new JButton("Modificar suplemento");
			btnModificarSuplemento.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					card.show(pnCards, "panelModificarSuplementosModalidad");  
				}
			});
			btnModificarSuplemento.setBounds(209, 244, 331, 42);
		}
		return btnModificarSuplemento;
	}
	private JButton getBtnAsignarTarifaA() {
		if (btnAsignarTarifaA == null) {
			btnAsignarTarifaA = new JButton("Asignar tarifa a suplemento");
			btnAsignarTarifaA.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					asignarTarifaSuplementosAlojamiento.rellenarTablaSuplementos();
					card.show(pnCards, "panelAsignarTarifaSuplementosModalidad");
					
				}
			});
			btnAsignarTarifaA.setBounds(209, 298, 331, 42);
		}
		return btnAsignarTarifaA;
	}
	private JButton getBtnHabitaciones() {
		if (btnHabitaciones == null) {
			btnHabitaciones = new JButton("Habitaciones");
			btnHabitaciones.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cardTiposHab.getPanelSuperCard();
					card.show(pnCards, "panelTarifasTipo");	
				}
			});
			btnHabitaciones.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return btnHabitaciones;
	}
}