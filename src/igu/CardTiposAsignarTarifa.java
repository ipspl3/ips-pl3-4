package igu;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

import logica.Conexion;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;


import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import java.util.Date;
import java.awt.Color;

import javax.swing.JTextField;

import com.toedter.calendar.JDateChooser;


import javax.swing.JComboBox;

public class CardTiposAsignarTarifa extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Connection con;
	private String[] cabecera = {"TIPO DE HABITACI�N", "IMPORTE POR D�A", "FECHA DE ALTA", "FECHA DE BAJA"};
	private JButton btnA�adir;
	private JPanel panelAsignarTarifa;
	private JLabel lblTipos;
	private JLabel lblImporte;
	private JTextField tfImporte;
	private JDateChooser dateChFechaInicio;
	private JDateChooser dateChFechaFin;
	private JLabel lblFechaInicio;
	private JLabel lblFechaFin;
	private String tipoSeleccionado;
	private JComboBox<String> cbTipoHab;
	private JButton btnAtras;
	private JScrollPane scTablaTarifasError;
	private JTable tablaTarifasError;
	private JLabel lblEstasSonLa;
	private JLabel lblTipoError;
	private CardGerenteHabitaciones padre;
	//PARA QUE SE VEAN LOS CARD EN EL DESIGN HAY QUE COMENTAR LA LINEA superCard.show(panelSuperCard, "panelElegirAccion"); DEL METODO GETPANELSUPERCARD();

	
	public class MiModeloTabla extends DefaultTableModel{
		private static final long serialVersionUID = 1L;
		public MiModeloTabla(Object[] columnNames, int numFilas) {
			super(columnNames,numFilas);
		}
		
		public boolean isCellEditable(int numFila, int numColumna){
			return false;
		}
	}
	

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public CardTiposAsignarTarifa(Conexion conexion, CardGerenteHabitaciones padre){
		this.padre = padre;
		this.con = conexion.getCon();
		setBounds(100, 100, 755, 626);
		setLayout(new BorderLayout(0, 0));
		add(getPanelAsignarTarifa(), BorderLayout.CENTER);
		rellenarTablaTarifas();
		setVisible(true);
		
	}
	
	public void rellenarTablaTarifas(){
		try {
			MiModeloTabla miModelo = new MiModeloTabla(cabecera, 0);
			
			Statement s = con.createStatement();
			
			ResultSet rs = s.executeQuery("select t.nombre, pt.importe_dia, pt.fecha_inicio, pt.fecha_fin "
					+ "from tipo_habitacion t, precio_tipo pt "
					+ "where t.nombre = pt.nombre order by pt.fecha_inicio desc");
	
			
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			
			while(rs.next()){
				Object[] fila = new Object[4];
				fila[0] = rs.getString("nombre");
				fila[1] = rs.getFloat("importe_dia");
				fila[2] = formato.format(rs.getDate("fecha_inicio"));
				fila[3] = formato.format(rs.getDate("fecha_fin"));
				
				miModelo.addRow(fila);
			}
			rs.close();
			s.close();
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
	}
	


	
	private void resetCampos(){
		tipoSeleccionado = null;
		cbTipoHab.setSelectedItem(null);
		tfImporte.setText(null);
		tfImporte.setBorder(new LineBorder(Color.BLACK));
		dateChFechaInicio.setDate(null);
		dateChFechaFin.setDate(null);
		scTablaTarifasError.setVisible(false);
		lblEstasSonLa.setVisible(false);
		lblTipoError.setVisible(false);
	}
	private boolean noHayCamposEnBlancoPanelAsignarTarifa() {
		return tfImporte.getText() != null && cbTipoHab.getSelectedItem()!= null && dateChFechaInicio.getDate() != null && dateChFechaFin.getDate() != null;
	}
	private JButton getBtnA�adir() {
		if (btnA�adir == null) {
			btnA�adir = new JButton("A\u00F1adir");
			btnA�adir.setBounds(604, 214, 100, 36);
			btnA�adir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(noHayCamposEnBlancoPanelAsignarTarifa()) {
						if(comprobarFechas(dateChFechaInicio, dateChFechaFin) && comprobarImporte()){
							tfImporte.setBorder(new LineBorder(Color.GREEN));
							try{
								float nuevoImporte = Float.parseFloat(tfImporte.getText());	
								
								if(!existeTarifa(tipoSeleccionado)){
									int opcion = JOptionPane.showConfirmDialog(null, "�Desea confirmar los cambios?", "Confirmar cambios", JOptionPane.YES_NO_OPTION);
									if(opcion == 0)//YES
										insertar(tipoSeleccionado, nuevoImporte);
								}
								else{
									scTablaTarifasError.setVisible(true);
									rellenarTablaTarifasError();
									lblEstasSonLa.setVisible(true);
									lblTipoError.setText(tipoSeleccionado);
									lblTipoError.setVisible(true);
									JOptionPane.showMessageDialog(null, "El error puede ser debido a :\n"
											+ "-Tipo de tarifa ya existente\n-No pueden existir dos tarifas cuyo tipo "
											+ "de habitaci�n sea el mismo y cuyas fechas sean conflictivas", "Error",JOptionPane.ERROR_MESSAGE);
								}	
							}
							catch(Exception e){
								e.printStackTrace();
							}
						}
					}
					else
						JOptionPane.showMessageDialog(null, "Faltan datos", "Error", JOptionPane.ERROR_MESSAGE);
						
				}
			});
			btnA�adir.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return btnA�adir;
	}
	private JPanel getPanelAsignarTarifa() {
		if (panelAsignarTarifa == null) {
			panelAsignarTarifa = new JPanel();
			panelAsignarTarifa.setBorder(new TitledBorder(null, "Asignar tarifa", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelAsignarTarifa.setName("panelNuevoTipo");
			panelAsignarTarifa.setLayout(null);
			panelAsignarTarifa.add(getLblNuevaTarifa());
			panelAsignarTarifa.add(getLblImporte());
			panelAsignarTarifa.add(getTxfImporte());
			panelAsignarTarifa.add(getDateChFechaInicio());
			panelAsignarTarifa.add(getLblFechaInicio());
			panelAsignarTarifa.add(getDateChFechaFin());
			panelAsignarTarifa.add(getLblFechaFin());
			panelAsignarTarifa.add(getCbTiposHab());
			panelAsignarTarifa.add(getBtnA�adir());
			panelAsignarTarifa.add(getBtnAtras());
			panelAsignarTarifa.add(getScTablaTarifasError());
			panelAsignarTarifa.add(getLblEstasSonLa());
			panelAsignarTarifa.add(getLblTipoError());
		}
		return panelAsignarTarifa;
	}
	
	private JLabel getLblNuevaTarifa() {
		if (lblTipos == null) {
			lblTipos = new JLabel("Tipo de habitaci\u00F3n:");
			lblTipos.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblTipos.setBounds(154, 36, 116, 26);
		}
		return lblTipos;
	}
	private JLabel getLblImporte() {
		if (lblImporte == null) {
			lblImporte = new JLabel("Importe:");
			lblImporte.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblImporte.setBounds(399, 35, 127, 26);
		}
		return lblImporte;
	}
	private JTextField getTxfImporte() {
		if (tfImporte == null) {
			tfImporte = new JTextField();
			tfImporte.setFont(new Font("Tahoma", Font.PLAIN, 14));
			tfImporte.setBounds(399, 62, 100, 32);
			tfImporte.setColumns(10);
		}
		return tfImporte;
	}
	private JDateChooser getDateChFechaInicio() {
		if (dateChFechaInicio == null) {
			dateChFechaInicio = new JDateChooser();
			dateChFechaInicio.getCalendarButton().setFont(new Font("Tahoma", Font.PLAIN, 12));
			dateChFechaInicio.setFont(new Font("Tahoma", Font.PLAIN, 12));
			dateChFechaInicio.setBounds(154, 140, 138, 32);
			dateChFechaInicio.setVisible(true);
			dateChFechaInicio.setMinSelectableDate(new Date());
		}
		return dateChFechaInicio;
	}
	private JLabel getLblFechaInicio() {
		if (lblFechaInicio == null) {
			lblFechaInicio = new JLabel("Fecha inicio:");
			lblFechaInicio.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblFechaInicio.setBounds(154, 114, 155, 26);
		}
		return lblFechaInicio;
	}
	private JDateChooser getDateChFechaFin() {
		if (dateChFechaFin == null) {
			dateChFechaFin = new JDateChooser();
			dateChFechaFin.getCalendarButton().setFont(new Font("Tahoma", Font.PLAIN, 12));
			dateChFechaFin.setFont(new Font("Tahoma", Font.PLAIN, 12));
			dateChFechaFin.setBounds(399, 140, 132, 32);
			dateChFechaFin.setVisible(true);
			dateChFechaFin.setMinSelectableDate(new Date());
		}
		return dateChFechaFin;
	}
	private JLabel getLblFechaFin() {
		if (lblFechaFin == null) {
			lblFechaFin = new JLabel("Fecha fin:");
			lblFechaFin.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblFechaFin.setBounds(399, 114, 135, 26);
		}
		return lblFechaFin;
	}
	
	private boolean existeTarifa(String nuevoTipo) throws SQLException{		

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
		String fecha_ini= sdf.format(new java.sql.Date(dateChFechaInicio.getDate().getTime()));
		
		String fecha_fin= sdf.format(new java.sql.Date(dateChFechaFin.getDate().getTime()));
		
		String consulta = "select count(pt.nombre) "
				+ "from precio_tipo pt "
				+ "where ((to_date(?,'dd-MM-YYYY') between pt.fecha_inicio "
				+ "and pt.fecha_fin) or (to_date(?,'dd-MM-YYYY') between "
				+ "pt.fecha_inicio and pt.fecha_fin)) and pt.nombre = ?";
		PreparedStatement ps = con.prepareStatement(consulta);
		ps.setString(1, fecha_ini);
		ps.setString(2, fecha_fin);
		ps.setString(3, nuevoTipo);
		ResultSet rs = ps.executeQuery();
			
		rs.next();
		int existeTarifa = rs.getInt("COUNT(PT.NOMBRE)");
		rs.close();
		ps.close();
		return existeTarifa>0;
	}
	
	private void insertar(String nuevoTipo, float nuevoImporte){
		
		try {
			PreparedStatement psPrecio = con.
					prepareStatement("insert into precio_tipo values(sec_preciotipo.nextVal, to_date(?), to_date(?), ?, ?)");
			psPrecio.setDate(1, new java.sql.Date(dateChFechaInicio.getDate().getTime()));
			psPrecio.setDate(2, new java.sql.Date(dateChFechaFin.getDate().getTime()));
			psPrecio.setFloat(3, nuevoImporte);
			psPrecio.setString(4, nuevoTipo);
			
			int insertado = psPrecio.executeUpdate();
			psPrecio.close();
			if(insertado == 1) {
				JOptionPane.showMessageDialog(null, "Tarifa insertada correctamente", "OK", JOptionPane.INFORMATION_MESSAGE); 
				resetCampos();
				scTablaTarifasError.setVisible(false);
				lblEstasSonLa.setVisible(false);
				lblTipoError.setVisible(false);
			}
		}
		catch(SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Error al insertar", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void rellenarTablaTarifasError() {
		try {
			String consulta = "select pt.nombre, pt.importe_dia, pt.fecha_inicio, pt.fecha_fin " + 
					"from precio_tipo pt, tipo_habitacion t " + 
					"where t.nombre= ? and pt.nombre = t.nombre order by pt.fecha_inicio desc";
			
			PreparedStatement ps= con.prepareStatement(consulta);
			ps.setString(1, tipoSeleccionado);
			ResultSet rs = ps.executeQuery();
			
			MiModeloTabla modelo = new MiModeloTabla(cabecera, 0);
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
			
			
			while(rs.next()) {
				Object[] fila = new Object[4];
				fila[0] = rs.getString("NOMBRE");
				fila[1] = rs.getFloat("IMPORTE_DIA");
				fila[2] = sdf.format(rs.getDate("FECHA_INICIO"));
				fila[3] = sdf.format(rs.getDate("FECHA_FIN"));
				modelo.addRow(fila);
			}
			
			tablaTarifasError.setModel(modelo);
			tablaTarifasError.repaint();
			
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
	}

	
	private boolean comprobarImporte(){
		String importe = tfImporte.getText();
		try{
			Double.parseDouble(importe);
			return true;
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "Formato de importe incorrecto", "Error", JOptionPane.ERROR_MESSAGE);
			tfImporte.setBorder(new LineBorder(Color.RED));
			return false;
		}
	}
	
	private boolean comprobarFechas(JDateChooser fechaInicio, JDateChooser fechaFin){
		try{
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			Date fecha_inicio = formato.parse(formato.format(fechaInicio.getDate()));
			Date fecha_fin = formato.parse(formato.format(fechaFin.getDate()));

			Date fecha_acc = formato.parse(formato.format(new Date()));
			
			if(fecha_inicio.compareTo(fecha_acc) >= 0){
				if(fecha_inicio.compareTo(fecha_fin) > 0){
					JOptionPane.showMessageDialog(null, "La fecha de fin debe de ser mayor que la fecha de inicio", "Error", JOptionPane.ERROR_MESSAGE);
					return false;
				}
				else 
					return true;
			}
			else{
				JOptionPane.showMessageDialog(null, "La fecha de inicio debe ser mayor que la fecha actual del sistema", "Error", JOptionPane.ERROR_MESSAGE);
				return false;
			}
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "Fechas incorrectas", "Error" , JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}
	

	private JComboBox<String> getCbTiposHab() {
		if (cbTipoHab == null) {
			cbTipoHab = new JComboBox<String>();
			cbTipoHab.setFont(new Font("Tahoma", Font.PLAIN, 12));
			cbTipoHab.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					tipoSeleccionado = (String)cbTipoHab.getSelectedItem();
				}
			});
			cbTipoHab.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					String tipo = tipoSeleccionado;
					cbTipoHab.removeAllItems();
					rellenarTiposHab();
					if(tipo!= null)
						cbTipoHab.setSelectedItem(tipo);
					else
						cbTipoHab.setSelectedItem(null);
					cbTipoHab.repaint();
				}
			});
			cbTipoHab.setBounds(154, 62, 143, 32);
		}
		return cbTipoHab;
	}
	
	
	private void rellenarTiposHab(){
		try{
			Statement s = con.createStatement();	
			ResultSet rs = s.executeQuery("select t.nombre from tipo_habitacion t");
			
			while(rs.next()){
				cbTipoHab.addItem(rs.getString("NOMBRE"));
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	private JButton getBtnAtras() {
		if (btnAtras == null) {
			btnAtras = new JButton("Atr\u00E1s");
			btnAtras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int opcion = JOptionPane.showConfirmDialog(null, "�Atenci�n! Si continua se perder�n los datos y no se realizar� ninguna operaci�n\n "
							+ "�Desea continuar con la cancelaci�n?", "Cancelaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.CANCEL_OPTION);
					if(opcion==0) {
						resetCampos();
						padre.mostrarOpciones();
					}
					
				}
			});
			btnAtras.setBounds(506, 214, 86, 36);
		}
		return btnAtras;
	}

	
	private JScrollPane getScTablaTarifasError() {
		if (scTablaTarifasError == null) {
			scTablaTarifasError = new JScrollPane();
			scTablaTarifasError.setBorder(new LineBorder(Color.RED, 3));
			scTablaTarifasError.setBounds(17, 297, 697, 287);
			scTablaTarifasError.setViewportView(getTablaTarifasError());
			scTablaTarifasError.setVisible(false);
		}
		return scTablaTarifasError;
	}
	private JTable getTablaTarifasError() {
		if (tablaTarifasError == null) {
			tablaTarifasError = new JTable();
			tablaTarifasError.setVisible(true);
		}
		return tablaTarifasError;
	}
	private JLabel getLblEstasSonLa() {
		if (lblEstasSonLa == null) {
			lblEstasSonLa = new JLabel("Estas son la tarifas actuales asociadas al tipo de habitaci\u00F3n ");
			lblEstasSonLa.setForeground(Color.RED);
			lblEstasSonLa.setBounds(17, 269, 388, 26);
			lblEstasSonLa.setVisible(false);
		}
		return lblEstasSonLa;
	}
	private JLabel getLblTipoError() {
		if (lblTipoError == null) {
			lblTipoError = new JLabel("");
			lblTipoError.setForeground(Color.RED);
			lblTipoError.setFont(new Font("Lucida Grande", Font.ITALIC, 13));
			lblTipoError.setBounds(399, 269, 100, 26);
			lblTipoError.setVisible(false);
		}
		return lblTipoError;
	}
}
