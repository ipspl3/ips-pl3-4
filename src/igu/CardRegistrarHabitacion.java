package igu;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JPanel;

import logica.Conexion;
import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;

import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.FlowLayout;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.awt.Dimension;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

public class CardRegistrarHabitacion extends JPanel {

	/**
	 * Create the panel.
	 */
	private static Conexion conn;
	private CardGerenteHabitaciones padre;
	private static PreparedStatement stmt;
	private JLabel lblDarDeAlta;
	private JPanel pnCentral;
	private JPanel pnSouth;
	private JButton btnRegistrar;
	private JPanel pn1;
	private JPanel pn2;
	private JPanel pn3;
	private JLabel lblNumero;
	private JTextField txtNumero;
	private JLabel lblPlanta;
	private JTextField txtPlanta;
	private JLabel lblTipo;
	private JComboBox cbtipo;
	private JLabel lblComentario;
	private JButton btnSeleccionarImagen;
	private VentanaGerentePrincipal vGp;

	private String pathImagen = "";
	private JScrollPane scrollPane;
	private JPanel panel;
	private JPanel panel_1;
	private JTextArea textArea;

	private ArrayList<JLabel> imagenes = new ArrayList<JLabel>();
	private JButton btnAtras;

	public CardRegistrarHabitacion(Conexion con, VentanaGerentePrincipal vgp, CardGerenteHabitaciones papa) {
		padre = papa;
		conn = con;
		vGp = vgp;
		setLayout(new BorderLayout(0, 0));
		add(getLblDarDeAlta(), BorderLayout.NORTH);
		add(getPnCentral(), BorderLayout.CENTER);
		add(getPnSouth(), BorderLayout.SOUTH);

		setVisible(true);
	}

	private JLabel getLblDarDeAlta() {
		if (lblDarDeAlta == null) {
			lblDarDeAlta = new JLabel("Dar de alta habitaci\u00F3n");
			lblDarDeAlta.setFont(new Font("Lucida Grande", Font.PLAIN, 19));
		}
		return lblDarDeAlta;
	}

	private JPanel getPnCentral() {
		if (pnCentral == null) {
			pnCentral = new JPanel();
			pnCentral.setLayout(new GridLayout(0, 1, 0, 0));
			pnCentral.add(getPn1());
			pnCentral.add(getPn2());
			pnCentral.add(getPn3());
		}
		return pnCentral;
	}

	private JPanel getPnSouth() {
		if (pnSouth == null) {
			pnSouth = new JPanel();
			pnSouth.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			pnSouth.add(getBtnRegistrar());
			pnSouth.add(getBtnAtras());
		}
		return pnSouth;
	}

	private JButton getBtnRegistrar() {
		if (btnRegistrar == null) {
			btnRegistrar = new JButton("Registrar");
			btnRegistrar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (getTxtNumero().getText().equals("") || getTxtPlanta().getText().equals("")) {
						JOptionPane.showMessageDialog(null, "Se han encontrado campos vaci�os.");
					} else {
						try {
							PreparedStatement s = conn.getCon()
									.prepareStatement("SELECT count(*) FROM HABITACION " + "WHERE PK_HABITACION=?");
							s.setString(1, getTxtNumero().getText());
							int numhabitacion = 0;
							ResultSet rs = s.executeQuery();
							while (rs.next()) {
								numhabitacion = rs.getInt(1);
							}
							s.close();
							if (numhabitacion > 0) {
								JOptionPane.showMessageDialog(null, "Numero habiitacion repetido.");
							} else {
								try {
									stmt = conn.getCon()
											.prepareStatement("INSERT INTO HABITACION VALUES (?,?,?,?,?,?)");
									stmt.setString(1, getTxtNumero().getText());
									stmt.setString(2, getTxtPlanta().getText());
									stmt.setString(3, getCbtipo().getSelectedItem().toString());
									stmt.setString(4, "libre");
									if (getTextArea().getText().equals("")) {
										stmt.setNull(5, java.sql.Types.VARCHAR);
									} else {
										stmt.setString(5, getTextArea().getText().toString());
									}
									if (pathImagen.equals("")) {
										stmt.setNull(6, java.sql.Types.VARCHAR);
									} else {
										stmt.setString(6, pathImagen);
									}
									stmt.executeUpdate();
									stmt.close();
									reiniciarPantalla();
									JOptionPane.showMessageDialog(null, "Habitacion a�adida correctament");

								} catch (SQLException e1) {
									e1.printStackTrace();
								}
							}
						} catch (SQLException e2) {
							e2.printStackTrace();
						}

					}
				}

			});
		}
		return btnRegistrar;
	}

	private void reiniciarPantalla() {
		getTxtNumero().setText("");
		getTxtPlanta().setText("");
		getTextArea().setText("");
		getCbtipo().setSelectedIndex(0);
		eliminarImagenes();
	}

	private JPanel getPn1() {
		if (pn1 == null) {
			pn1 = new JPanel();
			pn1.setLayout(new BorderLayout(0, 0));
			pn1.add(getPanel(), BorderLayout.NORTH);
			pn1.add(getPanel_1(), BorderLayout.SOUTH);
		}
		return pn1;
	}

	private JPanel getPn2() {
		if (pn2 == null) {
			pn2 = new JPanel();
			pn2.setLayout(null);
			pn2.add(getLblComentario());
			pn2.add(getScrollPane());
			pn2.add(getBtnSeleccionarImagen());
		}
		return pn2;
	}

	private JPanel getPn3() {
		if (pn3 == null) {
			pn3 = new JPanel();
		}
		return pn3;
	}

	private JLabel getLblNumero() {
		if (lblNumero == null) {
			lblNumero = new JLabel("Numero:");
		}
		return lblNumero;
	}

	private JTextField getTxtNumero() {
		if (txtNumero == null) {
			txtNumero = new JTextField();
			txtNumero.setColumns(10);
		}
		return txtNumero;
	}

	private JLabel getLblPlanta() {
		if (lblPlanta == null) {
			lblPlanta = new JLabel("Planta:");
		}
		return lblPlanta;
	}

	private JTextField getTxtPlanta() {
		if (txtPlanta == null) {
			txtPlanta = new JTextField();
			txtPlanta.setColumns(10);
		}
		return txtPlanta;
	}

	private JLabel getLblTipo() {
		if (lblTipo == null) {
			lblTipo = new JLabel("Tipo:");
		}
		return lblTipo;
	}

	private JComboBox getCbtipo() {
		if (cbtipo == null) {
			cbtipo = new JComboBox();
			String[] tipohabita = null;
			try {
				Statement tipohabitacion = conn.getCon().createStatement();
				ResultSet numhabitacion = tipohabitacion.executeQuery("SELECT COUNT(*) FROM TIPO_HABITACION");
				while (numhabitacion.next()) {
					tipohabita = new String[numhabitacion.getInt(1)];

				}
				int count = 0;
				ResultSet rs = tipohabitacion.executeQuery("SELECT NOMBRE FROM TIPO_HABITACION");
				while (rs.next()) {
					tipohabita[count] = rs.getString(1);
					count++;
				}
				tipohabitacion.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

			cbtipo.setModel(new DefaultComboBoxModel(tipohabita));
		}
		return cbtipo;
	}

	private JLabel getLblComentario() {
		if (lblComentario == null) {
			lblComentario = new JLabel("Comentario:");
			lblComentario.setBounds(22, 33, 77, 16);
		}
		return lblComentario;
	}

	private JButton getBtnSeleccionarImagen() {
		if (btnSeleccionarImagen == null) {
			btnSeleccionarImagen = new JButton("Seleccionar imagen");
			btnSeleccionarImagen.setBounds(93, 84, 165, 29);
			btnSeleccionarImagen.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (getPn3().getComponentCount() > 0) {
						eliminarImagenes();
					}

					FileNameExtensionFilter filter = new FileNameExtensionFilter("Imagenes", "jpg", "jpeg", "png");
					File dir = new File("./src/img/habitaciones");
					JFileChooser filechoose = new JFileChooser();
					filechoose.setMultiSelectionEnabled(true);
					filechoose.setCurrentDirectory(dir);
					filechoose.setFileFilter(filter);

					int opcion = filechoose.showOpenDialog((Component) null);
					if (opcion == JFileChooser.APPROVE_OPTION) {
						String ruta = "";
						File[] files = filechoose.getSelectedFiles();
						for (int i = 0; i < files.length; i++) {
							JLabel lblI = new JLabel("");
							getPn3().add(lblI);
							getPn3().setVisible(true);
							String fotoruta = "./src/img/habitaciones/" + files[i].getName();
							ruta = ruta + "./src/img/habitaciones/" + files[i].getName() + "@";
							ImageIcon iconhabitacion = new ImageIcon(fotoruta);
							lblI.setSize(new Dimension(150, 100));
							Icon icono = new ImageIcon(iconhabitacion.getImage().getScaledInstance(lblI.getWidth(),
									lblI.getHeight(), Image.SCALE_SMOOTH));
							lblI.setIcon(icono);
							lblI.setVisible(true);
							imagenes.add(lblI);

						}
						pathImagen = ruta;
					}
				}
			});
		}
		return btnSeleccionarImagen;
	}
	private void eliminarImagenes()
	{
		for (JLabel l : imagenes) {
			l.setVisible(false);
			getPn3().remove(l);
		}
		imagenes.clear();
		getPn3().removeAll();
		pathImagen="";
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(111, 6, 318, 70);
			scrollPane.setViewportView(getTextArea());
		}
		return scrollPane;
	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.add(getLblNumero());
			panel.add(getTxtNumero());
			panel.add(getLblPlanta());
			panel.add(getTxtPlanta());
		}
		return panel;
	}

	private JPanel getPanel_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.add(getLblTipo());
			panel_1.add(getCbtipo());
		}
		return panel_1;
	}

	private JTextArea getTextArea() {
		if (textArea == null) {
			textArea = new JTextArea();
		}
		return textArea;
	}

	private JButton getBtnAtras() {
		if (btnAtras == null) {
			btnAtras = new JButton("Atras");
			btnAtras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					padre.getSuperCard().show(padre.getPanelSuperCard(), "panelElegirAccion");
					reiniciarPantalla();
				}
			});
		}
		return btnAtras;
	}
}
