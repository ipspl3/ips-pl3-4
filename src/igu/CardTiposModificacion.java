package igu;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;


import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import java.util.Date;

import java.awt.Color;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;
import javax.swing.border.EtchedBorder;

public class CardTiposModificacion extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Connection con;
	private JPanel contentPane;
	private String[] cabecera = {"TIPO DE HABITACIÓN", "IMPORTE POR DÍA", "FECHA DE ALTA", "FECHA DE BAJA"};
	private int idPrecioTipo;
	private JPanel panelModificacion;
	private JLabel lblSeleccioneElSuplemento;
	private JLabel lblLogoInfo;
	private JTextArea txtInfo;
	private JScrollPane spTablaModificacion;
	private JTable tablaModificacion;
	private JPanel panelDatosModificacion;
	private JLabel lblNuevoNombreDelTipo;
	private JTextField tfNuevoNombreDelTipo;
	private JLabel lblNuevoImporte;
	private JTextField tfNuevoImporte;
	private JDateChooser dcFechaInicioModificacion;
	private JLabel lblNuevaFechaInicio;
	private JLabel lblNuevaFechaFinal;
	private JDateChooser dcFechaFinModificacion;
	private JButton btnModificar;
	private String antiguoNombreTipoHab;
	private Float antiguoImporte;
	private long antiguaFechaInicio;
	private long antiguaFechaFin;
	private JButton btnRefrescar;
	private JButton btnCancelar;
	private JButton btnAtrs;
	private CardGerenteHabitaciones padre;

	
	public class MiModeloTabla extends DefaultTableModel{
		private static final long serialVersionUID = 1L;
		public MiModeloTabla(Object[] columnNames, int numFilas) {
			super(columnNames,numFilas);
		}
		
		public boolean isCellEditable(int numFila, int numColumna){
			return false;
		}
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public CardTiposModificacion(Connection conexion, CardGerenteHabitaciones padre){
		this.con = conexion;
		this.padre = padre; 
		setBounds(100, 100,  755, 626);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setLayout(new BorderLayout(0, 0));
		add(getPanelModificacion(), BorderLayout.CENTER);
		rellenarTabla();
		setVisible(true);
	}
	
	private void rellenarTabla(){
		
		try {
			MiModeloTabla miModelo = new MiModeloTabla(cabecera, 0);
			
			String consulta = "select distinct nombre, importe_dia, fecha_inicio, fecha_fin " + 
					"from precio_tipo " + 
					"where fecha_inicio >= to_date(?, 'dd-MM-YYYY') order by fecha_fin desc";
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
			
			PreparedStatement ps = con.prepareStatement(consulta);
			ps.setString(1, sdf.format(new Date().getTime()));
			ResultSet rs = ps.executeQuery();
				
			while(rs.next()){
				Object[] fila = new Object[4];
				fila[0] = rs.getString("nombre");
				fila[1] = rs.getFloat("importe_dia");
				fila[2] = rs.getDate("fecha_inicio");
				fila[3] = rs.getDate("fecha_fin");
				
				miModelo.addRow(fila);
			}
			
			tablaModificacion.setModel(miModelo);
			modificarAspectoTabla(tablaModificacion);
			rs.close();
			ps.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private void modificarAspectoTabla(JTable tabla){
		tabla.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 12));
		tabla.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tabla.setRowHeight(20);
	}

	
	private int getIdPrecioTipo(String tipo, float importe, Long fi, Long ff){
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
			String consulta = "select id "
					+ "from precio_tipo "
					+ "where nombre =? and importe_dia=? and fecha_inicio = to_date(?, 'dd-MM-YYYY') and fecha_fin = to_date(?, 'dd-MM-YYYY')";
			PreparedStatement ps = con.prepareStatement(consulta);
			
			ps.setString(1, tipo);
			ps.setFloat(2, importe);
			ps.setString(3, sdf.format(new Date(fi)));
			ps.setString(4, sdf.format(new Date(ff)));
			ResultSet rs = ps.executeQuery();
					
			rs.next();
			int idPTipo = idPrecioTipo = rs.getInt("id");
			rs.close();
			ps.close();
			return idPTipo;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	
	private boolean comprobarImporte(JTextField tfImporte){
		String importe = tfImporte.getText();
		try{
			Double.parseDouble(importe);
			return true;
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "Formato de importe incorrecto");
			tfImporte.setBorder(new LineBorder(Color.RED));
			return false;
		}
	}

	
	
	private boolean existeNuevoTipo(){	
		try{
			String tipo = tfNuevoNombreDelTipo.getText();
			
			String consulta = "select count(nombre) "
					+ "from tipo_habitacion "
					+ "where nombre = ? ";
			PreparedStatement ps = con.prepareStatement(consulta);
			ps.setString(1, tipo);
			
			ResultSet rs = ps.executeQuery();
			rs.next();
			int existeTipo = 0;
			existeTipo = rs.getInt("count(nombre)");
			
			ps.close();
			rs.close();
			if(existeTipo >0)
				return true;
			return false;
		}
		catch(SQLException e){
			e.printStackTrace(); 
			return false;
		}
	}
	
	private void crearNuevoTipo(String tipo){
		try{
			String consulta =  "insert into tipo_habitacion values(?)";
			PreparedStatement ps = con.prepareStatement(consulta);
			ps.setString(1, tipo);
			ps.executeUpdate();
			ps.close();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	private void insertarTarifa(String tipo, float nuevoImporte, JDateChooser fechaInicio, JDateChooser fechaFin){
		try{
			StringBuilder sb = new StringBuilder();
			sb.append("insert into precio_tipo values(secuencia_preciosupl.nextVal,?,?,?,?)");
			
			PreparedStatement ps = con.prepareStatement(sb.toString());
			ps.setDate(1, new java.sql.Date(fechaInicio.getDate().getTime()));
			ps.setDate(2, new java.sql.Date(fechaFin.getDate().getTime()));
			ps.setFloat(3, nuevoImporte);
			ps.setString(4, tipo);
			
			ps.executeUpdate();
			
			ps.close();
			rellenarTabla();
			
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	
	private JPanel getPanelModificacion() {
		if (panelModificacion == null) {
			panelModificacion = new JPanel();
			panelModificacion.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Modificar tipo de habitaci\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			panelModificacion.setLayout(null);
			panelModificacion.add(getLblSeleccioneElSuplemento());
			panelModificacion.add(getLblLogoInfo());
			panelModificacion.add(getTxtInfo());
			panelModificacion.add(getSpTablaModificacion());
			panelModificacion.add(getPanelDatosModificacion());
			panelModificacion.add(getBtnRefrescar());
			panelModificacion.add(getBtnAtrs());
		
		}
		
		return panelModificacion;
	}
	private JLabel getLblSeleccioneElSuplemento() {
		if (lblSeleccioneElSuplemento == null) {
			lblSeleccioneElSuplemento = new JLabel("Seleccione el tipo de habitación:");
			lblSeleccioneElSuplemento.setFont(new Font("Tahoma", Font.BOLD, 12));
			lblSeleccioneElSuplemento.setBounds(21, 81, 228, 26);
		}
		return lblSeleccioneElSuplemento;
	}
	
	private JLabel getLblLogoInfo() {
		if (lblLogoInfo == null) {
			lblLogoInfo = new JLabel("");
			lblLogoInfo.setIcon(new ImageIcon(CardTiposModificacion.class.getResource("/img/iconoInfo.png")));
			lblLogoInfo.setBounds(21, 31, 31, 31);
		}
		return lblLogoInfo;
	}
	private JTextArea getTxtInfo() {
		if (txtInfo == null) {
			txtInfo = new JTextArea();
			txtInfo.setEditable(false);
			txtInfo.setLineWrap(true);
			txtInfo.setOpaque(false);
			txtInfo.setFont(new Font("Tahoma", Font.PLAIN, 12));
			txtInfo.setText("¡Atención! Para evitar conflictos solo se le mostrarán aquellos tipos de habitación que no estén aplicados a una o varias reservas y cuya fecha de inicio sea superior o igual a la fecha actual.");
			txtInfo.setBounds(64, 31, 665, 37);
		}
		return txtInfo;
	}
	private JScrollPane getSpTablaModificacion() {
		if (spTablaModificacion == null) {
			spTablaModificacion = new JScrollPane();
			spTablaModificacion.setBounds(22, 108, 707, 168);
			spTablaModificacion.setViewportView(getTablaModificacion());
		}
		return spTablaModificacion;
	}
	private JTable getTablaModificacion() {
		if (tablaModificacion == null) {
			tablaModificacion = new JTable();
			tablaModificacion.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent arg0) {
					panelDatosModificacion.setVisible(true);
					rellenarDatosEnComponentesPanelDatosModificacion();
				}
			});
			tablaModificacion.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tablaModificacion;
	}
	
	private void rellenarDatosEnComponentesPanelDatosModificacion(){
		try{
			antiguoNombreTipoHab = (String) tablaModificacion.getValueAt(tablaModificacion.getSelectedRow(), 0);
			antiguoImporte = (Float) tablaModificacion.getValueAt(tablaModificacion.getSelectedRow(), 1);
			antiguaFechaInicio = ((Date) tablaModificacion.getValueAt(tablaModificacion.getSelectedRow(), 2)).getTime();
			antiguaFechaFin = ((Date) tablaModificacion.getValueAt(tablaModificacion.getSelectedRow(), 3)).getTime();
			
					
			tfNuevoNombreDelTipo.setText(antiguoNombreTipoHab);
			tfNuevoImporte.setText(antiguoImporte.toString());
			tfNuevoImporte.setBorder(new LineBorder(Color.BLACK));
			dcFechaInicioModificacion.setDate(new Date(antiguaFechaInicio));
			dcFechaFinModificacion.setDate(new Date(antiguaFechaFin));
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private JPanel getPanelDatosModificacion() {
		if (panelDatosModificacion == null) {
			panelDatosModificacion = new JPanel();
			panelDatosModificacion.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Modificar tipo de habitaci\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			panelDatosModificacion.setFont(new Font("Tahoma", Font.PLAIN, 12));
			panelDatosModificacion.setBounds(21, 299, 708, 275);
			panelDatosModificacion.setLayout(null);
			panelDatosModificacion.add(getLblNuevoNombreDelTipo());
			panelDatosModificacion.add(getTfNuevoNombreDelTipo());
			panelDatosModificacion.add(getLblNuevoImporte());
			panelDatosModificacion.add(getTfNuevoImporte());
			panelDatosModificacion.add(getDcFechaInicioModificacion());
			panelDatosModificacion.add(getLblNuevaFechaInicio());
			panelDatosModificacion.add(getLblNuevaFechaFinal());
			panelDatosModificacion.add(getDcFechaFinModificacion());
			panelDatosModificacion.add(getBtnModificar());
			panelDatosModificacion.add(getBtnCancelar());
			panelDatosModificacion.setVisible(false);
		}
		return panelDatosModificacion;
	}
	private JLabel getLblNuevoNombreDelTipo() {
		if (lblNuevoNombreDelTipo == null) {
			lblNuevoNombreDelTipo = new JLabel("Nuevo nombre del tipo de habitación:");
			lblNuevoNombreDelTipo.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblNuevoNombreDelTipo.setBounds(144, 39, 219, 26);
		}
		return lblNuevoNombreDelTipo;
	}
	private JTextField getTfNuevoNombreDelTipo() {
		if (tfNuevoNombreDelTipo == null) {
			tfNuevoNombreDelTipo = new JTextField();
			tfNuevoNombreDelTipo.setBounds(144, 64, 147, 32);
			tfNuevoNombreDelTipo.setColumns(10);
		}
		return tfNuevoNombreDelTipo;
	}
	private JLabel getLblNuevoImporte() {
		if (lblNuevoImporte == null) {
			lblNuevoImporte = new JLabel("Nuevo importe:");
			lblNuevoImporte.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblNuevoImporte.setBounds(425, 39, 92, 26);
		}
		return lblNuevoImporte;
	}
	private JTextField getTfNuevoImporte() {
		if (tfNuevoImporte == null) {
			tfNuevoImporte = new JTextField();
			tfNuevoImporte.setFont(new Font("Tahoma", Font.PLAIN, 12));
			tfNuevoImporte.setBounds(425, 65, 115, 32);
			tfNuevoImporte.setColumns(10);
		}
		return tfNuevoImporte;
	}
	private JDateChooser getDcFechaInicioModificacion() {
		if (dcFechaInicioModificacion == null) {
			dcFechaInicioModificacion = new JDateChooser();
			dcFechaInicioModificacion.getCalendarButton().setFont(new Font("Tahoma", Font.PLAIN, 11));
			dcFechaInicioModificacion.setFont(new Font("Tahoma", Font.PLAIN, 11));
			dcFechaInicioModificacion.setBounds(144, 155, 155, 32);
			dcFechaInicioModificacion.setMinSelectableDate(new Date());
		}
		return dcFechaInicioModificacion;
	}
	private JLabel getLblNuevaFechaInicio() {
		if (lblNuevaFechaInicio == null) {
			lblNuevaFechaInicio = new JLabel("Nueva fecha de inicio:");
			lblNuevaFechaInicio.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblNuevaFechaInicio.setBounds(144, 129, 139, 26);
		}
		return lblNuevaFechaInicio;
	}
	private JLabel getLblNuevaFechaFinal() {
		if (lblNuevaFechaFinal == null) {
			lblNuevaFechaFinal = new JLabel("Nueva fecha final:");
			lblNuevaFechaFinal.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblNuevaFechaFinal.setBounds(425, 129, 109, 26);
		}
		return lblNuevaFechaFinal;
	}
	private JDateChooser getDcFechaFinModificacion() {
		if (dcFechaFinModificacion == null) {
			dcFechaFinModificacion = new JDateChooser();
			dcFechaFinModificacion.getCalendarButton().setFont(new Font("Tahoma", Font.PLAIN, 11));
			dcFechaFinModificacion.setFont(new Font("Tahoma", Font.PLAIN, 11));
			dcFechaFinModificacion.setBounds(425, 155, 155, 32);
			dcFechaFinModificacion.setMinSelectableDate(new Date());
		}
		return dcFechaFinModificacion;
	}
	
	private boolean comprobarFechaFinDeModificacion(){
		try{
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			
			Date fecha_inicio = formato.parse(formato.format(dcFechaInicioModificacion.getDate()));
			Date fecha_fin = formato.parse(formato.format(dcFechaFinModificacion.getDate()));
		
			if(fecha_fin.compareTo(fecha_inicio) < 0){
				JOptionPane.showMessageDialog(null, "La fecha de fin debe de ser mayor que\n la fecha de inicio.", "Error", JOptionPane.ERROR_MESSAGE);
				return false;
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	

	private void borrarFilaPrecioTipo() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
			String consulta = "delete from precio_tipo "
					+ "where id = ? and nombre = ? and importe_dia = ? "
					+ "and fecha_inicio = to_date(?, 'dd-MM-YYYY') "
					+ "and fecha_fin = to_date(?, 'dd-MM-YYYY')";
			
			PreparedStatement ps = con.prepareStatement(consulta);
			ps.setInt(1, idPrecioTipo);
			ps.setString(2, antiguoNombreTipoHab);
			ps.setFloat(3, antiguoImporte);
			ps.setString(4, sdf.format(new Date(antiguaFechaInicio)));
			ps.setString(5, sdf.format(new Date(antiguaFechaFin)));
			ps.executeUpdate();
			ps.close();
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	private int numeroDeTarifaAsociadasAlTipo(String tipo) {
		try {
			
			String consulta = "select count(nombre) "
					+ "from precio_tipo natural join tipo_habitacion "
					+ "where nombre = ? ";
			PreparedStatement ps = con.prepareStatement(consulta);
			ps.setString(1, tipo);
			
			ResultSet rs = ps.executeQuery();
			rs.next();
			int numeroDeTarifasAsociadasAlTipo = 0;
			numeroDeTarifasAsociadasAlTipo = rs.getInt("count(nombre)");
			
			rs.close();
			ps.close();
			return numeroDeTarifasAsociadasAlTipo;
			
		}
		catch(SQLException e) {
			e.printStackTrace();
			return 2;
		}
	}
	
	private void borrarTipo(String tipo) {
		try {
			String consulta = "delete from tipo_habitacion "
					+ "where nombre= ?";
			
			PreparedStatement ps = con.prepareStatement(consulta);
			ps.setString(1, tipo);
			ps.executeUpdate();
			ps.close();
			
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void resetCamposDatosModificacion() {
		tfNuevoNombreDelTipo.setText(null);
		tfNuevoImporte.setText(null);
		dcFechaInicioModificacion.setDate(null);
		dcFechaFinModificacion.setDate(null);
	}
	
	private int cambiarDatosAPrecioTipo(String tipo, float precio, JDateChooser fi, JDateChooser ff) {
		try {
			int idAntiguo = getIdPrecioTipo(antiguoNombreTipoHab, antiguoImporte,antiguaFechaInicio, antiguaFechaFin);
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
			
			String update = "update precio_tipo " + 
					"set nombre = ?, " + 
					"fecha_inicio = to_date(?, 'dd-MM-YYYY'), fecha_fin = to_date(?,'dd-MM-YYYY'), "+ 
					"importe_dia = ? " + 
					"where id = ?";
			PreparedStatement ps = con.prepareStatement(update);
			ps.setString(1, tipo);
			ps.setString(2, sdf.format(fi.getDate()));
			ps.setString(3, sdf.format(ff.getDate()));
			ps.setFloat(4, precio);
			ps.setInt(5, idAntiguo);
			int filasCambiadas = ps.executeUpdate();
			ps.close();
			return filasCambiadas;
			
		}
		catch(SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}
	private JButton getBtnModificar() {
		if (btnModificar == null) {
			btnModificar = new JButton("Modificar");
			btnModificar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String nuevoNombreTipo = tfNuevoNombreDelTipo.getText();
					
					if(nuevoNombreTipo!= null){
						if(comprobarFechaFinDeModificacion() && comprobarImporte(tfNuevoImporte)){
							if(!existeNuevoTipo()){
								int opcion = JOptionPane.showConfirmDialog(null, "¿Desea confirmar los cambios?", "Confirmar", JOptionPane.YES_NO_OPTION);
								if(opcion == 0) {
									getIdPrecioTipo(antiguoNombreTipoHab, antiguoImporte, antiguaFechaInicio, antiguaFechaFin);
									borrarFilaPrecioTipo();
									if(numeroDeTarifaAsociadasAlTipo(antiguoNombreTipoHab) <=0) {
										borrarTipo(antiguoNombreTipoHab);
									}
									crearNuevoTipo(nuevoNombreTipo);
									insertarTarifa(nuevoNombreTipo,Float.parseFloat(tfNuevoImporte.getText()), dcFechaInicioModificacion, dcFechaFinModificacion);
									rellenarTabla();
									tablaModificacion.repaint();
									resetCamposDatosModificacion();
									panelDatosModificacion.setVisible(false);
									JOptionPane.showMessageDialog(null, "Tipo de habitación modificado con éxito", "OK", JOptionPane.INFORMATION_MESSAGE);

								}
							}
							else {
								if(!esTarifaConflictiva(nuevoNombreTipo)) {
									int opcion = JOptionPane.showConfirmDialog(null, "¿Desea confirmar los cambios?", "Confirmación", JOptionPane.YES_NO_OPTION);
									if(opcion==0) {
										int filasCambiadas = cambiarDatosAPrecioTipo(nuevoNombreTipo, Float.parseFloat(tfNuevoImporte.getText()), dcFechaInicioModificacion, dcFechaFinModificacion);
										if(filasCambiadas >=1) {
											if(numeroDeTarifaAsociadasAlTipo(antiguoNombreTipoHab) <=0) 
												borrarTipo(antiguoNombreTipoHab);
											
											rellenarTabla();
											tablaModificacion.repaint();
											resetCamposDatosModificacion();
											panelDatosModificacion.setVisible(false);
											
											JOptionPane.showMessageDialog(null, "Tipo de habitación modificado con éxito", "OK", JOptionPane.INFORMATION_MESSAGE);
										}
									}
								}
							}
						}
						
					}
					else
						JOptionPane.showMessageDialog(null, "Introduzca un nombre para el tipo de habitación", "Error", JOptionPane.ERROR_MESSAGE);
				}
			});
			btnModificar.setFont(new Font("Tahoma", Font.PLAIN, 12));
			btnModificar.setBounds(592, 234, 102, 35);
		}
		return btnModificar;
	}
	private boolean esTarifaConflictiva(String tipo) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			
			String consulta = "select count(id) from precio_tipo where nombre = ? "
					+ "and fecha_inicio <= to_date(?, 'dd-MM-yyyy') and fecha_fin >= to_date(?, 'dd-MM-yyyy')";
			
			
			PreparedStatement ps = con.prepareStatement(consulta);
			ps.setString(1, tipo);
			ps.setString(2, sdf.format(dcFechaFinModificacion.getDate()));
			ps.setString(3, sdf.format(dcFechaInicioModificacion.getDate()));
			
			ResultSet rs = ps.executeQuery();
			int existe = 0;
			while(rs.next())
				existe = rs.getInt("count(id)");
			if(existe <= 0)
				return false;
			return true;
			
		}
		catch(Exception e) {
			e.printStackTrace();
			return true;
		}
	}
	private JButton getBtnRefrescar() {
		if (btnRefrescar == null) {
			btnRefrescar = new JButton("Refrescar");
			btnRefrescar.setToolTipText("Refrescar la información de la tabla");
			btnRefrescar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					rellenarTabla();
				}
			});
			btnRefrescar.setBounds(636, 81, 93, 29);
		}
		return btnRefrescar;
	}
	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int opcion = JOptionPane.showConfirmDialog(null, "¡Atención! Si continua se perderán los datos y no se realizará ninguna modificación\n "
							+ "¿Desea continuar con la cancelación?", "Cancelación", JOptionPane.YES_NO_OPTION, JOptionPane.CANCEL_OPTION);
					if(opcion==0)
						panelDatosModificacion.setVisible(false);
				}
			});
			btnCancelar.setBounds(478, 234, 102, 35);
		}
		return btnCancelar;
	}
	private JButton getBtnAtrs() {
		if (btnAtrs == null) {
			btnAtrs = new JButton("Atrás");
			btnAtrs.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					resetCamposDatosModificacion();
					padre.mostrarOpciones();
				}
			});
			btnAtrs.setBounds(648, 580, 88, 34);
		}
		return btnAtrs;
	}
}
