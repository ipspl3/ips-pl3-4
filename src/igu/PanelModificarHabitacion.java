package igu;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import logica.DataBase;
import logica.Habitacion;
import logica.Reserva;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class PanelModificarHabitacion extends JFrame{
	
	private ArrayList<Reserva> habitacionesReservadas;
	
	private JPanel pnReserva;
	private JButton btnCambiar;
	private DefaultComboBoxModel<String> combo1Model = null;
	private JComboBox<String> comboBox;
	private JLabel lblModalidad;
	private JLabel lblTxtHabitacion;
	private JLabel lblHabitacion;
	
	public PanelModificarHabitacion(CardCheckIn papa, ArrayList<Reserva> habitacionesReservadas, Habitacion hab) {
		setTitle("Modificar habitaci\u00F3n");
		
		getContentPane().add(getPnReserva());
		
		padre = papa;
		habitacion = hab;
		this.habitacionesReservadas = habitacionesReservadas;
		getContentPane().setLayout(null);
		getContentPane().add(getBtnCancelar());
		getContentPane().add(getBtnConfirmar());
		setResizable(false);
		setVisible(true);
		setBounds(100,100,406,176);
		setLocationRelativeTo(null);
		llenarCombo();
		lblTxtHabitacion.setText(habitacion.getId());
	}

	private JPanel getPnReserva() {
		if (pnReserva == null) {
			pnReserva = new JPanel();
			pnReserva.setBounds(10, 11, 373, 91);
			pnReserva.setLayout(null);
			pnReserva.add(getLblHabitacin());
			pnReserva.add(getLblTxtHabitacion());
			pnReserva.add(getLblModalidad());
			pnReserva.add(getComboBox());
			pnReserva.add(getBtnCambiar());
		}
		return pnReserva;
	}
	
	private JLabel getLblHabitacin() {
		if (lblHabitacion == null) {
			lblHabitacion = new JLabel("Habitaci\u00F3n:");
			lblHabitacion.setBounds(10, 11, 71, 14);
		}
		return lblHabitacion;
	}
	private JLabel getLblTxtHabitacion() {
		if (lblTxtHabitacion == null) {
			lblTxtHabitacion = new JLabel("");
			lblTxtHabitacion.setBounds(85, 11, 120, 14);
		}
		return lblTxtHabitacion;
	}
	private JLabel getLblModalidad() {
		if (lblModalidad == null) {
			lblModalidad = new JLabel("Modalidad:");
			lblModalidad.setBounds(10, 48, 71, 14);
		}
		return lblModalidad;
	}
	private JComboBox<String> getComboBox() {
		if (comboBox == null) {
			combo1Model = new DefaultComboBoxModel<String>();
			comboBox = new JComboBox<String>();
			comboBox.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent e) {
					btnCambiar.setEnabled(true);
				}
			});
			comboBox.setBounds(85, 45, 120, 20);
			comboBox.setModel(combo1Model);
		}
		return comboBox;
	}

	private void llenarCombo() {
		if(combo1Model.getSize()==0) {
			ArrayList<String> temp;
			try {
				System.out.println("El tipo de la hab es " + habitacion.tipo);
				temp = DataBase.Modalidades(habitacion.tipo);
				for (String e:temp) {
					combo1Model.addElement(e);
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		combo1Model.setSelectedItem(habitacion.modalidad);
	}
	private JButton getBtnCambiar() {
		if (btnCambiar == null) {
			btnCambiar = new JButton("Cambiar");
			btnCambiar.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					try {
						System.out.println("Elemento seleccionado en el combo box " + comboBox.getSelectedItem());
						DataBase.cambiarModalidad(habitacionesReservadas.get(0), comboBox.getSelectedItem(), habitacion);
						JOptionPane.showMessageDialog(null, "Modalidad cambiada correctamente");
						habitacion.modalidad = comboBox.getSelectedItem().toString();
						habitacion.modalidad_id = DataBase.pk_modalidad(comboBox.getSelectedItem().toString(), habitacion);
						llenarCombo();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
			});
			btnCambiar.setBounds(215, 44, 89, 23);
			btnCambiar.setEnabled(false);
		}
		return btnCambiar;
	}

	
	private CardCheckIn padre;
	private Habitacion habitacion;
	private JButton btnCancelar;
	private JButton btnConfirmar;
	
	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int opcion = JOptionPane.showConfirmDialog(null, "Los datos modificados no se guardar�n. �Desea cancelar?", "Confirmar cambios", JOptionPane.YES_NO_OPTION);
					if(opcion == 0){//YES
						dispose();
					}
				}
			});
			btnCancelar.setBounds(276, 113, 107, 23);
		}
		return btnCancelar;
	}
	private JButton getBtnConfirmar() {
		if (btnConfirmar == null) {
			btnConfirmar = new JButton("Confirmar");
			btnConfirmar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					padre.hab_modificada(habitacion);
					dispose();
				}
			});
			btnConfirmar.setBounds(146, 113, 107, 23);
		}
		return btnConfirmar;
	}
}
