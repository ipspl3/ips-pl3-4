package igu;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.toedter.calendar.JDateChooser;

import logica.Cliente;
import logica.DNI;
import logica.DataBase;
import logica.Habitacion;
import logica.Reserva;
import logica.Tarjeta;

import javax.swing.UIManager;
import javax.swing.JSeparator;

@SuppressWarnings("serial")
public class CardCheckIn extends JPanel{

	VentanaRecepcionista padre;
	private int id_reserva = -1;
	private JPanel panel;
	private JPanel pnDatos;
	private JTextField txtDni;
	private JLabel lblTarjeta;
	private JTextField txtTarjeta;
	private JRadioButton rdbtnDni;
	private JRadioButton rdbtnPasaporte;
	private JButton btnBuscar;
	private JLabel lblTipoTarjeta;
	private JButton btnModificarDatos;
	private JLabel lblPaisDeResidencia;
	private JTextField txtPaisDeResidencia;
	private JLabel lblNombre;
	private JTextField txtNombre;
	private JLabel lblTelefono;
	private JTextField txtTelefono;
	private JLabel lblDireccin;
	private JTextField txtDireccion;
	private JLabel lblApellidos;
	private JTextField txtApellidos;
	private JLabel lblCorreo;
	private JTextField txtCorreo;
	private JDateChooser nacimiento;
	private JLabel lblFnacimiento;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JScrollPane pnReservas;
	private JList<Habitacion> listReservas;
	private DefaultListModel<Habitacion> listModelReservas;
	private JButton btnModificarReserva;
	private JScrollPane pnAcompañantes;
	private JList<Cliente> listAcompañantes;
	private DefaultListModel<Cliente> listModelAcompañantes;
	private JButton btnNuevoAcompañante;
	private JButton btnEliminarAcompañante;
	private JSeparator separator;
	private JPanel pnAcompañantesModificar;
	private JPanel pnModificarReservas;
	private JButton btnModificarAcompañante;
	private JButton btnEliminarReserva;
	private JButton btnNuevaHabitacion;
	private JButton btnGuardarCheckin;
	private JButton btnCancelar;
	
	private Cliente cliente;
	private ArrayList<Reserva> habitacionesReservadas;
	private ArrayList<Cliente> acompañantes = new ArrayList<Cliente>();
	
	public CardCheckIn(VentanaRecepcionista papa) {
		
		padre = papa;
		setLayout(null);
		add(getPanel());
		add(getSeparator());
		add(getPnAcompañantesModificar());
		add(getPnModificarReservas());
		add(getBtnGuardarCheckin());
		add(getBtnCancelar());
		habitacionesReservadas = new ArrayList<Reserva>();
		setVisible(true);
	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBounds(10, 11, 622, 249);
			panel.setLayout(null);
			panel.add(getTxtDni());
			panel.add(getLblTarjeta());
			panel.add(getTxtTarjeta());
			panel.add(getRdbtnDni());
			panel.add(getRdbtnPasaporte());
			panel.add(getBtnBuscar());
			panel.add(getLblTipoTarjeta());
			panel.add(getBtnModificarDatos());
			panel.add(getPnDatos());
		}
		return panel;
	}
	private JPanel getPnDatos() {
		if (pnDatos == null) {
			pnDatos = new JPanel();
			pnDatos.setBounds(323, 0, 289, 238);
			pnDatos.setLayout(null);
			pnDatos.add(getLblNombre());
			pnDatos.add(getTxtNombre());
			pnDatos.add(getLblTelefono());
			pnDatos.add(getTxtTelefono());
			pnDatos.add(getLblDireccin());
			pnDatos.add(getTxtDireccion());
			pnDatos.add(getLblApellidos());
			pnDatos.add(getTxtApellidos());
			pnDatos.add(getLblCorreo());
			pnDatos.add(getTxtCorreo());
			pnDatos.add(getLblPaisDeResidencia());
			pnDatos.add(getTxtPaisDeResidencia());
			pnDatos.add(getNacimiento());
			pnDatos.add(getLblFnacimiento());
		}
		return pnDatos;
	}
	private JTextField getTxtDni() {
		if (txtDni == null) {
			txtDni = new JTextField();
			txtDni.setBackground(Color.WHITE);
			txtDni.addKeyListener(new KeyAdapter() {
				int limite = 9;
				@Override
				public void keyTyped(KeyEvent e) {
					if (txtDni.getText().length() == limite){
						DNI dni = new DNI(txtDni.getText());
						boolean bien;
						if (rdbtnDni.isSelected()){
							bien = dni.comprobarDNI();
							if (bien){
								txtDni.setBorder(new LineBorder(Color.BLACK));
								btnBuscar.setEnabled(true);
							}
							else{
								txtDni.setBorder(new LineBorder(Color.RED));
							}
						}
						else{
							bien = dni.comprobarPasaporte();
							if (bien){
								txtDni.setBorder(new LineBorder(Color.BLACK));
								btnBuscar.setEnabled(true);
							}
							else{
								txtDni.setBorder(new LineBorder(Color.RED));
							}
						}
					}
					if (txtDni.getText().length() >= limite){
						e.consume();
					}
				}
			});
			txtDni.setBounds(10, 37, 204, 20);
			txtDni.setColumns(10);
		}
		return txtDni;
	}

	private JLabel getLblTarjeta() {
		if (lblTarjeta == null) {
			lblTarjeta = new JLabel("Tarjeta: ");
			lblTarjeta.setBounds(10, 86, 69, 14);
		}
		return lblTarjeta;
	}
	private JTextField getTxtTarjeta() {
		if (txtTarjeta == null) {
			txtTarjeta = new JTextField();
			txtTarjeta.setBounds(58, 83, 202, 20);
			txtTarjeta.setColumns(10);
			txtTarjeta.setEditable(false);
		}
		return txtTarjeta;
	}
	
	private JRadioButton getRdbtnDni() {
		if (rdbtnDni == null) {
			rdbtnDni = new JRadioButton("DNI");
			rdbtnDni.setSelected(true);
			buttonGroup.add(rdbtnDni);
			rdbtnDni.setBounds(10, 7, 69, 23);
		}
		return rdbtnDni;
	}
	private JRadioButton getRdbtnPasaporte() {
		if (rdbtnPasaporte == null) {
			rdbtnPasaporte = new JRadioButton("Pasaporte");
			buttonGroup.add(rdbtnPasaporte);
			rdbtnPasaporte.setBounds(75, 7, 109, 23);
		}
		return rdbtnPasaporte;
	}
	private JButton getBtnBuscar() {
		if (btnBuscar == null) {
			btnBuscar = new JButton("Buscar");
			btnBuscar.addActionListener(new ActionListener() {
				@SuppressWarnings("deprecation")
				public void actionPerformed(ActionEvent e) {
					setNoEditable();
					listModelReservas.clear();
					listModelAcompañantes.clear();
					if (!txtDni.getText().isEmpty()){
						try {
							cliente = DataBase.buscarClientePorDni(txtDni.getText().toUpperCase());
							if (cliente == null ){
								JOptionPane.showMessageDialog(null, "El cliente aún no ha sido registrado");
								padre.nuevaReserva(txtDni.getText(), "dni", null);
							}
							else{
								ArrayList<Reserva> reservas = DataBase.buscarReservaPorDni(cliente.getDni().toString());
								boolean actual = false;
								Date ayer = new Date();
								ayer.setHours(0);
								ayer.setDate(ayer.getDate() -1);
								Date mañana = new Date();
								mañana.setHours(0);
								mañana.setDate(mañana.getDate() +1);
								for (Reserva res:reservas) {
									if (res.f_in_d.after(ayer) && res.f_in_d.before(mañana)) {
										actual = true;
										habitacionesReservadas.add(res);
										listModelReservas.addElement(res.getHab());
									}
								}
								id_reserva = habitacionesReservadas.get(0).getPk();
								if (actual) {
									JOptionPane.showMessageDialog(null, "Compruebe los datos guardados");
									txtNombre.setText(cliente.getName());
									txtApellidos.setText(cliente.getApellidos());
									txtTelefono.setText(String.valueOf(cliente.getTelefono()));
									txtDireccion.setText(cliente.getDireccion());
									txtCorreo.setText(cliente.getCorreo());
									nacimiento.setDate(new Date(cliente.getNacimiento()));
									txtPaisDeResidencia.setText(cliente.getResidencia());
									txtTarjeta.setText(cliente.getTarjeta().toString());
									btnModificarDatos.setEnabled(true);
									rellenarReservas();
								}
								else {
									JOptionPane.showMessageDialog(null, "El cliente no tiene reservas efectuadas para hoy");
									int opcion = JOptionPane.showConfirmDialog(null, "¿Desea efectuar la reserva ahora?", "Nueva reserva", JOptionPane.YES_NO_OPTION);
									if(opcion == 0){//YES
										padre.nuevaReserva(txtDni.getText(), "dni", cliente);
									}
									else {
										iniciar();
									}
								}
							}
						} 
						catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
					else if (!txtNombre.getText().isEmpty()){
						try {
							cliente = DataBase.buscarClientePorNombre(txtNombre.getText());
							if (cliente == null){
								JOptionPane.showMessageDialog(null, "El cliente aún no ha sido registrado");
								int opcion = JOptionPane.showConfirmDialog(null, "¿Desea efectuar la reserva ahora?", "Nueva reserva", JOptionPane.YES_NO_OPTION);
								if(opcion == 0){//YES
									padre.nuevaReserva(txtNombre.getText(), "nombre", null);
								}
								else {
									iniciar();
								}
							}
							else{
								if (cliente.getDni().que_es().equals("dni")) {
									rdbtnDni.setSelected(true);
								}
								else {
									rdbtnPasaporte.setSelected(true);
								}
								ArrayList<Reserva> reservas = DataBase.buscarReservaPorDni(cliente.getDni().toString());
								boolean actual = false;
								Date ayer = new Date();
								ayer.setHours(0);
								ayer.setDate(ayer.getDate() -1);
								Date mañana = new Date();
								mañana.setHours(0);
								mañana.setDate(mañana.getDate() +1);
								for (Reserva res:reservas) {
									if (res.f_in_d.after(ayer) && res.f_in_d.before(mañana)) {
										actual = true;
										habitacionesReservadas.add(res);
										listModelReservas.addElement(res.getHab());
									}
								}
								if (actual) {
									JOptionPane.showMessageDialog(null, "Compruebe los datos guardados");
									txtNombre.setText(cliente.getName());
									txtDni.setText(cliente.getDni().toString());
									txtApellidos.setText(cliente.getApellidos());
									txtTelefono.setText(String.valueOf(cliente.getTelefono()));
									txtDireccion.setText(cliente.getDireccion());
									txtCorreo.setText(cliente.getCorreo());
									nacimiento.setDate(new Date(cliente.getNacimiento()));
									txtPaisDeResidencia.setText(cliente.getResidencia());
									txtTarjeta.setText(cliente.getTarjeta().toString());
									btnModificarDatos.setEnabled(true);
									rellenarReservas();
								}
								else {
									JOptionPane.showMessageDialog(null, "El cliente no tiene reservas efectuadas para hoy");
									int opcion = JOptionPane.showConfirmDialog(null, "¿Desea efectuar la reserva ahora?", "Nueva reserva", JOptionPane.YES_NO_OPTION);
									if(opcion == 0){//YES
										padre.nuevaReserva(txtNombre.getText(), "nombre", cliente);
									}
									else {
										iniciar();
									}
								}
							}
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
					else {
						JOptionPane.showMessageDialog(null, "Introducca un DNI, pasaporte o nombre");
					}
				}
			});
			btnBuscar.setEnabled(false);
			btnBuscar.setBounds(224, 36, 89, 23);
		}
		return btnBuscar;
	}

	private void setNoEditable() {
		txtTelefono.setEditable(false);
		txtDireccion.setEditable(false);
		txtApellidos.setEditable(false);
		txtCorreo.setEditable(false);
		txtPaisDeResidencia.setEditable(false);
		nacimiento.setEnabled(false);
		txtTarjeta.setEditable(false);
		
	}
	
	private JLabel getLblTipoTarjeta() {
		if (lblTipoTarjeta == null) {
			lblTipoTarjeta = new JLabel("");
			lblTipoTarjeta.setBounds(151, 130, 109, 14);
		}
		return lblTipoTarjeta;
	}
	private JButton getBtnModificarDatos() {
		if (btnModificarDatos == null) {
			btnModificarDatos = new JButton("Modificar Datos");
			btnModificarDatos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					modificar();
				}
			});
			btnModificarDatos.setBounds(68, 215, 165, 23);
			btnModificarDatos.setEnabled(false);
		}
		return btnModificarDatos;
	}
	
	public void modificar() {
		new VentanaCambiarDatosCliente(this, cliente);
	}

	public void refrescar(Cliente cliente) {
		this.cliente = cliente;
		if (cliente.getDni().que_es().equals("dni")) {
			rdbtnDni.setSelected(true);
		}
		else {
			rdbtnPasaporte.setSelected(true);
		}
		txtNombre.setText(cliente.getName());
		txtApellidos.setText(cliente.getApellidos());
		txtTelefono.setText(String.valueOf(cliente.getTelefono()));
		txtDireccion.setText(cliente.getDireccion());
		txtCorreo.setText(cliente.getCorreo());
		nacimiento.setDate(new Date(cliente.getNacimiento()));
		txtPaisDeResidencia.setText(cliente.getResidencia());
		txtTarjeta.setText(cliente.getTarjeta().toString());
	}
	public void refrescarAcomp(Cliente cliente) {
		acompañantes.add(cliente);
		listModelAcompañantes.addElement(cliente);
	}

	public void refrescar() {
		listModelReservas.clear();
		for (Reserva res:habitacionesReservadas){
			listModelReservas.addElement(res.getHab());
		}
	}
	
	private JLabel getLblNombre() {
		if (lblNombre == null) {
			lblNombre = new JLabel("Nombre:");
			lblNombre.setBounds(0, 17, 59, 14);
		}
		return lblNombre;
	}
	private JTextField getTxtNombre() {
		if (txtNombre == null) {
			txtNombre = new JTextField();
			txtNombre.setBounds(68, 11, 194, 20);
			txtNombre.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (!btnBuscar.isEnabled()){
						btnBuscar.setEnabled(true);
					}
				}
			});
			txtNombre.setColumns(10);
		}
		return txtNombre;
	}
	private JLabel getLblTelefono() {
		if (lblTelefono == null) {
			lblTelefono = new JLabel("Telefono:");
			lblTelefono.setBounds(0, 110, 59, 14);
		}
		return lblTelefono;
	}
	private JTextField getTxtTelefono() {
		if (txtTelefono == null) {
			txtTelefono = new JTextField();
			txtTelefono.setBounds(68, 104, 194, 20);
			txtTelefono.setColumns(10);
			txtTelefono.setEditable(false);
		}
		return txtTelefono;
	}
	private JLabel getLblDireccin() {
		if (lblDireccin == null) {
			lblDireccin = new JLabel("Direcci\u00F3n:");
			lblDireccin.setBounds(0, 79, 59, 14);
		}
		return lblDireccin;
	}
	private JTextField getTxtDireccion() {
		if (txtDireccion == null) {
			txtDireccion = new JTextField();
			txtDireccion.setBounds(68, 73, 194, 20);
			txtDireccion.setColumns(10);
			txtDireccion.setEditable(false);
		}
		return txtDireccion;
	}
	private JLabel getLblApellidos() {
		if (lblApellidos == null) {
			lblApellidos = new JLabel("Apellidos:");
			lblApellidos.setBounds(0, 48, 74, 14);
		}
		return lblApellidos;
	}
	private JTextField getTxtApellidos() {
		if (txtApellidos == null) {
			txtApellidos = new JTextField();
			txtApellidos.setBounds(68, 42, 194, 20);
			txtApellidos.setEditable(false);
			txtApellidos.setColumns(10);
		}
		return txtApellidos;
	}
	private JLabel getLblCorreo() {
		if (lblCorreo == null) {
			lblCorreo = new JLabel("Correo:");
			lblCorreo.setBounds(0, 141, 59, 14);
		}
		return lblCorreo;
	}
	private JTextField getTxtCorreo() {
		if (txtCorreo == null) {
			txtCorreo = new JTextField();
			txtCorreo.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					if (!(txtCorreo.getText().contains("@gmail.com") || 
							txtCorreo.getText().contains("@gmail.es") ||
							txtCorreo.getText().contains("@hotmail.com") ||
							txtCorreo.getText().contains("@hotmail.es") ||
							txtCorreo.getText().contains("@uniovi.es"))){
						txtCorreo.setBorder(new LineBorder(Color.RED));
					}
					else {
						txtCorreo.setBorder(new LineBorder(Color.WHITE));
					}
				}
			});
			txtCorreo.setBounds(68, 135, 194, 20);
			txtCorreo.setEditable(false);
			txtCorreo.setColumns(10);
		}
		return txtCorreo;
	}
	private JLabel getLblPaisDeResidencia() {
		if (lblPaisDeResidencia == null) {
			lblPaisDeResidencia = new JLabel("Pais de residencia:");
			lblPaisDeResidencia.setBounds(0, 206, 147, 14);
		}
		return lblPaisDeResidencia;
	}
	private JTextField getTxtPaisDeResidencia() {
		if (txtPaisDeResidencia == null) {
			txtPaisDeResidencia = new JTextField();
			txtPaisDeResidencia.setEditable(false);
			txtPaisDeResidencia.setBounds(116, 200, 146, 20);
			txtPaisDeResidencia.setColumns(10);
		}
		return txtPaisDeResidencia;
	}
	
	@SuppressWarnings("deprecation")
	private JDateChooser getNacimiento() {
		if (nacimiento == null){
			nacimiento = new JDateChooser("dd/MM/yyyy", "##/##/####", '_');
			nacimiento.setVisible(true);
			nacimiento.setBounds(115, 166, 147, 23);
			Date today = new Date();
			today.setHours(0); 
			nacimiento.setMaxSelectableDate(today);
		}
		return nacimiento;	
	}
	private JLabel getLblFnacimiento() {
		if (lblFnacimiento == null) {
			lblFnacimiento = new JLabel("F.Nacimiento:");
			lblFnacimiento.setBounds(0, 175, 96, 14);
		}
		return lblFnacimiento;
	}

	public void registro(Cliente cliente) {
		this.cliente = cliente;
		txtNombre.setText(cliente.getName());
		txtDni.setText(cliente.getDni().toString());
		if (cliente.getDni().que_es().equals("dni")) {
			rdbtnDni.setSelected(true);
		}
		else {
			rdbtnPasaporte.setSelected(true);
		}
		txtApellidos.setText(cliente.getApellidos());
		txtTelefono.setText(String.valueOf(cliente.getTelefono()));
		txtDireccion.setText(cliente.getDireccion());
		txtCorreo.setText(cliente.getCorreo());
		nacimiento.setDate(new Date(cliente.getNacimiento()));
		txtPaisDeResidencia.setText(cliente.getResidencia());
		txtTarjeta.setText(cliente.getTarjeta().toString());
		pnModificarReservas.setVisible(true);
		pnAcompañantesModificar.setVisible(true);
		separator.setVisible(false);
		btnModificarDatos.setEnabled(true);
		Tarjeta tarjeta = new Tarjeta(txtTarjeta.getText());
		if (tarjeta.comprobarTarjeta()) {
			txtTarjeta.setBorder(new LineBorder(Color.WHITE));
			lblTipoTarjeta.setText(tarjeta.buscarTipo());
		}
		else {
			txtTarjeta.setBorder(new LineBorder(Color.RED));
			lblTipoTarjeta.setText("");
		}
		
		rellenarReservas();
	}
	
	private JList<Habitacion> getListReservas() {
		if (listReservas == null) {
			listModelReservas = new DefaultListModel<Habitacion>();
			listReservas = new JList<Habitacion>(listModelReservas);
		}
		return listReservas;
	}
	
	@SuppressWarnings("deprecation")
	private void rellenarReservas() {
		listModelReservas.clear();
		pnModificarReservas.setVisible(true);
		pnAcompañantesModificar.setVisible(true);
		ArrayList<Reserva> reservas;
		try {
			reservas = DataBase.buscarReservaPorDni(cliente.getDni().toString());
			System.out.println("size de la reserva... " + reservas.size());
			Date ayer = new Date();
			ayer.setHours(0);
			ayer.setDate(ayer.getDate() -1);
			Date mañana = new Date();
			mañana.setHours(0);
			mañana.setDate(mañana.getDate() +1);
			habitacionesReservadas.clear();
			for (Reserva res:reservas) {
				if (res.f_in_d.after(ayer) && res.f_in_d.before(mañana)) {
					habitacionesReservadas.add(res);
					listModelReservas.addElement(res.getHab());
				}
			}
			if (id_reserva == -1) {
				id_reserva = habitacionesReservadas.get(0).getPk();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void iniciar(){
		listModelReservas.clear();

		txtNombre.setText("");
		txtApellidos.setText("");
		txtDni.setText("");
		txtTelefono.setText("");
		txtDireccion.setText("");
		txtCorreo.setText("");
		nacimiento.setDate(null);
		txtPaisDeResidencia.setText("");
		lblTipoTarjeta.setText("");
		
		pnAcompañantesModificar.setVisible(false);
		pnModificarReservas.setVisible(false);
		btnBuscar.setEnabled(false);
		btnModificarDatos.setEnabled(false);
		
		txtTarjeta.setText("");
		
		setNoEditable();

		padre.iniciar();
	}
	
	// -------------------------------------------------------Acompañantes
	
	private JPanel getPnAcompañantesModificar() {
		if (pnAcompañantesModificar == null) {
			pnAcompañantesModificar = new JPanel();
			pnAcompañantesModificar.setVisible(false);
			pnAcompañantesModificar.setBounds(10, 445, 626, 161);
			pnAcompañantesModificar.setLayout(null);
			pnAcompañantesModificar.add(getPnAcompañantes());
			pnAcompañantesModificar.add(getBtnNuevoAcompañante());
			pnAcompañantesModificar.add(getBtnEliminarAcompañante());
			pnAcompañantesModificar.add(getBtnModificarAcompañante());
		}
		return pnAcompañantesModificar;
	}
	private JScrollPane getPnAcompañantes() {
		if (pnAcompañantes == null) {
			pnAcompañantes = new JScrollPane();
			pnAcompañantes.setBounds(0, 0, 449, 144);
			pnAcompañantes.setBorder(new TitledBorder(null, "Acompa\u00F1antes", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnAcompañantes.setViewportView(getListAcompañantes());
		}
		return pnAcompañantes;
	}
	private JList<Cliente> getListAcompañantes() {
		if (listAcompañantes == null) {
			listModelAcompañantes = new DefaultListModel<Cliente>();
			listAcompañantes = new JList<Cliente>(listModelAcompañantes);
		}
		return listAcompañantes;
	}
	private JButton getBtnNuevoAcompañante() {
		if (btnNuevoAcompañante == null) {
			btnNuevoAcompañante = new JButton("Nuevo acompa\u00F1ante");
			btnNuevoAcompañante.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					nuevoAcompañante();
				}
			});
			btnNuevoAcompañante.setBounds(463, 23, 163, 23);
		}
		return btnNuevoAcompañante;
	}
	private void nuevoAcompañante(){
		new PanelNuevoAcompañante(this);
	}
	private JButton getBtnModificarReserva() {
		if (btnModificarReserva == null) {
			btnModificarReserva = new JButton("Modificar");
			btnModificarReserva.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (listReservas.isSelectionEmpty()) {
						JOptionPane.showMessageDialog(null, "Seleccione primero una habitación");
					}
					else {
						modificarHabitacion();
					}
				}
			});
			btnModificarReserva.setBounds(459, 59, 167, 23);
		}
		return btnModificarReserva;
	}
	private void modificarHabitacion() {
		new PanelModificarHabitacion(this, habitacionesReservadas, listReservas.getSelectedValue());
	}
	private JButton getBtnEliminarAcompañante() {
		if (btnEliminarAcompañante == null) {
			btnEliminarAcompañante = new JButton("Eliminar");
			btnEliminarAcompañante.addActionListener(new ActionListener() {
				@SuppressWarnings("deprecation")
				public void actionPerformed(ActionEvent e) {
					if (listAcompañantes.isSelectionEmpty()) {
						JOptionPane.showMessageDialog(null, "Seleccione primero un acompañante");
					}
					else {
						int opcion = JOptionPane.showConfirmDialog(null, "¿Desea elimnar de forma definitiva este acompañante?", "Eliminar acompañante", JOptionPane.YES_NO_OPTION);
						if(opcion == 0){//YES
							System.out.println(listAcompañantes.size());
							acompañantes.remove(listAcompañantes.getSelectedValue());
							listModelAcompañantes.removeElement(listAcompañantes.getSelectedValue());
						}
					}
				}
			});
			btnEliminarAcompañante.setBounds(463, 115, 163, 23);
		}
		return btnEliminarAcompañante;
	}
	
	private JSeparator getSeparator() {
		if (separator == null) {
			separator = new JSeparator();
			separator.setVisible(false);
			separator.setBounds(10, 432, 622, 2);
		}
		return separator;
	}
	
	// -------------------------------------------------------Reservas
	
	private JPanel getPnModificarReservas() {
		if (pnModificarReservas == null) {
			pnModificarReservas = new JPanel();
			pnModificarReservas.setVisible(false);
			pnModificarReservas.setBounds(10, 271, 626, 144);
			pnModificarReservas.setLayout(null);
			pnModificarReservas.add(getPnReservas());
			pnModificarReservas.add(getBtnModificarReserva());
			pnModificarReservas.add(getBtnEliminarReserva());
			pnModificarReservas.add(getBtnNuevaHabitacion());
		}
		return pnModificarReservas;
	}
	private JScrollPane getPnReservas() {
		if (pnReservas == null) {
			pnReservas = new JScrollPane();
			pnReservas.setBounds(0, 0, 449, 138);
			pnReservas.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Reservas", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			pnReservas.setViewportView(getListReservas());
		}
		return pnReservas;
	}
	private JButton getBtnModificarAcompañante() {
		if (btnModificarAcompañante == null) {
			btnModificarAcompañante = new JButton("Modificar");
			btnModificarAcompañante.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (listAcompañantes.isSelectionEmpty()) {
						JOptionPane.showMessageDialog(null, "Seleccione primero un acompañante");
					}
					else {
						modificarAcompañante();
					}
				}
			});
			btnModificarAcompañante.setBounds(463, 69, 163, 23);
		}
		return btnModificarAcompañante;
	}
	private void modificarAcompañante() {
		new PanelNuevoAcompañante(this, listAcompañantes.getSelectedValue());
		listModelAcompañantes.removeElement(listAcompañantes.getSelectedValue());
		acompañantes.remove(listAcompañantes.getSelectedValue());
	}
	private JButton getBtnEliminarReserva() {
		if (btnEliminarReserva == null) {
			btnEliminarReserva = new JButton("Eliminar");
			btnEliminarReserva.addActionListener(new ActionListener() {
				@SuppressWarnings("unlikely-arg-type")
				public void actionPerformed(ActionEvent e) {
					if (listReservas.isSelectionEmpty()) {
						JOptionPane.showMessageDialog(null, "Seleccione primero un acompañante");
					}
					else {
						int opcion = JOptionPane.showConfirmDialog(null, "¿Desea elimnar de forma definitiva esta reserva?", "Eliminar reserva", JOptionPane.YES_NO_OPTION);
						if(opcion == 0){//YES
							if (habitacionesReservadas.size()==1) {
								JOptionPane.showMessageDialog(null, "La reserva debe tener almenos una habitación");
							}
							else {
								int pos = -1;
								for (int i=0; i<habitacionesReservadas.size(); i++) {
									if (listReservas.getSelectedValue().getId().equals(habitacionesReservadas.get(i).getHab().getId())) {
										pos = i;
									}
								}
								try {
									DataBase.eliminarReserva(habitacionesReservadas.get(pos));
									habitacionesReservadas.remove(listReservas.getSelectedValue());
									listModelReservas.remove(listReservas.getSelectedIndex());
								} catch (SQLException e1) {
									e1.printStackTrace();
								} 
							}
						}
						rellenarReservas();
					}
				}
			});
			btnEliminarReserva.setBounds(459, 100, 167, 23);
		}
		return btnEliminarReserva;
	}
	private JButton getBtnNuevaHabitacion() {
		if (btnNuevaHabitacion == null) {
			btnNuevaHabitacion = new JButton("Nueva habitaci\u00F3n");
			btnNuevaHabitacion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					añadirHabs();
				}
			});
			btnNuevaHabitacion.setBounds(459, 18, 167, 23);
		}
		return btnNuevaHabitacion;
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	
	private void añadirHabs() {
		new PanelNuevaHabitación(this, listModelReservas, habitacionesReservadas, id_reserva);
	}
	private JButton getBtnGuardarCheckin() {
		if (btnGuardarCheckin == null) {
			btnGuardarCheckin = new JButton("Guardar check-in");
			btnGuardarCheckin.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						DataBase.guardarCheckIn(habitacionesReservadas.get(0), acompañantes, habitacionesReservadas);
						JOptionPane.showMessageDialog(null, "El registro se ha guardado correctamente");
						iniciar();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
			});
			btnGuardarCheckin.setBounds(367, 615, 148, 23);
		}
		return btnGuardarCheckin;
	}
	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					iniciar();
				}
			});
			btnCancelar.setBounds(525, 615, 89, 23);
		}
		return btnCancelar;
	}

	public void hab_modificada(Habitacion habitacion) {
		for (int i=0; i<habitacionesReservadas.size(); i++) {
			if (habitacionesReservadas.get(i).getHab().getId() == habitacion.getId()) {
				habitacionesReservadas.get(i).getHab().setModalidad(habitacion.modalidad, habitacion.modalidad_id);
			}
		}
		refrescar();
	}

}

