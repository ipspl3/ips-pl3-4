package igu;

import java.awt.BorderLayout;
import javax.swing.JPanel;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.TitledBorder;

import java.awt.CardLayout;


public class CardGerenteHabitaciones extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private VentanaGerentePrincipal padre;
	private JPanel panelSuperCard;
	private JPanel panelElegirAccion;
	private CardLayout superCard = new CardLayout(0,0);
	private JLabel lblEscogerOperaciones;
	private JButton btnVerTarifasAsociadas;
	private JButton btnAsignarUnaTarifa;
	private JButton btnModificarHabitaciones;
	private JButton btnVerHabitaciones;
	private JButton btnDarDeAlta;
	private JButton btnCrearNuevoTipoDeHabitacion;
	private JButton btnModificarTipo;
	private CardTiposAsignarTarifa tiposAsignarTarifa;
	private CardTiposConsultarTarifas consultarTarifasTipo;
	//PARA QUE SE VEAN LOS CARD EN EL DESIGN HAY QUE COMENTAR LA LINEA superCard.show(panelSuperCard, "panelElegirAccion"); DEL METODO GETPANELSUPERCARD();


	

	/**
	 * Create the frame.
	 * @param papa 
	 */
	public CardGerenteHabitaciones(VentanaGerentePrincipal papa) {
		padre = papa;
		setBounds(100, 100, 755, 626);
		setLayout(new BorderLayout(0, 0));
		add(getPanelSuperCard(), BorderLayout.CENTER);
		setVisible(true);
		
	}
	
	public JPanel getPanelSuperCard() {
		if (panelSuperCard == null) {
			panelSuperCard = new JPanel();
			panelSuperCard.setLayout(superCard);
			panelSuperCard.add(getPanelElegirAccion(), "panelElegirAccion");
			CardVistaHabitaciones cardVistaHabitaciones = new CardVistaHabitaciones(this);
			cardVistaHabitaciones.todasHabitaciones();
			panelSuperCard.add(cardVistaHabitaciones, "cardVistaHabitaciones");
			panelSuperCard.add(new CardModificarHabitacion(padre.getCon(),padre, this), "panelModificarHabitacion");
			panelSuperCard.add(new CardRegistrarHabitacion(padre.getCon(),padre, this), "panelRegistrarHabitaciones");
			
			consultarTarifasTipo = new CardTiposConsultarTarifas(padre.getCon().getCon(), this);
			panelSuperCard.add(consultarTarifasTipo, "panelConsultarTarifasTipo");
			
			tiposAsignarTarifa = new CardTiposAsignarTarifa(padre.getCon(), this);
			panelSuperCard.add(tiposAsignarTarifa, "panelAsignarTarifasTipo");
			panelSuperCard.add(new CardTiposNuevoTipo(padre.getCon().getCon(), this), "panelNuevoTipo");
			panelSuperCard.add(new CardTiposModificacion(padre.getCon().getCon(), this), "panelModificarTipo");
		}
		//superCard.show(panelSuperCard, "panelElegirAccion");
		return panelSuperCard;
	}
	public CardLayout getSuperCard() {
		return superCard;
	}
	private JPanel getPanelElegirAccion() {
		if (panelElegirAccion == null) {
			panelElegirAccion = new JPanel();
			panelElegirAccion.setName("panelElegirAccion");
			panelElegirAccion.setBorder(new TitledBorder(null, "Tipos de habitaci\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelElegirAccion.setLayout(null);
			panelElegirAccion.add(getLblEscogerOperaciones());
			panelElegirAccion.add(getBtnVerTarifasAsociadas());
			panelElegirAccion.add(getBtnAsignarUnaTarifa());
			panelElegirAccion.add(getBtnDarAlta());
			panelElegirAccion.add(getBtnModificarHabitaciones());
			panelElegirAccion.add(getBtnVerHabitaciones());
			panelElegirAccion.add(getBtnCrearNuevoTipoDeHabitacion());
			panelElegirAccion.add(getBtnModificarTipo());
		}
		return panelElegirAccion;
	}
	private JButton getBtnDarAlta() {
		if (btnDarDeAlta == null) {
			btnDarDeAlta = new JButton("Dar de alta habitacion");
			btnDarDeAlta.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					superCard.show(panelSuperCard, "panelRegistrarHabitaciones");
				}
			});
			btnDarDeAlta.setBounds(221, 137, 303, 44);
		}
		return btnDarDeAlta;
	}
	private JLabel getLblEscogerOperaciones() {
		if (lblEscogerOperaciones == null) {
			lblEscogerOperaciones = new JLabel("\u00BFQu\u00E9 operaci\u00F3n desea realizar?");
			lblEscogerOperaciones.setHorizontalAlignment(SwingConstants.CENTER);
			lblEscogerOperaciones.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
			lblEscogerOperaciones.setBounds(66, 65, 613, 61);
		}
		return lblEscogerOperaciones;
	}
	private JButton getBtnVerTarifasAsociadas() {
		if (btnVerTarifasAsociadas == null) {
			btnVerTarifasAsociadas = new JButton("Ver tarifas asociadas a tipos de habitaci\u00F3n");
			btnVerTarifasAsociadas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					consultarTarifasTipo.rellenarTablaTarifas();
					superCard.show(panelSuperCard, "panelConsultarTarifasTipo");
				}
			});
			btnVerTarifasAsociadas.setMnemonic('V');
			btnVerTarifasAsociadas.setToolTipText("Muestra el listado de todas las tarifas asociadas a los distintos tipos de habitaciones.\nMuestra tambi\u00E9n la fecha de alta y la fecha de baja de cada una de las taarifas.");
			btnVerTarifasAsociadas.setBounds(221, 302, 303, 44);
		}
		return btnVerTarifasAsociadas;
	}
	public void mostrarOpciones() {
		superCard.show(panelSuperCard, "panelElegirAccion");
	}
	private JButton getBtnAsignarUnaTarifa() {
		if (btnAsignarUnaTarifa == null) {
			btnAsignarUnaTarifa = new JButton("Asignar una tarifa a un tipo de habitaci\u00F3n");
			btnAsignarUnaTarifa.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					superCard.show(panelSuperCard, "panelAsignarTarifasTipo");
				}
			});
			btnAsignarUnaTarifa.setMnemonic('A');
			btnAsignarUnaTarifa.setToolTipText("Asigne una tarifa a un tipo de habitaci\u00F3n especificando un importe y un rango de fechas en las que la tarifa se aplicar\u00E1.");
			btnAsignarUnaTarifa.setBounds(221, 358, 303, 44);
		}
		return btnAsignarUnaTarifa;
	}
	
	private JButton getBtnModificarHabitaciones() {
		if (btnModificarHabitaciones == null) {
			btnModificarHabitaciones = new JButton("Modificar habitacion");
			btnModificarHabitaciones.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					superCard.show(panelSuperCard, "panelModificarHabitacion");
				}
			});
			btnModificarHabitaciones.setBounds(221, 247, 303, 44);
		}
		return btnModificarHabitaciones;
	}
	private JButton getBtnVerHabitaciones() {
		if (btnVerHabitaciones == null) {
			btnVerHabitaciones = new JButton("Ver habitaciones");
			btnVerHabitaciones.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					superCard.show(panelSuperCard, "cardVistaHabitaciones");
				}
			});
			btnVerHabitaciones.setBounds(221, 192, 303, 44);
		}
		return btnVerHabitaciones;
	}
	private JButton getBtnCrearNuevoTipoDeHabitacion() {
		if (btnCrearNuevoTipoDeHabitacion == null) {
			btnCrearNuevoTipoDeHabitacion = new JButton("Crear nuevo tipo de habitaci\u00F3n");
			btnCrearNuevoTipoDeHabitacion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					superCard.show(panelSuperCard, "panelNuevoTipo");
				}
			});
			btnCrearNuevoTipoDeHabitacion.setToolTipText("Cree un nuevo tipo de habitaci\u00F3n asign\u00E1ndole una tarifa.");
			btnCrearNuevoTipoDeHabitacion.setMnemonic('A');
			btnCrearNuevoTipoDeHabitacion.setBounds(221, 414, 303, 44);
		}
		return btnCrearNuevoTipoDeHabitacion;
	}
	private JButton getBtnModificarTipo() {
		if (btnModificarTipo == null) {
			btnModificarTipo = new JButton("Modificar tipo de habitaci\u00F3n");
			btnModificarTipo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					superCard.show(panelSuperCard, "panelModificarTipo");
				}
			});
			btnModificarTipo.setToolTipText("Modifique los datos de una tarifa asociada a un tipo de habitaci\u00F3n.");
			btnModificarTipo.setMnemonic('A');
			btnModificarTipo.setBounds(221, 470, 303, 44);
		}
		return btnModificarTipo;
	}
}
