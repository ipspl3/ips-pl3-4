package igu;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.CardLayout;

import javax.swing.JLabel;
import java.awt.Font;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.GridLayout;
import javax.swing.JComboBox;
import com.toedter.calendar.JDateChooser;

import igu.CardModificarTurnosTrabajo.Trabajador;
import logica.ComparadorFecha;
import logica.Conexion;
import logica.Trabajadores;

import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;

public class CardModificarTurnosTrabajo extends JPanel {
	private JLabel lblModificarTurnosDe;
	private JPanel pnCentral;
	private JPanel pnSouth;
	private JButton btnModificar;
	private JButton btnAtras;
	private JPanel panel;
	private JPanel panel_1;
	private JPanel panel_2;
	private JLabel lblTrabajador;
	private JLabel lblFechaInicio;
	private JDateChooser fechaInicio;
	private JLabel lblFechaFinal;
	private JDateChooser fechafinal;
	private JButton btnBuscarTrabajadores;
	private JPanel panel_3;
	private JPanel panel_4;
	private JScrollPane scrollPane;
	private JList<String> listTrabajadoresActivos;
	private JButton btnVerHorarioTurno;
	private JLabel lblDiaDeTrabajo;
	private JDateChooser dateChooser;
	private JPanel panel_5;
	private JPanel panel_7;
	private JLabel lblHoraDeEntrada;
	private JTextField txthoraentrada;
	private JLabel lblHoraDeSalida;
	private JTextField txtHoraSalida;
	private JPanel panel_6;
	private JCheckBox chckbxJornadaPartida;
	private JPanel panel_8;
	private JPanel panel_9;
	private JLabel lblEntradaPartida;
	private JTextField txtpartidaentrada;
	private JLabel lblSalidaPartida;
	private JTextField txtpartidasalida;

	private VentanaAdministrador VA;
	private Connection conn;
	private List<Trabajador> trabajadores = new ArrayList<Trabajador>();

	/**
	 * Create the panel.
	 * 
	 * @param ventanaAdministrador
	 * @param con
	 */
	public CardModificarTurnosTrabajo(Conexion con, VentanaAdministrador ventanaAdministrador) {
		this.VA = ventanaAdministrador;
		conn = con.getCon();
		setLayout(new BorderLayout(0, 0));
		add(getLblModificarTurnosDe(), BorderLayout.NORTH);
		add(getPnCentral(), BorderLayout.CENTER);
		add(getPnSouth(), BorderLayout.SOUTH);

	}

	private JLabel getLblModificarTurnosDe() {
		if (lblModificarTurnosDe == null) {
			lblModificarTurnosDe = new JLabel("Modificar turnos de trabajo");
			lblModificarTurnosDe.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 20));
		}
		return lblModificarTurnosDe;
	}

	private JPanel getPnCentral() {
		if (pnCentral == null) {
			pnCentral = new JPanel();
			pnCentral.setLayout(new GridLayout(0, 1, 0, 0));
			pnCentral.add(getPanel_1());
			pnCentral.add(getPanel_1_1());
			pnCentral.add(getPanel_2());
		}
		return pnCentral;
	}

	private JPanel getPnSouth() {
		if (pnSouth == null) {
			pnSouth = new JPanel();
			pnSouth.add(getBtnAtras());
			pnSouth.add(getBtnModificar());
		}
		return pnSouth;
	}

	private JButton getBtnModificar() {
		if (btnModificar == null) {
			btnModificar = new JButton("Modificar");
			btnModificar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					comprobarCamposVaciosModificar();
					Trabajador worker = getWorker();
					Date fi = worker.getFechainicio();
					Date ff = worker.getFechafinal();
					String dni = worker.getDNI();
					
					
					if (!getChckbxJornadaPartida().isSelected()) {
						String[] he = getTxthoraentrada().getText().split(":");
						int horaentrada = Integer.parseInt(he[0]) * 100 + Integer.parseInt(he[1]);
						String[] hs = getTxtHoraSalida().getText().split(":");
						int horasalida = Integer.parseInt(hs[0]) * 100 + Integer.parseInt(hs[1]);
						int horastrabajadas =is8horasTrabajadas();
						if(horastrabajadas > 800)
						{
							JOptionPane.showMessageDialog(null, "Duaracion de jornada mayor a 8 horas.");
						}
						else
						{
							ResultSet rs = comprobar12horasSigientes(ff,dni);
							int result=700;
							try {
								result = rs.getRow();
							} catch (SQLException e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}
							try {
								if(result!=0)//Esta vacio, no hay turno dentro de 12 horas
								{
									ComprobarCriteriosDeTrabajo(worker, fi, dni, horaentrada, horastrabajadas);
								}
								else//Esta vacio a las 12 horas siguientes
								{//al no estar vacio comprobar 12 horas siguiente que pasa
									int horadiasiguiente = horaDeDia(rs);
									if(horasalida-horadiasiguiente>1200)//Comprobar dia siguiente 
									{
										JOptionPane.showMessageDialog(null,
												"No guarda 12 horas de descanso con el dia anterior de trabajo.");

									}
									else //Si guarda 12 horas
									{//Comprobar dia anterior y 40 horas
										ComprobarCriteriosDeTrabajo(worker, fi, dni, horaentrada, horastrabajadas);
									}
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
								
						}
						
					}
					else//Sobre Jornada partida
					{
						String[] he = getTxthoraentrada().getText().split(":");
						int horaentrada = Integer.parseInt(he[0]) * 100 + Integer.parseInt(he[1]);
						String[] hs = getTxtHoraSalida().getText().split(":");
						int horasalidap = Integer.parseInt(hs[0]) * 100 + Integer.parseInt(hs[1]);

						String[] hep = getTxtpartidaentrada().getText().split(":");
						int horaentradap = Integer.parseInt(hep[0]) * 100 + Integer.parseInt(hep[1]);
						String[] hsp = getTxtpartidaentrada().getText().split(":");
						int horasalida = Integer.parseInt(hsp[0]) * 100 + Integer.parseInt(hsp[1]);
						
						int horastrabajadas =is8horasTrabajadasJornadaPartida();
						if(horastrabajadas > 800)
						{
							JOptionPane.showMessageDialog(null, "Duaracion de jornada mayor a 8 horas.");
						}
						else
						{
							ResultSet rs = comprobar12horasSigientes(ff,dni);
							int result=700;
							try {
								result = rs.getRow();
							} catch (SQLException e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}
							try {
								if(result!=0)//Esta vacio, no hay turno dentro de 12 horas
								{
									ComprobarCriteriosDeTrabajo(worker, fi, dni, horaentrada, horastrabajadas);
								}
								else//Esta vacio a las 12 horas siguientes
								{//al no estar vacio comprobar 12 horas siguiente que pasa
									int horadiasiguiente = horaDeDia(rs);
									if(horasalida-horadiasiguiente>1200)//Comprobar dia siguiente 
									{
										JOptionPane.showMessageDialog(null,
												"No guarda 12 horas de descanso con el dia anterior de trabajo.");

									}
									else //Si guarda 12 horas
									{//Comprobar dia anterior y 40 horas
										ComprobarCriteriosDeTrabajo(worker, fi, dni, horaentrada, horastrabajadas);
									}
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
								
						}
						
						
					}
					
				}

				
			

				

				

				

				
			
			});

		}
		return btnModificar;
	}
	private void ComprobarCriteriosDeTrabajo(Trabajador worker, Date fi, String dni, int horaentrada,
			int horastrabajadas) throws SQLException {
		ResultSet rs;
		rs = comprobar12horasAnteriores(fi, dni);
		if(rs.getRow()!=0)
		{
			int horastrabajadastotales = comprobar40horas(horastrabajadas, fi, dni);
			if (horastrabajadastotales <= 4000) 
			{
				actualizardatosTurno(worker);
			}//Comprobar 40 horas
			else
			{
				//Se pasa de las 40 horas
				double horasreales = horastrabajadas / 100;
				String[] horas = String.valueOf(horasreales).split(",");
				JOptionPane.showMessageDialog(null,
						"No puede trabajar horas su limite son 40 horas.");
			}
				
		}
		else//No esta vacio las 12 horas anteriores
		{
			int horadiasanterior = horaDeDia(rs);
			if(horadiasanterior-horaentrada>1200)//turno anterior no respeta limite 12 horas
			{
				JOptionPane.showMessageDialog(null,
						"No guarda 12 horas de descanso con el dia anterior de trabajo.");

			}
			else //Se respeta limite con lo anterior
			{//Mirar si cumple 40 horas
				int horastrabajadastotales = comprobar40horas(horastrabajadas, fi, dni);
				if (horastrabajadastotales <= 4000) 
				{
					actualizardatosTurno(worker);
				}//Comprobar 40 horas
				else
				{
					//Se pasa de las 40 horas
					double horasreales = horastrabajadas / 100;
					String[] horas = String.valueOf(horasreales).split(",");
					JOptionPane.showMessageDialog(null,
							"No puede trabajar horas su limite son 40 horas.");
				}
			}
		}
	}

	private int horaDeDia(ResultSet rs) throws SQLException {
		int horadiasanterior=0;
		while (rs.next()) {
			String[] hora = rs.getString(1).split(":");
			horadiasanterior = Integer.parseInt(hora[0]) * 100
					+ Integer.parseInt(hora[1]);
		}
		return horadiasanterior;
	}
	private ResultSet comprobar12horasAnteriores(Date fi, String dni) throws SQLException {
		ResultSet rs;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fi);
		calendar.add(Calendar.DAY_OF_YEAR, -1);
		java.sql.Date diaanterior = new java.sql.Date(calendar.getTimeInMillis());
		PreparedStatement ps = conn.prepareStatement(
				"SELECT HORA_SALIDA FROM TURNO " + "WHERE DNI=? AND FECHA_INICIO=?");
		ps.setString(1, dni);
		ps.setDate(2, (Date) diaanterior);
		rs = ps.executeQuery();
		return rs;
	}
	private ResultSet comprobar12horasSigientes(Date ff, String dni) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(ff);
		calendar.add(Calendar.DAY_OF_YEAR, 1);
		java.sql.Date diasiguiente = new java.sql.Date(calendar.getTimeInMillis());
		PreparedStatement ps;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(
						"SELECT HORA_ENTRADA FROM TURNO " + "WHERE DNI=? AND FECHA_INICIO=?");
			ps.setString(1, dni);
			ps.setDate(2, diasiguiente);
			rs = ps.executeQuery();
			

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return rs;
	}
	private int is8horasTrabajadasJornadaPartida() {
		String[] he = getTxthoraentrada().getText().split(":");
		int horaentrada = Integer.parseInt(he[0]) * 100 + Integer.parseInt(he[1]);
		String[] hs = getTxtHoraSalida().getText().split(":");
		int horasalida = Integer.parseInt(hs[0]) * 100 + Integer.parseInt(hs[1]);

		String[] hep = getTxtpartidaentrada().getText().split(":");
		int horaentradap = Integer.parseInt(hep[0]) * 100 + Integer.parseInt(hep[1]);
		String[] hsp = getTxtpartidaentrada().getText().split(":");
		int horasalidap = Integer.parseInt(hsp[0]) * 100 + Integer.parseInt(hsp[1]);

		int horastrabajadas = (horasalida - horaentrada) + (horasalidap - horaentradap);
		return horastrabajadas;
		
	}
	
	private int is8horasTrabajadas() {
		String[] he = getTxthoraentrada().getText().split(":");
		int horaentrada = Integer.parseInt(he[0]) * 100 + Integer.parseInt(he[1]);
		String[] hs = getTxtHoraSalida().getText().split(":");
		int horasalida = Integer.parseInt(hs[0]) * 100 + Integer.parseInt(hs[1]);
		int horastrabajadas = horasalida-horaentrada;
		return horastrabajadas;
		
	}
	private Trabajador getWorker() {
		String[] trabajador = getListTrabajadoresActivos().getSelectedValue().split("--");
		String nombre = trabajador[0];
		String id = trabajador[2];
		Trabajador worker = null;
		for (Trabajador t : trabajadores) {
			if (nombre.equals(t.getName()) && id.equals(t.getTurnoid())) {
				worker = t;
			}
		}
		return worker;
	}

	private int comprobar40horas(int i, Date fi, String dni) {
		int horastotales = i;
		Calendar c = Calendar.getInstance();
		c.setTime(fi);

		PreparedStatement ps;
		ResultSet rs;

		// calendar.add(Calendar.DAY_OF_YEAR, 1);
		for (int a = 1; a < 7; a++) {
			c.add(Calendar.DAY_OF_YEAR, 1);
			java.sql.Date fecha = new java.sql.Date(c.getTimeInMillis());
			try {
				ps = conn.prepareStatement(
						"SELECT HORA_ENTRADA,HORA_SALIDA " + "FROM TURNO " + "WHERE DNI=? AND FECHA_INICIO=?");
				ps.setString(1, dni);
				ps.setDate(2, fecha);
				rs = ps.executeQuery();
				while(rs.next())
				{
					String[] he = rs.getString(1).split(":");
					int horaentrada = (Integer.parseInt(he[0]) * 100) + Integer.parseInt(he[1]);
					String[] hs = rs.getString(2).split(":");
					int horasalida = Integer.parseInt(hs[0]) * 100 + Integer.parseInt(hs[1]);

					horastotales = horastotales + (horasalida - horaentrada);
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return horastotales;
	}

	private void actualizardatosTurno(Trabajador worker) {
		PreparedStatement stmt;
		try {
			stmt = conn.prepareStatement(
					"UPDATE TURNO " + "SET FECHA_INICIO = ?,FECHA_FIN = ?,HORA_ENTRADA = ?, HORA_SALIDA = ? "
							+ "WHERE ID = ? AND DNI = ?");
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(getDateChooser().getDate());
			java.sql.Date fecha = new java.sql.Date(calendar.getTimeInMillis());

			stmt.setDate(1, fecha);
			stmt.setDate(2, fecha);
			stmt.setString(3, getTxthoraentrada().getText());
			stmt.setString(4, getTxtHoraSalida().getText());
			stmt.setString(5, worker.getTurnoid());
			stmt.setString(6, worker.getDNI());

			stmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JOptionPane.showMessageDialog(null, "Turno actualizado correctamente");

	}

	private void actualizardatosTurno2(Trabajador worker) {
		PreparedStatement stmt;
		try {
			stmt = conn.prepareStatement(
					"UPDATE TURNO " + "SET FECHA_INICIO = ?,FECHA_FIN = ?,HORA_ENTRADA = ?, HORA_SALIDA = ? "
							+ "WHERE ID = ? AND DNI = ?");

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(getDateChooser().getDate());
			java.sql.Date fecha = new java.sql.Date(calendar.getTimeInMillis());

			stmt.setDate(1, fecha);
			stmt.setDate(2, fecha);
			stmt.setString(3, getTxthoraentrada().getText());
			stmt.setString(4, getTxtHoraSalida().getText());
			stmt.setString(5, worker.getTurnoid());
			stmt.setString(6, worker.getDNI());

			stmt.executeUpdate();

			int id = 0;
			stmt = conn.prepareStatement("SELECT count(*) FROM TURNO");
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				id = rs.getInt(1);
			}

			stmt = conn.prepareStatement("INSERT INTO TURNO VALUES (?,?,?,?,?,?,?)");

			stmt.setInt(1, id);
			stmt.setString(2, "activo");
			stmt.setDate(3, fecha);
			stmt.setDate(4, fecha);
			stmt.setString(5, getTxtpartidaentrada().getText());
			stmt.setString(6, getTxtpartidasalida().getText());
			stmt.setString(7, worker.getDNI());

			stmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JOptionPane.showMessageDialog(null, "Turno actualizado correctamente");

	}

	private JButton getBtnAtras() {
		if (btnAtras == null) {
			btnAtras = new JButton("Atras");
			btnAtras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						VA.getCard().show(VA.getPanelCards(), "panelElegirOpcionesTrabajadores");
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			});
		}
		return btnAtras;
	}

	private JPanel getPanel_1() {
		if (panel == null) {
			panel = new JPanel();
			panel.setLayout(new BorderLayout(0, 0));
			panel.add(getPanel_3(), BorderLayout.CENTER);
			panel.add(getPanel_4(), BorderLayout.SOUTH);
		}
		return panel;
	}

	private JPanel getPanel_1_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setLayout(null);
			panel_1.add(getLblTrabajador());
			panel_1.add(getScrollPane());
			panel_1.add(getBtnVerHorarioTurno());
		}
		return panel_1;
	}

	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.setLayout(new BorderLayout(0, 0));
			panel_2.add(getPanel_5(), BorderLayout.WEST);
			panel_2.add(getPanel_7());
		}
		return panel_2;
	}

	private JLabel getLblTrabajador() {
		if (lblTrabajador == null) {
			lblTrabajador = new JLabel("Trabajadores activos:");
			lblTrabajador.setBounds(6, 53, 134, 16);
		}
		return lblTrabajador;
	}

	private JLabel getLblFechaInicio() {
		if (lblFechaInicio == null) {
			lblFechaInicio = new JLabel("Fecha inicio:");
		}
		return lblFechaInicio;
	}

	private JDateChooser getFechaInicio() {
		if (fechaInicio == null) {
			fechaInicio = new JDateChooser();
		}
		return fechaInicio;
	}

	private JLabel getLblFechaFinal() {
		if (lblFechaFinal == null) {
			lblFechaFinal = new JLabel("Fecha final:");
		}
		return lblFechaFinal;
	}

	private JDateChooser getFechafinal() {
		if (fechafinal == null) {
			fechafinal = new JDateChooser();
		}
		return fechafinal;
	}

	private JButton getBtnBuscarTrabajadores() {
		if (btnBuscarTrabajadores == null) {
			btnBuscarTrabajadores = new JButton("Buscar trabajadores activos");
			btnBuscarTrabajadores.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					if (getFechaInicio().getDate() == null || getFechafinal().getDate() == null) {
						JOptionPane.showMessageDialog(null,
								"Debe seleccionar las fechas en las que desea buscar trabajadores activos.");

					}
					SimpleDateFormat Formato = new SimpleDateFormat("dd/MM/yyyy");
					ComparadorFecha comparadorfecha = new ComparadorFecha(Formato.format(getFechaInicio().getDate()),
							Formato.format(getFechafinal().getDate()));
					if (!comparadorfecha.CheckFechaFinal()) {
						JOptionPane.showMessageDialog(null, "Fecha final menor que fecha inical.");
					} else {
						try {
							PreparedStatement workers = conn
									.prepareStatement("SELECT t.NOMBRE,tu.ID,tu.FECHA_INICIO,tu.FECHA_FIN,t.DNI "
											+ "FROM TRABAJADOR t,TURNO tu "
											+ "WHERE t.DNI = tu.DNI AND tu.TIPO = 'activo'");
							ResultSet rs = workers.executeQuery();
							DefaultListModel<String> modeloLista = new DefaultListModel<String>();

							while (rs.next()) {
								String nombre = rs.getString(1);
								String id = rs.getString(2);
								Date fechainicio = rs.getDate(3);
								Date fechafinal = rs.getDate(4);
								String dni = rs.getString(5);

								Calendar calendar = Calendar.getInstance();
								calendar.setTime(getFechaInicio().getDate());
								java.sql.Date diainicio = new java.sql.Date(calendar.getTimeInMillis());

								calendar.setTime(getFechafinal().getDate());
								java.sql.Date diafinal = new java.sql.Date(calendar.getTimeInMillis());

								if (fechainicio.compareTo(diainicio) >= 0 && fechafinal.compareTo(diafinal) <= 0) {
									modeloLista.addElement(nombre + "--Turno--" + id);
									Trabajador trabajador = new Trabajador(nombre, id, fechainicio, fechafinal, dni);
									trabajadores.add(trabajador);

								}
							}
							getListTrabajadoresActivos().setModel(modeloLista);
							workers.close();
							rs.close();

						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}

				}
			});
		}
		return btnBuscarTrabajadores;
	}

	private JPanel getPanel_3() {
		if (panel_3 == null) {
			panel_3 = new JPanel();
			panel_3.add(getLblFechaInicio());
			panel_3.add(getFechaInicio());
			panel_3.add(getLblFechaFinal());
			panel_3.add(getFechafinal());
		}
		return panel_3;
	}

	private JPanel getPanel_4() {
		if (panel_4 == null) {
			panel_4 = new JPanel();
			panel_4.add(getBtnBuscarTrabajadores());
		}
		return panel_4;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(152, 6, 348, 108);
			scrollPane.setViewportView(getListTrabajadoresActivos());
		}
		return scrollPane;
	}

	private JList<String> getListTrabajadoresActivos() {
		if (listTrabajadoresActivos == null) {
			listTrabajadoresActivos = new JList<String>();
			listTrabajadoresActivos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		}
		return listTrabajadoresActivos;
	}

	private JButton getBtnVerHorarioTurno() {
		if (btnVerHorarioTurno == null) {
			btnVerHorarioTurno = new JButton("Ver horario \ny fecha turno");
			btnVerHorarioTurno.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (!getListTrabajadoresActivos().getSelectedValue().equals(null)) {
						String[] trabajador = getListTrabajadoresActivos().getSelectedValue().split("--");
						String nombre = trabajador[0];
						String id = trabajador[2];
						Trabajador worker = null;
						for (Trabajador t : trabajadores) {
							if (nombre.equals(t.getName()) && id.equals(t.getTurnoid())) {
								worker = t;
							}
						}
						Date fi = worker.getFechainicio();
						Date ff = worker.getFechafinal();
						String dni = worker.getDNI();

						try {
							PreparedStatement turno = conn.prepareStatement("SELECT count(*) " + "FROM TURNO "
									+ "WHERE DNI=? AND FECHA_INICIO = ? AND FECHA_FIN = ?");
							turno.setString(1, dni);
							turno.setDate(2, fi);
							turno.setDate(3, ff);
							ResultSet rs = turno.executeQuery();
							int jornadapartida = 0;
							while (rs.next()) {
								jornadapartida = rs.getInt(1);
							}
							turno = conn.prepareStatement("SELECT HORA_ENTRADA,HORA_SALIDA " + "FROM TURNO "
									+ "WHERE DNI=? AND FECHA_INICIO = ? AND FECHA_FIN = ?");
							turno.setString(1, dni);
							turno.setDate(2, fi);
							turno.setDate(3, ff);
							rs = turno.executeQuery();
							if (jornadapartida == 2) {
								int count = 0;

								while (rs.next()) {
									if (count == 0) {
										getChckbxJornadaPartida().setSelected(true);
										getDateChooser().setDate(fi);
										getTxthoraentrada().setText(rs.getString(1));
										getTxtHoraSalida().setText(rs.getString(2));
										count++;
									} else {
										getTxtpartidaentrada().setText(rs.getString(1));
										getTxtpartidasalida().setText(rs.getString(2));
									}

								}

							} else {
								while (rs.next()) {
									getChckbxJornadaPartida().setSelected(false);
									getDateChooser().setDate(fi);
									getTxthoraentrada().setText(rs.getString(1));
									getTxtHoraSalida().setText(rs.getString(2));
								}
							}
							// Contar trabajadores para ver si tiene jornada
							// dividida
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					} else {
						JOptionPane.showMessageDialog(null, "Selecciona el trabajador y turno que desea modificar.");

					}
				}
			});
			btnVerHorarioTurno.setBounds(526, 17, 223, 91);
		}
		return btnVerHorarioTurno;
	}

	private JLabel getLblDiaDeTrabajo() {
		if (lblDiaDeTrabajo == null) {
			lblDiaDeTrabajo = new JLabel("Dia de trabajo:");
		}
		return lblDiaDeTrabajo;
	}

	private JDateChooser getDateChooser() {
		if (dateChooser == null) {
			dateChooser = new JDateChooser();
		}
		return dateChooser;
	}

	private JPanel getPanel_5() {
		if (panel_5 == null) {
			panel_5 = new JPanel();
			panel_5.setLayout(new GridLayout(0, 1, 0, 0));
			panel_5.add(getPanel_6());
			panel_5.add(getChckbxJornadaPartida());
		}
		return panel_5;
	}

	private JPanel getPanel_7() {
		if (panel_7 == null) {
			panel_7 = new JPanel();
			panel_7.setLayout(new GridLayout(0, 1, 0, 0));
			panel_7.add(getPanel_8());
			panel_7.add(getPanel_9());
		}
		return panel_7;
	}

	private JLabel getLblHoraDeEntrada() {
		if (lblHoraDeEntrada == null) {
			lblHoraDeEntrada = new JLabel("Hora de entrada:");
		}
		return lblHoraDeEntrada;
	}

	private JTextField getTxthoraentrada() {
		if (txthoraentrada == null) {
			txthoraentrada = new JTextField();
			txthoraentrada.setColumns(5);
		}
		return txthoraentrada;
	}

	private JLabel getLblHoraDeSalida() {
		if (lblHoraDeSalida == null) {
			lblHoraDeSalida = new JLabel("Hora de salida:");
		}
		return lblHoraDeSalida;
	}

	private JTextField getTxtHoraSalida() {
		if (txtHoraSalida == null) {
			txtHoraSalida = new JTextField();
			txtHoraSalida.setColumns(5);
		}
		return txtHoraSalida;
	}

	private JPanel getPanel_6() {
		if (panel_6 == null) {
			panel_6 = new JPanel();
			panel_6.add(getLblDiaDeTrabajo());
			panel_6.add(getDateChooser());
		}
		return panel_6;
	}

	private JCheckBox getChckbxJornadaPartida() {
		if (chckbxJornadaPartida == null) {
			chckbxJornadaPartida = new JCheckBox("Jornada partida");
			chckbxJornadaPartida.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (chckbxJornadaPartida.isSelected()) {
						getLblEntradaPartida().setEnabled(true);
						getLblSalidaPartida().setEnabled(true);
						getTxtpartidaentrada().setEnabled(true);
						getTxtpartidasalida().setEnabled(true);
					} else if (!chckbxJornadaPartida.isSelected()) {
						getLblEntradaPartida().setEnabled(false);
						getLblSalidaPartida().setEnabled(false);
						getTxtpartidaentrada().setEnabled(false);
						getTxtpartidasalida().setEnabled(false);
					}
				}
			});
		}
		return chckbxJornadaPartida;
	}

	private JPanel getPanel_8() {
		if (panel_8 == null) {
			panel_8 = new JPanel();
			panel_8.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			panel_8.add(getLblHoraDeEntrada());
			panel_8.add(getTxthoraentrada());
			panel_8.add(getLblHoraDeSalida());
			panel_8.add(getTxtHoraSalida());
		}
		return panel_8;
	}

	private JPanel getPanel_9() {
		if (panel_9 == null) {
			panel_9 = new JPanel();
			panel_9.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			panel_9.add(getLblEntradaPartida());
			panel_9.add(getTxtpartidaentrada());
			panel_9.add(getLblSalidaPartida());
			panel_9.add(getTxtpartidasalida());
		}
		return panel_9;
	}

	private JLabel getLblEntradaPartida() {
		if (lblEntradaPartida == null) {
			lblEntradaPartida = new JLabel("Hora de entrada:");
			lblEntradaPartida.setEnabled(false);
		}
		return lblEntradaPartida;
	}

	private JTextField getTxtpartidaentrada() {
		if (txtpartidaentrada == null) {
			txtpartidaentrada = new JTextField();
			txtpartidaentrada.setEnabled(false);
			txtpartidaentrada.setColumns(5);

		}
		return txtpartidaentrada;
	}

	private JLabel getLblSalidaPartida() {
		if (lblSalidaPartida == null) {
			lblSalidaPartida = new JLabel("Hora de salida:");
			lblSalidaPartida.setEnabled(false);

		}
		return lblSalidaPartida;
	}

	private JTextField getTxtpartidasalida() {
		if (txtpartidasalida == null) {
			txtpartidasalida = new JTextField();
			txtpartidasalida.setEnabled(false);
			txtpartidasalida.setColumns(5);

		}
		return txtpartidasalida;
	}

	private void comprobarCamposVaciosModificar() {
		if (getTxthoraentrada().getText().equals("") || getTxtHoraSalida().getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Escriba la hora que desea modificar.");
		}
		if ((getChckbxJornadaPartida().isSelected()) && (getTxtpartidaentrada().getText().equals("")
				|| getTxtpartidasalida().getText().equals(""))) {
			JOptionPane.showMessageDialog(null, "Escriba el horario de la jornada partida.");

		}
	}

	public class Trabajador {
		private String name;
		private String turnoid;
		private Date fechainicio;
		private Date fechafinal;
		private String dni;

		public Trabajador(String name, String turnoid, Date fechainicio, Date fechafinal, String dni) {
			this.name = name;
			this.turnoid = turnoid;
			this.fechainicio = fechainicio;
			this.fechafinal = fechafinal;
			this.dni = dni;
		}

		public String getName() {
			return name;
		}

		public String getTurnoid() {
			return turnoid;
		}

		public Date getFechainicio() {
			return fechainicio;
		}

		public Date getFechafinal() {
			return fechafinal;
		}

		public String getDNI() {
			return dni;
		}

	}

}
