package igu;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import logica.Conexion;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import java.util.Date;

import java.awt.Color;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;

import javax.swing.JComboBox;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.border.EtchedBorder;

public class CardSuplementosAlojamientoNuevoSuplemento extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Conexion con;
	private JPanel contentPane;
	private JScrollPane spTablaSuplementos;
	private JTable tablaSuplementos;
	private String[] cabecera = {"SUPLEMENTO", "TIPO DE HABITACIÓN", "IMPORTE POR DÍA", "FECHA DE ALTA", "FECHA DE BAJA"};
	private JPanel panelCentral;
	private JPanel panelTabla;
	private String tipoSeleccionado;
	private int id_supl;
	private JPanel panelNuevoSuplemento;
	private JLabel lblTipoDeHabitacionCrearNuevoSuplemento;
	private JComboBox<String> cbTipoHabitacionCrearNuevoSuplemento;
	private JLabel lblNombreSuplementoCrearNuevoSuplemento;
	private JTextField tfNombreSuplementoCrearNuevoSuplemento;
	private JLabel lblImporteCrearNuevoSuplemento;
	private JTextField tfImporteCrearNuevoSuplemento;
	private JDateChooser dcFechaInicioCrearNuevoSuplemento;
	private JLabel lblFechaInicioCrearNuevoSuplemento;
	private JLabel lblFechaFinCrarNuevoSuplemento;
	private JDateChooser dcFechaFinCrearNuevoSuplemento;
	private JButton btnCrearSuplemento;
	private String nombreNuevoSuplemento;
	private JLabel lblEstosSonLos;

	
	public class MiModeloTabla extends DefaultTableModel{
		private static final long serialVersionUID = 1L;
		public MiModeloTabla(Object[] columnNames, int numFilas) {
			super(columnNames,numFilas);
		}
		
		public boolean isCellEditable(int numFila, int numColumna){
			return false;
		}
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public CardSuplementosAlojamientoNuevoSuplemento(Conexion conexion) throws SQLException {
		this.con = conexion;
		setBounds(100, 100,  755, 626);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setLayout(new BorderLayout(0, 0));
		add(getPanelCentral(), BorderLayout.CENTER);
		rellenarTablaSuplementos();
		setVisible(true);
	}
	private JScrollPane getSpTablaTarifasTipo() {
		if (spTablaSuplementos == null) {
			spTablaSuplementos = new JScrollPane();
			spTablaSuplementos.setBounds(6, 6, 706, 265);
			spTablaSuplementos.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
			spTablaSuplementos.setViewportView(getTablaTarifasTipo());
		}
		return spTablaSuplementos;
	}
	private JTable getTablaTarifasTipo() {
		if (tablaSuplementos == null) {
			tablaSuplementos = new JTable();
			tablaSuplementos.setBorder(null);
			tablaSuplementos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tablaSuplementos;
	}
	
	private void rellenarTablaSuplementos() {
		try {
		
			MiModeloTabla miModelo = new MiModeloTabla(cabecera, 0);
			SimpleDateFormat formato = new SimpleDateFormat("dd-MMM-yyyy");//Formatea el tipo Date en java
			
			String consulta = "select s.nombre_suplemento, s.nombre, ps.importe, ps.fecha_inicio, ps.fecha_fin "
					+ "from suplemento_modalidad s, precio_suplemento ps "
					+ "where s.id_supl_mod = ps.id_supl_mod order by ps.fecha_fin desc";
			PreparedStatement ps = con.getCon().prepareStatement(consulta);
			ResultSet rs = ps.executeQuery();
				
			while(rs.next()){
				Object[] fila = new Object[5];
				fila[0] = rs.getString("NOMBRE_SUPLEMENTO");
				fila[1] = rs.getString("NOMBRE");
				fila[2] = rs.getFloat("IMPORTE");
				fila[3] = formato.format(rs.getDate("FECHA_INICIO"));
				fila[4] = formato.format(rs.getDate("FECHA_FIN"));
				
				miModelo.addRow(fila);
			}
			
			tablaSuplementos.setModel(miModelo);
			modificarTablaSuplementos(tablaSuplementos);
			
			rs.close();
			ps.close();
			
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	private void modificarTablaSuplementos(JTable tabla){
		tabla.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 12));
		tabla.setFont(new Font("Tahoma", Font.PLAIN, 12));
		TableColumn columna = tabla.getColumn(cabecera[0]);	
		columna.setMinWidth(150);
		columna.setMaxWidth(150);
		tabla.setRowHeight(25);
	}
	private JPanel getPanelCentral() {
		if (panelCentral == null) {
			panelCentral = new JPanel();
			panelCentral.setBorder(new TitledBorder(null, "Crear un nuevo suplemento de modalidad de alojamiento", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelCentral.setLayout(null);
			panelCentral.add(getPanelNuevoSuplemento());
			panelCentral.add(getPanelTabla());
			panelCentral.add(getLblEstosSonLos());
		}
		return panelCentral;
	}
	private JPanel getPanelTabla() {
		if (panelTabla == null) {
			panelTabla = new JPanel();
			panelTabla.setBorder(new LineBorder(Color.BLACK, 3));
			panelTabla.setBounds(18, 329, 718, 277);
			panelTabla.setLayout(null);
			panelTabla.add(getSpTablaTarifasTipo());
		}
		return panelTabla;
	}

	
	private boolean comprobarImporte(JTextField tfImporte){
		String importe = tfImporte.getText();
		try{
			Double.parseDouble(importe);
			return true;
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "Formato de importe incorrecto");
			tfImporte.setBorder(new LineBorder(Color.RED));
			return false;
		}
	}
	
	private boolean comprobarFechas(JDateChooser fechaInicio, JDateChooser fechaFin){
		try{
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			Date fecha_inicio = formato.parse(formato.format(fechaInicio.getDate()));
			Date fecha_fin = formato.parse(formato.format(fechaFin.getDate()));

			Date fecha_acc = formato.parse(formato.format(new Date()));
			
			if(fecha_inicio.compareTo(fecha_acc) >= 0){
				if(fecha_inicio.compareTo(fecha_fin) > 0){
					JOptionPane.showMessageDialog(null, "La fecha de baja debe de ser mayor que la fecha de alta");
					return false;
				}
				else 
					return true;
			}
			else{
				JOptionPane.showMessageDialog(null, "La fecha de inicio debe ser mayor que la fecha actual del sistema");
				return false;
			}
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "Fechas incorrectas");
			return false;
		}
	}
	
	private void rellenarTiposHab(JComboBox<String> cb){
		try{
			Statement s = con.getCon().createStatement();	
			ResultSet rs = s.executeQuery("select t.nombre from tipo_habitacion t");
			
			while(rs.next()){
				cb.addItem(rs.getString("NOMBRE"));
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	private JPanel getPanelNuevoSuplemento() {
		if (panelNuevoSuplemento == null) {
			panelNuevoSuplemento = new JPanel();
			panelNuevoSuplemento.setBounds(18, 32, 718, 262);
			panelNuevoSuplemento.setFont(new Font("Tahoma", Font.PLAIN, 12));
			panelNuevoSuplemento.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			panelNuevoSuplemento.setLayout(null);
			panelNuevoSuplemento.add(getLblTipoDeHabitacionCrearNuevoSuplemento());
			panelNuevoSuplemento.add(getCbTipoHabitacionCrearNuevoSuplemento());
			panelNuevoSuplemento.add(getLblNombreSuplementoCrearNuevoSuplemento());
			panelNuevoSuplemento.add(getTfNombreSuplementoCrearNuevoSuplemento());
			panelNuevoSuplemento.add(getLblImporteCrearNuevoSuplemento());
			panelNuevoSuplemento.add(getTfImporteCrearNuevoSuplemento());
			panelNuevoSuplemento.add(getDcFechaInicioCrearNuevoSuplemento());
			panelNuevoSuplemento.add(getLblFechaInicioCrearNuevoSuplemento());
			panelNuevoSuplemento.add(getLblFechaFinCrarNuevoSuplemento());
			panelNuevoSuplemento.add(getDcFechaFinCrearNuevoSuplemento());
			panelNuevoSuplemento.add(getBtnCrearSuplemento());
		}
		return panelNuevoSuplemento;
	}
	private JLabel getLblTipoDeHabitacionCrearNuevoSuplemento() {
		if (lblTipoDeHabitacionCrearNuevoSuplemento == null) {
			lblTipoDeHabitacionCrearNuevoSuplemento = new JLabel("Tipo de habitaci\u00F3n:");
			lblTipoDeHabitacionCrearNuevoSuplemento.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblTipoDeHabitacionCrearNuevoSuplemento.setBounds(85, 34, 123, 26);
		}
		return lblTipoDeHabitacionCrearNuevoSuplemento;
	}
	private JComboBox<String> getCbTipoHabitacionCrearNuevoSuplemento() {
		if (cbTipoHabitacionCrearNuevoSuplemento == null) {
			cbTipoHabitacionCrearNuevoSuplemento = new JComboBox<String>();
			cbTipoHabitacionCrearNuevoSuplemento.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					tipoSeleccionado = (String)cbTipoHabitacionCrearNuevoSuplemento.getSelectedItem();
				}
			});
			cbTipoHabitacionCrearNuevoSuplemento.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					String tipo = tipoSeleccionado;
					cbTipoHabitacionCrearNuevoSuplemento.removeAllItems();
					rellenarTiposHab(cbTipoHabitacionCrearNuevoSuplemento);
					if(tipo!= null)
						cbTipoHabitacionCrearNuevoSuplemento.setSelectedItem(tipo);
					else
						cbTipoHabitacionCrearNuevoSuplemento.setSelectedItem(null);
					cbTipoHabitacionCrearNuevoSuplemento.repaint();
				}
			});
			cbTipoHabitacionCrearNuevoSuplemento.setFont(new Font("Tahoma", Font.PLAIN, 12));
			cbTipoHabitacionCrearNuevoSuplemento.setBounds(85, 62, 123, 32);
		}
		return cbTipoHabitacionCrearNuevoSuplemento;
	}
	private JLabel getLblNombreSuplementoCrearNuevoSuplemento() {
		if (lblNombreSuplementoCrearNuevoSuplemento == null) {
			lblNombreSuplementoCrearNuevoSuplemento = new JLabel("Nombre suplemento:");
			lblNombreSuplementoCrearNuevoSuplemento.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblNombreSuplementoCrearNuevoSuplemento.setBounds(283, 34, 123, 26);
		}
		return lblNombreSuplementoCrearNuevoSuplemento;
	}
	private JTextField getTfNombreSuplementoCrearNuevoSuplemento() {
		if (tfNombreSuplementoCrearNuevoSuplemento == null) {
			tfNombreSuplementoCrearNuevoSuplemento = new JTextField();
			tfNombreSuplementoCrearNuevoSuplemento.setFont(new Font("Tahoma", Font.PLAIN, 12));
			tfNombreSuplementoCrearNuevoSuplemento.setBounds(279, 62, 182, 32);
			tfNombreSuplementoCrearNuevoSuplemento.setColumns(10);
		}
		return tfNombreSuplementoCrearNuevoSuplemento;
	}
	private JLabel getLblImporteCrearNuevoSuplemento() {
		if (lblImporteCrearNuevoSuplemento == null) {
			lblImporteCrearNuevoSuplemento = new JLabel("Importe:");
			lblImporteCrearNuevoSuplemento.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblImporteCrearNuevoSuplemento.setBounds(519, 34, 92, 26);
		}
		return lblImporteCrearNuevoSuplemento;
	}
	private JTextField getTfImporteCrearNuevoSuplemento() {
		if (tfImporteCrearNuevoSuplemento == null) {
			tfImporteCrearNuevoSuplemento = new JTextField();
			tfImporteCrearNuevoSuplemento.setFont(new Font("Tahoma", Font.PLAIN, 12));
			tfImporteCrearNuevoSuplemento.setBounds(519, 62, 92, 32);
			tfImporteCrearNuevoSuplemento.setColumns(10);
		}
		return tfImporteCrearNuevoSuplemento;
	}
	private JDateChooser getDcFechaInicioCrearNuevoSuplemento() {
		if (dcFechaInicioCrearNuevoSuplemento == null) {
			dcFechaInicioCrearNuevoSuplemento = new JDateChooser();
			dcFechaInicioCrearNuevoSuplemento.getCalendarButton().setFont(new Font("Tahoma", Font.PLAIN, 11));
			dcFechaInicioCrearNuevoSuplemento.setFont(new Font("Tahoma", Font.PLAIN, 11));
			dcFechaInicioCrearNuevoSuplemento.setBounds(85, 148, 155, 32);
			dcFechaInicioCrearNuevoSuplemento.setMinSelectableDate(new Date());
		}
		return dcFechaInicioCrearNuevoSuplemento;
	}
	private JLabel getLblFechaInicioCrearNuevoSuplemento() {
		if (lblFechaInicioCrearNuevoSuplemento == null) {
			lblFechaInicioCrearNuevoSuplemento = new JLabel("Fecha de alta:");
			lblFechaInicioCrearNuevoSuplemento.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblFechaInicioCrearNuevoSuplemento.setBounds(85, 122, 92, 26);
		}
		return lblFechaInicioCrearNuevoSuplemento;
	}
	private JLabel getLblFechaFinCrarNuevoSuplemento() {
		if (lblFechaFinCrarNuevoSuplemento == null) {
			lblFechaFinCrarNuevoSuplemento = new JLabel("Fecha de baja:");
			lblFechaFinCrarNuevoSuplemento.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblFechaFinCrarNuevoSuplemento.setBounds(283, 122, 92, 26);
		}
		return lblFechaFinCrarNuevoSuplemento;
	}
	private JDateChooser getDcFechaFinCrearNuevoSuplemento() {
		if (dcFechaFinCrearNuevoSuplemento == null) {
			dcFechaFinCrearNuevoSuplemento = new JDateChooser();
			dcFechaFinCrearNuevoSuplemento.getCalendarButton().setFont(new Font("Tahoma", Font.PLAIN, 11));
			dcFechaFinCrearNuevoSuplemento.setFont(new Font("Tahoma", Font.PLAIN, 11));
			dcFechaFinCrearNuevoSuplemento.setBounds(283, 148, 155, 32);
			dcFechaFinCrearNuevoSuplemento.setMinSelectableDate(new Date());
		}
		return dcFechaFinCrearNuevoSuplemento;
	}
	
	private boolean existeSuplemento(String nombreSuplemento, String tipo){	
		try{
			
			String consulta = "select count(nombre_suplemento) "
					+ "from suplemento_modalidad s "
					+ "where s.nombre_suplemento = ? and s.nombre = ? ";
			PreparedStatement ps = con.getCon().prepareStatement(consulta);
			ps.setString(1, nombreSuplemento);
			ps.setString(2, tipo);
			
			ResultSet rs = ps.executeQuery();
			rs.next();
			int cantidadDeSuplementosConEseNombre = rs.getInt("COUNT(NOMBRE_SUPLEMENTO)");
			
			ps.close();
			rs.close();
			if(cantidadDeSuplementosConEseNombre >0)
				return true;
			return false;
		}
		catch(SQLException e){
			e.printStackTrace();
			return false;
		}
	}
	
	private void crearNuevoSuplemento(String nombreSuplemento, String tipo){
		try{
			
			Statement st = con.getCon().createStatement();
			ResultSet rs = st.executeQuery("select SECUENCIA_SUPLMOD.nextVal from dual");
			rs.next();
			int idSuplementoActual = id_supl = rs.getInt("NEXTVAL");
			
			StringBuilder sb = new StringBuilder();
			sb.append("insert into suplemento_modalidad values(?,?,?)");
			PreparedStatement ps = con.getCon().prepareStatement(sb.toString());
			ps.setString(1, nombreSuplemento.toLowerCase());
			ps.setString(2, tipo);
			ps.setInt(3, idSuplementoActual);
			ps.executeUpdate();
			
			rs.close();
			st.close();
			ps.close();
		}
		catch(SQLException e){
			
		}
	}
	
	private boolean insertarTarifa(float nuevoImporte, JDateChooser fechaInicio, JDateChooser fechaFin){
		try{
			StringBuilder sb = new StringBuilder();
			sb.append("insert into precio_suplemento values(secuencia_preciosupl.nextVal,?,?,?,?,?)");
			
			PreparedStatement ps = con.getCon().prepareStatement(sb.toString());
			ps.setDate(1, new java.sql.Date(fechaInicio.getDate().getTime()));
			ps.setDate(2, new java.sql.Date(fechaFin.getDate().getTime()));
			ps.setFloat(3, nuevoImporte);
			ps.setInt(4, id_supl);
			ps.setString(5, tipoSeleccionado);
			
			int actualizacion = ps.executeUpdate();
			ps.close();
			if(actualizacion >=1) {
				return true;
			}
			else
				return false;
			
		}
		catch(SQLException e){
			e.printStackTrace();
			return false;
		}
	}
	private JButton getBtnCrearSuplemento() {
		if (btnCrearSuplemento == null) {
			btnCrearSuplemento = new JButton("Crear");
			btnCrearSuplemento.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					nombreNuevoSuplemento = tfNombreSuplementoCrearNuevoSuplemento.getText().toLowerCase();
					if(tipoSeleccionado != null && nombreNuevoSuplemento != null){
						if(comprobarFechas(dcFechaInicioCrearNuevoSuplemento,dcFechaFinCrearNuevoSuplemento) && comprobarImporte(tfImporteCrearNuevoSuplemento)){
							tfImporteCrearNuevoSuplemento.setBorder(new LineBorder(Color.GREEN));					
							float nuevoImporte = Float.parseFloat(tfImporteCrearNuevoSuplemento.getText());				
							
							if(!existeSuplemento(nombreNuevoSuplemento,tipoSeleccionado)){
								int opcion = JOptionPane.showConfirmDialog(null, "¿Desea confirmar los cambios?", "Confirmar cambios", JOptionPane.YES_NO_OPTION);
								if(opcion == 0){//YES
									crearNuevoSuplemento(nombreNuevoSuplemento, tipoSeleccionado);
									if(insertarTarifa(nuevoImporte, dcFechaInicioCrearNuevoSuplemento, getDcFechaFinCrearNuevoSuplemento())) {
										JOptionPane.showMessageDialog(null, "Suplemento insertado correctamente", "Ok", JOptionPane.INFORMATION_MESSAGE);
										rellenarTablaSuplementos();
										tablaSuplementos.repaint();
										panelTabla.setBorder(new LineBorder(Color.GREEN,3));
										lblEstosSonLos.setVisible(true);
										panelTabla.setVisible(true);
										resetCampos();
									}
									else {
										JOptionPane.showMessageDialog(null, "No se han podido insertar los datos", "Error", JOptionPane.ERROR_MESSAGE);
									}
								}
							}
							else{
								panelTabla.setBorder(new LineBorder(Color.RED,3));
								panelTabla.setVisible(true);
								lblEstosSonLos.setVisible(true);
								JOptionPane.showMessageDialog(null, "Tipo de suplemento ya existente\nPara asignar una tarifa\nvaya a \"Asignar tarifa\"", "Error", JOptionPane.ERROR_MESSAGE);
							}	
								
						}
						//No hace falta else, ya saltan las excepciones en el metodo comprobar fechas
					}
					else{
						JOptionPane.showMessageDialog(null, "¡Faltan datos!", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			});
			btnCrearSuplemento.setFont(new Font("Tahoma", Font.PLAIN, 12));
			btnCrearSuplemento.setBounds(620, 221, 92, 35);
		}
		return btnCrearSuplemento;
	}
	
	private void resetCampos(){
		tfImporteCrearNuevoSuplemento.setText(null);
		tfImporteCrearNuevoSuplemento.setBorder(new LineBorder(Color.BLACK));
		cbTipoHabitacionCrearNuevoSuplemento.setSelectedItem(null);
		tfNombreSuplementoCrearNuevoSuplemento.setText(null);
		dcFechaInicioCrearNuevoSuplemento.setDate(null);
		dcFechaFinCrearNuevoSuplemento.setDate(null);
		id_supl = -1;
		tipoSeleccionado = null;
		nombreNuevoSuplemento = null;
	}
	
	private JLabel getLblEstosSonLos() {
		if (lblEstosSonLos == null) {
			lblEstosSonLos = new JLabel("Estos son los suplementos de modalidad existentes:");
			lblEstosSonLos.setForeground(Color.BLACK);
			lblEstosSonLos.setBounds(18, 298, 393, 29);
		}
		return lblEstosSonLos;
	}
}
