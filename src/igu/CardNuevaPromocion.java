package igu;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import logica.ComparadorFecha;
import logica.Conexion;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import com.toedter.calendar.JDateChooser;

public class CardNuevaPromocion extends JPanel {

	private static Connection conn;
	private static PreparedStatement stmt;

	private JPanel contentPane;
	private JLabel lblNuevaPromocion;
	private JPanel pnCenter;
	private JPanel pnSouth;
	private JButton btnCrear;
	private JLabel label;
	private JTextField txtnombre;
	private JLabel fechainicio;
	private JLabel fechafinal;
	private JLabel descuento;
	private JTextField txtdescuento;

	private JPanel panel;
	private JPanel panel_1;

	private Calendar c = Calendar.getInstance();
	private JPanel panel_2;
	private JPanel panel_3;
	private JDateChooser dCfechainicio;
	private JPanel panel_4;
	private JPanel panel_5;
	private JDateChooser dCfechafinal;
	
	
	/**
	 * Create the frame.
	 */
	public CardNuevaPromocion(Conexion con) {


		setBounds(100, 100, 450, 300);
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(new BorderLayout(0, 0));
		add(getLblNuevaPromocion(), BorderLayout.NORTH);
		add(getPnCenter(), BorderLayout.CENTER);
		add(getPnSouth(), BorderLayout.SOUTH);

		conn = (Connection) con.getCon();
		
		setVisible(true);
		

	}

	private JLabel getLblNuevaPromocion() {
		if (lblNuevaPromocion == null) {
			lblNuevaPromocion = new JLabel("Nueva promocion");
			lblNuevaPromocion.setFont(new Font("Arial", Font.PLAIN, 30));
		}
		return lblNuevaPromocion;
	}

	private JPanel getPnCenter() {
		if (pnCenter == null) {
			pnCenter = new JPanel();
			pnCenter.setLayout(new GridLayout(0, 1, 0, 0));
			pnCenter.add(getPanel());
			pnCenter.add(getPanel_1());
		}
		return pnCenter;
	}

	private JPanel getPnSouth() {
		if (pnSouth == null) {
			pnSouth = new JPanel();
			pnSouth.add(getBtnCrear());
		}
		return pnSouth;
	}

	private JButton getBtnCrear() {
		if (btnCrear == null) {
			btnCrear = new JButton("Crear");
			btnCrear.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					try {
						String nombre = getTxtnombre().getText();
						if (getTxtnombre().getText().equals("") || nombre.equals("")
								|| getTxtdescuento().getText().equals("")) {
							JOptionPane.showMessageDialog(null, "Se han encontrado campos vaci�os.");
						}
						else
						{
							
							SimpleDateFormat Formato = new SimpleDateFormat("dd/MM/yyyy");
						
							ComparadorFecha comparadorfecha = new ComparadorFecha(Formato.format(getDCfechainicio().getDate()),Formato.format(getDCfechafinal().getDate()));

							int porcentaje = Integer.parseInt(getTxtdescuento().getText().toString()) * (-1);

							if (nombre.equals(null)) {
								JOptionPane.showMessageDialog(null, "El campo nombre esta vacio");
							}

							else if (!comparadorfecha.CheckFechaActual()) {
								JOptionPane.showMessageDialog(null, "Fecha de inicio no correcta,comprueba"
										+ " que los valores no son inferiores a la fecha actual.");

							} else if (!comparadorfecha.CheckFechaFinal()) {
								JOptionPane.showMessageDialog(null, "Fecha final incorecta comprueba que los valores"
										+ " no son inferiores a la fecha de inicio.");

							} else if (porcentaje >= 0 || porcentaje < -100) {
								JOptionPane.showMessageDialog(null, "Porcentaje de descuento no adecuado");

							} else {
								
								
								// Fecha introducida por el gerente
								
								java.sql.Date dateinicio = new java.sql.Date(Formato.parse(comparadorfecha.getFechaInicio()).getTime());
								
								java.sql.Date datefinal = new java.sql.Date(Formato.parse(comparadorfecha.getFechaFinal()).getTime());
								

								
								PreparedStatement comprobar = conn.prepareStatement("SELECT count(*) "
										+ "FROM TARIFA_POR_FECHA "
										+ "WHERE FECHA_INICIO=? AND FECHA_FIN=? AND PORCENTAJE<0");
								
								comprobar.setDate(1, (java.sql.Date) dateinicio );
								comprobar.setDate(2, (java.sql.Date) datefinal);
								
								ResultSet rs = comprobar.executeQuery();
								int repeateddates = 0;
								while(rs.next())
								{
									repeateddates = rs.getInt(1);
								}
								comprobar.close();
								
								if(repeateddates>0)
								{
									JOptionPane.showMessageDialog(null, "Ya existe una promocion para ese rango de fechas.");

								}
								else
								{
									Statement statement = conn.createStatement();
									ResultSet id = statement.executeQuery("SELECT count(*) FROM tarifa_por_fecha");
									int idnumber = 0;
									while (id.next()) {
										idnumber = id.getInt(1) + 1;
									}

									stmt = conn.prepareStatement("INSERT INTO TARIFA_POR_FECHA VALUES (?,?,?,?,?)");

									stmt.setInt(1, idnumber);

									stmt.setString(2, nombre);							

									stmt.setDate(3, (java.sql.Date) dateinicio);
									stmt.setDate(4, (java.sql.Date) datefinal);

									stmt.setInt(5, porcentaje);

									stmt.executeUpdate();
									stmt.close();

									JOptionPane.showMessageDialog(null,
											"La nueva promocion " + nombre + " se ha a�adido correctamente.");
								}

								
							}
						}


					} catch (SQLException | ParseException e1) {
						e1.printStackTrace();

					}
				}

				private void reiniciarPantalla() {
					getTxtdescuento().setText("");
					getTxtnombre().setText("");
					
				}
			});
		}
		return btnCrear;
	}

	private JLabel getLabel() {
		if (label == null) {
			label = new JLabel("Nombre:");
		}
		return label;
	}

	private JTextField getTxtnombre() {
		if (txtnombre == null) {
			txtnombre = new JTextField();
			txtnombre.setColumns(10);
		}
		return txtnombre;
	}

	private JLabel getFechainicio() {
		if (fechainicio == null) {
			fechainicio = new JLabel("Fecha de inicio:");
		}
		return fechainicio;
	}

	private JLabel getFechafinal() {
		if (fechafinal == null) {
			fechafinal = new JLabel("Fecha final:");
		}
		return fechafinal;
	}

	private JLabel getDescuento() {
		if (descuento == null) {
			descuento = new JLabel("DESCUENTO (porcentaje) :");
		}
		return descuento;
	}

	private JTextField getTxtdescuento() {
		if (txtdescuento == null) {
			txtdescuento = new JTextField();
			txtdescuento.setColumns(10);
		}
		return txtdescuento;
	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setLayout(new GridLayout(0, 1, 0, 0));
			panel.add(getPanel_2());
			panel.add(getPanel_3());
		}
		return panel;
	}

	private JPanel getPanel_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setLayout(new GridLayout(0, 1, 0, 0));
			panel_1.add(getPanel_4());
			panel_1.add(getPanel_5());
		}
		return panel_1;
	}

	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.add(getLabel());
			panel_2.add(getTxtnombre());
		}
		return panel_2;
	}

	private JPanel getPanel_3() {
		if (panel_3 == null) {
			panel_3 = new JPanel();
			panel_3.add(getDescuento());
			panel_3.add(getTxtdescuento());
		}
		return panel_3;
	}
	private JDateChooser getDCfechainicio() {
		if (dCfechainicio == null) {
			dCfechainicio = new JDateChooser();
		}
		return dCfechainicio;
	}
	private JPanel getPanel_4() {
		if (panel_4 == null) {
			panel_4 = new JPanel();
			panel_4.add(getFechainicio());
			panel_4.add(getDCfechainicio());
		}
		return panel_4;
	}
	private JPanel getPanel_5() {
		if (panel_5 == null) {
			panel_5 = new JPanel();
			panel_5.add(getFechafinal());
			panel_5.add(getDCfechafinal());
		}
		return panel_5;
	}
	private JDateChooser getDCfechafinal() {
		if (dCfechafinal == null) {
			dCfechafinal = new JDateChooser();
		}
		return dCfechafinal;
	}
}
