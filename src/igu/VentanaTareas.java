package igu;

import javax.swing.JFrame;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.toedter.calendar.JDateChooser;

import logica.DataBase;
import logica.Habitacion;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JComboBox;

@SuppressWarnings("serial")
public class VentanaTareas extends JFrame  {

	CardVistaHabitaciones padre;
	Habitacion hab;

	private JLabel lblHabitacion;
	private JLabel lblTxtHabitacion;
	private JLabel lblTarea;
	private JButton btnAadir;
	private JButton btnCancelar;
	private JTextField txtTarea;
	private JLabel lblFecha;
	private JDateChooser calendar;
	private JLabel lblHora;
	private JComboBox<?> comboBox;

	public VentanaTareas(CardVistaHabitaciones papa, Habitacion hab) {

		padre = papa;
		this.hab = hab;

		setTitle("Nueva Tarea Limpieza");
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaTareas.class.getResource("/img/logo.jpg")));
		setBounds(0, 0, 502, 207);
		setResizable(false);

		setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		getContentPane().add(getLblHabitacion());
		getContentPane().add(getLblTxtHabitacion());
		getContentPane().add(getLblTarea());
		getContentPane().add(getBtnAadir());
		getContentPane().add(getBtnCancelar());
		getContentPane().add(getTxtTarea());
		getContentPane().add(getLblFecha());
		getContentPane().add(getCalendar());
		getContentPane().add(getLblHora());
		getContentPane().add(getComboBox());
		setVisible(true);
	}

	private JLabel getLblHabitacion() {
		if (lblHabitacion == null) {
			lblHabitacion = new JLabel("Habitacion:");
			lblHabitacion.setBounds(10, 25, 66, 14);
		}
		return lblHabitacion;
	}

	private JLabel getLblTxtHabitacion() {
		if (lblTxtHabitacion == null) {
			lblTxtHabitacion = new JLabel(String.valueOf(hab.id));
			lblTxtHabitacion.setBounds(86, 25, 53, 14);
		}
		return lblTxtHabitacion;
	}

	private JLabel getLblTarea() {
		if (lblTarea == null) {
			lblTarea = new JLabel("Tarea:");
			lblTarea.setBounds(10, 64, 66, 14);
		}
		return lblTarea;
	}
	private JButton getBtnAadir() {
		if (btnAadir == null) {
			btnAadir = new JButton("A�adir");
			btnAadir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					nuevaTarea();
					close();
				}
			});
			btnAadir.setBounds(291, 136, 89, 23);
		}
		return btnAadir;
	}

	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					close();
				}
			});
			btnCancelar.setBounds(397, 136, 89, 23);
		}
		return btnCancelar;
	}
	private JTextField getTxtTarea() {
		if (txtTarea == null) {
			txtTarea = new JTextField();
			txtTarea.setBounds(20, 85, 466, 40);
			txtTarea.setColumns(10);
		}
		return txtTarea;
	}

	private void close() {
		dispose();
	}
	private JLabel getLblFecha() {
		if (lblFecha == null) {
			lblFecha = new JLabel("Fecha:");
			lblFecha.setBounds(136, 25, 46, 14);
		}
		return lblFecha;
	}

	@SuppressWarnings("deprecation")
	private JDateChooser getCalendar() {
		if (calendar == null){
			calendar = new JDateChooser("dd/MM/yyyy", "##/##/####", '_');
			calendar.setVisible(true);
			calendar.setBounds(193, 16, 114, 23);
			Date today = new Date();
			today.setHours(0); 
			calendar.setMinSelectableDate(today);
			calendar.setDate(today);
		}
		return calendar;	
	}
	private JLabel getLblHora() {
		if (lblHora == null) {
			lblHora = new JLabel("Hora:");
			lblHora.setBounds(334, 25, 46, 14);
		}
		return lblHora;
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JComboBox getComboBox() {
		if (comboBox == null) {
			comboBox = new JComboBox(new DefaultComboBoxModel<Object>(new String[] {"_","0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23"}));
			comboBox.setBounds(380, 19, 66, 20);
		}
		return comboBox;
	}

	private void nuevaTarea() {
		Date today = calendar.getDate();
		String hoy;
		if (comboBox.getSelectedItem().toString().equals("_")) {
			Calendar calendario = new GregorianCalendar();
			int hora = calendario.get(Calendar.HOUR_OF_DAY);
			int minutos = calendario.get(Calendar.MINUTE);
			int segundos = calendario.get(Calendar.SECOND);
			hoy = String.valueOf(hora) + ":" + String.valueOf(minutos) + ":" + String.valueOf(segundos);
		}
		else{
			hoy = String.valueOf(comboBox.getSelectedItem()) + ":00:00";
		}
		try {
			DataBase.nuevaTarea(hab.id, txtTarea.getText(), today, hoy);
			JOptionPane.showMessageDialog(null, "Tarea creada correctamente");
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "No se ha podido registrar la tarea. Consulte un t�cnico.");
			e.printStackTrace();
		}
	}
}
