package igu;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import logica.Conexion;

import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import java.awt.Font;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;

import java.awt.Rectangle;
import java.awt.Toolkit;

import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import java.util.Date;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.CardLayout;

import javax.swing.JSeparator;
import javax.swing.JTextField;

import com.toedter.calendar.JDateChooser;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComboBox;

public class VentanaTarifasTipo extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Conexion con;
	private JPanel contentPane;
	private JPanel panelTarifasActuales;
	private JLabel lblTarifasActuales;
	private JScrollPane spTablaTarifasTipo;
	private JTable tablaTarifasTipo;
	private String[] cabecera = {"TIPO DE HABITACI�N", "IMPORTE POR D�A", "FECHA DE ALTA", "FECHA DE BAJA"};
	private JPanel panelBotones;
	private JButton btnModificar;
	private JButton btnCrearNuevaTarifa;
	private JButton btnBorrar;
	private JPanel panelCentral;
	private JPanel panelCard;
	private JPanel panelTabla;
	private JPanel panelContenedorCard;
	private JPanel panelBotones2;
	private JButton btnA�adir;
	private JSeparator separator;
	private JPanel panelNuevoTipo;
	private JLabel lblTipos;
	private JLabel lblImporte;
	private JTextField tfNuevoImporte;
	private JDateChooser dateChFechaInicio;
	private JDateChooser dateChFechaFin;
	private JLabel lblFechaInicio;
	private JLabel lblFechaFin;
	private String tipoSeleccionado;
	private CardLayout card = new CardLayout();
	private JPanel panelModificacion;
	private JLabel lblTarifaMod;
	private JTextField txfTarifaMod;
	private JLabel lblImporteMod;
	private JTextField txfImporteMod;
	private JLabel lblFechaInicioMod;
	private JLabel lblFechaFinMod;
	private JDateChooser dtChFechaInicioMod;
	private JDateChooser dtChFechaFinMod;
	private boolean enModificacion;
	private JComboBox<String> cbTipoHab;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//VentanaAltaTarifasTipo frame = new VentanaAltaTarifasTipo();
					Conexion conexion = new Conexion();
					conexion.establecerConexion();	
					VentanaTarifasTipo ventana = new VentanaTarifasTipo(conexion);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public class MiModeloTabla extends DefaultTableModel{
		private static final long serialVersionUID = 1L;
		public MiModeloTabla(Object[] columnNames, int numFilas) {
			super(columnNames,numFilas);
		}
		
		public boolean isCellEditable(int numFila, int numColumna){
			return false;
		}
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public VentanaTarifasTipo(Conexion conexion) throws SQLException {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaTarifasTipo.class.getResource("/img/logo.jpg")));
		this.con = conexion;
		setTitle("Modificaci\u00F3n de tarifas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 800);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(getPanelTarifasActuales(), BorderLayout.NORTH);
		contentPane.add(getPanelCentral(), BorderLayout.CENTER);
		setLocationRelativeTo(null);
		rellenarTablaTarifas();
		setVisible(true);
	}

	private JPanel getPanelTarifasActuales() {
		if (panelTarifasActuales == null) {
			panelTarifasActuales = new JPanel();
			panelTarifasActuales.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			panelTarifasActuales.add(getLblTarifasActuales());
		}
		return panelTarifasActuales;
	}
	private JLabel getLblTarifasActuales() {
		if (lblTarifasActuales == null) {
			lblTarifasActuales = new JLabel("TARIFAS ACTUALES:");
			lblTarifasActuales.setFont(new Font("Tahoma", Font.BOLD, 24));
			lblTarifasActuales.setHorizontalTextPosition(SwingConstants.LEFT);
			lblTarifasActuales.setHorizontalAlignment(SwingConstants.LEFT);
		}
		return lblTarifasActuales;
	}
	private JScrollPane getSpTablaTarifasTipo() {
		if (spTablaTarifasTipo == null) {
			spTablaTarifasTipo = new JScrollPane();
			spTablaTarifasTipo.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
			spTablaTarifasTipo.setViewportView(getTablaTarifasTipo());
		}
		return spTablaTarifasTipo;
	}
	private JTable getTablaTarifasTipo() {
		if (tablaTarifasTipo == null) {
			tablaTarifasTipo = new JTable();
			tablaTarifasTipo.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					if(enModificacion){
						int opcion = JOptionPane.showConfirmDialog(null, "Atenci�n est� modificando una tarifa, �desea abandonar la operaci�n?\n"
								+ "Los cambios no ser�n guardados", "Abandonar modificaci�n.", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
						if(opcion==0){
							tablaTarifasTipo.setEnabled(true);
							tipoSeleccionado = (String) tablaTarifasTipo.getValueAt(tablaTarifasTipo.getSelectedRow(), 0);
							System.out.println(tipoSeleccionado);
							enModificacion = false;
							panelCard.setVisible(false);
							panelBotones2.setVisible(false);
						}
					
					}
					else{
						tipoSeleccionado = (String) tablaTarifasTipo.getValueAt(tablaTarifasTipo.getSelectedRow(), 0);
						System.out.println(tipoSeleccionado);
					}
				}
			});
			tablaTarifasTipo.setBorder(null);
			tablaTarifasTipo.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tablaTarifasTipo;
	}
	
	private void rellenarTablaTarifas() throws SQLException{
		
		MiModeloTabla miModelo = new MiModeloTabla(cabecera, 0);
		
		Statement s = con.getCon().createStatement();
		
		ResultSet rs = s.executeQuery("select t.nombre, pt.importe_dia, pt.fecha_inicio, pt.fecha_fin "
				+ "from tipo_habitacion t, precio_tipo pt "
				+ "where t.nombre = pt.pk_tipohab");

		
		SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
		
		while(rs.next()){
			Object[] fila = new Object[4];
			fila[0] = rs.getString("nombre");
			fila[1] = rs.getFloat("importe_dia");
			fila[2] = formato.format(rs.getDate("fecha_inicio"));
			fila[3] = formato.format(rs.getDate("fecha_fin"));
			
			miModelo.addRow(fila);
		}
		
		tablaTarifasTipo.setModel(miModelo);
		modificarTablaTipos();
	}
	
	private void modificarTablaTipos(){
		tablaTarifasTipo.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 16));
		tablaTarifasTipo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tablaTarifasTipo.setRowHeight(25);
	}

	private JPanel getPanelBotones() {
		if (panelBotones == null) {
			panelBotones = new JPanel();
			panelBotones.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
			panelBotones.setBounds(new Rectangle(0, 0, 50, 41));
			GridBagLayout gbl_panelBotones = new GridBagLayout();
			gbl_panelBotones.columnWidths = new int[] {140, 0};
			gbl_panelBotones.rowHeights = new int[] {41, 41, 41, 0};
			gbl_panelBotones.columnWeights = new double[]{0.0, Double.MIN_VALUE};
			gbl_panelBotones.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
			panelBotones.setLayout(gbl_panelBotones);
			GridBagConstraints gbc_btnCrearNuevaTarifa = new GridBagConstraints();
			gbc_btnCrearNuevaTarifa.fill = GridBagConstraints.BOTH;
			gbc_btnCrearNuevaTarifa.insets = new Insets(0, 0, 5, 0);
			gbc_btnCrearNuevaTarifa.gridx = 0;
			gbc_btnCrearNuevaTarifa.gridy = 0;
			panelBotones.add(getBtnCrearNuevaTarifa(), gbc_btnCrearNuevaTarifa);
			GridBagConstraints gbc_btnModificar = new GridBagConstraints();
			gbc_btnModificar.fill = GridBagConstraints.BOTH;
			gbc_btnModificar.insets = new Insets(0, 0, 5, 0);
			gbc_btnModificar.gridx = 0;
			gbc_btnModificar.gridy = 1;
			panelBotones.add(getBtnModificar(), gbc_btnModificar);
			GridBagConstraints gbc_btnBorrar = new GridBagConstraints();
			gbc_btnBorrar.fill = GridBagConstraints.BOTH;
			gbc_btnBorrar.gridx = 0;
			gbc_btnBorrar.gridy = 2;
			panelBotones.add(getBtnBorrar(), gbc_btnBorrar);
		}
		return panelBotones;
	}
	private JButton getBtnModificar() {
		if (btnModificar == null) {
			btnModificar = new JButton("Modificar");
			btnModificar.setVisible(false);
			btnModificar.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
			btnModificar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String tipo = (String) tablaTarifasTipo.getValueAt(tablaTarifasTipo.getSelectedRow(), 0);
					if(tipo != null) {
						panelCard.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
						panelBotones2.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
						panelBotones2.setVisible(true);
						panelCard.setVisible(true);
						enModificacion = true;
						panelModificacion.setBorder(new TitledBorder(null, "Modificar tarifa", TitledBorder.LEADING, TitledBorder.TOP, null, null));
						card.show(panelCard, "panelModificacion");
						tablaTarifasTipo.setEnabled(false);
						rellenarDatosModificacion(tipo);
									
					}
									
				}
			});
			//btnModificar.setVisible(false);
			btnModificar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		}
		return btnModificar;
	}
	
	private void modificar(String nuevoNombre, float nuevoImporte, Date nuevaFechaInicio, Date nuevaFechaFin) throws SQLException{
		PreparedStatement psUpdatePrecio = con.getCon().prepareStatement("update precio_tipo"
				+ " set importe_dia = ?,  fecha_inicio = ?, fecha_fin = ?"
				+ " where (ID_TIPOHAB in"
				+ " (select id"
				+ " from tipo_habitacion"
				+ " where nombre = ?))");
		
		psUpdatePrecio.setFloat(1, nuevoImporte);
		psUpdatePrecio.setDate(2, new java.sql.Date(dateChFechaInicio.getDate().getTime()));
		psUpdatePrecio.setDate(3, new java.sql.Date(dateChFechaFin.getDate().getTime()));
		psUpdatePrecio.setString(4, tipoSeleccionado);
		
		psUpdatePrecio.executeUpdate();
		
		psUpdatePrecio.close();
		dispose();
	}
	
	private void rellenarDatosModificacion(String tipo) {
		
		float importe = (float) tablaTarifasTipo.getValueAt(tablaTarifasTipo.getSelectedRow(), 1);
		try {
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			Date fecha_ini = formato.parse((String) tablaTarifasTipo.getValueAt(tablaTarifasTipo.getSelectedRow(), 2));
			Date fecha_final = formato.parse((String)tablaTarifasTipo.getValueAt(tablaTarifasTipo.getSelectedRow(), 3));
			
			txfTarifaMod.setText(tipo);
			txfImporteMod.setText(importe+"");
			dtChFechaInicioMod.setDate(fecha_ini);
			dtChFechaFinMod.setDate(fecha_final);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	private JButton getBtnCrearNuevaTarifa() {
		if (btnCrearNuevaTarifa == null) {
			btnCrearNuevaTarifa = new JButton("Crear nueva tarifa");
			btnCrearNuevaTarifa.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
			btnCrearNuevaTarifa.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					resetCampos();
					panelCard.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
					panelBotones2.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
					panelContenedorCard.setVisible(true);
					card.first(panelCard);
					lblFechaFin.setVisible(true);
					lblFechaInicio.setVisible(true);
					lblImporte.setVisible(true);
					lblTipos.setVisible(true);
					tfNuevoImporte.setVisible(true);
					dateChFechaFin.setVisible(true);
					dateChFechaInicio.setVisible(true);
					cbTipoHab.setVisible(true);
					btnA�adir.setVisible(true);
					panelNuevoTipo.setBorder(new TitledBorder(null, "Crear nueva tarifa", TitledBorder.LEADING, TitledBorder.TOP, null, null));
					try {
						rellenarTablaTarifas();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			});
			btnCrearNuevaTarifa.setFont(new Font("Tahoma", Font.PLAIN, 15));
		}
		return btnCrearNuevaTarifa;
	}
	
	private void resetCampos(){
		cbTipoHab.setSelectedIndex(-1);
		tfNuevoImporte.setText("");
		tfNuevoImporte.setBorder(new LineBorder(Color.BLACK));
		dateChFechaInicio.setDate(null);
		dateChFechaFin.setDate(null);
	}
	private JButton getBtnBorrar() {
		if (btnBorrar == null) {
			btnBorrar = new JButton("Borrar");
			btnBorrar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String tipo = (String) tablaTarifasTipo.getValueAt(tablaTarifasTipo.getSelectedRow(), 0);
					if(tipo != null) {
						int opcion = JOptionPane.showConfirmDialog(null, "�Desea borrar esta tarifa?");
						if(opcion == 0) { //YES_OPTION
							//PreparedStatement ps =
						}
					}
				}
			});
			btnBorrar.setVisible(false);
			btnBorrar.setFont(new Font("Tahoma", Font.PLAIN, 15));
			btnBorrar.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		}
		return btnBorrar;
	}
	private JPanel getPanelCentral() {
		if (panelCentral == null) {
			panelCentral = new JPanel();
			GridBagLayout gbl_panelCentral = new GridBagLayout();
			gbl_panelCentral.columnWidths = new int[]{976, 0};
			gbl_panelCentral.rowHeights = new int[]{360, 0, 340, 0};
			gbl_panelCentral.columnWeights = new double[]{0.0, Double.MIN_VALUE};
			gbl_panelCentral.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
			panelCentral.setLayout(gbl_panelCentral);
			GridBagConstraints gbc_panelTabla = new GridBagConstraints();
			gbc_panelTabla.fill = GridBagConstraints.BOTH;
			gbc_panelTabla.insets = new Insets(0, 0, 5, 0);
			gbc_panelTabla.gridx = 0;
			gbc_panelTabla.gridy = 0;
			panelCentral.add(getPanelTabla(), gbc_panelTabla);
			GridBagConstraints gbc_separator_1 = new GridBagConstraints();
			gbc_separator_1.insets = new Insets(0, 0, 5, 0);
			gbc_separator_1.gridx = 0;
			gbc_separator_1.gridy = 1;
			panelCentral.add(getSeparator(), gbc_separator_1);
			GridBagConstraints gbc_panelContenedorCard = new GridBagConstraints();
			gbc_panelContenedorCard.fill = GridBagConstraints.BOTH;
			gbc_panelContenedorCard.gridx = 0;
			gbc_panelContenedorCard.gridy = 2;
			panelCentral.add(getPanelContenedorCard(), gbc_panelContenedorCard);
		}
		return panelCentral;
	}
	private JPanel getPanelCard() {
		if (panelCard == null) {
			panelCard = new JPanel();
			panelCard.setBorder(null);
			panelCard.setLayout(card);
			panelCard.add(getPanelNuevoTipo(), "panelNuevoTipo");
			panelCard.add(getPanelModificacion(), "panelModificacion");
		}
		return panelCard;
	}
	private JPanel getPanelTabla() {
		if (panelTabla == null) {
			panelTabla = new JPanel();
			panelTabla.setLayout(new BorderLayout(0, 0));
			panelTabla.add(getSpTablaTarifasTipo());
			panelTabla.add(getPanelBotones(), BorderLayout.EAST);
		}
		return panelTabla;
	}
	private JPanel getPanelContenedorCard() {
		if (panelContenedorCard == null) {
			panelContenedorCard = new JPanel();
			panelContenedorCard.setLayout(new BorderLayout(0, 0));
			panelContenedorCard.add(getPanelCard());
			panelContenedorCard.add(getPanelBotones2(), BorderLayout.EAST);
		}
		return panelContenedorCard;
	}
	private JPanel getPanelBotones2() {
		if (panelBotones2 == null) {
			panelBotones2 = new JPanel();
			panelBotones2.setBounds(new Rectangle(0, 0, 50, 41));
			panelBotones2.setBorder(null);
			GridBagLayout gbl_panelBotones2 = new GridBagLayout();
			gbl_panelBotones2.columnWidths = new int[]{140, 0};
			gbl_panelBotones2.rowHeights = new int[]{41, 41, 41, 0};
			gbl_panelBotones2.columnWeights = new double[]{0.0, Double.MIN_VALUE};
			gbl_panelBotones2.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
			panelBotones2.setLayout(gbl_panelBotones2);
			GridBagConstraints gbc_btnRealizarCambios = new GridBagConstraints();
			gbc_btnRealizarCambios.fill = GridBagConstraints.BOTH;
			gbc_btnRealizarCambios.insets = new Insets(0, 0, 5, 0);
			gbc_btnRealizarCambios.gridx = 0;
			gbc_btnRealizarCambios.gridy = 0;
			panelBotones2.add(getBtnA�adir(), gbc_btnRealizarCambios);
		}
		return panelBotones2;
	}
	private JButton getBtnA�adir() {
		if (btnA�adir == null) {
			btnA�adir = new JButton("A\u00F1adir");
			btnA�adir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(comprobarFechas() && comprobarImporte()){
						System.out.println("TODO OK");
						tfNuevoImporte.setBorder(new LineBorder(Color.GREEN));
						int opcion = JOptionPane.showConfirmDialog(null, "�Desea confirmar los cambios?", "Confirmar cambios", JOptionPane.YES_NO_OPTION);
						if(opcion == 0){//YES
							try{
//								String nuevoTipo = txfNuevaTarifa.getText().toLowerCase();
								float nuevoImporte = Float.parseFloat(tfNuevoImporte.getText());	
								
								Statement s1 = con.getCon().createStatement();
								if(!existeTarifa(s1, tipoSeleccionado)){
									insertar(s1, tipoSeleccionado, nuevoImporte);
								}
								else{
									JOptionPane.showMessageDialog(null, "El error puede ser debido a :\n"
											+ "-Tipo de tarifa ya existente\n-No pueden existir dos tarifas cuyo tipo "
											+ "de habitaci�n sea el mismo y cuyas fechas sean conflictivas", "Error",JOptionPane.ERROR_MESSAGE);
								}	
								
							}
							catch(Exception e){
								e.printStackTrace();
								System.out.println("Error");
							}
						}
					}
				}
			});
			btnA�adir.setFont(new Font("Tahoma", Font.PLAIN, 15));
			btnA�adir.setVisible(false);
			btnA�adir.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		}
		return btnA�adir;
	}
	
//	private boolean nombreTarifaExiste(String tipoNuevo){		
//		try {
//			Statement s1 = con.getCon().createStatement();
//			ResultSet rsCheck = s1.executeQuery ("select count(nombre) "
//					+ "from tipo_habitacion where nombre = '" + tipoNuevo + "'");
//			
//			rsCheck.next();
//			int existeTarifa = rsCheck.getInt("COUNT(NOMBRE)");
//			return existeTarifa>0;
//		} catch (SQLException e) {
//			e.printStackTrace();
//			return false;
//		}		
//	}
	
	private JSeparator getSeparator() {
		if (separator == null) {
			separator = new JSeparator();
		}
		return separator;
	}
	private JPanel getPanelNuevoTipo() {
		if (panelNuevoTipo == null) {
			panelNuevoTipo = new JPanel();
			panelNuevoTipo.setName("panelNuevoTipo");
			panelNuevoTipo.setLayout(null);
			panelNuevoTipo.add(getLblNuevaTarifa());
			panelNuevoTipo.add(getLblImporte());
			panelNuevoTipo.add(getTxfImporte());
			panelNuevoTipo.add(getDateChFechaInicio());
			panelNuevoTipo.add(getLblFechaInicio());
			panelNuevoTipo.add(getDateChFechaFin());
			panelNuevoTipo.add(getLblFechaFin());
			panelNuevoTipo.add(getCbTiposHab());
		}
		return panelNuevoTipo;
	}
	
	private JLabel getLblNuevaTarifa() {
		if (lblTipos == null) {
			lblTipos = new JLabel("Tipo de habitaci\u00F3n:");
			lblTipos.setBounds(55, 34, 192, 26);
			lblTipos.setVisible(false);
		}
		return lblTipos;
	}
	private JLabel getLblImporte() {
		if (lblImporte == null) {
			lblImporte = new JLabel("Importe:");
			lblImporte.setBounds(55, 139, 127, 26);
			lblImporte.setVisible(false);
		}
		return lblImporte;
	}
	private JTextField getTxfImporte() {
		if (tfNuevoImporte == null) {
			tfNuevoImporte = new JTextField();
			tfNuevoImporte.setBounds(55, 164, 109, 32);
			tfNuevoImporte.setColumns(10);
			tfNuevoImporte.setVisible(false);
		}
		return tfNuevoImporte;
	}
	private JDateChooser getDateChFechaInicio() {
		if (dateChFechaInicio == null) {
			dateChFechaInicio = new JDateChooser();
			dateChFechaInicio.setBounds(342, 60, 155, 32);
			dateChFechaInicio.setVisible(false);
			dateChFechaInicio.setMinSelectableDate(new Date());
		}
		return dateChFechaInicio;
	}
	private JLabel getLblFechaInicio() {
		if (lblFechaInicio == null) {
			lblFechaInicio = new JLabel("Fecha inicio:");
			lblFechaInicio.setBounds(342, 34, 155, 26);
			lblFechaInicio.setVisible(false);
		}
		return lblFechaInicio;
	}
	private JDateChooser getDateChFechaFin() {
		if (dateChFechaFin == null) {
			dateChFechaFin = new JDateChooser();
			dateChFechaFin.setBounds(597, 60, 155, 32);
			dateChFechaFin.setVisible(false);
			dateChFechaFin.setMinSelectableDate(new Date());
		}
		return dateChFechaFin;
	}
	private JLabel getLblFechaFin() {
		if (lblFechaFin == null) {
			lblFechaFin = new JLabel("Fecha fin:");
			lblFechaFin.setBounds(597, 34, 135, 26);
			lblFechaFin.setVisible(false);
		}
		return lblFechaFin;
	}
	
	private boolean existeTarifa(Statement s1, String nuevoTipo) throws SQLException{		

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
		String fecha_ini= sdf.format(new java.sql.Date(dateChFechaInicio.getDate().getTime()));
		System.out.println(new java.sql.Date(dateChFechaInicio.getDate().getTime()).toString());
		System.out.println(fecha_ini);
		String fecha_fin= sdf.format(new java.sql.Date(dateChFechaFin.getDate().getTime()));
		
		ResultSet rsCheck = s1.executeQuery ("select count(pt.pk_tipohab) "
				+ "from precio_tipo pt "
				+ "where ((to_date('"+ fecha_ini + "','dd-MM-YYYY') between pt.fecha_inicio "
				+ "and pt.fecha_fin) or (to_date('" + fecha_fin + "','dd-MM-YYYY') between "
				+ "pt.fecha_inicio and pt.fecha_fin)) and pt.pk_tipohab = '" + nuevoTipo + "'");
		
			
		rsCheck.next();
		int existeTarifa = rsCheck.getInt("COUNT(PT.PK_TIPOHAB)");
		System.out.println("Existe: " + existeTarifa);
		return existeTarifa>0;
	}
	
	private void insertar(Statement s1, String nuevoTipo, float nuevoImporte) throws SQLException{
		
//		if(!nombreTarifaExiste(nuevoTipo)){
//			
//			PreparedStatement psTipo = con.getCon().prepareStatement("insert into tipo_habitacion values(?)");
//			
//			psTipo.setString(1, nuevoTipo);
//			
//			psTipo.executeUpdate();
//		}
		
		PreparedStatement psPrecio = con.getCon().
				prepareStatement("insert into precio_tipo values(sec_preciotipo.nextVal, to_date(?), to_date(?), ?, ?)");
		psPrecio.setDate(1, new java.sql.Date(dateChFechaInicio.getDate().getTime()));
		psPrecio.setDate(2, new java.sql.Date(dateChFechaFin.getDate().getTime()));
		psPrecio.setFloat(3, nuevoImporte);
		psPrecio.setString(4, nuevoTipo);
		
		psPrecio.executeUpdate();
		
		System.out.println("Insertado");
		psPrecio.close();
		s1.close();
		panelContenedorCard.setVisible(false);
		rellenarTablaTarifas();
		tablaTarifasTipo.repaint();
	}

	
	private boolean comprobarImporte(){
		String importe = tfNuevoImporte.getText();
		try{
			Double.parseDouble(importe);
			return true;
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "Formato de importe incorrecto");
			tfNuevoImporte.setBorder(new LineBorder(Color.RED));
			return false;
		}
	}
	
	private boolean comprobarFechas(){
		try{
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			Date fecha_inicio = formato.parse(formato.format(dateChFechaInicio.getDate()));
			Date fecha_fin = formato.parse(formato.format(dateChFechaFin.getDate()));

			Date fecha_acc = formato.parse(formato.format(new Date()));
			
			if(fecha_inicio.compareTo(fecha_acc) >= 0){
				if(fecha_inicio.compareTo(fecha_fin) > 0){
					JOptionPane.showMessageDialog(null, "La fecha de fin debe de ser mayor que la fecha de inicio");
					return false;
				}
				else 
					return true;
			}
			else{
				JOptionPane.showMessageDialog(null, "La fecha de inicio debe ser mayor que la fecha actual del sistema");
				return false;
			}
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "Fechas incorrectas");
			return false;
		}
	}
	private JPanel getPanelModificacion() {
		if (panelModificacion == null) {
			panelModificacion = new JPanel();
			panelModificacion.setName("panelModificacion");
			panelModificacion.setLayout(null);
			panelModificacion.add(getLblTarifaMod());
			panelModificacion.add(getTxfTarifaMod());
			panelModificacion.add(getLblImporteMod());
			panelModificacion.add(getTxfImporteMod());
			panelModificacion.add(getLblFechaInicioMod());
			panelModificacion.add(getLblFechaFinMod());
			panelModificacion.add(getDtChFechaInicioMod());
			panelModificacion.add(getDtChFechaFinMod());
		}
		return panelModificacion;
	}
	private JLabel getLblTarifaMod() {
		if (lblTarifaMod == null) {
			lblTarifaMod = new JLabel("Tarifa:");
			lblTarifaMod.setBounds(69, 47, 92, 26);
		}
		return lblTarifaMod;
	}
	private JTextField getTxfTarifaMod() {
		if (txfTarifaMod == null) {
			txfTarifaMod = new JTextField();
			txfTarifaMod.setBounds(69, 72, 186, 32);
			txfTarifaMod.setColumns(10);
		}
		return txfTarifaMod;
	}
	private JLabel getLblImporteMod() {
		if (lblImporteMod == null) {
			lblImporteMod = new JLabel("Importe:");
			lblImporteMod.setBounds(69, 143, 92, 26);
		}
		return lblImporteMod;
	}
	private JTextField getTxfImporteMod() {
		if (txfImporteMod == null) {
			txfImporteMod = new JTextField();
			txfImporteMod.setBounds(69, 170, 186, 32);
			txfImporteMod.setColumns(10);
		}
		return txfImporteMod;
	}
	private JLabel getLblFechaInicioMod() {
		if (lblFechaInicioMod == null) {
			lblFechaInicioMod = new JLabel("Fecha inicio:");
			lblFechaInicioMod.setBounds(344, 47, 147, 26);
		}
		return lblFechaInicioMod;
	}
	private JLabel getLblFechaFinMod() {
		if (lblFechaFinMod == null) {
			lblFechaFinMod = new JLabel("Fecha fin:");
			lblFechaFinMod.setBounds(599, 47, 115, 26);
		}
		return lblFechaFinMod;
	}
	private JDateChooser getDtChFechaInicioMod() {
		if (dtChFechaInicioMod == null) {
			dtChFechaInicioMod = new JDateChooser();
			dtChFechaInicioMod.setBounds(344, 72, 155, 32);
		}
		return dtChFechaInicioMod;
	}
	private JDateChooser getDtChFechaFinMod() {
		if (dtChFechaFinMod == null) {
			dtChFechaFinMod = new JDateChooser();
			dtChFechaFinMod.setBounds(599, 72, 155, 32);
		}
		return dtChFechaFinMod;
	}
	private JComboBox<String> getCbTiposHab() {
		if (cbTipoHab == null) {
			cbTipoHab = new JComboBox<String>();
			cbTipoHab.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					tipoSeleccionado = (String)cbTipoHab.getSelectedItem();
					System.out.println(tipoSeleccionado);
				}
			});
			cbTipoHab.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					String tipo = tipoSeleccionado;
					cbTipoHab.removeAllItems();
					rellenarTiposHab();
					if(tipo!= null)
						cbTipoHab.setSelectedItem(tipo);
					else
						cbTipoHab.setSelectedItem(null);
					cbTipoHab.repaint();
				}
			});
			cbTipoHab.setBounds(55, 60, 186, 32);
			cbTipoHab.setVisible(false);
		}
		return cbTipoHab;
	}
	
	
	private void rellenarTiposHab(){
		try{
			Statement s = con.getCon().createStatement();	
			ResultSet rs = s.executeQuery("select t.nombre from tipo_habitacion t");
			
			while(rs.next()){
				cbTipoHab.addItem(rs.getString("NOMBRE"));
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
