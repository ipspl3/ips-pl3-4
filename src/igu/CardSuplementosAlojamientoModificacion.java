package igu;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import logica.Conexion;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import java.util.Date;

import java.awt.Color;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;

public class CardSuplementosAlojamientoModificacion extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Conexion con;
	private JPanel contentPane;
	private String[] cabecera = {"SUPLEMENTO", "TIPO DE HABITACIÓN", "IMPORTE POR DÍA", "FECHA DE ALTA", "FECHA DE BAJA"};
	private String tipoSeleccionado;
	private int id_supl;
	private JPanel panelModificacion;
	private JLabel lblSeleccioneElSuplemento;
	private JLabel lblLogoInfo;
	private JTextArea txtInfo;
	private JScrollPane spTablaModificacion;
	private JTable tablaModificacion;
	private JPanel panelDatosModificacion;
	private JLabel lblNuevoNombreDelSuplemento;
	private JTextField tfNuevoNombreDelSuplemento;
	private JLabel lblNuevoTipoDeHab;
	private JComboBox<String> cbNuevoTipoHab;
	private JLabel lblNuevoImporte;
	private JTextField tfNuevoImporte;
	private JDateChooser dcFechaInicioModificacion;
	private JLabel lblNuevaFechaInicio;
	private JLabel lblNuevaFechaFinal;
	private JDateChooser dcFechaFinModificacion;
	private JButton btnModificar;
	private String antiguoNombreSuplemento;
	private String antiguoTipoHab;
	private Float antiguoImporte;
	private long antiguaFechaInicio;
	private long antiguaFechaFin;
	private JButton btnRefrescar;
	private JButton btnCancelar;

	
	public class MiModeloTabla extends DefaultTableModel{
		private static final long serialVersionUID = 1L;
		public MiModeloTabla(Object[] columnNames, int numFilas) {
			super(columnNames,numFilas);
		}
		
		public boolean isCellEditable(int numFila, int numColumna){
			return false;
		}
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public CardSuplementosAlojamientoModificacion(Conexion conexion) throws SQLException {
		this.con = conexion;
		setBounds(100, 100,  755, 626);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setLayout(new BorderLayout(0, 0));
		add(getPanelModificacion(), BorderLayout.CENTER);
		rellenarTablaSuplementos();
		setVisible(true);
	}
	
	private void rellenarTablaSuplementos() throws SQLException{
		
		MiModeloTabla miModelo = new MiModeloTabla(cabecera, 0);
		SimpleDateFormat formato = new SimpleDateFormat("dd-MMM-yyyy");//Formatea el tipo Date en java
		
		String consulta = "select s.nombre_suplemento, s.nombre, ps.importe, ps.fecha_inicio, ps.fecha_fin "
				+ "from suplemento_modalidad s, precio_suplemento ps "
				+ "where s.id_supl_mod = ps.id_supl_mod order by ps.fecha_fin desc";
		PreparedStatement ps = con.getCon().prepareStatement(consulta);
		ResultSet rs = ps.executeQuery();
			
		while(rs.next()){
			Object[] fila = new Object[5];
			fila[0] = rs.getString("NOMBRE_SUPLEMENTO");
			fila[1] = rs.getString("NOMBRE");
			fila[2] = rs.getFloat("IMPORTE");
			fila[3] = formato.format(rs.getDate("FECHA_INICIO"));
			fila[4] = formato.format(rs.getDate("FECHA_FIN"));
			
			miModelo.addRow(fila);
		}
		
		rs.close();
		ps.close();
	}
	
	
	private void modificarTablaSuplementos(JTable tabla){
		tabla.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 12));
		tabla.setFont(new Font("Tahoma", Font.PLAIN, 12));
		TableColumn columna = tabla.getColumn(cabecera[0]);//MODIFICO EL ANCHO DE LA PRIMERA COLUMNA
		columna.setMinWidth(100);
		columna.setMaxWidth(100);
		
		columna = tabla.getColumn(cabecera[2]);
		columna.setMinWidth(120);
		columna.setMaxWidth(120);
		
		columna = tabla.getColumn(cabecera[3]);
		columna.setMinWidth(110);
		columna.setMaxWidth(110);
		
		columna = tabla.getColumn(cabecera[4]);
		columna.setMinWidth(110);
		columna.setMaxWidth(110);
		tabla.setRowHeight(25);
	}

	
	private int getIdSuplemento(String nombreSuplemento, String nombreTipo){
		try {
			
			String consulta = "select s.id_supl_mod "
					+ "from suplemento_modalidad s "
					+ "where s.nombre_suplemento =? and s.nombre=?";
			PreparedStatement ps = con.getCon().prepareStatement(consulta);
			
			ps.setString(1, nombreSuplemento);
			ps.setString(2, nombreTipo);
			ResultSet rs = ps.executeQuery();
			
			
			rs.next();
			int idSuplemento = rs.getInt("ID_SUPL_MOD");
			id_supl = idSuplemento;
			rs.close();
			ps.close();
			return idSuplemento;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	
	private boolean comprobarImporte(JTextField tfImporte){
		String importe = tfImporte.getText();
		try{
			Double.parseDouble(importe);
			return true;
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "Formato de importe incorrecto");
			tfImporte.setBorder(new LineBorder(Color.RED));
			return false;
		}
	}
	
	private void rellenarTiposHab(JComboBox<String> cb){
		try{
			Statement s = con.getCon().createStatement();	
			ResultSet rs = s.executeQuery("select t.nombre from tipo_habitacion t");
			
			while(rs.next()){
				cb.addItem(rs.getString("NOMBRE"));
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	private boolean existeSuplemento(String nombreSuplemento, String tipo){	
		try{
			
			String consulta = "select count(nombre_suplemento) "
					+ "from suplemento_modalidad s "
					+ "where s.nombre_suplemento = ? and s.nombre = ? ";
			PreparedStatement ps = con.getCon().prepareStatement(consulta);
			ps.setString(1, nombreSuplemento);
			ps.setString(2, tipo);
			
			ResultSet rs = ps.executeQuery();
			rs.next();
			int cantidadDeSuplementosConEseNombre = rs.getInt("COUNT(NOMBRE_SUPLEMENTO)");
			
			ps.close();
			rs.close();
			if(cantidadDeSuplementosConEseNombre >0)
				return true;
			return false;
		}
		catch(SQLException e){
			e.printStackTrace(); 
			return false;
		}
	}
	
	private void crearNuevoSuplemento(String nombreSuplemento, String tipo){
		try{
			
			Statement st = con.getCon().createStatement();
			ResultSet rs = st.executeQuery("select SECUENCIA_SUPLMOD.nextVal from dual");
			rs.next();
			int idSuplementoActual = id_supl = rs.getInt("NEXTVAL");
			
			StringBuilder sb = new StringBuilder();
			sb.append("insert into suplemento_modalidad values(?,?,?)");
			PreparedStatement ps = con.getCon().prepareStatement(sb.toString());
			ps.setString(1, nombreSuplemento.toLowerCase());
			ps.setString(2, tipo);
			ps.setInt(3, idSuplementoActual);
			ps.executeUpdate();
			
			rs.close();
			st.close();
			ps.close();
		}
		catch(SQLException e){
			
		}
	}
	
	private void insertarTarifa(float nuevoImporte, JDateChooser fechaInicio, JDateChooser fechaFin){
		try{
			StringBuilder sb = new StringBuilder();
			sb.append("insert into precio_suplemento values(secuencia_preciosupl.nextVal,?,?,?,?,?)");
			
			PreparedStatement ps = con.getCon().prepareStatement(sb.toString());
			ps.setDate(1, new java.sql.Date(fechaInicio.getDate().getTime()));
			ps.setDate(2, new java.sql.Date(fechaFin.getDate().getTime()));
			ps.setFloat(3, nuevoImporte);
			ps.setInt(4, id_supl);
			ps.setString(5, tipoSeleccionado);
			
			ps.executeUpdate();
			
			ps.close();
			rellenarTablaSuplementos();
			
		}
		catch(SQLException e){
			
		}
	}
	
	
	private JPanel getPanelModificacion() {
		if (panelModificacion == null) {
			panelModificacion = new JPanel();
			panelModificacion.setBorder(new TitledBorder(null, "Modificar suplemento por modalidad de alojamiento", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelModificacion.setLayout(null);
			panelModificacion.add(getLblSeleccioneElSuplemento());
			panelModificacion.add(getLblLogoInfo());
			panelModificacion.add(getTxtInfo());
			panelModificacion.add(getSpTablaModificacion());
			panelModificacion.add(getPanelDatosModificacion());
			panelModificacion.add(getBtnRefrescar());
		
		}
		
		return panelModificacion;
	}
	private JLabel getLblSeleccioneElSuplemento() {
		if (lblSeleccioneElSuplemento == null) {
			lblSeleccioneElSuplemento = new JLabel("Seleccione el suplemento:");
			lblSeleccioneElSuplemento.setFont(new Font("Tahoma", Font.BOLD, 12));
			lblSeleccioneElSuplemento.setBounds(21, 81, 167, 26);
		}
		return lblSeleccioneElSuplemento;
	}
	
	private JLabel getLblLogoInfo() {
		if (lblLogoInfo == null) {
			lblLogoInfo = new JLabel("");
			lblLogoInfo.setIcon(new ImageIcon(CardSuplementosAlojamientoModificacion.class.getResource("/img/iconoInfo.png")));
			lblLogoInfo.setBounds(21, 31, 31, 31);
		}
		return lblLogoInfo;
	}
	private JTextArea getTxtInfo() {
		if (txtInfo == null) {
			txtInfo = new JTextArea();
			txtInfo.setEditable(false);
			txtInfo.setLineWrap(true);
			txtInfo.setOpaque(false);
			txtInfo.setFont(new Font("Tahoma", Font.PLAIN, 12));
			txtInfo.setText("¡Atención! Para evitar conflictos solo se le mostrarán aquellos suplementos de alojamiento que no estén aplicados a una o varias reservas y cuya fecha de inicio sea superior o igual a la fecha actual.");
			txtInfo.setBounds(64, 31, 665, 37);
		}
		return txtInfo;
	}
	private JScrollPane getSpTablaModificacion() {
		if (spTablaModificacion == null) {
			spTablaModificacion = new JScrollPane();
			spTablaModificacion.setBounds(22, 108, 707, 168);
			spTablaModificacion.setViewportView(getTablaModificacion());
		}
		return spTablaModificacion;
	}
	private JTable getTablaModificacion() {
		if (tablaModificacion == null) {
			tablaModificacion = new JTable();
			tablaModificacion.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent arg0) {
					panelDatosModificacion.setVisible(true);
					rellenarDatosEnComponentesPanelDatosModificacion();
				}
			});
			tablaModificacion.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			rellenarTablaModificacion();
		}
		return tablaModificacion;
	}
	
	private void rellenarDatosEnComponentesPanelDatosModificacion(){
		try{
		
			antiguoNombreSuplemento = (String) tablaModificacion.getValueAt(tablaModificacion.getSelectedRow(), 0);
			antiguoTipoHab = (String) tablaModificacion.getValueAt(tablaModificacion.getSelectedRow(), 1);
			antiguoImporte = (Float) tablaModificacion.getValueAt(tablaModificacion.getSelectedRow(), 2);
			antiguaFechaInicio = ((Date) tablaModificacion.getValueAt(tablaModificacion.getSelectedRow(), 3)).getTime();
			antiguaFechaFin = ((Date) tablaModificacion.getValueAt(tablaModificacion.getSelectedRow(), 4)).getTime();
			
					
			tfNuevoNombreDelSuplemento.setText(antiguoNombreSuplemento);
			cbNuevoTipoHab.setSelectedItem(antiguoTipoHab);
			tfNuevoImporte.setText(antiguoImporte.toString());
			tfNuevoImporte.setBorder(new LineBorder(Color.BLACK));
			dcFechaInicioModificacion.setDate(new Date(antiguaFechaInicio));
			dcFechaFinModificacion.setDate(new Date(antiguaFechaFin));
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	private void rellenarTablaModificacion(){
		try{
			MiModeloTabla miModelo = new MiModeloTabla(cabecera, 0);
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");//Formatea el tipo Date en java
			
			String consulta = "select s.nombre_suplemento, s.nombre, ps.importe, ps.fecha_inicio, ps.fecha_fin "
					+ "from suplemento_modalidad s, precio_suplemento ps "
					+ "where s.id_supl_mod = ps.id_supl_mod and ps.fecha_inicio >= to_date(?, 'dd-MM-YYYY') and "
					+ "s.id_supl_mod not in ( "
					+ "select rs.id_supl_mod from reserva_hab rs)";
			PreparedStatement ps = con.getCon().prepareStatement(consulta);
			ps.setString(1, formato.format(new Date()));
			ResultSet rs = ps.executeQuery();
				
			while(rs.next()){
				Object[] fila = new Object[5];
				fila[0] = rs.getString("NOMBRE_SUPLEMENTO");
				fila[1] = rs.getString("NOMBRE");
				fila[2] = rs.getFloat("IMPORTE");
				fila[3] = rs.getDate("FECHA_INICIO");
				fila[4] = rs.getDate("FECHA_FIN");
				
				miModelo.addRow(fila);
			}
			
			tablaModificacion.setModel(miModelo);
			modificarTablaSuplementos(tablaModificacion);
			
			rs.close();
			ps.close();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	private JPanel getPanelDatosModificacion() {
		if (panelDatosModificacion == null) {
			panelDatosModificacion = new JPanel();
			panelDatosModificacion.setBorder(new TitledBorder(null, "Modificar tarifa", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelDatosModificacion.setFont(new Font("Tahoma", Font.PLAIN, 12));
			panelDatosModificacion.setBounds(21, 299, 708, 298);
			panelDatosModificacion.setLayout(null);
			panelDatosModificacion.add(getLblNuevoNombreDelSuplemento());
			panelDatosModificacion.add(getTfNuevoNombreDelSuplemento());
			panelDatosModificacion.add(getLblNuevoTipoDeHab());
			panelDatosModificacion.add(getCbNuevoTipoHab());
			panelDatosModificacion.add(getLblNuevoImporte());
			panelDatosModificacion.add(getTfNuevoImporte());
			panelDatosModificacion.add(getDcFechaInicioModificacion());
			panelDatosModificacion.add(getLblNuevaFechaInicio());
			panelDatosModificacion.add(getLblNuevaFechaFinal());
			panelDatosModificacion.add(getDcFechaFinModificacion());
			panelDatosModificacion.add(getBtnModificar());
			panelDatosModificacion.add(getBtnCancelar());
			panelDatosModificacion.setVisible(false);
		}
		return panelDatosModificacion;
	}
	private JLabel getLblNuevoNombreDelSuplemento() {
		if (lblNuevoNombreDelSuplemento == null) {
			lblNuevoNombreDelSuplemento = new JLabel("Nuevo nombre del suplemento:");
			lblNuevoNombreDelSuplemento.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblNuevoNombreDelSuplemento.setBounds(53, 39, 182, 26);
		}
		return lblNuevoNombreDelSuplemento;
	}
	private JTextField getTfNuevoNombreDelSuplemento() {
		if (tfNuevoNombreDelSuplemento == null) {
			tfNuevoNombreDelSuplemento = new JTextField();
			tfNuevoNombreDelSuplemento.setBounds(53, 64, 147, 32);
			tfNuevoNombreDelSuplemento.setColumns(10);
		}
		return tfNuevoNombreDelSuplemento;
	}
	private JLabel getLblNuevoTipoDeHab() {
		if (lblNuevoTipoDeHab == null) {
			lblNuevoTipoDeHab = new JLabel("Nuevo tipo de habitaci\u00F3n:");
			lblNuevoTipoDeHab.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblNuevoTipoDeHab.setBounds(299, 36, 147, 26);
		}
		return lblNuevoTipoDeHab;
	}
	private JComboBox<String> getCbNuevoTipoHab() {
		if (cbNuevoTipoHab == null) {
			cbNuevoTipoHab = new JComboBox<String>();
			cbNuevoTipoHab.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					tipoSeleccionado = (String)cbNuevoTipoHab.getSelectedItem();
				}
			});
			cbNuevoTipoHab.setFont(new Font("Tahoma", Font.PLAIN, 12));
			cbNuevoTipoHab.setBounds(299, 64, 147, 32);
			rellenarTiposHab(cbNuevoTipoHab);
			cbNuevoTipoHab.setSelectedItem(null);
		}
		return cbNuevoTipoHab;
	}
	private JLabel getLblNuevoImporte() {
		if (lblNuevoImporte == null) {
			lblNuevoImporte = new JLabel("Nuevo importe:");
			lblNuevoImporte.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblNuevoImporte.setBounds(520, 36, 92, 26);
		}
		return lblNuevoImporte;
	}
	private JTextField getTfNuevoImporte() {
		if (tfNuevoImporte == null) {
			tfNuevoImporte = new JTextField();
			tfNuevoImporte.setFont(new Font("Tahoma", Font.PLAIN, 12));
			tfNuevoImporte.setBounds(520, 64, 115, 32);
			tfNuevoImporte.setColumns(10);
		}
		return tfNuevoImporte;
	}
	private JDateChooser getDcFechaInicioModificacion() {
		if (dcFechaInicioModificacion == null) {
			dcFechaInicioModificacion = new JDateChooser();
			dcFechaInicioModificacion.getCalendarButton().setFont(new Font("Tahoma", Font.PLAIN, 11));
			dcFechaInicioModificacion.setFont(new Font("Tahoma", Font.PLAIN, 11));
			dcFechaInicioModificacion.setBounds(54, 155, 155, 32);
			dcFechaInicioModificacion.setMinSelectableDate(new Date());
		}
		return dcFechaInicioModificacion;
	}
	private JLabel getLblNuevaFechaInicio() {
		if (lblNuevaFechaInicio == null) {
			lblNuevaFechaInicio = new JLabel("Nueva fecha de inicio:");
			lblNuevaFechaInicio.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblNuevaFechaInicio.setBounds(54, 129, 139, 26);
		}
		return lblNuevaFechaInicio;
	}
	private JLabel getLblNuevaFechaFinal() {
		if (lblNuevaFechaFinal == null) {
			lblNuevaFechaFinal = new JLabel("Nueva fecha final:");
			lblNuevaFechaFinal.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblNuevaFechaFinal.setBounds(300, 129, 109, 26);
		}
		return lblNuevaFechaFinal;
	}
	private JDateChooser getDcFechaFinModificacion() {
		if (dcFechaFinModificacion == null) {
			dcFechaFinModificacion = new JDateChooser();
			dcFechaFinModificacion.getCalendarButton().setFont(new Font("Tahoma", Font.PLAIN, 11));
			dcFechaFinModificacion.setFont(new Font("Tahoma", Font.PLAIN, 11));
			dcFechaFinModificacion.setBounds(300, 155, 155, 32);
			dcFechaFinModificacion.setMinSelectableDate(new Date());
		}
		return dcFechaFinModificacion;
	}
	
	private boolean comprobarFechaFinDeModificacion(){
		try{
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			
			Date fecha_inicio = formato.parse(formato.format(dcFechaInicioModificacion.getDate()));
			Date fecha_fin = formato.parse(formato.format(dcFechaFinModificacion.getDate()));
		
			if(fecha_fin.compareTo(fecha_inicio) < 0){
				JOptionPane.showMessageDialog(null, "La fecha de fin debe de ser mayor que\n la fecha de inicio.", "Error", JOptionPane.ERROR_MESSAGE);
				return false;
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	

	private void borrarFilaPrecioSuplemento() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
			String consulta = "delete from precio_suplemento ps "
					+ "where ps.id_supl_mod = ? and ps.nombre = ? "
					+ "and ps.fecha_inicio = to_date(?, 'dd-MM-YYYY') "
					+ "and ps.fecha_fin = to_date(?, 'dd-MM-YYYY')";
			
			PreparedStatement ps = con.getCon().prepareStatement(consulta);
			ps.setInt(1, id_supl);
			ps.setString(2, antiguoTipoHab);
			ps.setString(3, sdf.format(new Date(antiguaFechaInicio)));
			ps.setString(4, sdf.format(new Date(antiguaFechaFin)));
			ps.executeUpdate();
			ps.close();
		}
		catch(SQLException e) {
			
		}
	}
	
	private int numeroDeTarifasAsociadasAlSuplemento(String nombreSupl, String tipo) {
		try {
			
			String consulta = "select count(ps.id_supl_mod) "
					+ "from precio_suplemento ps, suplemento_modalidad s "
					+ "where s.nombre_suplemento = ? and s.nombre = ? and ps.id_supl_mod = s.id_supl_mod";
			PreparedStatement ps = con.getCon().prepareStatement(consulta);
			ps.setString(1, nombreSupl);
			ps.setString(2, tipo);
			
			ResultSet rs = ps.executeQuery();
			rs.next();
			int numeroDeTarifasAsociadasAlSuplemento = rs.getInt("COUNT(PS.ID_SUPL_MOD)");
			
			rs.close();
			ps.close();
			return numeroDeTarifasAsociadasAlSuplemento;
			
		}
		catch(SQLException e) {
			e.printStackTrace();
			return 2;
		}
	}
	
	private void borrarSuplemento(int id) {
		try {
			String consulta = "delete from suplemento_modalidad s "
					+ "where s.id_supl_mod = ?";
			
			PreparedStatement ps = con.getCon().prepareStatement(consulta);
			ps.setInt(1, id);
			ps.executeUpdate();
			ps.close();
			
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void resetCamposDatosModificacion() {
		tfNuevoNombreDelSuplemento.setText(null);
		tfNuevoImporte.setText(null);
		dcFechaInicioModificacion.setDate(null);
		dcFechaFinModificacion.setDate(null);
		cbNuevoTipoHab.setSelectedItem(null);
	}
	
	private int cambiarDatosASuplemento(String nuevoNombre, String tipo, float precio, JDateChooser fi, JDateChooser ff) {
		try {
			int idNuevoSupl = getIdSuplemento(nuevoNombre, tipo);
			int idAntiguo = getIdSuplemento(antiguoNombreSuplemento, antiguoTipoHab);
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
			
			String update = "update precio_suplemento ps " + 
					"set ps.id_supl_mod = ?, ps.nombre = ?, " + 
					"ps.fecha_inicio = to_date(?, 'dd-MM-YYYY'), ps.fecha_fin = to_date(?,'dd-MM-YYYY'), "+ 
					"ps.importe = ? " + 
					"where ps.nombre = ?  and ps.id_supl_mod =? " + 
					"and ps.fecha_inicio = to_date(?, 'dd-MM-YYYY') and ps.fecha_fin = to_date(?, 'dd-MM-YYYY')";
			PreparedStatement ps = con.getCon().prepareStatement(update);
			ps.setInt(1, idNuevoSupl);
			ps.setString(2, tipo);
			ps.setString(3, sdf.format(fi.getDate()));
			ps.setString(4, sdf.format(ff.getDate()));
			ps.setFloat(5, precio);
			ps.setString(6, antiguoTipoHab);
			ps.setInt(7, idAntiguo);
			ps.setString(8, sdf.format(new Date(antiguaFechaInicio)));
			ps.setString(9, sdf.format(new Date(antiguaFechaFin)));
			int filasCambiadas = ps.executeUpdate();
			ps.close();
			return filasCambiadas;
			
		}
		catch(SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}
	private JButton getBtnModificar() {
		if (btnModificar == null) {
			btnModificar = new JButton("Modificar");
			btnModificar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String nuevoNombreSuplemento = tfNuevoNombreDelSuplemento.getText();
					
					if(tipoSeleccionado!= null && nuevoNombreSuplemento!= null && nuevoNombreSuplemento.length()!=0){
						if(comprobarFechaFinDeModificacion() && comprobarImporte(tfNuevoImporte)){
							
							if(!existeSuplemento(nuevoNombreSuplemento, tipoSeleccionado)){
								int opcion = JOptionPane.showConfirmDialog(null, "¿Desea confirmar los cambios?", "Confirmación", JOptionPane.YES_NO_OPTION);
								if(opcion==0) {
									int antiguo_Id = getIdSuplemento(antiguoNombreSuplemento, antiguoTipoHab);
									borrarFilaPrecioSuplemento();
									if(numeroDeTarifasAsociadasAlSuplemento(antiguoNombreSuplemento,antiguoTipoHab) <=0) {
										borrarSuplemento(antiguo_Id);
									}
									crearNuevoSuplemento(nuevoNombreSuplemento, tipoSeleccionado);
									insertarTarifa(Float.parseFloat(tfNuevoImporte.getText()), dcFechaInicioModificacion, dcFechaFinModificacion);
									rellenarTablaModificacion();
									tablaModificacion.repaint();
									resetCamposDatosModificacion();
									panelDatosModificacion.setVisible(false);
								}
								
							}
							else {
								int opcion = JOptionPane.showConfirmDialog(null, "¿Desea confirmar los cambios?", "Confirmación", JOptionPane.YES_NO_OPTION);
								if(opcion==0) {
									int filasCambiadas = cambiarDatosASuplemento(nuevoNombreSuplemento, tipoSeleccionado, Float.parseFloat(tfNuevoImporte.getText()), dcFechaInicioModificacion, dcFechaFinModificacion);
									int antiguo_Id = getIdSuplemento(antiguoNombreSuplemento, antiguoTipoHab);
									if(numeroDeTarifasAsociadasAlSuplemento(antiguoNombreSuplemento,antiguoTipoHab) <=0) {
										borrarSuplemento(antiguo_Id);
									}
									rellenarTablaModificacion();
									tablaModificacion.repaint();
									resetCamposDatosModificacion();
									panelDatosModificacion.setVisible(false);
									if(filasCambiadas >=1) {
										ImageIcon icono = new ImageIcon("src/img/check.png");
										JOptionPane.showMessageDialog(null, "Los datos se han modificaco correctamente", "Confirmación correcta" , JOptionPane.DEFAULT_OPTION, icono);
									}
								}
							}
								
						}
					}
					else{
						JOptionPane.showMessageDialog(null, "¡Faltan datos!", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			});
			btnModificar.setFont(new Font("Tahoma", Font.PLAIN, 12));
			btnModificar.setBounds(585, 242, 102, 35);
		}
		return btnModificar;
	}
	private JButton getBtnRefrescar() {
		if (btnRefrescar == null) {
			btnRefrescar = new JButton("Refrescar");
			btnRefrescar.setToolTipText("Refrescar la información de la tabla");
			btnRefrescar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					rellenarTablaModificacion();
				}
			});
			btnRefrescar.setBounds(636, 81, 93, 29);
		}
		return btnRefrescar;
	}
	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int opcion = JOptionPane.showConfirmDialog(null, "¡Atención! Si continua se perderán los datos y no se realizará ninguna modificación\n "
							+ "¿Desea continuar con la cancelación?", "Cancelación", JOptionPane.YES_NO_OPTION, JOptionPane.CANCEL_OPTION);
					if(opcion==0)
						panelDatosModificacion.setVisible(false);
				}
			});
			btnCancelar.setBounds(474, 242, 102, 35);
		}
		return btnCancelar;
	}
}
