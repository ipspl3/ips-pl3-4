package igu;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import logica.ComparadorFecha;
import logica.Conexion;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridLayout;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import com.toedter.calendar.JDateChooser;

public class CardConsultarPromocionoSuplemento extends JPanel {

	private static Connection conn;
	private static PreparedStatement stmt;

	private JPanel contentPane;
	private JLabel lblConsultarPromocionoSuplemento;
	private JPanel pnCentro;
	private JPanel pnFechas;
	private JScrollPane scrollPaneResultados;
	private JLabel lblfechainicio;
	private JLabel lblfechafinal;
	private JPanel pnSouth;
	private JButton btnBuscar;
	private JButton btnCancelar;
	private JTable table;

	private JCheckBox chckbxPromociones;
	private JCheckBox chckbxSuplementos;
	private JPanel panel;

	private Calendar c = Calendar.getInstance();
	private JDateChooser dCfechainicio;
	private JPanel panel_1;
	private JPanel panel_2;
	private JDateChooser dCfechafinal;
	
	
	public CardConsultarPromocionoSuplemento(Conexion con) {


		setBounds(100, 100, 472, 341);
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(new BorderLayout(0, 0));
		add(getLblConsultarPromocionoSuplemento(), BorderLayout.NORTH);
		add(getPnCentro(), BorderLayout.CENTER);
		add(getPnSouth(), BorderLayout.SOUTH);

		conn = con.getCon();
		setVisible(true);

	}

	private JLabel getLblConsultarPromocionoSuplemento() {
		if (lblConsultarPromocionoSuplemento == null) {
			lblConsultarPromocionoSuplemento = new JLabel("Consultar Promocion o Suplemento");
		}
		return lblConsultarPromocionoSuplemento;
	}

	private JPanel getPnCentro() {
		if (pnCentro == null) {
			pnCentro = new JPanel();
			pnCentro.setLayout(new GridLayout(0, 1, 0, 0));
			pnCentro.add(getPnFechas());
			pnCentro.add(getPanel());
			pnCentro.add(getScrollPaneResultados());
		}
		return pnCentro;
	}

	private JPanel getPnFechas() {
		if (pnFechas == null) {
			pnFechas = new JPanel();
			pnFechas.setLayout(new GridLayout(0, 1, 0, 0));
			pnFechas.add(getPanel_1());
			pnFechas.add(getPanel_2());
		}
		return pnFechas;
	}

	private JScrollPane getScrollPaneResultados() {
		if (scrollPaneResultados == null) {
			scrollPaneResultados = new JScrollPane();
			scrollPaneResultados.setViewportView(getTable());
		}
		return scrollPaneResultados;
	}

	private JLabel getLblfechainicio() {
		if (lblfechainicio == null) {
			lblfechainicio = new JLabel("Fecha de inicio:");
		}
		return lblfechainicio;
	}

	private JLabel getLblfechafinal() {
		if (lblfechafinal == null) {
			lblfechafinal = new JLabel("Fecha final:");
		}
		return lblfechafinal;
	}

	private JPanel getPnSouth() {
		if (pnSouth == null) {
			pnSouth = new JPanel();
			pnSouth.add(getBtnBuscar());
		}
		return pnSouth;
	}

	private JButton getBtnBuscar() {
		if (btnBuscar == null) {
			btnBuscar = new JButton("Buscar");
			btnBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					SimpleDateFormat Formato = new SimpleDateFormat("dd/MM/yyyy");
					
					ComparadorFecha comparadorfecha = new ComparadorFecha(Formato.format(getDCfechainicio().getDate()),Formato.format(getDCfechafinal().getDate()));
				

					if (!comparadorfecha.CheckFechaActual()) {
						JOptionPane.showMessageDialog(null, "Fecha de inicio no correcta,comprueba"
								+ " que los valores no son inferiores a la fecha actual.");

					} else if (!comparadorfecha.CheckFechaFinal()) {
						JOptionPane.showMessageDialog(null, "Fecha final incorecta comprueba que los valores"
								+ " no son inferiores a la fecha de inicio.");

					} else {

						try {
							SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

							java.sql.Date dateinicio = new java.sql.Date(format.parse(comparadorfecha.getFechaInicio()).getTime());
							java.sql.Date datefinal = new java.sql.Date(format.parse(comparadorfecha.getFechaFinal()).getTime());

							if (getChckbxPromociones().isSelected() && getChckbxSuplementos().isSelected()) {
								stmt = conn.prepareStatement("SELECT * " + "FROM TARIFA_POR_FECHA "
										+ "WHERE FECHA_INICIO>=? AND FECHA_FIN<=?");
								stmt.setDate(1, new java.sql.Date(dateinicio.getTime()));
								stmt.setDate(2, new java.sql.Date(datefinal.getTime()));

								ResultSet promociones = stmt.executeQuery();

								DefaultTableModel model = (DefaultTableModel) getTable().getModel();
								model.setRowCount(0);

								while (promociones.next()) {
									Object[] promocion = new Object[5];

									promocion[0] = promociones.getInt(1);
									promocion[1] = promociones.getString(2);
									promocion[2] = promociones.getDate(3).toString();
									promocion[3] = promociones.getDate(4).toString();
									promocion[4] = promociones.getInt(5);

									model.addRow(promocion);

									getTable().setModel(model);

								}
								JOptionPane.showMessageDialog(null,
										"Las promociones y suplementos disponibles se muestran en la tabla.");
							} else if (getChckbxPromociones().isSelected()) {
								stmt = conn.prepareStatement("SELECT * " + "FROM TARIFA_POR_FECHA "
										+ "WHERE FECHA_INICIO>=? AND FECHA_FIN<=? AND PORCENTAJE<0");
								stmt.setDate(1, new java.sql.Date(dateinicio.getTime()));
								stmt.setDate(2, new java.sql.Date(datefinal.getTime()));

								ResultSet promociones = stmt.executeQuery();

								DefaultTableModel model = (DefaultTableModel) getTable().getModel();
								model.setRowCount(0);

								while (promociones.next()) {
									Object[] promocion = new Object[5];

									promocion[0] = promociones.getInt(1);
									promocion[1] = promociones.getString(2);
									promocion[2] = promociones.getDate(3).toString();
									promocion[3] = promociones.getDate(4).toString();
									promocion[4] = promociones.getInt(5);

									model.addRow(promocion);

									getTable().setModel(model);
								}
								JOptionPane.showMessageDialog(null,
										"Las promociones disponibles se muestran en la tabla.");
							} else if (getChckbxSuplementos().isSelected()) {
								stmt = conn.prepareStatement("SELECT * " + "FROM TARIFA_POR_FECHA "
										+ "WHERE FECHA_INICIO>=? AND FECHA_FIN<=? AND PORCENTAJE>0");
								stmt.setDate(1, new java.sql.Date(dateinicio.getTime()));
								stmt.setDate(2, new java.sql.Date(datefinal.getTime()));

								ResultSet promociones = stmt.executeQuery();

								DefaultTableModel model = (DefaultTableModel) getTable().getModel();
								model.setRowCount(0);

								while (promociones.next()) {
									Object[] promocion = new Object[5];

									promocion[0] = promociones.getInt(1);
									promocion[1] = promociones.getString(2);
									promocion[2] = promociones.getDate(3).toString();
									promocion[3] = promociones.getDate(4).toString();
									promocion[4] = promociones.getInt(5);

									model.addRow(promocion);

									getTable().setModel(model);
								}
								JOptionPane.showMessageDialog(null,
										"Las suplementos disponibles se muestran en la tabla.");
							} else {
								JOptionPane.showMessageDialog(null, "Seleccione que desea visualizar");

							}

						} catch (Exception a) {
							System.out.println("**ERROR OBTAINING RESULT**");
							a.printStackTrace();
						}

					}

				}
			});
		}
		return btnBuscar;
	}

	private JTable getTable() {
		if (table == null) {
			table = new JTable();
			DefaultTableModel model = new DefaultTableModel();

			model.addColumn("ID");
			model.addColumn("NOMBRE");
			model.addColumn("FECHA INICIO");
			model.addColumn("FECHA FINAL");
			model.addColumn("DESCUENTO");

			table.setModel(model);

		}
		return table;
	}

	private JCheckBox getChckbxPromociones() {
		if (chckbxPromociones == null) {
			chckbxPromociones = new JCheckBox("Promociones");
			chckbxPromociones.setSelected(true);
		}
		return chckbxPromociones;
	}

	private JCheckBox getChckbxSuplementos() {
		if (chckbxSuplementos == null) {
			chckbxSuplementos = new JCheckBox("Suplementos");
			chckbxSuplementos.setSelected(true);
		}
		return chckbxSuplementos;
	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.add(getChckbxPromociones());
			panel.add(getChckbxSuplementos());
		}
		return panel;
	}
	private JDateChooser getDCfechainicio() {
		if (dCfechainicio == null) {
			dCfechainicio = new JDateChooser();
		}
		return dCfechainicio;
	}
	private JPanel getPanel_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.add(getLblfechainicio());
			panel_1.add(getDCfechainicio());
		}
		return panel_1;
	}
	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.add(getLblfechafinal());
			panel_2.add(getDCfechafinal());
		}
		return panel_2;
	}
	private JDateChooser getDCfechafinal() {
		if (dCfechafinal == null) {
			dCfechafinal = new JDateChooser();
		}
		return dCfechafinal;
	}
}
