package igu;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import logica.Conexion;
import logica.Trabajadores;


import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.DefaultListModel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.ListSelectionModel;
import javax.swing.SpinnerDateModel;
import javax.swing.border.BevelBorder;



import javax.swing.border.TitledBorder;


import java.awt.CardLayout;
import java.awt.Color;


import javax.swing.JTextField;


import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JOptionPane;
import com.toedter.calendar.JDateChooser;
import javax.swing.JCheckBox;
import javax.swing.UIManager;
import javax.swing.JTextPane;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
public class CardTrabajadores extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Connection con;
	private JPanel contentPane;
	private JScrollPane spTablaTurnos;
	private JTable tablaEstados;
	private String[] cabecera = {"TRABAJADOR", "ESTADO", "FECHA INICIO", "FECHA FIN", "HORA ENTRADA", "HORA SALIDA"};
	private JPanel panelEstadoTrabajadores;
	private JPanel panelTabla;
	private JPanel panelFiltros;
	private JLabel lblNombreEmpleado;
	private JTextField tfEmpleado;
	private JButton btnLupa;
	private JScrollPane scrollPaneListaEmpleados;
	private JList<String> listaEmpleadosConsultarEstado;
	private DefaultListModel<String> modeloLista = new DefaultListModel<String>();
	private JDateChooser dcFechaInicio;
	private JLabel lblFechaInicio;
	private JLabel lblFechaFin;
	private JDateChooser dcFechaFin;
	private ArrayList<String> empleadosSeleccionados = new ArrayList<String>();
	private Trabajadores trabajadores;
	private boolean primero = true;
	private JButton btnFiltrar;
	private JCheckBox chckbxRecepcin;
	private JPanel panelAreas;
	private JCheckBox chckbxLimpieza;
	private JCheckBox chckbxMantenimiento;
	private JCheckBox chckbxRestauracin;
	private MiModeloTabla modeloTabla;
	private JPanel panelFiltrarPorEmpleados;
	private JPanel panelSuperCard;
	private JPanel panelElegirOpcion;
	private JLabel lblquOperacinDesea;
	private JButton btnConsultarElEstado;
	private JButton btnAsignarUnNuevo;
	private CardLayout superCard = new CardLayout(0,0);
	private JPanel panelNuevoTurno;
	private JLabel lblNombreDelEmpleado;
	private JTextField tfEmpleadoNuevoTurno;
	private JButton btnLupaNuevoTipo;
	private JScrollPane spListaNuevoTurno;
	private JList<String> listaEmpleadosNuevoTurno;
	private JTextPane txtpnSeleccioneEllosEmpleados;
	private JLabel lblreaDeTrabajo;
	private JComboBox<String> cbAreaTrabajoNuevoTurno;
	private JLabel lblFechaDeInicio;
	private JDateChooser dcFechaInicioNuevoTurno;
	private JLabel lblFechaFinal;
	private JDateChooser dcFechaFinNuevoTurno;
	private JPanel panelDias;
	private JButton btnContinuar;
	private JSpinner spinnerEntradaDia1;
	private JLabel lblHoraDeEntradaDia1;
	private JLabel lblHoraDeSalidaDia1;
	private JSpinner spinnerSalidaDia1;
	private JLabel lblHoraEntradaDia2;
	private JSpinner spinnerEntradaDia2;
	private JLabel lbHoraSalidaDia2;
	private JSpinner spinnerSalidaDia2;
	private JLabel lblEntradaDia3;
	private JSpinner spinnerEntradaDia3;
	private JLabel lblHoraSalidaDia3;
	private JSpinner spinnerSalidaDia3;
	private JLabel lblEntradaDia4;
	private JSpinner spinnerEntradaDia4;
	private JLabel lblSalidaDia4;
	private JSpinner spinnerSalidaDia4;
	private JLabel lblEntradaDia5;
	private JLabel lblEntradaDia6;
	private JSpinner spinnerEntradaDia5;
	private JSpinner spinnerSalidaDia5;
	private JSpinner spinnerEntradaDia6;
	private JSpinner spinnerSalidaDia6;
	private JLabel lblSalidaDia5;
	private JLabel lblSalidaDia6;
	private JPanel panelHoras;
	private JCheckBox boxJPD1;
	private JPanel panelDia1;
	private JPanel panelDia2;
	private JCheckBox boxJPD2;
	private JPanel panelDia3;
	private JCheckBox boxJPD3;
	private JTextPane txtpnSeleccioneEllosEmpleados_1;
	private JScrollPane spTablaNuevoTurno;
	private JTable tablaNuevoTurno;
	private JButton btnAtrasNuevoTurno1;
	private JLabel lblEstosSonLos;
	private JPanel panelJornadaPartidaDia1;
	private JLabel lblEntradaJPD1;
	private JSpinner spinnerEntradaJPD1;
	private JLabel lblSalidaJPD1;
	private JSpinner spinnerSalidaJPD1;
	private JPanel panelJornadaPartidaDia2;
	private JLabel lblEntradaJPD2;
	private JSpinner spinnerEntradaJPD2;
	private JLabel lblSalidaJPD2;
	private JSpinner spinnerSalidaJPD2;
	private JPanel panelJornadaPartidaDia3;
	private JLabel lblEntradaJPD3;
	private JSpinner spinnerEntradaJPD3;
	private JLabel lblSalidaJPD3;
	private JSpinner spinnerSalidaJPD3;
	private String areaSeleccionadaNuevoTurno;
	private JPanel panelDia4;
	private JCheckBox boxJPD4;
	private JPanel panelJornadaPartidaDia4;
	private JPanel panelDia5;
	private JCheckBox boxJPD5;
	private JPanel panelJornadaPartidaDia5;
	private JPanel panelDia6;
	private JCheckBox boxJPD6;
	private JPanel panelJornadaPartidaDia6;
	private JLabel lblEntradaJPD4;
	private JSpinner spinnerEntradaJPD4;
	private JLabel lblSalidaJPD4;
	private JSpinner spinnerSalidaJPD4;
	private JLabel lblEntradaJPD5;
	private JSpinner spinnerEntradaJPD5;
	private JLabel lblSalidaJPD5;
	private JSpinner spinnerSalidaJPD5;
	private JLabel lblEntadaJPD6;
	private JSpinner spinnerEntradaJPD6;
	private JLabel lblSalidaJPD6;
	private JSpinner spinnerSalidaJPD6;
	private JButton btnAnterior;
	private JButton btnDia7;
	private JPanel panelHorarioDia7;
	private JPanel panelDia7;
	private JCheckBox boxJPD7;
	private JLabel lblHoraDeEntradaDia7;
	private JLabel lblHoraDeSalidaDia7;
	private JSpinner spinnerEntradaDia7;
	private JSpinner spinnerSalidaDia7;
	private JPanel panelJPD7;
	private JLabel lblHoraDeEntradaJPD7;
	private JSpinner spinnerEntradaJPD7;
	private JLabel lblHoraDeSalidaJPD7;
	private JSpinner spinnerSalidaJPD7;
	private JButton btnAnteriorDia7;
	private JButton btnAsignarTurno;
	private JPanel panelHorario;
	private List<Integer> horasTotalesPorDia = new ArrayList<Integer>();
	private JButton btndeseaAsignarUn;
	private JToggleButton tglbtnNoHorario1;
	private JToggleButton tglbtnNoHorario2;
	private JToggleButton tglbtnNoHorario3;
	private JToggleButton tglbtnNoHorario4;
	private JToggleButton tglbtnNoHorario5;
	private JToggleButton tglbtnNoHorario6;
	private JToggleButton tglbtnNoHorario7;
	private List<String> listaDeNombreDeDias;
	private DialogoElegirHorarios dialogoElegirHorarios;
	private JButton btnNuevaConsulta;
	
	
	private class MiModeloTabla extends DefaultTableModel{
		private static final long serialVersionUID = 1L;
		public MiModeloTabla(Object[] columnNames, int numFilas) {
			super(columnNames,numFilas);
		}
		
		public boolean isCellEditable(int numFila, int numColumna){
			return false;
		}
	}
	
	private class AccionTglBtn implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			JToggleButton boton = (JToggleButton) e.getSource();
			if(boton.isSelected()) {
				if(boton.getName().contains("1"))
					deshabilitarHabilitarComponentes(boxJPD1, spinnerEntradaDia1, spinnerEntradaJPD1,
							spinnerSalidaDia1, spinnerSalidaJPD1, lblHoraDeEntradaDia1, lblHoraDeSalidaDia1, lblEntradaJPD1, lblSalidaJPD1, false);
				else if(boton.getName().contains("2"))
					deshabilitarHabilitarComponentes(boxJPD2, spinnerEntradaDia2, spinnerEntradaJPD2,
							spinnerSalidaDia2, spinnerSalidaJPD2, lblHoraEntradaDia2, lbHoraSalidaDia2, lblEntradaJPD2, lblSalidaJPD2, false);
				else if(boton.getName().contains("3"))
					deshabilitarHabilitarComponentes(boxJPD3, spinnerEntradaDia3, spinnerEntradaJPD3,
							spinnerSalidaDia3, spinnerSalidaJPD3, lblEntradaDia3, lblHoraSalidaDia3, lblEntradaJPD3, lblSalidaJPD3, false);
				else if(boton.getName().contains("4"))
					deshabilitarHabilitarComponentes(boxJPD4, spinnerEntradaDia4, spinnerEntradaJPD4,
							spinnerSalidaDia4, spinnerSalidaJPD4, lblEntradaDia4, lblSalidaDia4, lblEntradaJPD4, lblSalidaJPD4, false);
				else if(boton.getName().contains("5"))
					deshabilitarHabilitarComponentes(boxJPD5, spinnerEntradaDia5, spinnerSalidaJPD5,
							spinnerSalidaDia5, spinnerEntradaJPD5, lblEntradaDia5, lblSalidaDia5, lblEntradaJPD5, lblSalidaJPD5, false);
				else if(boton.getName().contains("6"))
					deshabilitarHabilitarComponentes(boxJPD6, spinnerEntradaDia6, spinnerSalidaJPD6,
							spinnerSalidaDia6, spinnerEntradaJPD6, lblEntradaDia6, lblSalidaDia6, lblEntadaJPD6, lblSalidaJPD6, false);
				else if(boton.getName().contains("7"))
					deshabilitarHabilitarComponentes(boxJPD7, spinnerEntradaDia7, spinnerSalidaJPD7,
							spinnerSalidaDia7, spinnerEntradaJPD7, lblHoraDeEntradaDia7, lblHoraDeSalidaDia7, lblHoraDeEntradaJPD7, lblHoraDeSalidaJPD7, false);
				

			}
			else {
				if(boton.getName().contains("1"))
					deshabilitarHabilitarComponentes(boxJPD1, spinnerEntradaDia1, spinnerEntradaJPD1,
							spinnerSalidaDia1, spinnerSalidaJPD1, lblHoraDeEntradaDia1, lblHoraDeSalidaDia1, lblEntradaJPD1, lblSalidaJPD1, true);
				else if(boton.getName().contains("2"))
					deshabilitarHabilitarComponentes(boxJPD2, spinnerEntradaDia2, spinnerEntradaJPD2,
							spinnerSalidaDia2, spinnerSalidaJPD2, lblHoraEntradaDia2, lbHoraSalidaDia2, lblEntradaJPD2, lblSalidaJPD2, true);
				else if(boton.getName().contains("3"))
					deshabilitarHabilitarComponentes(boxJPD3, spinnerEntradaDia3, spinnerEntradaJPD3,
							spinnerSalidaDia3, spinnerSalidaJPD3, lblEntradaDia3, lblHoraSalidaDia3, lblEntradaJPD3, lblSalidaJPD3, true);
				else if(boton.getName().contains("4"))
					deshabilitarHabilitarComponentes(boxJPD4, spinnerEntradaDia4, spinnerEntradaJPD4,
							spinnerSalidaDia4, spinnerSalidaJPD4, lblEntradaDia4, lblSalidaDia4, lblEntradaJPD4, lblSalidaJPD4, true);
				else if(boton.getName().contains("5"))
					deshabilitarHabilitarComponentes(boxJPD5, spinnerEntradaDia5, spinnerSalidaJPD5,
							spinnerSalidaDia5, spinnerEntradaJPD5, lblEntradaDia5, lblSalidaDia5, lblEntradaJPD5, lblSalidaJPD5, true);
				else if(boton.getName().contains("6"))
					deshabilitarHabilitarComponentes(boxJPD6, spinnerEntradaDia6, spinnerSalidaJPD6,
							spinnerSalidaDia6, spinnerEntradaJPD6, lblEntradaDia6, lblSalidaDia6, lblEntadaJPD6, lblSalidaJPD6, true);
				else if(boton.getName().contains("7"))
					deshabilitarHabilitarComponentes(boxJPD7, spinnerEntradaDia7, spinnerSalidaJPD7,
							spinnerSalidaDia7, spinnerEntradaJPD7, lblHoraDeEntradaDia7, lblHoraDeSalidaDia7, lblHoraDeEntradaJPD7, lblHoraDeSalidaJPD7, true);
			}
		}
		
		private void deshabilitarHabilitarComponentes(JCheckBox box, JSpinner spinner1, JSpinner spinner2, 
				JSpinner spinner3, JSpinner spinner4, JLabel label1, JLabel label2, JLabel label3, JLabel label4, boolean estado) {
			box.setEnabled(estado);
			spinner1.setEnabled(estado);
			spinner2.setEnabled(estado);
			spinner3.setEnabled(estado);
			spinner4.setEnabled(estado);
			label1.setEnabled(estado);
			label2.setEnabled(estado);
			label3.setEnabled(estado);
			label4.setEnabled(estado);
		}
		
	}
	
	private class AccionCheck implements ChangeListener{

		@Override
		public void stateChanged(ChangeEvent e){
			JCheckBox box = (JCheckBox) e.getSource();
			String nombre = box.getName();
			if(box.isSelected()) {
				if(nombre.contains("1"))
					panelJornadaPartidaDia1.setVisible(true);
				else if(nombre.contains("2"))
					panelJornadaPartidaDia2.setVisible(true);
				else if(nombre.contains("3"))
					panelJornadaPartidaDia3.setVisible(true);
				else if(nombre.contains("4"))
					panelJornadaPartidaDia4.setVisible(true);
				else if(nombre.contains("5"))
					panelJornadaPartidaDia5.setVisible(true);
				else if(nombre.contains("6"))
					panelJornadaPartidaDia6.setVisible(true);
				else if(nombre.contains("7"))
					panelJPD7.setVisible(true);
			}
			
			else if(!box.isSelected()) {
				if(nombre.contains("1"))
					panelJornadaPartidaDia1.setVisible(false);
				else if(nombre.contains("2"))
					panelJornadaPartidaDia2.setVisible(false);
				else if(nombre.contains("3"))
					panelJornadaPartidaDia3.setVisible(false);	
				else if(nombre.contains("4"))
					panelJornadaPartidaDia4.setVisible(false);
				else if(nombre.contains("5"))
					panelJornadaPartidaDia5.setVisible(false);
				else if(nombre.contains("6"))
					panelJornadaPartidaDia6.setVisible(false);
				else if(nombre.contains("7"))
					panelJPD7.setVisible(false);
			}
			
		}
		
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public CardTrabajadores(Conexion conexion) throws SQLException {
		this.con = conexion.getCon();
		setBounds(100, 100, 755, 626);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setLayout(new BorderLayout(0, 0));
		add(getPanelSuperCard(), BorderLayout.CENTER);
		rellenarTablaTurnos();
		setVisible(true);
	}
	private JScrollPane getSpTablaTurnos() {
		if (spTablaTurnos == null) {
			spTablaTurnos = new JScrollPane();
			spTablaTurnos.setBounds(0, 0, 731, 301);
			spTablaTurnos.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
			spTablaTurnos.setViewportView(getTablaTurnos());
		}
		return spTablaTurnos;
	}
	private JTable getTablaTurnos() {
		if (tablaEstados == null) {
			tablaEstados = new JTable();
			tablaEstados.setBorder(null);
			tablaEstados.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tablaEstados;
	}
	
	private void rellenarTablaTurnos(){
		try {
			modeloTabla = new MiModeloTabla(cabecera, 0);
			
			Statement s = con.createStatement();
			
			ResultSet rs = s.executeQuery("select tr.nombre, t.tipo, t.fecha_inicio, "
					+ "t.fecha_fin, t.hora_entrada, t.hora_salida "
					+ "from trabajador tr, turno t "
					+ "where tr.dni = t.dni order by t.fecha_inicio desc");
	
			
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			
			while(rs.next()){
				Object[] fila = new Object[6];
				fila[0] = rs.getString("nombre");
				fila[1] = rs.getString("tipo");
				fila[2] = formato.format(rs.getDate("fecha_inicio"));
				fila[3] = formato.format(rs.getDate("fecha_fin"));
				fila[4] = rs.getString("hora_entrada");
				fila[5] = rs.getString("hora_salida");
				
				modeloTabla.addRow(fila);
			}
			
			tablaEstados.setModel(modeloTabla);
			modificarTablaTipos(tablaEstados);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void mostrarTurnos(){
		superCard.show(panelSuperCard, "panelNuevoTurno");
	}
	
	private void modificarTablaTipos(JTable tabla){
		tabla.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 12));
		tabla.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tabla.setRowHeight(25);
		
		tabla.getTableHeader().setReorderingAllowed(false);
		
		TableColumn columna;
		for(int i=0; i<6; i++){
			columna = tabla.getColumn(cabecera[i]);
			if(i==0){
				columna.setMinWidth(175);
				columna.setMaxWidth(175);
			}
			if(i== 2 || i==4 || i==5){
				columna.setMinWidth(103);
				columna.setMaxWidth(103);
			}
			if(i==3){
				columna.setMinWidth(90);
				columna.setMaxWidth(90);
			}
			columna.setResizable(false);
		}
		
		
	}
	
	private JPanel getPanelEstadoTrabajadores() {
		if (panelEstadoTrabajadores == null) {
			panelEstadoTrabajadores = new JPanel();
			panelEstadoTrabajadores.setLayout(null);
			panelEstadoTrabajadores.add(getPanelTabla());
			panelEstadoTrabajadores.add(getPanelFiltros());
			panelEstadoTrabajadores.add(getBtnNuevaConsulta());
		}
		return panelEstadoTrabajadores;
	}
	private JPanel getPanelTabla() {
		if (panelTabla == null) {
			panelTabla = new JPanel();
			panelTabla.setBounds(6, 263, 731, 301);
			panelTabla.setLayout(null);
			panelTabla.add(getSpTablaTurnos());
		}
		return panelTabla;
	}
	
	private JPanel getPanelFiltros() {
		if (panelFiltros == null) {
			panelFiltros = new JPanel();
			panelFiltros.setBorder(new TitledBorder(null, "Consultar estado de los trabajadores", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelFiltros.setBounds(6, 0, 731, 259);
			panelFiltros.setName("panelNuevoTipo");
			panelFiltros.setLayout(null);
			panelFiltros.add(getDcFechaInicio());
			panelFiltros.add(getLblFechaInicio());
			panelFiltros.add(getLblFechaFin());
			panelFiltros.add(getDcFechaFin());
			panelFiltros.add(getBtnFiltrar());
			panelFiltros.add(getPanelAreas());
			panelFiltros.add(getPanelFiltrarPorEmpleados());
		}
		return panelFiltros;
	}
	

	private JLabel getLblNombreEmpleado() {
		if (lblNombreEmpleado == null) {
			lblNombreEmpleado = new JLabel("Nombre empleado:");
			lblNombreEmpleado.setBounds(22, 16, 143, 26);
			lblNombreEmpleado.setFont(new Font("Tahoma", Font.PLAIN, 13));
		}
		return lblNombreEmpleado;
	}
	private JTextField getTfEmpleado() {
		if (tfEmpleado == null) {
			tfEmpleado = new JTextField();
			tfEmpleado.setBounds(20, 41, 165, 32);
			tfEmpleado.setFont(new Font("Tahoma", Font.PLAIN, 12));
			tfEmpleado.setColumns(10);
		}
		return tfEmpleado;
	}
	
	private JButton getBtnLupa() {
		if (btnLupa == null) {
			btnLupa = new JButton("");
			btnLupa.setBounds(197, 41, 32, 32);
			btnLupa.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(tfEmpleado.getText()!= null){
						try{
							String trabajador = trabajadores.encontrarTrabajador(tfEmpleado.getText());

							int pos = 0;
							for(int i=0; i<listaEmpleadosConsultarEstado.getModel().getSize(); i++) {
								String empleado = listaEmpleadosConsultarEstado.getModel().getElementAt(i);
								if(empleado.equals(trabajador)) {
									pos= i;
									break;
								}
							}
							
							listaEmpleadosConsultarEstado.addSelectionInterval(pos, pos);
						}
						catch(Exception exc){
							exc.printStackTrace();
						}
					}
				}
			});
			btnLupa.setIcon(new ImageIcon(CardTrabajadores.class.getResource("/img/lupa3.png")));
		}
		return btnLupa;
	}
	
	
	private JScrollPane getScrollPaneListaEmpleados() {
		if (scrollPaneListaEmpleados == null) {
			scrollPaneListaEmpleados = new JScrollPane();
			scrollPaneListaEmpleados.setBounds(20, 115, 218, 104);
			scrollPaneListaEmpleados.setViewportView(getListaEmpleados());
		}
		return scrollPaneListaEmpleados;
	}
	private JList<String> getListaEmpleados() {
		if (listaEmpleadosConsultarEstado == null) {
			listaEmpleadosConsultarEstado = new JList<String>();
			listaEmpleadosConsultarEstado.setFont(new Font("Tahoma", Font.PLAIN, 13));
			listaEmpleadosConsultarEstado.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					int indices[] = listaEmpleadosConsultarEstado.getSelectedIndices();
					empleadosSeleccionados = new ArrayList<String>();

					for(int i=0; i<indices.length; i++){
						empleadosSeleccionados.add(modeloLista.get(indices[i]));
					}
//					System.out.println("Numero de seleccionados: " +indices.length);
//					System.out.println("El empleado seleccionado es: " +  empleadosSeleccionados.get(0));
					if(indices.length==1)
						tfEmpleado.setText(empleadosSeleccionados.get(0));
					else
						tfEmpleado.setText(null);
					
				}
			});
			iniciarLista(listaEmpleadosConsultarEstado);
		}
		return listaEmpleadosConsultarEstado;
	}
	
	private void iniciarLista(JList<String> lista){
		try{
			
			modeloLista = new DefaultListModel<String>();
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("select distinct(nombre), dni from trabajador order by nombre asc");
			if(trabajadores== null)
				trabajadores = new Trabajadores();
			while(rs.next()){
				String nombre = rs.getString("NOMBRE");
				String dni = rs.getString("DNI");
				modeloLista.addElement(nombre);
				if(primero)
					trabajadores.a�adirTrabajador(nombre, dni);
			}
			primero = false;
			lista.setModel(modeloLista);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
//	private void filtrarListaEmpleados(){
//		
//		try{
//			if(!checkEmpleadoCorrecto()){
//				modeloLista.removeAllElements();
//				Statement st = con.getCon().createStatement();
//				ResultSet rs = st.executeQuery("select nombre from trabajador where area= '" +areaSeleccionada + "'");
//				while(rs.next()){
//					modeloLista.addElement(rs.getString("NOMBRE"));
//				}
//				listaEmpleados.repaint();
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//		}
//		
//	}
//	private boolean checkEmpleadoCorrecto(){
//		try{
//			Statement st = con.getCon().createStatement();
//			ResultSet rs = st.executeQuery("select count(nombre) from trabajador where area= '" + areaSeleccionada + "' and "
//					+ "nombre = '" + empleadosSeleccionados.get(0)+ "'");
//			rs.next();
//			int numero = rs.getInt("COUNT(NOMBRE)");
//			if(numero>0)
//				return true;
//			return false;
//		}
//		catch (Exception e){
//			e.printStackTrace();
//			return false;
//		}
//	}
	private JDateChooser getDcFechaInicio() {
		if (dcFechaInicio == null) {
			dcFechaInicio = new JDateChooser();
			dcFechaInicio.setFont(new Font("Tahoma", Font.PLAIN, 11));
			dcFechaInicio.getCalendarButton().setFont(new Font("Tahoma", Font.PLAIN, 12));
			dcFechaInicio.setBounds(284, 142, 155, 32);
			dcFechaInicio.setVisible(true);
			//dcFechaInicio.setMinSelectableDate(new Date());
		}
		return dcFechaInicio;
	}
	private JLabel getLblFechaInicio() {
		if (lblFechaInicio == null) {
			lblFechaInicio = new JLabel("Fecha inicio:");
			lblFechaInicio.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblFechaInicio.setBounds(284, 116, 93, 26);
		}
		return lblFechaInicio;
	}
	private JLabel getLblFechaFin() {
		if (lblFechaFin == null) {
			lblFechaFin = new JLabel("Fecha fin:");
			lblFechaFin.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblFechaFin.setBounds(471, 116, 104, 26);
		}
		return lblFechaFin;
	}
	private JDateChooser getDcFechaFin() {
		if (dcFechaFin == null) {
			dcFechaFin = new JDateChooser();
			dcFechaFin.getCalendarButton().setFont(new Font("Tahoma", Font.PLAIN, 12));
			dcFechaFin.setFont(new Font("Tahoma", Font.PLAIN, 11));
			dcFechaFin.setBounds(471, 142, 155, 32);
			dcFechaFin.setVisible(true);
			//dcFechaFin.setMinSelectableDate(new Date());
			
		}
		return dcFechaFin;
	}
	
	
	
	private boolean comprobarFechas(JDateChooser inicio, JDateChooser fin){
		try{
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			Date fecha_inicio = formato.parse(formato.format(inicio.getDate()));
			Date fecha_fin = formato.parse(formato.format(fin.getDate()));
			
			if(fecha_inicio.compareTo(fecha_fin) > 0){
				JOptionPane.showMessageDialog(null, "La fecha de fin debe de ser mayor que la fecha de inicio");
				return false;
			}
			return true;
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "Por favor, inserte un rango de fechas", "Faltan fechas", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}


	private JButton getBtnFiltrar() {
		if (btnFiltrar == null) {
			btnFiltrar = new JButton("Filtrar");
			btnFiltrar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(comprobarFechas(dcFechaInicio, dcFechaFin)){
						if(hayAlgunCheckActivado()){
							if(!noHaySeleccionesEnLaLista())
								listaEmpleadosConsultarEstado.clearSelection();
							filtrarPorChecks();
						}
						else if(!noHaySeleccionesEnLaLista()){
							filtrarPorLista();
						}
					}
					
				}
			});
			btnFiltrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnFiltrar.setBounds(616, 205, 93, 35);
		}
		return btnFiltrar;
	}
	
	private void filtrarPorLista(){
		try{
			modeloTabla = new MiModeloTabla(cabecera, 0);
			StringBuilder sb = new StringBuilder();
			PreparedStatement ps;
			
			for(int i=0;i<empleadosSeleccionados.size(); i++){
				sb = new StringBuilder();
				sb.append("select tr.nombre, t.tipo, t.fecha_inicio, t.fecha_fin, t.hora_entrada, t.hora_salida ");
				sb.append("from turno t natural join trabajador tr ");
				sb.append("where to_date(?, 'dd-MM-YYYY') <= t.fecha_fin and ");
				sb.append("t.fecha_inicio <= to_date(?,'dd-MM-YYYY') and tr.nombre = ?");
				ps = con.prepareStatement(sb.toString());
				ps.setDate(1, new java.sql.Date(dcFechaInicio.getDate().getTime()));
				ps.setDate(2, new java.sql.Date(dcFechaFin.getDate().getTime()));
				ps.setString(3, empleadosSeleccionados.get(i));
				ResultSet rs = ps.executeQuery();
				rellenarTabla(rs);	
			}
			tablaEstados.setModel(modeloTabla);
			modificarTablaTipos(tablaEstados);
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void filtrarPorChecks(){
		try{
			
			modeloTabla = new MiModeloTabla(cabecera, 0);
			if(chckbxLimpieza.isSelected()){
				filtroCheck("limpieza");
			}
			if(chckbxRecepcin.isSelected()){
				filtroCheck("recepci�n");
			}
			if(chckbxMantenimiento.isSelected()){
				filtroCheck("mantenimiento");
			}
			if(chckbxRestauracin.isSelected()){
				filtroCheck("restauraci�n");
			}
		}
		catch(Exception e){
			
		}
	}
	
	private boolean noHaySeleccionesEnLaLista(){
		return listaEmpleadosConsultarEstado.isSelectionEmpty();
	}
	
	private void rellenarTabla(ResultSet rs){
		try{
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			while(rs.next()){
				Object[] fila = new Object[6];
				fila[0] = rs.getString("nombre");
				fila[1] = rs.getString("tipo");
				fila[2] = formato.format(rs.getDate("fecha_inicio"));
				fila[3] = formato.format(rs.getDate("fecha_fin"));
				fila[4] = rs.getString("hora_entrada");
				fila[5] = rs.getString("hora_salida");
				
				modeloTabla.addRow(fila);
			}
			
		}
		catch(Exception e){
			
		}
	}
	
	private void filtroCheck(String area){
		try{
			StringBuilder sb = new StringBuilder();
			
			sb.append("select tr.nombre, t.tipo, t.fecha_inicio, t.fecha_fin, t.hora_entrada, t.hora_salida ");
			sb.append("from trabajador tr natural join turno t ");
			sb.append("where tr.area=? and to_date(?, 'dd-MM-YYYY') <= t.fecha_fin and ");
			sb.append("t.fecha_inicio <= to_date(?,'dd-MM-YYYY')");
			
			PreparedStatement ps = con.prepareStatement(sb.toString());
			ps.setString(1, area);
			ps.setDate(2, new java.sql.Date(dcFechaInicio.getDate().getTime()));	
			ps.setDate(3, new java.sql.Date(dcFechaFin.getDate().getTime()));
			ResultSet rs = ps.executeQuery();
			rellenarTabla(rs);	
			tablaEstados.setModel(modeloTabla);
			modificarTablaTipos(tablaEstados);
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private boolean hayAlgunCheckActivado(){
		return chckbxLimpieza.isSelected() || chckbxMantenimiento.isSelected() || chckbxRecepcin.isSelected() || chckbxRestauracin.isSelected();
	}
	private JCheckBox getChckbxRecepcin() {
		if (chckbxRecepcin == null) {
			chckbxRecepcin = new JCheckBox("Recepci\u00F3n");
			chckbxRecepcin.setBounds(9, 25, 95, 35);
			chckbxRecepcin.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return chckbxRecepcin;
	}
	private JPanel getPanelAreas() {
		if (panelAreas == null) {
			panelAreas = new JPanel();
			panelAreas.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Filtrar por \u00E1reas", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			panelAreas.setFont(new Font("Tahoma", Font.PLAIN, 12));
			panelAreas.setBounds(274, 24, 435, 69);
			panelAreas.setLayout(null);
			panelAreas.add(getChckbxRecepcin());
			panelAreas.add(getChckbxLimpieza());
			panelAreas.add(getChckbxMantenimiento());
			panelAreas.add(getChckbxRestauracin());
		}
		return panelAreas;
	}
	private JCheckBox getChckbxLimpieza() {
		if (chckbxLimpieza == null) {
			chckbxLimpieza = new JCheckBox("Limpieza");
			chckbxLimpieza.setBounds(110, 25, 83, 35);
			chckbxLimpieza.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return chckbxLimpieza;
	}
	private JCheckBox getChckbxMantenimiento() {
		if (chckbxMantenimiento == null) {
			chckbxMantenimiento = new JCheckBox("Mantenimiento");
			chckbxMantenimiento.setBounds(200, 25, 119, 35);
			chckbxMantenimiento.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return chckbxMantenimiento;
	}
	private JCheckBox getChckbxRestauracin() {
		if (chckbxRestauracin == null) {
			chckbxRestauracin = new JCheckBox("Restauraci\u00F3n");
			chckbxRestauracin.setBounds(323, 25, 107, 35);
			chckbxRestauracin.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return chckbxRestauracin;
	}
	private JPanel getPanelFiltrarPorEmpleados() {
		if (panelFiltrarPorEmpleados == null) {
			panelFiltrarPorEmpleados = new JPanel();
			panelFiltrarPorEmpleados.setBorder(new TitledBorder(null, "Filtrar por empleados", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelFiltrarPorEmpleados.setBounds(6, 24, 260, 229);
			panelFiltrarPorEmpleados.setLayout(null);
			panelFiltrarPorEmpleados.add(getLblNombreEmpleado());
			panelFiltrarPorEmpleados.add(getTfEmpleado());
			panelFiltrarPorEmpleados.add(getBtnLupa());
			panelFiltrarPorEmpleados.add(getScrollPaneListaEmpleados());
			panelFiltrarPorEmpleados.add(getTxtpnSeleccioneEllosEmpleados_1());
		}
		return panelFiltrarPorEmpleados;
	}
	private JPanel getPanelSuperCard() {
		if (panelSuperCard == null) {
			panelSuperCard = new JPanel();
			panelSuperCard.setBorder(new TitledBorder(null, "Trabajadores", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelSuperCard.setLayout(superCard);
			panelSuperCard.add(getPanelElegirOpcion(), "panelElegirOpcion");
			panelSuperCard.add(getPanelEstadoTrabajadores(), "panelConsultarEstado");
			panelSuperCard.add(getPanelNuevoTurno(), "panelNuevoTurno");
			panelSuperCard.add(getPanelHoras(), "panelHoras");
			panelSuperCard.add(getPanelHorarioDia7(), "panelHorarioDia7");
//			panelSuperCard.add(padre, "panelPadre");
		}
		return panelSuperCard;
	}
	private JPanel getPanelElegirOpcion() {
		if (panelElegirOpcion == null) {
			panelElegirOpcion = new JPanel();
			panelElegirOpcion.setLayout(null);
			panelElegirOpcion.add(getLblquOperacinDesea());
			panelElegirOpcion.add(getBtnConsultarElEstado());
			panelElegirOpcion.add(getBtnAsignarUnNuevo());
		}
		return panelElegirOpcion;
	}
	private JLabel getLblquOperacinDesea() {
		if (lblquOperacinDesea == null) {
			lblquOperacinDesea = new JLabel("\u00BFQu\u00E9 operaci\u00F3n desea realizar?");
			lblquOperacinDesea.setHorizontalAlignment(SwingConstants.CENTER);
			lblquOperacinDesea.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
			lblquOperacinDesea.setBounds(172, 59, 399, 50);
		}
		return lblquOperacinDesea;
	}
	private JButton getBtnConsultarElEstado() {
		if (btnConsultarElEstado == null) {
			btnConsultarElEstado = new JButton("Consultar el estado de los trabajadores");
			btnConsultarElEstado.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					superCard.show(panelSuperCard, "panelConsultarEstado");
				}
			});
			btnConsultarElEstado.setBounds(220, 163, 303, 44);
		}
		return btnConsultarElEstado;
	}
	
	public void mostrarConsultarEstado(){
		superCard.show(panelSuperCard, "panelConsultarEstado");
	}
	private JButton getBtnAsignarUnNuevo() {
		if (btnAsignarUnNuevo == null) {
			btnAsignarUnNuevo = new JButton("Asignar un nuevo turno de trabajo");
			btnAsignarUnNuevo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					superCard.show(panelSuperCard, "panelNuevoTurno");
				}
			});
			btnAsignarUnNuevo.setBounds(220, 219, 303, 44);
		}
		return btnAsignarUnNuevo;
	}
	private JPanel getPanelNuevoTurno() {
		if (panelNuevoTurno == null) {
			panelNuevoTurno = new JPanel();
			panelNuevoTurno.setLayout(null);
			panelNuevoTurno.add(getLblNombreDelEmpleado());
			panelNuevoTurno.add(getTfEmpleadoNuevoTurno());
			panelNuevoTurno.add(getBtnLupaNuevoTipo());
			panelNuevoTurno.add(getSpListaNuevoTurno());
			panelNuevoTurno.add(getTxtpnSeleccioneEllosEmpleados());
			panelNuevoTurno.add(getLblreaDeTrabajo());
			panelNuevoTurno.add(getCbAreaTrabajo());
			panelNuevoTurno.add(getLblFechaDeInicio());
			panelNuevoTurno.add(getDcFechaInicioNuevoTurno());
			panelNuevoTurno.add(getLblFechaFinal());
			panelNuevoTurno.add(getDcFechaFinNuevoTurno());
			panelNuevoTurno.add(getBtnContinuar());
			panelNuevoTurno.add(getSpTablaNuevoTurno());
			panelNuevoTurno.add(getBtnAtrasNuevoTurno1());
			panelNuevoTurno.add(getLblEstosSonLos());
		}
		return panelNuevoTurno;
	}
	private JLabel getLblNombreDelEmpleado() {
		if (lblNombreDelEmpleado == null) {
			lblNombreDelEmpleado = new JLabel("Nombre del empleado:");
			lblNombreDelEmpleado.setBounds(18, 6, 177, 33);
		}
		return lblNombreDelEmpleado;
	}
	
//	private void rellenarListaEmpleados(JList<String> lista){
//		try{
//			modeloLista = new DefaultListModel<String>();
//			Statement st = con.createStatement();
//			ResultSet rs = st.executeQuery("select nombre from trabajador where nombre like '" +tfEmpleado.getText() + "%'");
//			while(rs.next()){
//				modeloLista.addElement(rs.getString("NOMBRE"));
//			}
//			
//			lista.setModel(modeloLista);
//		}
//		catch(Exception e){
//			e.printStackTrace();
//		}	
//	}
	private JTextField getTfEmpleadoNuevoTurno() {
		if (tfEmpleadoNuevoTurno == null) {
			tfEmpleadoNuevoTurno = new JTextField();
			tfEmpleadoNuevoTurno.setBounds(18, 37, 177, 33);
			tfEmpleadoNuevoTurno.setColumns(10);
		}
		return tfEmpleadoNuevoTurno;
	}
	private JButton getBtnLupaNuevoTipo() {
		if (btnLupaNuevoTipo == null) {
			btnLupaNuevoTipo = new JButton("");
			btnLupaNuevoTipo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(tfEmpleadoNuevoTurno.getText()!= null){
						try{
							String nombreEmpleadoBarraBusqueda = tfEmpleadoNuevoTurno.getText().toLowerCase();
							String nombreReal = trabajadores.encontrarTrabajador(nombreEmpleadoBarraBusqueda);
							if(nombreReal != null) {
								listaEmpleadosNuevoTurno.setSelectedValue(nombreReal, true);
							}
							else {
								JOptionPane.showMessageDialog(null, "No se ha encontrado ninguna coincidencia", "Error", JOptionPane.ERROR_MESSAGE);
							}
//							rellenarListaEmpleados(listaEmpleadosNuevoTurno);
						}
						catch(Exception exc){
							exc.printStackTrace();
						}
					}
				}
			});
			btnLupaNuevoTipo.setIcon(new ImageIcon(CardTrabajadores.class.getResource("/img/lupa3.png")));
			btnLupaNuevoTipo.setBounds(207, 37, 34, 33);
		}
		return btnLupaNuevoTipo;
	}
	
	private JScrollPane getSpListaNuevoTurno() {
		if (spListaNuevoTurno == null) {
			spListaNuevoTurno = new JScrollPane();
			spListaNuevoTurno.setBounds(18, 122, 328, 167);
			spListaNuevoTurno.setViewportView(getListaEmpleadosNuevoTurno());
		}
		return spListaNuevoTurno;
	}
	private JList<String> getListaEmpleadosNuevoTurno() {
		if (listaEmpleadosNuevoTurno == null) {
			listaEmpleadosNuevoTurno = new JList<String>();
//			listaEmpleadosNuevoTurno.addFocusListener(new FocusAdapter() {
//				@Override
//				public void focusLost(FocusEvent arg0) {
//					int indices[] = listaEmpleadosNuevoTurno.getSelectedIndices();
//					empleadosSeleccionados = new ArrayList<String>();
//					areasIguales = true;
//					for(int i=0; i<indices.length; i++){
//						empleadosSeleccionados.add(modeloLista.get(indices[i]));
//					}
//					if(indices.length==1)
//						tfEmpleadoNuevoTurno.setText(empleadosSeleccionados.get(0));
//					else
//						tfEmpleadoNuevoTurno.setText(null);
//					if(areaSeleccionada== null)
//						rellenarComboAreaTrabajoPorTrabajador();
//				}
//			});
			iniciarLista(listaEmpleadosNuevoTurno);
		}
		return listaEmpleadosNuevoTurno;
	}
	
//	private void rellenarComboAreaTrabajoPorTrabajador(){
//		try{
//			Statement st = con.getCon().createStatement();
//			areasPorEmpleados = new ArrayList<String>();
//			String area = null;
//			for(int i=0; i<empleadosSeleccionados.size(); i++){
//				ResultSet rsArea = st.executeQuery("select area from trabajador where nombre= '" + empleadosSeleccionados.get(i) + "'");
//				rsArea.next();
//				area = rsArea.getString("AREA");
//				if(!areasPorEmpleados.contains(area) && i!=0){
//					areasIguales = false;
//				}
//				areasPorEmpleados.add(area);
//				
//			}
//			if(areasIguales){
//				cbAreaTrabajoNuevoTurno.setSelectedItem(area);
//				areaSeleccionada = area;
//				cbAreaTrabajoNuevoTurno.repaint();
//			}
//			else{
//				cbAreaTrabajoNuevoTurno.setSelectedIndex(-1);
//				areaSeleccionada = null;
//				JOptionPane.showMessageDialog(null, "Los trabajadores tienen que pertenecer a la misma area de trabajo", "Error", JOptionPane.ERROR_MESSAGE);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//		}
//	}
	

	private JTextPane getTxtpnSeleccioneEllosEmpleados() {
		if (txtpnSeleccioneEllosEmpleados == null) {
			txtpnSeleccioneEllosEmpleados = new JTextPane();
			txtpnSeleccioneEllosEmpleados.setEditable(false);
			txtpnSeleccioneEllosEmpleados.setOpaque(false);
			txtpnSeleccioneEllosEmpleados.setText("Seleccione el/los empleado/s de la lista:");
			txtpnSeleccioneEllosEmpleados.setBounds(18, 82, 215, 41);
		}
		return txtpnSeleccioneEllosEmpleados;
	}
	private JLabel getLblreaDeTrabajo() {
		if (lblreaDeTrabajo == null) {
			lblreaDeTrabajo = new JLabel("\u00C1rea de trabajo:");
			lblreaDeTrabajo.setBounds(265, 10, 117, 25);
		}
		return lblreaDeTrabajo;
	}
	private JComboBox<String> getCbAreaTrabajo() {
		if (cbAreaTrabajoNuevoTurno == null) {
			cbAreaTrabajoNuevoTurno = new JComboBox<String>();
			cbAreaTrabajoNuevoTurno.setBounds(258, 40, 144, 29);
			iniciarComboAreaTrabajo();
			cbAreaTrabajoNuevoTurno.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(cbAreaTrabajoNuevoTurno.getSelectedItem()!= null) {
						areaSeleccionadaNuevoTurno = (String) cbAreaTrabajoNuevoTurno.getSelectedItem();
						marcarEmpleadosEnListaEmpleadosNuevoTurnoPorAreaSeleccionada();
						
					}
					
				}
			});
		}
		return cbAreaTrabajoNuevoTurno;
	}
	
	private void marcarEmpleadosEnListaEmpleadosNuevoTurnoPorAreaSeleccionada() {
		try {
			PreparedStatement ps = con.prepareStatement("select distinct(nombre) from trabajador "
					+ "where area = ? order by nombre asc");
			ps.setString(1, areaSeleccionadaNuevoTurno);
			ResultSet rs = ps.executeQuery();
			
			ArrayList<Integer> indices = new ArrayList<Integer>();
			int numeroTotalDeObjetosEnLaLista = listaEmpleadosConsultarEstado.getModel().getSize();
			while(rs.next()) {
				String nombre = rs.getString("NOMBRE");
				for(int i=0; i<numeroTotalDeObjetosEnLaLista; i++) {
					String empleadoEnLista = (String) listaEmpleadosNuevoTurno.getModel().getElementAt(i);
					if(empleadoEnLista.equals(nombre)) {
						indices.add(i);
					}
				}
			}
			

			int[] dondeEstanEnLaLista =  new int[indices.size()];
			for(int i= 0; i< dondeEstanEnLaLista.length; i++) {
				dondeEstanEnLaLista[i] = indices.get(i);
			}
			
			listaEmpleadosNuevoTurno.setSelectedIndices(dondeEstanEnLaLista);
			
			rs.close();
			ps.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private void iniciarComboAreaTrabajo(){
		try{
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("select distinct(area) from trabajador");
			while(rs.next()){
				cbAreaTrabajoNuevoTurno.addItem(rs.getString("AREA"));
			}
			cbAreaTrabajoNuevoTurno.setSelectedItem(null);
			cbAreaTrabajoNuevoTurno.repaint();
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
//	private void filtrarListaEmpleados(){
//		
//		try{
//			if(!checkEmpleadoCorrecto()){
//				modeloLista.removeAllElements();
//				Statement st = con.getCon().createStatement();
//				ResultSet rs = st.executeQuery("select nombre from trabajador where area= '" +areaSeleccionada + "'");
//				while(rs.next()){
//					modeloLista.addElement(rs.getString("NOMBRE"));
//				}
//				listaEmpleadosNuevoTurno.repaint();
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//		}
//		
//	}
	
//	private boolean checkEmpleadoCorrecto(){
//		try{
//			Statement st = con.getCon().createStatement();
//			ResultSet rs = st.executeQuery("select count(nombre) from trabajador where area= '" + areaSeleccionada + "' and "
//					+ "nombre = '" + empleadosSeleccionados.get(0)+ "'");
//			rs.next();
//			int numero = rs.getInt("COUNT(NOMBRE)");
//			if(numero>0)
//				return true;
//			return false;
//		}
//		catch (Exception e){
//			e.printStackTrace();
//			return false;
//		}
//	}
	private JLabel getLblFechaDeInicio() {
		if (lblFechaDeInicio == null) {
			lblFechaDeInicio = new JLabel("Fecha de inicio:");
			lblFechaDeInicio.setBounds(428, 10, 125, 25);
		}
		return lblFechaDeInicio;
	}
	private JDateChooser getDcFechaInicioNuevoTurno() {
		if (dcFechaInicioNuevoTurno == null) {
			dcFechaInicioNuevoTurno = new JDateChooser();
			dcFechaInicioNuevoTurno.setBounds(428, 40, 132, 25);
			dcFechaInicioNuevoTurno.setMinSelectableDate(new Date());
		}
		return dcFechaInicioNuevoTurno;
	}
	private JLabel getLblFechaFinal() {
		if (lblFechaFinal == null) {
			lblFechaFinal = new JLabel("Fecha final:");
			lblFechaFinal.setBounds(590, 10, 125, 25);
		}
		return lblFechaFinal;
	}
	private JDateChooser getDcFechaFinNuevoTurno() {
		if (dcFechaFinNuevoTurno == null) {
			dcFechaFinNuevoTurno = new JDateChooser();
			dcFechaFinNuevoTurno.setBounds(590, 40, 132, 25);
			dcFechaFinNuevoTurno.setMinSelectableDate(new Date());
		}
		return dcFechaFinNuevoTurno;
	}
	private JPanel getPanelDias() {
		if (panelDias == null) {
			panelDias = new JPanel();
			panelDias.setBorder(new TitledBorder(null, "Establecer horario", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelDias.setBounds(6, 6, 731, 590);
			panelDias.setLayout(null);
			panelDias.add(getPanelDia1());
			panelDias.add(getPanelDia2());
			panelDias.add(getPanelDia3());
			panelDias.add(getPanelJornadaPartidaDia1());
			panelDias.add(getPanelJornadaPartidaDia2());
			panelDias.add(getPanelJornadaPartidaDia3());
			panelDias.add(getPanelDia4());
			panelDias.add(getPanelJornadaPartidaDia4());
			panelDias.add(getPanelDia5());
			panelDias.add(getPanelJPD5());
			panelDias.add(getPanelDia6());
			panelDias.add(getPanelJornadaPartidaDia6());
			panelDias.add(getBtnAnterior());
			panelDias.add(getBtnDia7());
			panelDias.add(getBtndeseaAsignarUn());
			panelDias.add(getTglbtnNoHorario());
			panelDias.add(getTglbtnNoHorario2());
			panelDias.add(getTglbtnHoHorario3());
			panelDias.add(getTglbtnNoHorario4());
			panelDias.add(getTglbtnNoHorario5());
			panelDias.add(getTglbtnNoHorario6());
		}
		return panelDias;
	}
	private JButton getBtnContinuar() {
		if (btnContinuar == null) {
			btnContinuar = new JButton("Continuar");
			btnContinuar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					List<String> lista = listaEmpleadosNuevoTurno.getSelectedValuesList();
					if(lista.size()>0 ) {
						if(comprobarFechas(dcFechaInicioNuevoTurno,dcFechaFinNuevoTurno)&&
								comprobarEsUnaSemana(dcFechaInicioNuevoTurno, dcFechaFinNuevoTurno)) {
							if(comprobarSonFechasValidasParaTodosLosEmpleados(lista)){
								lblEstosSonLos.setVisible(false);
								spTablaNuevoTurno.setVisible(false);
								cambiarElTituloDeLosPanelesDias();
								superCard.show(panelSuperCard, "panelHoras");
							}
							else {
								JOptionPane.showMessageDialog(null, "No se puede asignar turnos de trabajo con estas fechas a estas personas"
										+ "\nFechas conflictivas", "Error al asignar turnos de trabajo", JOptionPane.ERROR_MESSAGE);
								rellenarTablaAdvertenciaNuevoTurno(lista);
								lblEstosSonLos.setVisible(true);
								spTablaNuevoTurno.setVisible(true);
								
							}
						}
					}
					else
						JOptionPane.showMessageDialog(null, "No hay ning�n empleado seleccionado", "Error", JOptionPane.ERROR_MESSAGE);
					
				}
			});
			btnContinuar.setBounds(605, 256, 117, 33);
		}
		return btnContinuar;
	}
	
	
	private boolean comprobarEsUnaSemana(JDateChooser fechaInicio, JDateChooser fechaFin) {
		long diferenciaEn_ms = fechaFin.getDate().getTime() - fechaInicio.getDate().getTime();
		long dias = diferenciaEn_ms / (1000 * 60 * 60 * 24);
		
		if(dias == 6)
			return true;
		
		else {
			JOptionPane.showMessageDialog(null, "El rango de fechas debe ser una semana", "Seleccione una semana", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}
	
	
	private void cambiarElTituloDeLosPanelesDias(){
		try {
			listaDeNombreDeDias = new ArrayList<String>();
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-YYYY");
			
			String date = sdf.format(dcFechaInicioNuevoTurno.getDate());
			panelDia1.setBorder(new TitledBorder(null, date, TitledBorder.LEADING, TitledBorder.TOP, null, null));
			
			Calendar calendar = GregorianCalendar.getInstance();
			calendar.setTime(dcFechaInicioNuevoTurno.getDate());
			String dia = sdf.format(calendar.getTime());
			panelDia1.setBorder(new TitledBorder(null, dia, TitledBorder.LEADING, TitledBorder.TOP, null, null));
			
			for(int i=1; i<7; i++) {
				calendar.add(Calendar.DATE, 1);
				dia = sdf.format(calendar.getTime());
				if(i==1) {
					panelDia2.setBorder(new TitledBorder(null, dia, TitledBorder.LEADING, TitledBorder.TOP, null, null));
				}
				else if(i==2) {
					panelDia3.setBorder(new TitledBorder(null, dia, TitledBorder.LEADING, TitledBorder.TOP, null, null));
				}
				else if(i==3) {
					panelDia4.setBorder(new TitledBorder(null, dia, TitledBorder.LEADING, TitledBorder.TOP, null, null));
				}
				else if(i==4) {
					panelDia5.setBorder(new TitledBorder(null, dia, TitledBorder.LEADING, TitledBorder.TOP, null, null));
				}
				else if(i==5) {
					panelDia6.setBorder(new TitledBorder(null, dia, TitledBorder.LEADING, TitledBorder.TOP, null, null));
				}
				else if(i==6) {
					panelDia7.setBorder(new TitledBorder(null, dia, TitledBorder.LEADING, TitledBorder.TOP, null, null));
				}
				listaDeNombreDeDias.add(dia);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private void rellenarTablaAdvertenciaNuevoTurno(List<String> lista) {
		try {
			MiModeloTabla modeloTablaAdvertencia = new MiModeloTabla(cabecera, 0);
			
			String consulta = "select tr.nombre, t.tipo, t.fecha_inicio, t.fecha_fin, t.hora_entrada, t.hora_salida " + 
					"from trabajador tr natural join turno t " + 
					"where to_date(?, 'dd-MM-YYYY') <= t.fecha_fin and " + 
					"t.fecha_inicio <= to_date(?, 'dd,MM-YYYY') and tr.nombre= ?";
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
			PreparedStatement  ps = con.prepareStatement(consulta);
			ResultSet rs;
			for(int i=0; i< lista.size(); i++) {
				ps.setDate(1, new java.sql.Date(dcFechaInicioNuevoTurno.getDate().getTime()));
				ps.setDate(2, new java.sql.Date(dcFechaFinNuevoTurno.getDate().getTime()));
				ps.setString(3, lista.get(i));
				rs = ps.executeQuery();
				
				while(rs.next()) {
					Object[] fila = new Object[6];
					fila[0] = rs.getString("nombre");
					fila[1] = rs.getString("tipo");
					fila[2] = sdf.format(rs.getDate("fecha_inicio"));
					fila[3] = sdf.format(rs.getDate("fecha_fin"));
					fila[4] = rs.getString("hora_entrada");
					fila[5] = rs.getString("hora_salida");
					modeloTablaAdvertencia.addRow(fila);
				}
			}
			
			tablaNuevoTurno.setModel(modeloTablaAdvertencia);
			modificarTablaTipos(tablaNuevoTurno);
			tablaNuevoTurno.repaint();
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private boolean comprobarSonFechasValidasParaTodosLosEmpleados(List<String> lista) {
		try {
			String consulta = "select count(nombre) " + 
					"from trabajador tr natural join turno t " + 
					"where to_date(?, 'dd-MM-YYYY') <= t.fecha_fin and " + 
					"t.fecha_inicio <= to_date(?, 'dd,MM-YYYY') and tr.nombre= ?";
			
			PreparedStatement  ps = con.prepareStatement(consulta);
			ResultSet rs;
			for(int i=0; i< lista.size(); i++) {
				ps.setDate(1, new java.sql.Date(dcFechaInicioNuevoTurno.getDate().getTime()));
				ps.setDate(2, new java.sql.Date(dcFechaFinNuevoTurno.getDate().getTime()));
				ps.setString(3, lista.get(i));
				rs = ps.executeQuery();
				
				while(rs.next()) {
					int coincide = rs.getInt("COUNT(NOMBRE)");
					if(coincide >0)
						return false;
				}
			}
			
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private JSpinner getSpinnerEntradaDia1() {
		if (spinnerEntradaDia1 == null) {
			spinnerEntradaDia1 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerEntradaDia1);
			spinnerEntradaDia1.setBounds(123, 61, 91, 25);
		}
		return spinnerEntradaDia1;
	}
	private JLabel getLblHoraDeEntradaDia1() {
		if (lblHoraDeEntradaDia1 == null) {
			lblHoraDeEntradaDia1 = new JLabel("Hora de entrada:");
			lblHoraDeEntradaDia1.setBounds(10, 62, 112, 25);
		}
		return lblHoraDeEntradaDia1;
	}
	private JLabel getLblHoraDeSalidaDia1() {
		if (lblHoraDeSalidaDia1 == null) {
			lblHoraDeSalidaDia1 = new JLabel("Hora de salida:");
			lblHoraDeSalidaDia1.setBounds(10, 99, 112, 25);
		}
		return lblHoraDeSalidaDia1;
	}
	private JSpinner getSpinnerSalidaDia1() {
		if (spinnerSalidaDia1 == null) {
			spinnerSalidaDia1 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerSalidaDia1);
			spinnerSalidaDia1.setBounds(123, 98, 91, 25);
		}
		return spinnerSalidaDia1;
	}
	private JLabel getLblHoraEntradaDia2() {
		if (lblHoraEntradaDia2 == null) {
			lblHoraEntradaDia2 = new JLabel("Hora de entrada:");
			lblHoraEntradaDia2.setBounds(10, 61, 112, 25);
		}
		return lblHoraEntradaDia2;
	}
	private JSpinner getSpinnerEntradaDia2() {
		if (spinnerEntradaDia2 == null) {
			spinnerEntradaDia2 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerEntradaDia2);
			spinnerEntradaDia2.setBounds(123, 60, 91, 25);
		}
		return spinnerEntradaDia2;
	}
	private JLabel getLbHoraSalidaDia2() {
		if (lbHoraSalidaDia2 == null) {
			lbHoraSalidaDia2 = new JLabel("Hora de salida:");
			lbHoraSalidaDia2.setBounds(10, 99, 112, 25);
		}
		return lbHoraSalidaDia2;
	}
	private JSpinner getSpinnerSalidaDia2() {
		if (spinnerSalidaDia2 == null) {
			spinnerSalidaDia2 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerSalidaDia2);
			spinnerSalidaDia2.setBounds(123, 98, 91, 25);
		}
		return spinnerSalidaDia2;
	}
	private JLabel getLblEntradaDia3() {
		if (lblEntradaDia3 == null) {
			lblEntradaDia3 = new JLabel("Hora de entrada:");
			lblEntradaDia3.setBounds(10, 61, 112, 25);
		}
		return lblEntradaDia3;
	}
	private JSpinner getSpinnerEntradaDia3() {
		if (spinnerEntradaDia3 == null) {
			spinnerEntradaDia3 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerEntradaDia3);
			spinnerEntradaDia3.setBounds(119, 60, 91, 25);
		}
		return spinnerEntradaDia3;
	}
	
	private void asignarModeloSpinnersHoras(JSpinner spinner) {
		SpinnerDateModel dateModel = new SpinnerDateModel();
		spinner.setModel(dateModel);
		JSpinner.DateEditor dateEditor = new JSpinner.DateEditor(spinner, "HH:mm");
		spinner.setEditor(dateEditor);
		
	}
	private JLabel getLblHoraSalidaDia3() {
		if (lblHoraSalidaDia3 == null) {
			lblHoraSalidaDia3 = new JLabel("Hora de salida:");
			lblHoraSalidaDia3.setBounds(10, 99, 112, 25);
		}
		return lblHoraSalidaDia3;
	}
	private JSpinner getSpinnerSalidaDia3() {
		if (spinnerSalidaDia3 == null) {
			spinnerSalidaDia3 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerSalidaDia3);
			spinnerSalidaDia3.setBounds(119, 99, 91, 25);
		}
		return spinnerSalidaDia3;
	}
	private JLabel getLblEntradaDia4() {
		if (lblEntradaDia4 == null) {
			lblEntradaDia4 = new JLabel("Hora de entrada:");
			lblEntradaDia4.setBounds(10, 62, 112, 25);
		}
		return lblEntradaDia4;
	}
	private JSpinner getSpinnerEntradaDia4() {
		if (spinnerEntradaDia4 == null) {
			spinnerEntradaDia4 = new JSpinner();
			spinnerEntradaDia4.setBounds(123, 61, 91, 25);
			asignarModeloSpinnersHoras(spinnerEntradaDia4);
		}
		return spinnerEntradaDia4;
	}
	private JLabel getLblSalidaDia4() {
		if (lblSalidaDia4 == null) {
			lblSalidaDia4 = new JLabel("Hora de salida:");
			lblSalidaDia4.setBounds(10, 99, 112, 25);
		}
		return lblSalidaDia4;
	}
	private JSpinner getSpinnerSalidaDia4() {
		if (spinnerSalidaDia4 == null) {
			spinnerSalidaDia4 = new JSpinner();
			spinnerSalidaDia4.setBounds(123, 99, 91, 25);
			asignarModeloSpinnersHoras(spinnerSalidaDia4);
		}
		return spinnerSalidaDia4;
	}
	private JLabel getLblEntradaDia5() {
		if (lblEntradaDia5 == null) {
			lblEntradaDia5 = new JLabel("Hora de entrada:");
			lblEntradaDia5.setBounds(10, 62, 112, 25);
		}
		return lblEntradaDia5;
	}
	private JLabel getLblEntradaDia6() {
		if (lblEntradaDia6 == null) {
			lblEntradaDia6 = new JLabel("Hora de entrada:");
			lblEntradaDia6.setBounds(10, 61, 112, 25);
		}
		return lblEntradaDia6;
	}
	private JSpinner getSpinnerEntradaDia5() {
		if (spinnerEntradaDia5 == null) {
			spinnerEntradaDia5 = new JSpinner();
			spinnerEntradaDia5.setBounds(123, 61, 91, 25);
			asignarModeloSpinnersHoras(spinnerEntradaDia5);
		}
		return spinnerEntradaDia5;
	}
	private JSpinner getSpinnerSalidaDia5() {
		if (spinnerSalidaDia5 == null) {
			spinnerSalidaDia5 = new JSpinner();
			spinnerSalidaDia5.setBounds(123, 98, 91, 25);
			asignarModeloSpinnersHoras(spinnerSalidaDia5);
		}
		return spinnerSalidaDia5;
	}
	private JSpinner getSpinnerEntradaDia6() {
		if (spinnerEntradaDia6 == null) {
			spinnerEntradaDia6 = new JSpinner();
			spinnerEntradaDia6.setBounds(123, 61, 91, 25);
			asignarModeloSpinnersHoras(spinnerEntradaDia6);
		}
		return spinnerEntradaDia6;
	}
	private JSpinner getSpinnerSalidaDia6() {
		if (spinnerSalidaDia6 == null) {
			spinnerSalidaDia6 = new JSpinner();
			spinnerSalidaDia6.setBounds(123, 98, 91, 25);
			asignarModeloSpinnersHoras(spinnerSalidaDia6);
		}
		return spinnerSalidaDia6;
	}
	private JLabel getLabelSalidaDia5() {
		if (lblSalidaDia5 == null) {
			lblSalidaDia5 = new JLabel("Hora de salida:");
			lblSalidaDia5.setBounds(10, 99, 112, 25);
		}
		return lblSalidaDia5;
	}
	private JLabel getLabelSalidaDia6() {
		if (lblSalidaDia6 == null) {
			lblSalidaDia6 = new JLabel("Hora de salida:");
			lblSalidaDia6.setBounds(10, 98, 112, 25);
		}
		return lblSalidaDia6;
	}
	private JPanel getPanelHoras() {
		if (panelHoras == null) {
			panelHoras = new JPanel();
			panelHoras.setLayout(null);
			panelHoras.add(getPanelDias());
		}
		return panelHoras;
	}
	private JCheckBox getChckbxJornadaPartidaDia1() {
		if (boxJPD1 == null) {
			boxJPD1 = new JCheckBox("Jornada partida");
			boxJPD1.setName("box1");
			boxJPD1.addChangeListener(new AccionCheck());
			boxJPD1.setBounds(10, 26, 128, 23);
		}
		return boxJPD1;
	}
	
	
	private JPanel getPanelDia1() {
		if (panelDia1 == null) {
			panelDia1 = new JPanel();
			panelDia1.setBorder(new TitledBorder(null, "Dia 1", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelDia1.add(getSpinnerEntradaDia1());
			panelDia1.add(getLblHoraDeEntradaDia1());
			panelDia1.add(getLblHoraDeSalidaDia1());
			panelDia1.add(getSpinnerSalidaDia1());
			panelDia1.setBounds(16, 37, 224, 147);
			panelDia1.setLayout(null);
			panelDia1.add(getChckbxJornadaPartidaDia1());
		}
		return panelDia1;
	}
	private JPanel getPanelDia2() {
		if (panelDia2 == null) {
			panelDia2 = new JPanel();
			panelDia2.setBorder(new TitledBorder(null, "Dia 2", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelDia2.add(getLblHoraEntradaDia2());
			panelDia2.add(getSpinnerEntradaDia2());
			panelDia2.add(getLbHoraSalidaDia2());
			panelDia2.add(getSpinnerSalidaDia2());
			panelDia2.setBounds(252, 37, 224, 147);
			panelDia2.setLayout(null);
			panelDia2.add(getChckbxJornadaPartidaDia2());
		}
		return panelDia2;
	}
	private JCheckBox getChckbxJornadaPartidaDia2() {
		if (boxJPD2 == null) {
			boxJPD2 = new JCheckBox("Jornada partida");
			boxJPD2.setName("box2");
			boxJPD2.addChangeListener(new AccionCheck());
			boxJPD2.setBounds(10, 26, 128, 23);
		}
		return boxJPD2;
	}
	private JPanel getPanelDia3() {
		if (panelDia3 == null) {
			panelDia3 = new JPanel();
			panelDia3.setBorder(new TitledBorder(null, "Dia 3", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelDia3.setBounds(488, 37, 224, 147);
			panelDia3.setLayout(null);
			panelDia3.add(getLblEntradaDia3());
			panelDia3.add(getSpinnerEntradaDia3());
			panelDia3.add(getLblHoraSalidaDia3());
			panelDia3.add(getSpinnerSalidaDia3());
			panelDia3.add(getChckbxJornadaPartidaDia3());
		}
		return panelDia3;
	}
	private JCheckBox getChckbxJornadaPartidaDia3() {
		if (boxJPD3 == null) {
			boxJPD3 = new JCheckBox("Jornada partida");
			boxJPD3.setName("box3");
			boxJPD3.addChangeListener(new AccionCheck());
			boxJPD3.setBounds(10, 26, 128, 23);
		}
		return boxJPD3;
	}
	private JTextPane getTxtpnSeleccioneEllosEmpleados_1() {
		if (txtpnSeleccioneEllosEmpleados_1 == null) {
			txtpnSeleccioneEllosEmpleados_1 = new JTextPane();
			txtpnSeleccioneEllosEmpleados_1.setFont(new Font("Lucida Grande", Font.PLAIN, 12));
			txtpnSeleccioneEllosEmpleados_1.setEditable(false);
			txtpnSeleccioneEllosEmpleados_1.setOpaque(false);
			txtpnSeleccioneEllosEmpleados_1.setText("Seleccione el/los empleado/s de la siguiente lista:");
			txtpnSeleccioneEllosEmpleados_1.setBounds(20, 81, 234, 32);
		}
		return txtpnSeleccioneEllosEmpleados_1;
	}
	private JScrollPane getSpTablaNuevoTurno() {
		if (spTablaNuevoTurno == null) {
			spTablaNuevoTurno = new JScrollPane();
			spTablaNuevoTurno.setBorder(new LineBorder(new Color(255, 0, 0), 3));
			spTablaNuevoTurno.setVisible(false);
			spTablaNuevoTurno.setBounds(18, 339, 708, 244);
			spTablaNuevoTurno.setViewportView(getTablaNuevoTurno());
		}
		return spTablaNuevoTurno;
	}
	private JTable getTablaNuevoTurno() {
		if (tablaNuevoTurno == null) {
			tablaNuevoTurno = new JTable();
		}
		return tablaNuevoTurno;
	}
	private JButton getBtnAtrasNuevoTurno1() {
		if (btnAtrasNuevoTurno1 == null) {
			btnAtrasNuevoTurno1 = new JButton("Atr\u00E1s");
			btnAtrasNuevoTurno1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					superCard.show(panelSuperCard, "panelElegirOpcion");
				}
			});
			btnAtrasNuevoTurno1.setBounds(497, 256, 97, 33);
		}
		return btnAtrasNuevoTurno1;
	}
	private JLabel getLblEstosSonLos() {
		if (lblEstosSonLos == null) {
			lblEstosSonLos = new JLabel("Estos son los turnos de trabajo de los empleados seleccionados:");
			lblEstosSonLos.setVisible(false);
			lblEstosSonLos.setForeground(Color.RED);
			lblEstosSonLos.setBounds(18, 307, 441, 33);
		}
		return lblEstosSonLos;
	}
	private JPanel getPanelJornadaPartidaDia1() {
		if (panelJornadaPartidaDia1 == null) {
			panelJornadaPartidaDia1 = new JPanel();
			panelJornadaPartidaDia1.setName("panelJornadaPartidaDia1");
			panelJornadaPartidaDia1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panelJornadaPartidaDia1.setBounds(18, 180, 220, 94);
			panelJornadaPartidaDia1.setLayout(null);
			panelJornadaPartidaDia1.add(getLblEntrada2Dia1());
			panelJornadaPartidaDia1.add(getSpinnerEntrada2Dia1());
			panelJornadaPartidaDia1.add(getLblSalida2Dia1());
			panelJornadaPartidaDia1.add(getSpinnerSalida2Dia1());
			panelJornadaPartidaDia1.setVisible(false);
		}
		return panelJornadaPartidaDia1;
	}
	private JLabel getLblEntrada2Dia1() {
		if (lblEntradaJPD1 == null) {
			lblEntradaJPD1 = new JLabel("Hora de entrada:");
			lblEntradaJPD1.setBounds(10, 14, 112, 25);
		}
		return lblEntradaJPD1;
	}
	private JSpinner getSpinnerEntrada2Dia1() {
		if (spinnerEntradaJPD1 == null) {
			spinnerEntradaJPD1 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerEntradaJPD1);
			spinnerEntradaJPD1.setBounds(123, 14, 91, 25);
		}
		return spinnerEntradaJPD1;
	}
	private JLabel getLblSalida2Dia1() {
		if (lblSalidaJPD1 == null) {
			lblSalidaJPD1 = new JLabel("Hora de salida:");
			lblSalidaJPD1.setBounds(10, 53, 112, 25);
		}
		return lblSalidaJPD1;
	}
	private JSpinner getSpinnerSalida2Dia1() {
		if (spinnerSalidaJPD1 == null) {
			spinnerSalidaJPD1 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerSalidaJPD1);
			spinnerSalidaJPD1.setBounds(123, 53, 91, 25);
		}
		return spinnerSalidaJPD1;
	}
	private JPanel getPanelJornadaPartidaDia2() {
		if (panelJornadaPartidaDia2 == null) {
			panelJornadaPartidaDia2 = new JPanel();
			panelJornadaPartidaDia2.setName("panelJornadaPartidaDia2");
			panelJornadaPartidaDia2.setLayout(null);
			panelJornadaPartidaDia2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panelJornadaPartidaDia2.setBounds(254, 180, 220, 94);
			panelJornadaPartidaDia2.add(getLblEntrada2Dia2());
			panelJornadaPartidaDia2.add(getSpinnerEntrada2Dia2());
			panelJornadaPartidaDia2.add(getLblSalida2Dia2());
			panelJornadaPartidaDia2.add(getSpinnerSalida2Dia2());
			panelJornadaPartidaDia2.setVisible(false);
		}
		return panelJornadaPartidaDia2;
	}
	private JLabel getLblEntrada2Dia2() {
		if (lblEntradaJPD2 == null) {
			lblEntradaJPD2 = new JLabel("Hora de entrada:");
			lblEntradaJPD2.setBounds(10, 14, 112, 25);
		}
		return lblEntradaJPD2;
	}
	private JSpinner getSpinnerEntrada2Dia2() {
		if (spinnerEntradaJPD2 == null) {
			spinnerEntradaJPD2 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerEntradaJPD2);
			spinnerEntradaJPD2.setBounds(123, 14, 91, 25);
		}
		return spinnerEntradaJPD2;
	}
	private JLabel getLblSalida2Dia2() {
		if (lblSalidaJPD2 == null) {
			lblSalidaJPD2 = new JLabel("Hora de salida:");
			lblSalidaJPD2.setBounds(10, 53, 112, 25);
		}
		return lblSalidaJPD2;
	}
	private JSpinner getSpinnerSalida2Dia2() {
		if (spinnerSalidaJPD2 == null) {
			spinnerSalidaJPD2 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerSalidaJPD2);
			spinnerSalidaJPD2.setBounds(123, 53, 91, 25);
		}
		return spinnerSalidaJPD2;
	}
	private JPanel getPanelJornadaPartidaDia3() {
		if (panelJornadaPartidaDia3 == null) {
			panelJornadaPartidaDia3 = new JPanel();
			panelJornadaPartidaDia3.setName("panelJornadaPartidaDia3");
			panelJornadaPartidaDia3.setLayout(null);
			panelJornadaPartidaDia3.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panelJornadaPartidaDia3.setBounds(490, 180, 220, 94);
			panelJornadaPartidaDia3.add(getLblEntrada2Dia3());
			panelJornadaPartidaDia3.add(getSpinnerEntrada2Dia3());
			panelJornadaPartidaDia3.add(getLblSalida2Dia3());
			panelJornadaPartidaDia3.add(getSpinnerSalida2Dia3());
			panelJornadaPartidaDia3.setVisible(false);
		}
		return panelJornadaPartidaDia3;
	}
	private JLabel getLblEntrada2Dia3() {
		if (lblEntradaJPD3 == null) {
			lblEntradaJPD3 = new JLabel("Hora de entrada:");
			lblEntradaJPD3.setBounds(10, 14, 112, 25);
		}
		return lblEntradaJPD3;
	}
	private JSpinner getSpinnerEntrada2Dia3() {
		if (spinnerEntradaJPD3 == null) {
			spinnerEntradaJPD3 = new JSpinner();
			spinnerEntradaJPD3.setBounds(123, 14, 91, 25);
			asignarModeloSpinnersHoras(spinnerEntradaJPD3);
		}
		return spinnerEntradaJPD3;
	}
	private JLabel getLblSalida2Dia3() {
		if (lblSalidaJPD3 == null) {
			lblSalidaJPD3 = new JLabel("Hora de salida:");
			lblSalidaJPD3.setBounds(10, 53, 112, 25);
		}
		return lblSalidaJPD3;
	}
	private JSpinner getSpinnerSalida2Dia3() {
		if (spinnerSalidaJPD3 == null) {
			spinnerSalidaJPD3 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerSalidaJPD3);
			spinnerSalidaJPD3.setBounds(123, 53, 91, 25);
			asignarModeloSpinnersHoras(spinnerEntradaJPD3);
		}
		return spinnerSalidaJPD3;
	}
	private JPanel getPanelDia4() {
		if (panelDia4 == null) {
			panelDia4 = new JPanel();
			panelDia4.setBorder(new TitledBorder(null, "Dia 4", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelDia4.setBounds(16, 306, 224, 147);
			panelDia4.setLayout(null);
			panelDia4.add(getChckbxJornadaPartidaDia4());
			panelDia4.add(getLblEntradaDia4());
			panelDia4.add(getSpinnerEntradaDia4());
			panelDia4.add(getLblSalidaDia4());
			panelDia4.add(getSpinnerSalidaDia4());
		}
		return panelDia4;
	}
	private JCheckBox getChckbxJornadaPartidaDia4() {
		if (boxJPD4 == null) {
			boxJPD4 = new JCheckBox("Jornada partida");
			boxJPD4.setName("box4");
			boxJPD4.addChangeListener(new AccionCheck());
			boxJPD4.setBounds(10, 26, 128, 23);
		}
		return boxJPD4;
	}
	private JPanel getPanelJornadaPartidaDia4() {
		if (panelJornadaPartidaDia4 == null) {
			panelJornadaPartidaDia4 = new JPanel();
			panelJornadaPartidaDia4.setVisible(false);
			panelJornadaPartidaDia4.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panelJornadaPartidaDia4.setBounds(18, 449, 220, 94);
			panelJornadaPartidaDia4.setLayout(null);
			panelJornadaPartidaDia4.add(getLblEntradaJornadaPartidaDia4());
			panelJornadaPartidaDia4.add(getSpinnerEntrada2Dia4());
			panelJornadaPartidaDia4.add(getLblSalidaJornadaPartidaDia4());
			panelJornadaPartidaDia4.add(getSpinnerSalida2Dia4());
		}
		return panelJornadaPartidaDia4;
	}
	private JPanel getPanelDia5() {
		if (panelDia5 == null) {
			panelDia5 = new JPanel();
			panelDia5.setBorder(new TitledBorder(null, "Dia 5", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelDia5.setBounds(252, 306, 224, 147);
			panelDia5.setLayout(null);
			panelDia5.add(getLblEntradaDia5());
			panelDia5.add(getSpinnerEntradaDia5());
			panelDia5.add(getLabelSalidaDia5());
			panelDia5.add(getSpinnerSalidaDia5());
			panelDia5.add(getChckbxJornadaPartidaDia5());
		}
		return panelDia5;
	}
	private JCheckBox getChckbxJornadaPartidaDia5() {
		if (boxJPD5 == null) {
			boxJPD5 = new JCheckBox("Jornada partida");
			boxJPD5.setName("box5");
			boxJPD5.addChangeListener(new AccionCheck());
			boxJPD5.setBounds(10, 26, 128, 23);
		}
		return boxJPD5;
	}
	private JPanel getPanelJPD5() {
		if (panelJornadaPartidaDia5 == null) {
			panelJornadaPartidaDia5 = new JPanel();
			panelJornadaPartidaDia5.setLayout(null);
			panelJornadaPartidaDia5.setVisible(false);
			panelJornadaPartidaDia5.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panelJornadaPartidaDia5.setBounds(254, 449, 220, 94);
			panelJornadaPartidaDia5.add(getLblEntradaJornadaPartidaDia5());
			panelJornadaPartidaDia5.add(getSpinnerEntradaJPD5());
			panelJornadaPartidaDia5.add(getLblSalidaJornadaPartidaDia5());
			panelJornadaPartidaDia5.add(getSpinnerSalidaJPD5());
		}
		return panelJornadaPartidaDia5;
	}
	private JPanel getPanelDia6() {
		if (panelDia6 == null) {
			panelDia6 = new JPanel();
			panelDia6.setBorder(new TitledBorder(null, "Dia 6", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelDia6.setBounds(488, 306, 224, 147);
			panelDia6.setLayout(null);
			panelDia6.add(getLblEntradaDia6());
			panelDia6.add(getSpinnerEntradaDia6());
			panelDia6.add(getSpinnerSalidaDia6());
			panelDia6.add(getLabelSalidaDia6());
			panelDia6.add(getChckbxJornadaPartidaDia6());
		}
		return panelDia6;
	}
	private JCheckBox getChckbxJornadaPartidaDia6() {
		if (boxJPD6 == null) {
			boxJPD6 = new JCheckBox("Jornada partida");
			boxJPD6.setName("box6");
			boxJPD6.addChangeListener(new AccionCheck());
			boxJPD6.setBounds(10, 26, 128, 23);
		}
		return boxJPD6;
	}
	private JPanel getPanelJornadaPartidaDia6() {
		if (panelJornadaPartidaDia6 == null) {
			panelJornadaPartidaDia6 = new JPanel();
			panelJornadaPartidaDia6.setLayout(null);
			panelJornadaPartidaDia6.setVisible(false);
			panelJornadaPartidaDia6.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panelJornadaPartidaDia6.setBounds(490, 449, 220, 94);
			panelJornadaPartidaDia6.add(getLblEntadaJPD6());
			panelJornadaPartidaDia6.add(getSpinnerEntradaJPD6());
			panelJornadaPartidaDia6.add(getLblSalidaJPD6());
			panelJornadaPartidaDia6.add(getSpinnerSalidaJPD6());
		}
		return panelJornadaPartidaDia6;
	}
	private JLabel getLblEntradaJornadaPartidaDia4() {
		if (lblEntradaJPD4 == null) {
			lblEntradaJPD4 = new JLabel("Hora de entrada:");
			lblEntradaJPD4.setBounds(10, 17, 112, 25);
		}
		return lblEntradaJPD4;
	}
	private JSpinner getSpinnerEntrada2Dia4() {
		if (spinnerEntradaJPD4 == null) {
			spinnerEntradaJPD4 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerEntradaJPD4);
			spinnerEntradaJPD4.setBounds(123, 16, 91, 25);
		}
		return spinnerEntradaJPD4;
	}
	private JLabel getLblSalidaJornadaPartidaDia4() {
		if (lblSalidaJPD4 == null) {
			lblSalidaJPD4 = new JLabel("Hora de salida:");
			lblSalidaJPD4.setBounds(10, 53, 112, 25);
		}
		return lblSalidaJPD4;
	}
	private JSpinner getSpinnerSalida2Dia4() {
		if (spinnerSalidaJPD4 == null) {
			spinnerSalidaJPD4 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerSalidaJPD4);
			spinnerSalidaJPD4.setBounds(123, 54, 91, 25);
		}
		return spinnerSalidaJPD4;
	}
	private JLabel getLblEntradaJornadaPartidaDia5() {
		if (lblEntradaJPD5 == null) {
			lblEntradaJPD5 = new JLabel("Hora de entrada:");
			lblEntradaJPD5.setBounds(10, 17, 112, 25);
		}
		return lblEntradaJPD5;
	}
	private JSpinner getSpinnerEntradaJPD5() {
		if (spinnerEntradaJPD5 == null) {
			spinnerEntradaJPD5 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerEntradaJPD5);
			spinnerEntradaJPD5.setBounds(123, 17, 91, 25);
		}
		return spinnerEntradaJPD5;
	}
	private JLabel getLblSalidaJornadaPartidaDia5() {
		if (lblSalidaJPD5 == null) {
			lblSalidaJPD5 = new JLabel("Hora de salida:");
			lblSalidaJPD5.setBounds(10, 53, 112, 25);
		}
		return lblSalidaJPD5;
	}
	private JSpinner getSpinnerSalidaJPD5() {
		if (spinnerSalidaJPD5 == null) {
			spinnerSalidaJPD5 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerSalidaJPD5);
			spinnerSalidaJPD5.setBounds(123, 53, 91, 25);
		}
		return spinnerSalidaJPD5;
	}
	private JLabel getLblEntadaJPD6() {
		if (lblEntadaJPD6 == null) {
			lblEntadaJPD6 = new JLabel("Hora de entrada:");
			lblEntadaJPD6.setBounds(10, 17, 112, 25);
		}
		return lblEntadaJPD6;
	}
	private JSpinner getSpinnerEntradaJPD6() {
		if (spinnerEntradaJPD6 == null) {
			spinnerEntradaJPD6 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerEntradaJPD6);
			spinnerEntradaJPD6.setBounds(123, 17, 91, 25);
		}
		return spinnerEntradaJPD6;
	}
	private JLabel getLblSalidaJPD6() {
		if (lblSalidaJPD6 == null) {
			lblSalidaJPD6 = new JLabel("Hora de salida:");
			lblSalidaJPD6.setBounds(10, 53, 112, 25);
		}
		return lblSalidaJPD6;
	}
	private JSpinner getSpinnerSalidaJPD6() {
		if (spinnerSalidaJPD6 == null) {
			spinnerSalidaJPD6 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerSalidaJPD6);
			spinnerSalidaJPD6.setBounds(123, 53, 91, 25);
		}
		return spinnerSalidaJPD6;
	}
	
	private void aplicarMismoHorarioALosDias(List<Integer> listaSeleccion){
		Calendar horasDia1Entrada = GregorianCalendar.getInstance();
		horasDia1Entrada.setTime((Date) spinnerEntradaDia1.getModel().getValue());
		int horas1 = horasDia1Entrada.get(Calendar.HOUR_OF_DAY);
		int minutos1 = horasDia1Entrada.get(Calendar.MINUTE);
		
		Calendar horasDia1Salida = GregorianCalendar.getInstance();
		horasDia1Salida.setTime((Date) spinnerSalidaDia1.getModel().getValue());
		int horas2 = horasDia1Salida.get(Calendar.HOUR_OF_DAY);
		int minutos2 = horasDia1Salida.get(Calendar.MINUTE);
		
		//CALENDARIO PARA HORARIO DE ENTRADA DE JORNADA PARTIDA
		Calendar horasDia1Entrada2 = GregorianCalendar.getInstance();
		horasDia1Entrada2.setTime((Date) spinnerEntradaJPD1.getModel().getValue());
		int horasJPEntrada = horasDia1Entrada2.get(Calendar.HOUR_OF_DAY);
		int minutosJPEntrada = horasDia1Entrada2.get(Calendar.MINUTE);
		
		Calendar horasDia1Salida2 = GregorianCalendar.getInstance();
		horasDia1Salida2.setTime((Date) spinnerSalidaJPD1.getModel().getValue());
		int horasJPSalida = horasDia1Salida2.get(Calendar.HOUR_OF_DAY);
		int minutosJPSalida = horasDia1Salida2.get(Calendar.MINUTE);
		
		boolean seleccionadoBox1 = boxJPD1.isSelected();
		
		if(listaSeleccion.contains(2)) {
			aplicarHorarioA(spinnerEntradaDia2, spinnerSalidaDia2, spinnerEntradaJPD2, spinnerSalidaJPD2, 
					horas1, minutos1, horas2, minutos2, horasJPEntrada, minutosJPEntrada, 
					horasJPSalida, minutosJPSalida);
			if(seleccionadoBox1) {
				boxJPD2.setSelected(true);
			}
		}
		if(listaSeleccion.contains(3)) {
			aplicarHorarioA(spinnerEntradaDia3, spinnerSalidaDia3, spinnerEntradaJPD3, spinnerSalidaJPD3, 
					horas1, minutos1, horas2, minutos2, horasJPEntrada, minutosJPEntrada, 
					horasJPSalida, minutosJPSalida);	
			if(seleccionadoBox1) {
				boxJPD3.setSelected(true);
			}
		}
		if(listaSeleccion.contains(4)) {
			aplicarHorarioA(spinnerEntradaDia4, spinnerSalidaDia4, spinnerEntradaJPD4, spinnerSalidaJPD4, 
					horas1, minutos1, horas2, minutos2, horasJPEntrada, minutosJPEntrada, 
					horasJPSalida, minutosJPSalida);
			if(seleccionadoBox1) {
				boxJPD4.setSelected(true);
			}
		}
		if(listaSeleccion.contains(5)) {
			aplicarHorarioA(spinnerEntradaDia5, spinnerSalidaDia5, spinnerEntradaJPD5, spinnerSalidaJPD5, 
					horas1, minutos1, horas2, minutos2, horasJPEntrada, minutosJPEntrada, 
					horasJPSalida, minutosJPSalida);
			if(seleccionadoBox1) {
				boxJPD5.setSelected(true);
			}
		}
		if(listaSeleccion.contains(6)) {
			aplicarHorarioA(spinnerEntradaDia6, spinnerSalidaDia6, spinnerEntradaJPD6, spinnerSalidaJPD6, 
					horas1, minutos1, horas2, minutos2, horasJPEntrada, minutosJPEntrada, 
					horasJPSalida, minutosJPSalida);
			if(seleccionadoBox1) {
				boxJPD6.setSelected(true);
			}
		}
		if(listaSeleccion.contains(7)) {
			aplicarHorarioA(spinnerEntradaDia7, spinnerSalidaDia7, spinnerEntradaJPD7, spinnerSalidaJPD7, 
					horas1, minutos1, horas2, minutos2, horasJPEntrada, minutosJPEntrada, 
					horasJPSalida, minutosJPSalida);
			if(seleccionadoBox1) {
				boxJPD7.setSelected(true);
			}
		}	
		
	}
	
	private void aplicarHorarioA(JSpinner entrada, JSpinner salida, JSpinner entrada2, JSpinner salida2, int horas1, 
			int minutos1, int horas2, int minutos2, int horasJPEntrada, int minutosJPEntrada,
			int horasJPSalida, int minutosJPSalida) {
		boolean box1Seleccionado = boxJPD1.isSelected();
		
		Calendar horarioEntrada = GregorianCalendar.getInstance();
		horarioEntrada.setTime((Date) entrada.getModel().getValue());
		horarioEntrada.set(Calendar.HOUR_OF_DAY, horas1);
		horarioEntrada.set(Calendar.MINUTE, minutos1);
		entrada.setValue(horarioEntrada.getTime());
		
		Calendar horarioSalida = GregorianCalendar.getInstance();
		horarioSalida.setTime((Date) salida.getModel().getValue());
		horarioSalida.set(Calendar.HOUR_OF_DAY, horas2);
		horarioSalida.set(Calendar.MINUTE, minutos2);
		salida.setValue(horarioSalida.getTime());	
		
		if(box1Seleccionado) {
			aplicarHorarioAJornadaPartida(entrada2, salida2, horasJPEntrada, minutosJPEntrada, horasJPSalida, minutosJPSalida);
		}
	}
	
	private void aplicarHorarioAJornadaPartida(JSpinner entradaJP, JSpinner salidaJP, int horasJPEntrada, int minutosJPEntrada,
			int horasJPSalida, int minutosJPSalida) {
		
		Calendar horarioEntrada = GregorianCalendar.getInstance();
		horarioEntrada.setTime((Date) entradaJP.getModel().getValue());
		horarioEntrada.set(Calendar.HOUR_OF_DAY, horasJPEntrada);
		horarioEntrada.set(Calendar.MINUTE, minutosJPEntrada);
		entradaJP.setValue(horarioEntrada.getTime());
		
		Calendar horarioSalida = GregorianCalendar.getInstance();
		horarioSalida.setTime((Date) salidaJP.getModel().getValue());
		horarioSalida.set(Calendar.HOUR_OF_DAY, horasJPSalida);
		horarioSalida.set(Calendar.MINUTE, minutosJPSalida);
		salidaJP.setValue(horarioSalida.getTime());
		
	}
	
	
	private JButton getBtnAnterior() {
		if (btnAnterior == null) {
			btnAnterior = new JButton("Anterior");
			btnAnterior.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					superCard.show(panelSuperCard, "panelNuevoTurno");
				}
			});
			btnAnterior.setBounds(510, 549, 104, 35);
		}
		return btnAnterior;
	}
	private JButton getBtnDia7() {
		if (btnDia7 == null) {
			btnDia7 = new JButton("D\u00EDa 7 >>");
			btnDia7.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					superCard.show(panelSuperCard, "panelHorarioDia7");
					
				}
			});
			btnDia7.setBounds(618, 549, 94, 35);
		}
		return btnDia7;
	}
	private JPanel getPanelHorarioDia7() {
		if (panelHorarioDia7 == null) {
			panelHorarioDia7 = new JPanel();
			panelHorarioDia7.setLayout(null);
			panelHorarioDia7.add(getPanelHorario());
		}
		return panelHorarioDia7;
	}
	private JPanel getPanelDia7() {
		if (panelDia7 == null) {
			panelDia7 = new JPanel();
			panelDia7.setBounds(27, 49, 224, 147);
			panelDia7.setBorder(new TitledBorder(null, "Dia 7", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelDia7.setLayout(null);
			panelDia7.add(getChckbxJornadaPartidaDia7());
			panelDia7.add(getLblHoraDeEntradaDia7());
			panelDia7.add(getLblHoraDeSalidaDia7());
			panelDia7.add(getSpinnerEntradaDia7());
			panelDia7.add(getSpinnerSalidaDia7());
		}
		return panelDia7;
	}
	private JCheckBox getChckbxJornadaPartidaDia7() {
		if (boxJPD7 == null) {
			boxJPD7 = new JCheckBox("Jornada partida");
			boxJPD7.setName("box7");
			boxJPD7.addChangeListener(new AccionCheck());
			boxJPD7.setBounds(10, 26, 128, 23);
		}
		return boxJPD7;
	}
	private JLabel getLblHoraDeEntradaDia7() {
		if (lblHoraDeEntradaDia7 == null) {
			lblHoraDeEntradaDia7 = new JLabel("Hora de enrtrada:");
			lblHoraDeEntradaDia7.setBounds(10, 61, 113, 23);
		}
		return lblHoraDeEntradaDia7;
	}
	private JLabel getLblHoraDeSalidaDia7() {
		if (lblHoraDeSalidaDia7 == null) {
			lblHoraDeSalidaDia7 = new JLabel("Hora de salida:");
			lblHoraDeSalidaDia7.setBounds(10, 99, 113, 23);
		}
		return lblHoraDeSalidaDia7;
	}
	private JSpinner getSpinnerEntradaDia7() {
		if (spinnerEntradaDia7 == null) {
			spinnerEntradaDia7 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerEntradaDia7);
			spinnerEntradaDia7.setBounds(123, 61, 91, 25);
		}
		return spinnerEntradaDia7;
	}
	private JSpinner getSpinnerSalidaDia7() {
		if (spinnerSalidaDia7 == null) {
			spinnerSalidaDia7 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerSalidaDia7);
			spinnerSalidaDia7.setBounds(123, 97, 91, 25);
		}
		return spinnerSalidaDia7;
	}
	private JPanel getPanelJPD7() {
		if (panelJPD7 == null) {
			panelJPD7 = new JPanel();
			panelJPD7.setBounds(29, 192, 220, 94);
			panelJPD7.setVisible(false);
			panelJPD7.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panelJPD7.setLayout(null);
			panelJPD7.add(getLblHoraDeEntradaJPD7());
			panelJPD7.add(getSpinnerEntradaJPD7());
			panelJPD7.add(getLblHoraDeSalidaJPD7());
			panelJPD7.add(getSpinnerSalidaJPD7());
		}
		return panelJPD7;
	}
	private JLabel getLblHoraDeEntradaJPD7() {
		if (lblHoraDeEntradaJPD7 == null) {
			lblHoraDeEntradaJPD7 = new JLabel("Hora de entrada:");
			lblHoraDeEntradaJPD7.setBounds(10, 17, 109, 27);
		}
		return lblHoraDeEntradaJPD7;
	}
	private JSpinner getSpinnerEntradaJPD7() {
		if (spinnerEntradaJPD7 == null) {
			spinnerEntradaJPD7 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerEntradaJPD7);
			spinnerEntradaJPD7.setBounds(123, 17, 91, 27);
		}
		return spinnerEntradaJPD7;
	}
	private JLabel getLblHoraDeSalidaJPD7() {
		if (lblHoraDeSalidaJPD7 == null) {
			lblHoraDeSalidaJPD7 = new JLabel("Hora de salida:");
			lblHoraDeSalidaJPD7.setBounds(10, 52, 109, 27);
		}
		return lblHoraDeSalidaJPD7;
	}
	private JSpinner getSpinnerSalidaJPD7() {
		if (spinnerSalidaJPD7 == null) {
			spinnerSalidaJPD7 = new JSpinner();
			asignarModeloSpinnersHoras(spinnerSalidaJPD7);
			spinnerSalidaJPD7.setBounds(123, 52, 91, 27);
		}
		return spinnerSalidaJPD7;
	}
	private JButton getBtnAnteriorDia7() {
		if (btnAnteriorDia7 == null) {
			btnAnteriorDia7 = new JButton("Anterior");
			btnAnteriorDia7.setBounds(515, 549, 86, 35);
			btnAnteriorDia7.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					superCard.show(panelSuperCard, "panelHoras");
				}
			});
		}
		return btnAnteriorDia7;
	}
	private JButton getBtnAsignarTurno() {
		if (btnAsignarTurno == null) {
			btnAsignarTurno = new JButton("Asignar turno");
			btnAsignarTurno.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(comprobarMenosDe8HorasAlDia()) 
						if(menosDe40hSemanales())
							if(comprobar12horasDeDiferencia())
								if(comprobarFechasLimitePorEncima12hDif())
									if(comprobarFechasLimitePorDebajo12hDif())
										if(insertarTurnos())
											JOptionPane.showMessageDialog(null, "Se ha insertado correctamente", "OK", JOptionPane.INFORMATION_MESSAGE);
										else
											JOptionPane.showMessageDialog(null, "Turnos conflictivos", "Error", JOptionPane.ERROR_MESSAGE);
									else
										JOptionPane.showMessageDialog(null, "Entre la �ltima jornada laboral de esta semana y la\nsiguiente jornada laboral "
												+ "no transcurren 12 horas o m�s", "Error", JOptionPane.ERROR_MESSAGE);
								else
									JOptionPane.showMessageDialog(null, "Entre la �ltima jornada laboral y la\nprimera jornada laboral de esta "
											+ "semana\nno transcurren 12 horas o m�s", "Error", JOptionPane.ERROR_MESSAGE);
							else
								JOptionPane.showMessageDialog(null, "Debe haber un m�nimo de 12 horas de diferencia entre cada jornada laboral",
										"Error en las 12 horas de diferencia", JOptionPane.ERROR_MESSAGE);
						else
							JOptionPane.showMessageDialog(null, "Las jornadas semanales no deben exceder de las 40 horas", "Explotador", JOptionPane.ERROR_MESSAGE);				
					else
						JOptionPane.showMessageDialog(null, "Las jornadas laborales deben de ser de menos de 8 horas", "Error", JOptionPane.ERROR_MESSAGE);
				}
			});
			btnAsignarTurno.setBounds(613, 549, 112, 35);
		}
		return btnAsignarTurno;
	}
	
	private boolean insertarTurnos() {
		List<String> lista = listaEmpleadosNuevoTurno.getSelectedValuesList();
		if(comprobarSonFechasValidasParaTodosLosEmpleados(lista)) {
			for (String empleado : lista) {		
				if(!tglbtnNoHorario1.isSelected()) {
					iniciarInsercion(0, spinnerEntradaDia1, spinnerSalidaDia1, spinnerEntradaJPD1, spinnerSalidaJPD1, empleado, boxJPD1);
				}
				if(!tglbtnNoHorario2.isSelected()) {
					iniciarInsercion(1, spinnerEntradaDia2, spinnerSalidaDia2, spinnerEntradaJPD2, spinnerSalidaJPD2, empleado, boxJPD2);
				}
				if(!tglbtnNoHorario3.isSelected()) {
					iniciarInsercion(2, spinnerEntradaDia3, spinnerSalidaDia3, spinnerEntradaJPD3, spinnerSalidaJPD3, empleado, boxJPD3);
				}
				if(!tglbtnNoHorario4.isSelected()) {
					iniciarInsercion(3, spinnerEntradaDia3, spinnerSalidaDia3, spinnerEntradaJPD3, spinnerSalidaJPD3, empleado, boxJPD3);
				}
				if(!tglbtnNoHorario5.isSelected()) {
					iniciarInsercion(4, spinnerEntradaDia5, spinnerSalidaDia5, spinnerEntradaJPD5, spinnerSalidaJPD5, empleado, boxJPD5);
				}
				if(!tglbtnNoHorario6.isSelected()) {
					iniciarInsercion(5, spinnerEntradaDia6, spinnerSalidaDia6, spinnerEntradaJPD6, spinnerSalidaJPD6, empleado, boxJPD6);
				}
				if(!tglbtnNoHorario7.isSelected()) {
					iniciarInsercion(6, spinnerEntradaDia7, spinnerSalidaDia7, spinnerEntradaJPD7, spinnerSalidaJPD7, empleado, boxJPD7);
				}
			}
			return true;
		}
		else
			return false;
		
	}
	
	private void iniciarInsercion(int diaSemana, JSpinner entrada1, JSpinner salida1, JSpinner entrada2, JSpinner salida2, String nombreE, JCheckBox box) {
		String fechaEntrada = devolverFechaEntrada(diaSemana);
		String horaEntrada = devolverHora(entrada1);
		String horaSalida = devolverHora(salida1);
		String fechaSalida = devolverFechaSalida(diaSemana, horaEntrada, horaSalida);
		String dni = trabajadores.getDni(nombreE);
		
		insertarTurnoEnBBDD(fechaEntrada, fechaSalida, horaEntrada, horaSalida, dni);
		
		if(box.isSelected()) {
			horaEntrada = devolverHora(entrada2);
			horaSalida = devolverHora(salida2);
			fechaSalida = devolverFechaSalida(diaSemana, horaEntrada, horaSalida);
			insertarTurnoEnBBDD(fechaEntrada, fechaSalida, horaEntrada, horaSalida, dni);
		}	
	}
	
	private String devolverFechaEntrada(int diaSemana) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
		Calendar fechaInicioSemana = GregorianCalendar.getInstance();
		fechaInicioSemana.setTime(dcFechaInicioNuevoTurno.getDate());
		fechaInicioSemana.add(Calendar.DATE, diaSemana);
		return sdf.format(fechaInicioSemana.getTime());
	}
	
	private String devolverFechaSalida(int diaSemana, String hEntrada, String hSalida) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
		Calendar fecha = GregorianCalendar.getInstance();
		fecha.setTime(dcFechaInicioNuevoTurno.getDate());
		fecha.add(Calendar.DATE, diaSemana);
		
		int horaEntrada = convertirAInt(hEntrada.substring(0,2));
		int horaSalida = convertirAInt(hSalida.substring(0,2));
		
		if(horaEntrada > horaSalida)
			fecha.add(Calendar.DATE, 1);
		
		return sdf.format(fecha.getTime());
	}
	
	private String devolverHora(JSpinner spinner) {
		int hora;	
		int minutos;
		String patron = "%02d:%02d";
		
		Calendar horario = GregorianCalendar.getInstance();
		horario.setTime((Date) spinner.getModel().getValue());
		hora= horario.get(Calendar.HOUR_OF_DAY);
		minutos= horario.get(Calendar.MINUTE);
		
		String resultado = String.format(patron,hora,minutos);
		return resultado;		
	}
	
	private void insertarTurnoEnBBDD(String fechaEntrada, String fechaSalida, String horaEntrada, String horaSalida, String dni) {
		try {
			
			String insercion = "insert into turno values(SEC_TURNO.nextVal, ?, to_date(?, 'dd-MM-YYYY'), "
					+ "to_date(?, 'dd-MM-YYYY'), ?, ?, ?)";
			
			PreparedStatement ps = con.prepareStatement(insercion);
			ps.setString(1, "activo");
			ps.setString(2, fechaEntrada);
			ps.setString(3, fechaSalida);
			ps.setString(4, horaEntrada);
			ps.setString(5, horaSalida);
			ps.setString(6, dni);
			
			ps.executeUpdate();
			
			ps.close();		
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private boolean menosDe40hSemanales() {
		int horasTotales = 0;
		for (Integer horas : horasTotalesPorDia) {
			horasTotales+= horas;
		}
		return horasTotales <= 40;
		
	}
	
	private boolean comprobar12horasDeDiferencia() {
		//FECHA INICIAL DE LA SEMANA
		Calendar fechaInicialDeLaSemana = GregorianCalendar.getInstance();
		Date f = dcFechaInicioNuevoTurno.getDate();
		fechaInicialDeLaSemana.setTime(f);	
		
		List<Boolean> validosTodoLosDias = new ArrayList<Boolean>();
		if(!tglbtnNoHorario1.isSelected() && !tglbtnNoHorario2.isSelected()) {
			if(!boxJPD1.isSelected())
				validosTodoLosDias.add(aplicarComprobaciones12Horas(fechaInicialDeLaSemana, spinnerEntradaDia1, spinnerSalidaDia1, spinnerEntradaDia2, 0));
			else
				validosTodoLosDias.add(aplicarComprobaciones12Horas(fechaInicialDeLaSemana, spinnerEntradaJPD1, spinnerSalidaJPD1, spinnerEntradaDia2, 0));
		}
		else
			validosTodoLosDias.add(false);
		if(!tglbtnNoHorario2.isSelected() && !tglbtnNoHorario3.isSelected()) {
			if(!boxJPD2.isSelected())
				validosTodoLosDias.add(aplicarComprobaciones12Horas(fechaInicialDeLaSemana, spinnerEntradaDia2, spinnerSalidaDia2, spinnerEntradaDia3, 2));
			else
				validosTodoLosDias.add(aplicarComprobaciones12Horas(fechaInicialDeLaSemana, spinnerEntradaJPD2, spinnerSalidaJPD2, spinnerEntradaDia3, 2));
		}
		if(!tglbtnNoHorario3.isSelected() && !tglbtnNoHorario4.isSelected()) {
			if(!boxJPD3.isSelected())
				validosTodoLosDias.add(aplicarComprobaciones12Horas(fechaInicialDeLaSemana, spinnerEntradaDia3, spinnerSalidaDia3, spinnerEntradaDia4, 3));
			else
				validosTodoLosDias.add(aplicarComprobaciones12Horas(fechaInicialDeLaSemana, spinnerEntradaJPD3, spinnerSalidaJPD3, spinnerEntradaDia4, 3));
		}
		if(!tglbtnNoHorario4.isSelected() && !tglbtnNoHorario5.isSelected()) {
			if(!boxJPD4.isSelected())
				validosTodoLosDias.add(aplicarComprobaciones12Horas(fechaInicialDeLaSemana, spinnerEntradaDia4, spinnerSalidaDia4, spinnerEntradaDia5, 4));
			else
				validosTodoLosDias.add(aplicarComprobaciones12Horas(fechaInicialDeLaSemana, spinnerEntradaJPD4, spinnerSalidaJPD4, spinnerEntradaDia5, 4));
		}
		if(!tglbtnNoHorario5.isSelected() && !tglbtnNoHorario6.isSelected()) {
			if(!boxJPD5.isSelected())
				validosTodoLosDias.add(aplicarComprobaciones12Horas(fechaInicialDeLaSemana, spinnerEntradaDia5, spinnerSalidaDia5, spinnerEntradaDia6, 5));
			else
				validosTodoLosDias.add(aplicarComprobaciones12Horas(fechaInicialDeLaSemana, spinnerEntradaJPD5, spinnerSalidaJPD5, spinnerEntradaDia6, 5));
		}
		if(!tglbtnNoHorario6.isSelected() && !tglbtnNoHorario7.isSelected()) {
			if(!boxJPD6.isSelected())
				validosTodoLosDias.add(aplicarComprobaciones12Horas(fechaInicialDeLaSemana, spinnerEntradaDia6, spinnerSalidaDia6, spinnerEntradaDia7, 6));
			else
				validosTodoLosDias.add(aplicarComprobaciones12Horas(fechaInicialDeLaSemana, spinnerEntradaJPD6, spinnerSalidaJPD6, spinnerEntradaDia7, 6));
		}
		for (Boolean comprobacion : validosTodoLosDias) {
			if(comprobacion)
				return false;
		}
		return true;
	}
	
	
	
	private boolean comprobarFechasLimitePorEncima12hDif() {
		try {
			if(!tglbtnNoHorario1.isSelected()) {
				List<String> lista = listaEmpleadosNuevoTurno.getSelectedValuesList();
				
				String consulta = "select t.fecha_fin, t.hora_salida, t.hora_entrada " + 
						"from turno t natural join trabajador tr " + 
						"where t.fecha_fin < to_date(?,'dd-MM-YYYY') and nombre = ? and t.tipo = 'activo' " + 
						"order by t.fecha_fin desc";
				
				Calendar fechaInicialDeLaSemana = GregorianCalendar.getInstance();
				Date fi = dcFechaInicioNuevoTurno.getDate();
				fechaInicialDeLaSemana.setTime(fi);	
				
				
				PreparedStatement ps = con.prepareStatement(consulta);
				ps.setDate(1, new java.sql.Date(dcFechaInicioNuevoTurno.getDate().getTime()));
				ResultSet rs = null;
				
				for (String empleado : lista) {
					ps.setString(2, empleado);
					rs = ps.executeQuery();
					
					while(rs.next()){
						Date fechaUltimoTurnoActivo = rs.getDate("FECHA_FIN");
						
						Calendar fUltimoTurnoActivo = GregorianCalendar.getInstance();
						fUltimoTurnoActivo.setTime(fechaUltimoTurnoActivo);
						long diferencia = fechaInicialDeLaSemana.getTime().getTime() - fUltimoTurnoActivo.getTime().getTime();
						int dias = (int) (diferencia/86400000);
						
						if(dias<=1) {
							String hEntrada = rs.getString("HORA_ENTRADA");
							String hSalida = rs.getString("HORA_SALIDA");
							if(ayudanteFechaLimite12hPorEncima(fechaInicialDeLaSemana, fUltimoTurnoActivo, hEntrada, hSalida))
								return false;
						}				
					}
				}
				
				rs.close();
				ps.close();
				return true;
			}
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private boolean ayudanteFechaLimite12hPorEncima(Calendar fechaInicialDeLaSemana, Calendar fUltimoTurnoActivo, String hEntrada, String hSalida) {
		
		
		//HORAS Y MINUTOS DE LA SALIDA DEL DIA ANTERIOR
		Calendar horarioUltimaSalida = (Calendar) fUltimoTurnoActivo.clone();
		int horasUltimaSalida = convertirAInt(hSalida.substring(0,2));
		int minutosUltimaSalida = convertirAInt(hSalida.substring(3, 5));
		horarioUltimaSalida.set(Calendar.HOUR_OF_DAY, horasUltimaSalida);
		horarioUltimaSalida.set(Calendar.MINUTE, minutosUltimaSalida);
		
		//HORAS ENTRADA DIA ANTERIOR
		Calendar horarioUltimaEntrada = (Calendar) fUltimoTurnoActivo.clone();
		int horasUltimaEntrada = convertirAInt(hEntrada.substring(0,2));
		int minutosUltimaEntrada = convertirAInt(hEntrada.substring(3, 5));
		horarioUltimaEntrada.set(Calendar.HOUR_OF_DAY, horasUltimaEntrada);
		horarioUltimaEntrada.set(Calendar.MINUTE, minutosUltimaEntrada);
		
		
		if(horasUltimaEntrada>horasUltimaSalida) {
			horarioUltimaSalida.add(Calendar.DATE, 1);
		}
		
		//HORAS Y MINUTOS DIA ACTUAL
		Calendar calendarioParaSacarHorasYMinutos = GregorianCalendar.getInstance();
		calendarioParaSacarHorasYMinutos.setTime((Date) spinnerEntradaDia1.getModel().getValue());
		int horasActualSpinner= calendarioParaSacarHorasYMinutos.get(Calendar.HOUR_OF_DAY);
		int minutosActualSpinner = calendarioParaSacarHorasYMinutos.get(Calendar.MINUTE);
		
		//FECHA DIA ACTUAL
		Calendar fechaActual = GregorianCalendar.getInstance();
		fechaActual.setTime(fechaInicialDeLaSemana.getTime());
		fechaActual.set(Calendar.HOUR_OF_DAY, horasActualSpinner);
		fechaActual.set(Calendar.MINUTE, minutosActualSpinner);
		
		long diferenciaEnMs = fechaActual.getTime().getTime() - horarioUltimaSalida.getTime().getTime();
		long horas = (diferenciaEnMs / (1000 * 60 * 60));
	
		System.out.println(horas);
		return horas < 12;
	
	}
	
	private boolean comprobarFechasLimitePorDebajo12hDif() {
		try {
			if(!tglbtnNoHorario7.isSelected()) {
				List<String> lista = listaEmpleadosNuevoTurno.getSelectedValuesList();
				
				String consulta = "select fecha_inicio, hora_entrada " + 
						"from turno natural join trabajador " + 
						"where fecha_inicio > to_date(?, 'dd-MM-yyyy') and nombre = ? " + 
						"and tipo = 'activo' " + 
						"order by fecha_inicio asc";
				
				Calendar fechaFinalDeLaSemana = GregorianCalendar.getInstance();
				Date fi = dcFechaFinNuevoTurno.getDate();
				fechaFinalDeLaSemana.setTime(fi);	
				
				boolean jornadaPartidaDia7 = boxJPD7.isSelected();
				
				PreparedStatement ps = con.prepareStatement(consulta);
				ps.setDate(1, new java.sql.Date(dcFechaFinNuevoTurno.getDate().getTime()));
				ResultSet rs = null;
				
				for (String empleado : lista) {
					ps.setString(2, empleado);
					rs = ps.executeQuery();
					
					while(rs.next()){
						Date fechaSiguienteTurnoActivo = rs.getDate("FECHA_INICIO");
						
						Calendar fSiguienteTurnoActivo = GregorianCalendar.getInstance();
						fSiguienteTurnoActivo.setTime(fechaSiguienteTurnoActivo);
						long diferencia = fSiguienteTurnoActivo.getTime().getTime() - fechaFinalDeLaSemana.getTime().getTime();
						int dias = (int) (diferencia/86400000);
						
						if(dias<=1) {
							String hEntrada = rs.getString("HORA_ENTRADA");
							if(!jornadaPartidaDia7)
								if(ayudanteFechaLimite12hPorDebajo(fechaFinalDeLaSemana, spinnerSalidaDia7, spinnerEntradaDia7, fSiguienteTurnoActivo, hEntrada))
									return false;
							else
								if(ayudanteFechaLimite12hPorDebajo(fechaFinalDeLaSemana, spinnerSalidaJPD7, spinnerEntradaJPD7, fSiguienteTurnoActivo, hEntrada))
									return false;
						}
						System.out.println("Dias " + dias);					
					}
				}
				
				rs.close();
				ps.close();
				return true;
			}
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private int convertirAInt(String cadena) {
		try {
			return Integer.parseInt(cadena);
		}
		catch(Exception e) {
			e.printStackTrace();
			return -1;
		}
	}
	
	private boolean ayudanteFechaLimite12hPorDebajo(Calendar fechaFinalDeLaSemana, JSpinner horaSalida, JSpinner horaEntrada,
			Calendar fSiguienteTurnoActivo, String hEntrada) {
		
		Calendar ultimoHorarioAsignadoDeSalida = GregorianCalendar.getInstance();
		ultimoHorarioAsignadoDeSalida.setTime((Date) horaSalida.getModel().getValue());
		int horasUltimoDiaSalida = ultimoHorarioAsignadoDeSalida.get(Calendar.HOUR_OF_DAY);
		int minutosUltimoDiaSalida = ultimoHorarioAsignadoDeSalida.get(Calendar.MINUTE);
		
		//ULTIMO DIA ASIGNADO HORARIO SALIDA
		Calendar dia7Salida = (Calendar) fechaFinalDeLaSemana.clone();
		dia7Salida.set(Calendar.HOUR_OF_DAY, horasUltimoDiaSalida);
		dia7Salida.set(Calendar.MINUTE, minutosUltimoDiaSalida);
		
		Calendar ultimoHorarioAsignadoDeEntrada = GregorianCalendar.getInstance();
		ultimoHorarioAsignadoDeEntrada.setTime((Date) horaEntrada.getModel().getValue());
		int horasUltimoDiaEntrada = ultimoHorarioAsignadoDeEntrada.get(Calendar.HOUR_OF_DAY); 
		
		//ULTIMO DIA ASIGNADO HORARIO ENTRADA	
		if(horasUltimoDiaEntrada>horasUltimoDiaSalida) {
			dia7Salida.add(Calendar.DATE, 1);
		}
		
		//HORAS ENTRADA DIA ANTERIOR
		Calendar horarioSiguienteEntrada = (Calendar) fSiguienteTurnoActivo.clone();
		int horasUltimaEntrada = convertirAInt(hEntrada.substring(0,2));
		int minutosUltimaEntrada = convertirAInt(hEntrada.substring(3, 5));
		horarioSiguienteEntrada.set(Calendar.HOUR_OF_DAY, horasUltimaEntrada);
		horarioSiguienteEntrada.set(Calendar.MINUTE, minutosUltimaEntrada);
		
		long diferenciaEnMs = horarioSiguienteEntrada.getTime().getTime() - dia7Salida.getTime().getTime();
		long horas = (diferenciaEnMs / (1000 * 60 * 60));
	
		System.out.println(horas);
		return horas < 12;
	
	}
	

	
	private boolean aplicarComprobaciones12Horas(Calendar fechaInicialDeLaSemana, JSpinner entradaDiaAnterior, JSpinner salidaDiaAnterior,
			JSpinner entradaDiaActual, int diaDeLaSemana) {
	
		
		//HORAS Y MINUTOS DE LA SALIDA DEL DIA ANTERIOR
		Calendar horasYminutosSalidaDiaAnterior = GregorianCalendar.getInstance();
		horasYminutosSalidaDiaAnterior.setTime((Date) salidaDiaAnterior.getModel().getValue());
		int horasSalidaDiaAnterior = horasYminutosSalidaDiaAnterior.get(Calendar.HOUR_OF_DAY);
		int minutosSalidaDiaAnterior= horasYminutosSalidaDiaAnterior.get(Calendar.MINUTE);
		
		//HORAS ENTRADA DIA ANTERIOR
		Calendar horarioEntradaDiaAnterior = GregorianCalendar.getInstance();
		horarioEntradaDiaAnterior.setTime((Date) entradaDiaAnterior.getModel().getValue());
		int horasEntradaDiaAnterior = horarioEntradaDiaAnterior.get(Calendar.HOUR_OF_DAY);
		
		//FECHA DIA ANTERIOR
		Calendar fechaDelDiaAnterior = GregorianCalendar.getInstance();
		fechaDelDiaAnterior.setTime(fechaInicialDeLaSemana.getTime());
		fechaDelDiaAnterior.add(Calendar.DATE, diaDeLaSemana-1);
		if(horasEntradaDiaAnterior>horasSalidaDiaAnterior) {
			fechaDelDiaAnterior.add(Calendar.DATE, 1);
		}
		fechaDelDiaAnterior.set(Calendar.HOUR_OF_DAY, horasSalidaDiaAnterior);
		fechaDelDiaAnterior.set(Calendar.MINUTE, minutosSalidaDiaAnterior);
		
		//HORAS Y MINUTOS DIA ACTUAL
		Calendar calendarioParaSacarHorasYMinutos = GregorianCalendar.getInstance();
		calendarioParaSacarHorasYMinutos.setTime((Date) entradaDiaActual.getModel().getValue());
		int horasActualSpinner= calendarioParaSacarHorasYMinutos.get(Calendar.HOUR_OF_DAY);
		int minutosActualSpinner = calendarioParaSacarHorasYMinutos.get(Calendar.MINUTE);
		
		//FECHA DIA ACTUAL
		Calendar fechaActual = GregorianCalendar.getInstance();
		fechaActual.setTime(fechaInicialDeLaSemana.getTime());
		fechaActual.add(Calendar.DATE, diaDeLaSemana);
		fechaActual.set(Calendar.HOUR_OF_DAY, horasActualSpinner);
		fechaActual.set(Calendar.MINUTE, minutosActualSpinner);
		
		long diferenciaEnMs = fechaActual.getTime().getTime() - fechaDelDiaAnterior.getTime().getTime();
		long horas = (diferenciaEnMs / (1000 * 60 * 60));
	
		System.out.println(horas);
		return horas < 12;
		
//		System.out.println("HorasEntradaDiaAnterior" +  horasEntradaDiaAnterior);
//		System.out.println("HorasSalidaDiaAnterior" +  horasSalidaDiaAnterior);
		
//		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY");
//		System.out.println(sdf.format(fechaActual.getTime()));
//
//		System.out.println(fechaDelDiaAnterior.get(Calendar.DATE) + "-" + fechaDelDiaAnterior.get(Calendar.MONTH)+ "-"+
//				fechaDelDiaAnterior.get(Calendar.YEAR) + ", " + fechaDelDiaAnterior.get(Calendar.HOUR_OF_DAY) + ":" + fechaDelDiaAnterior.get(Calendar.MINUTE));
//		System.out.println(fechaActual.get(Calendar.DATE) + "-" + fechaActual.get(Calendar.MONTH)+ "-"+
//				fechaActual.get(Calendar.YEAR) + ", " + fechaActual.get(Calendar.HOUR_OF_DAY) + ":" + fechaActual.get(Calendar.MINUTE));
//		System.out.println(horas);
	
	}
	
	private boolean comprobarMenosDe8HorasAlDia() {
		horasTotalesPorDia = new ArrayList<Integer>();
		if(!tglbtnNoHorario1.isSelected())
			if(!aplicarComprobaciones8Horas(spinnerEntradaDia1, spinnerSalidaDia1, boxJPD1, spinnerEntradaJPD1, spinnerSalidaJPD1))
				return false;
		if(!tglbtnNoHorario2.isSelected())
			if(!aplicarComprobaciones8Horas(spinnerEntradaDia2, spinnerSalidaDia2, boxJPD2, spinnerEntradaJPD2, spinnerSalidaJPD2))
				return false;
		if(!tglbtnNoHorario3.isSelected())
			if(!aplicarComprobaciones8Horas(spinnerEntradaDia3, spinnerSalidaDia3, boxJPD3, spinnerEntradaJPD3, spinnerSalidaJPD3))
				return false;
		if(!tglbtnNoHorario4.isSelected())
			if(!aplicarComprobaciones8Horas(spinnerEntradaDia4, spinnerSalidaDia4, boxJPD4, spinnerEntradaJPD4, spinnerSalidaJPD4))
				return false;
		if(!tglbtnNoHorario5.isSelected())
			if(!aplicarComprobaciones8Horas(spinnerEntradaDia5, spinnerSalidaDia5, boxJPD5, spinnerEntradaJPD5, spinnerSalidaJPD5))
				return false;
		if(!tglbtnNoHorario6.isSelected())
			if(!aplicarComprobaciones8Horas(spinnerEntradaDia6, spinnerSalidaDia6, boxJPD6, spinnerEntradaJPD6, spinnerSalidaJPD6))
				return false;
		if(!tglbtnNoHorario7.isSelected())
			if(!aplicarComprobaciones8Horas(spinnerEntradaDia7, spinnerSalidaDia7, boxJPD7, spinnerEntradaJPD7, spinnerSalidaJPD7))
				return false;
		return true;
	}
	
	private boolean aplicarComprobaciones8Horas(JSpinner entrada, JSpinner salida, JCheckBox box, JSpinner entrada2, JSpinner salida2) {
		Calendar horasEntrada = GregorianCalendar.getInstance();
		horasEntrada.setTime((Date) entrada.getModel().getValue());
		
		Calendar horasSalida = GregorianCalendar.getInstance();
		horasSalida.setTime((Date) salida.getModel().getValue());
		
		if(!box.isSelected()) {
			return comprobar8hDiferenciaSinJP(horasEntrada, horasSalida);
		}
		else {
			Calendar horasEntradaJP = GregorianCalendar.getInstance();
			horasEntradaJP.setTime((Date) entrada2.getModel().getValue());
			
			Calendar horasSalidaJP = GregorianCalendar.getInstance();
			horasSalidaJP.setTime((Date) salida2.getModel().getValue());
			
			return comprobar8hDiferenciaJornadaPartida(horasEntrada, horasSalida, horasEntradaJP, horasSalidaJP);
		}
	}
	
	private boolean comprobar8hDiferenciaJornadaPartida(Calendar hEntrada, Calendar hSalida, Calendar hEntradaJP, Calendar hSalidaJP) {
		int horasSalida= hSalida.get(Calendar.HOUR_OF_DAY);
		int horasEntrada= hEntrada.get(Calendar.HOUR_OF_DAY);
		
		int horasSalida2= hSalidaJP.get(Calendar.HOUR_OF_DAY);
		int horasEntrada2= hEntradaJP.get(Calendar.HOUR_OF_DAY);
		
		int sumaHoras1Horario = 0;
		int sumaHoras2Horario = 0;
		
		if(horasEntrada > horasSalida){
			if(horasSalida>=0 && horasSalida <=8){
				int resta12hMenosHorasEntrada = 12-horasEntrada;
				sumaHoras1Horario = horasSalida+resta12hMenosHorasEntrada;
				if(sumaHoras1Horario>=8)
					return false;
			}
			return false;
		}
		else if(horasEntrada == 0 && horasSalida >8)
			return false;
		else
			sumaHoras1Horario = horasSalida-horasEntrada;
		
		if(horasEntrada2 > horasSalida2){
			if(horasSalida2>=0 && horasSalida2 <=8){
				int resta12hMenosHorasEntrada = 12-horasEntrada2;
				sumaHoras2Horario = horasSalida2+resta12hMenosHorasEntrada;
				if(sumaHoras2Horario>=8)
					return false;
			}
			else
				return false;
		}
		else if(horasEntrada2 == 0) {
			if(horasSalida2 >=8)
				return false;
			sumaHoras2Horario = horasSalida2;	
		}
		else
			sumaHoras2Horario = horasSalida2-horasEntrada2;
		int sumaTotal = sumaHoras1Horario + sumaHoras2Horario;
		horasTotalesPorDia.add(sumaTotal);
		return sumaTotal <=8;
			
	}
	
	private boolean comprobar8hDiferenciaSinJP(Calendar hEntrada, Calendar hSalida){
		int horasSalida= hSalida.get(Calendar.HOUR);
		int horasEntrada= hEntrada.get(Calendar.HOUR);
		
		if(horasEntrada > horasSalida){
			if(horasSalida>=0 && horasSalida <=8){
				int resta12hMenosHorasEntrada = 12-horasEntrada;
				int sumaHoras = horasSalida+resta12hMenosHorasEntrada;
				horasTotalesPorDia.add(sumaHoras);
				if(sumaHoras<=8)
					return true;
				return false;
			}
			return false;
		}
//		else if(horasSalida == horasEntrada)
//			return false;
		else if(horasEntrada == 0 && horasSalida >8) {
			return false;
		}
		else{
			int diferencia = horasSalida-horasEntrada;
			horasTotalesPorDia.add(diferencia);
			return diferencia <=8;
		}
		
	}
	
	private JPanel getPanelHorario() {
		if (panelHorario == null) {
			panelHorario = new JPanel();
			panelHorario.setBorder(new TitledBorder(null, "Establecer horario", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelHorario.setBounds(6, 6, 731, 590);
			panelHorario.setLayout(null);
			panelHorario.add(getPanelDia7());
			panelHorario.add(getPanelJPD7());
			panelHorario.add(getBtnAnteriorDia7());
			panelHorario.add(getBtnAsignarTurno());
			panelHorario.add(getTglbtnNoHorario7());
		}
		return panelHorario;
	}
	

	private JButton getBtndeseaAsignarUn() {
		if (btndeseaAsignarUn == null) {
			btndeseaAsignarUn = new JButton("\u00BFDesea asignar un mismo horario a varios d\u00EDas?");
			btndeseaAsignarUn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dialogoElegirHorarios = new DialogoElegirHorarios(listaDeNombreDeDias);
					
					aplicarMismoHorarioALosDias(dialogoElegirHorarios.boxSeleccionados);
				}
			});
			btndeseaAsignarUn.setForeground(UIManager.getColor("Button.light"));
			btndeseaAsignarUn.setBorderPainted(false);
			btndeseaAsignarUn.setBounds(6, 549, 342, 35);
		}
		return btndeseaAsignarUn;
	}
	private JToggleButton getTglbtnNoHorario() {
		if (tglbtnNoHorario1 == null) {
			tglbtnNoHorario1 = new JToggleButton("x");
			tglbtnNoHorario1.addActionListener(new AccionTglBtn());
			tglbtnNoHorario1.setToolTipText("Dejar d\u00EDa sin asignar horario");
			tglbtnNoHorario1.setName("x1");
			tglbtnNoHorario1.setBounds(197, 13, 43, 29);
		}
		return tglbtnNoHorario1;
	}
	private JToggleButton getTglbtnNoHorario2() {
		if (tglbtnNoHorario2 == null) {
			tglbtnNoHorario2 = new JToggleButton("x");
			tglbtnNoHorario2.addActionListener(new AccionTglBtn());
			tglbtnNoHorario2.setToolTipText("Dejar d\u00EDa sin asignar horario");
			tglbtnNoHorario2.setName("x2");
			tglbtnNoHorario2.setBounds(433, 13, 43, 29);
		}
		return tglbtnNoHorario2;
	}
	private JToggleButton getTglbtnHoHorario3() {
		if (tglbtnNoHorario3 == null) {
			tglbtnNoHorario3 = new JToggleButton("x");
			tglbtnNoHorario3.addActionListener(new AccionTglBtn());
			tglbtnNoHorario3.setToolTipText("Dejar d\u00EDa sin asignar horario");
			tglbtnNoHorario3.setName("x3");
			tglbtnNoHorario3.setBounds(669, 13, 43, 29);
		}
		return tglbtnNoHorario3;
	}
	private JToggleButton getTglbtnNoHorario4() {
		if (tglbtnNoHorario4 == null) {
			tglbtnNoHorario4 = new JToggleButton("x");
			tglbtnNoHorario4.addActionListener(new AccionTglBtn());
			tglbtnNoHorario4.setToolTipText("Dejar d\u00EDa sin asignar horario");
			tglbtnNoHorario4.setName("x4");
			tglbtnNoHorario4.setBounds(197, 282, 43, 29);
		}
		return tglbtnNoHorario4;
	}
	private JToggleButton getTglbtnNoHorario5() {
		if (tglbtnNoHorario5 == null) {
			tglbtnNoHorario5 = new JToggleButton("x");
			tglbtnNoHorario5.addActionListener(new AccionTglBtn());
			tglbtnNoHorario5.setToolTipText("Dejar d\u00EDa sin asignar horario");
			tglbtnNoHorario5.setName("x5");
			tglbtnNoHorario5.setBounds(433, 282, 43, 29);
		}
		return tglbtnNoHorario5;
	}
	private JToggleButton getTglbtnNoHorario6() {
		if (tglbtnNoHorario6 == null) {
			tglbtnNoHorario6 = new JToggleButton("x");
			tglbtnNoHorario6.addActionListener(new AccionTglBtn());
			tglbtnNoHorario6.setToolTipText("Dejar d\u00EDa sin asignar horario");
			tglbtnNoHorario6.setName("x6");
			tglbtnNoHorario6.setBounds(669, 282, 43, 29);
		}
		return tglbtnNoHorario6;
	}
	private JToggleButton getTglbtnNoHorario7() {
		if (tglbtnNoHorario7 == null) {
			tglbtnNoHorario7 = new JToggleButton("x");
			tglbtnNoHorario7.addActionListener(new AccionTglBtn());
			tglbtnNoHorario7.setToolTipText("Dejar d\u00EDa sin asignar horario");
			tglbtnNoHorario7.setName("x7");
			tglbtnNoHorario7.setBounds(206, 25, 43, 29);
		}
		return tglbtnNoHorario7;
	}
	private JButton getBtnNuevaConsulta() {
		if (btnNuevaConsulta == null) {
			btnNuevaConsulta = new JButton("Nueva consulta");
			btnNuevaConsulta.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					limpiarDatos();
				}
			});
			btnNuevaConsulta.setBounds(605, 568, 132, 28);
		}
		return btnNuevaConsulta;
	}
	
	private void limpiarDatos() {
		iniciarLista(listaEmpleadosConsultarEstado);
		chckbxLimpieza.setSelected(false);
		chckbxMantenimiento.setSelected(false);
		chckbxRecepcin.setSelected(false);
		chckbxRestauracin.setSelected(false);
		dcFechaInicio.setDate(null);
		dcFechaFin.setDate(null);
		rellenarTablaTurnos();
	}
}






























