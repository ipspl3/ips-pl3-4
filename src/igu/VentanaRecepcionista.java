package igu;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import javax.swing.border.TitledBorder;

import logica.Cliente;
import logica.DataBase;

import javax.swing.JButton;
import java.awt.CardLayout;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.UIManager;

@SuppressWarnings("serial")
public class VentanaRecepcionista extends JFrame {

	private JPanel contentPane;
	private JPanel pnUsuario;
	private JLabel label;
	private JLabel lblComo;
	private JLabel lblRecepcionista;
	private JPanel pnAccBotones;
	private JButton btnNewReserva;
	private JPanel pnAccCards;

	private JPanel cardLogo;

	public CardLayout c1;

	private JButton btnGestionReserva;

	private CardNuevaReserva nuevaReserva = new CardNuevaReserva(this);
	private CardGestionarReserva gestionReserva = new CardGestionarReserva(this);
	private CardVistaHabitaciones vistaHabs = new CardVistaHabitaciones(this);
	private CardCheckIn checkIn = new CardCheckIn(this);
	private CardCheckOut checkOut = new CardCheckOut(this);
	private JButton btnVistaHabitaciones;
	private JPanel panel;
	private JButton btnCheckin;
	private JButton btnCheckout;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaRecepcionista frame = new VentanaRecepcionista();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaRecepcionista() {
		setTitle("Ventana Recepcionista");
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaRecepcionista.class.getResource("/img/logo.jpg")));
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getPnUsuario());
		contentPane.add(getPnAccBotones());
		contentPane.add(getAccCards());
		contentPane.add(getPanel());
		setLocationRelativeTo(null);
		try {
			DataBase.Connect();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Base de datos no disponible.\nConsulte con un t�cnico.");
		}
	}
	private JPanel getPnUsuario() {
		if (pnUsuario == null) {
			pnUsuario = new JPanel();
			pnUsuario.setBorder(new TitledBorder(null, "Usuario", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnUsuario.setForeground(Color.LIGHT_GRAY);
			pnUsuario.setBounds(10, 11, 222, 74);
			pnUsuario.setLayout(null);
			pnUsuario.add(getLabel());
			pnUsuario.add(getLblComo());
			pnUsuario.add(getLblRecepcionista());
		}
		return pnUsuario;
	}
	private JLabel getLabel() {
		if (label == null) {
			label = new JLabel("---");
			label.setBounds(22, 25, 168, 14);
		}
		return label;
	}
	private JLabel getLblComo() {
		if (lblComo == null) {
			lblComo = new JLabel("Como: ");
			lblComo.setBounds(14, 46, 46, 14);
		}
		return lblComo;
	}
	private JLabel getLblRecepcionista() {
		if (lblRecepcionista == null) {
			lblRecepcionista = new JLabel("Recepcionista");
			lblRecepcionista.setBounds(92, 46, 108, 14);
		}
		return lblRecepcionista;
	}
	private JPanel getPnAccBotones() {
		if (pnAccBotones == null) {
			pnAccBotones = new JPanel();
			pnAccBotones.setBounds(10, 213, 222, 447);
			pnAccBotones.setBorder(new TitledBorder(null, "Acciones", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnAccBotones.setLayout(null);
			pnAccBotones.add(getBtnNewReserva());
			pnAccBotones.add(getBtnGestionReserva());
			pnAccBotones.add(getBtnVistaHabitaciones());
		}
		return pnAccBotones;
	}
	private JPanel getAccCards() {
		if (pnAccCards == null) {
			pnAccCards = new JPanel();
			pnAccCards.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnAccCards.setBounds(238, 11, 646, 649);
			pnAccCards.setLayout(new CardLayout(0, 0));
			pnAccCards.add(nuevaReserva, "Nueva reserva");
			pnAccCards.add(getCardLogo(), "Inicial");
			pnAccCards.add(gestionReserva, "Gestion");
			pnAccCards.add(vistaHabs, "Vista");
			pnAccCards.add(checkIn, "Check-in");
			pnAccCards.add(checkOut, "Check-out");
			c1 = (CardLayout)(pnAccCards.getLayout());
			c1.show(pnAccCards, "Inicial");
		}
		return pnAccCards;
	}
	
	private JButton getBtnNewReserva() {
		if (btnNewReserva == null) {
			btnNewReserva = new JButton("Nueva reserva");
			btnNewReserva.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					c1.show(pnAccCards, "Nueva reserva");
					
				}
			});
			btnNewReserva.setBounds(10, 26, 202, 23);
		}
		return btnNewReserva;
	}

	private JPanel getCardLogo() {
		if (cardLogo == null) {
			cardLogo = new JPanel();
			cardLogo.setLayout(null);
		}
		return cardLogo;
	}

	void iniciar(){
		c1.show(pnAccCards, "Inicial");
	}
	
	private JButton getBtnGestionReserva() {
		if (btnGestionReserva == null) {
			btnGestionReserva = new JButton("Gestionar reservas");
			btnGestionReserva.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					nuevaReserva.iniciar();
					vistaHabs.iniciar();
					c1.show(pnAccCards, "Gestion");
					gestionReserva.todasReservas();
				}
			});
			btnGestionReserva.setBounds(10, 60, 202, 23);
		}
		return btnGestionReserva;
	}
	private JButton getBtnVistaHabitaciones() {
		if (btnVistaHabitaciones == null) {
			btnVistaHabitaciones = new JButton("Vista Habitaciones");
			btnVistaHabitaciones.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					nuevaReserva.iniciar();
					gestionReserva.iniciar();
					c1.show(pnAccCards, "Vista");
					vistaHabs.todasHabitaciones();
				}
			});
			btnVistaHabitaciones.setBounds(10, 94, 202, 23);
		}
		return btnVistaHabitaciones;
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Checks", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			panel.setBounds(10, 85, 222, 117);
			panel.setLayout(null);
			panel.add(getBtnCheckin());
			panel.add(getBtnCheckout());
		}
		return panel;
	}
	private JButton getBtnCheckin() {
		if (btnCheckin == null) {
			btnCheckin = new JButton("Check-in");
			btnCheckin.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					c1.show(pnAccCards, "Check-in");
				}
			});
			btnCheckin.setBounds(10, 34, 202, 23);
		}
		return btnCheckin;
	}
	private JButton getBtnCheckout() {
		if (btnCheckout == null) {
			btnCheckout = new JButton("Check-out");
			btnCheckout.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					c1.show(pnAccCards, "Check-out");
					checkOut.todosRegistros();
				}
			});
			btnCheckout.setBounds(10, 68, 202, 23);
		}
		return btnCheckout;
	}

	public void nuevaReserva(String dato, String tipo, Cliente cliente) {
		c1.show(pnAccCards, "Nueva reserva");
		nuevaReserva.registro(dato, tipo, cliente);
	}
	
	public void registro(Cliente cliente) {
		nuevaReserva.iniciar();
		c1.show(pnAccCards, "Check-in");
		checkIn.registro(cliente);
	}
}