package igu;

import javax.swing.JPanel;

import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPasswordField;
import java.awt.GridLayout;
import javax.swing.border.TitledBorder;

import logica.Conexion;
import logica.DNI;
import logica.ValidadorNIE;

import javax.swing.JRadioButton;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;

public class CardRegistrarTrabajador extends JPanel {
	private JPanel pnCentral;
	private JLabel lblRegistrarTrabajador;
	private JLabel lblNombre;
	private JTextField txtNombre;
	private JLabel lblApellidos;
	private JTextField txtApellidos;
	private JLabel lblEmail;
	private JTextField txtemail;
	private JTextField txtDNIoPasa;
	private JLabel lblDireccin;
	private JLabel lblPerfil;
	private JComboBox cbPerfil;
	private JLabel lblFechaDeNacimiento;
	private JComboBox cBdias;
	private JComboBox cBmeses;
	private JComboBox cBa�os;
	private JLabel lblNombreDeUsuario;
	private JTextField txtnombreusuario;
	private JLabel lblContrasea;
	private JPasswordField pwdcontrase�a;
	private JLabel lblRepetirContrasea;
	private JPasswordField txtrpsw;
	private JPanel pn1;
	private JPanel pn2;
	private JPanel pn3;
	private JPanel pn4;
	private JPanel pnRegistroUsuario;
	private JPanel panel;
	private JPanel panel_1;
	private JPanel panel_2;
	private JPanel pncentercenter;
	private JRadioButton rdbtnDni;
	private JRadioButton rdbtnPasaporte;
	private JScrollPane scrollPane;
	private JTextArea textArea;
	private JLabel lblTelefono;
	private JTextField txtTelefono;
	private JLabel lblCiudad;
	private JTextField txtCiudad;
	private JLabel lblCp;
	private JTextField txtCodigoPostal;
	private JPanel panel_3;
	private JPanel panel_4;
	private JRadioButton rdbtnNie;
	private JPanel panel_5;
	private JPanel pnSouth;
	private JButton btnRegistrar;
	private JButton btnAtras;
	private JPanel pnEast;

	private ButtonGroup id = new ButtonGroup();
	private Connection conn;

	/**
	 * Create the panel.
	 */
	public CardRegistrarTrabajador(Conexion con) {
		conn = con.getCon();
		setLayout(new BorderLayout(0, 0));
		add(getLblRegistrarTrabajador(), BorderLayout.NORTH);
		add(getPnCentral(), BorderLayout.CENTER);
		add(getPnSouth(), BorderLayout.SOUTH);

	}

	private JPanel getPnCentral() {
		if (pnCentral == null) {
			pnCentral = new JPanel();
			pnCentral.setLayout(new BorderLayout(0, 0));
			pnCentral.add(getPanel_3_1(), BorderLayout.CENTER);
			pnCentral.add(getPn1(), BorderLayout.NORTH);
			pnCentral.add(getPanel_2(), BorderLayout.SOUTH);
		}
		return pnCentral;
	}

	private JLabel getLblRegistrarTrabajador() {
		if (lblRegistrarTrabajador == null) {
			lblRegistrarTrabajador = new JLabel("Registrar trabajador");
			lblRegistrarTrabajador.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 19));
		}
		return lblRegistrarTrabajador;
	}

	private JLabel getLblNombre() {
		if (lblNombre == null) {
			lblNombre = new JLabel("Nombre:");
		}
		return lblNombre;
	}

	private JTextField getTxtNombre() {
		if (txtNombre == null) {
			txtNombre = new JTextField();
			txtNombre.setColumns(10);
		}
		return txtNombre;
	}

	private JLabel getLblApellidos() {
		if (lblApellidos == null) {
			lblApellidos = new JLabel("Apellidos:");
		}
		return lblApellidos;
	}

	private JTextField getTxtApellidos() {
		if (txtApellidos == null) {
			txtApellidos = new JTextField();
			txtApellidos.setColumns(10);
		}
		return txtApellidos;
	}

	private JLabel getLblEmail() {
		if (lblEmail == null) {
			lblEmail = new JLabel("e-mail:");
		}
		return lblEmail;
	}

	private JTextField getTxtemail() {
		if (txtemail == null) {
			txtemail = new JTextField();
			txtemail.setColumns(10);
		}
		return txtemail;
	}

	private JTextField getTxtDNIoPasa() {
		if (txtDNIoPasa == null) {
			txtDNIoPasa = new JTextField();
			txtDNIoPasa.setBounds(320, 40, 130, 26);
			txtDNIoPasa.setColumns(10);
		}
		return txtDNIoPasa;
	}

	private JLabel getLblDireccin() {
		if (lblDireccin == null) {
			lblDireccin = new JLabel("Direccion:");
			lblDireccin.setBounds(139, 21, 64, 16);
		}
		return lblDireccin;
	}

	private JLabel getLblPerfil() {
		if (lblPerfil == null) {
			lblPerfil = new JLabel("Perfil:");
		}
		return lblPerfil;
	}

	private JComboBox getCbPerfil() {
		if (cbPerfil == null) {
			cbPerfil = new JComboBox();
			String[] perfiles = new String[] { "gerente", "administrador", "recepcionista", "encargado del restaurante",
					"limpieza", "mantenimiento", "restauraci�n" };
			cbPerfil.setModel(new DefaultComboBoxModel(perfiles));

		}
		return cbPerfil;
	}

	private JLabel getLblFechaDeNacimiento() {
		if (lblFechaDeNacimiento == null) {
			lblFechaDeNacimiento = new JLabel("Fecha de nacimiento:");
		}
		return lblFechaDeNacimiento;
	}

	private JComboBox getCBdias() {
		if (cBdias == null) {
			String[] dias = new String[31];
			cBdias = new JComboBox();
			for (int i = 1; i <= 31; i++) {
				dias[i - 1] = String.valueOf(i);
			}
			cBdias.setModel(new DefaultComboBoxModel(dias));
			cBdias.setSelectedIndex(0);
		}
		return cBdias;
	}

	private JComboBox getCBmeses() {
		if (cBmeses == null) {
			cBmeses = new JComboBox();
			String[] meses = new String[12];
			for (int i = 1; i <= 12; i++) {
				meses[i - 1] = String.valueOf(i);
			}
			cBmeses.setModel(new DefaultComboBoxModel(meses));
			cBmeses.setSelectedIndex(0);

		}
		return cBmeses;
	}

	private JComboBox getCBa�os() {
		if (cBa�os == null) {
			cBa�os = new JComboBox();
			String[] a�os = new String[55];
			int count = 0;
			for (int i = 2001; i >= 1947; i--) {
				a�os[count] = String.valueOf(i);
				count++;
			}
			cBa�os.setModel(new DefaultComboBoxModel(a�os));
			cBa�os.setSelectedIndex(0);

		}
		return cBa�os;
	}

	private JLabel getLblNombreDeUsuario() {
		if (lblNombreDeUsuario == null) {
			lblNombreDeUsuario = new JLabel("Nombre de usuario:");
		}
		return lblNombreDeUsuario;
	}

	private JTextField getTxtnombreusuario() {
		if (txtnombreusuario == null) {
			txtnombreusuario = new JTextField();
			txtnombreusuario.setColumns(10);
		}
		return txtnombreusuario;
	}

	private JLabel getLblContrasea() {
		if (lblContrasea == null) {
			lblContrasea = new JLabel("Contrase\u00F1a:");
			lblContrasea.setBounds(292, 10, 75, 16);
		}
		return lblContrasea;
	}

	private JPasswordField getPwdcontrase�a() {
		if (pwdcontrase�a == null) {
			pwdcontrase�a = new JPasswordField();
			pwdcontrase�a.setBounds(379, 5, 140, 21);
		}
		return pwdcontrase�a;
	}

	private JLabel getLblRepetirContrasea() {
		if (lblRepetirContrasea == null) {
			lblRepetirContrasea = new JLabel("Repetir contrase\u00F1a:");
			lblRepetirContrasea.setBounds(242, 10, 121, 16);
		}
		return lblRepetirContrasea;
	}

	private JPasswordField getTxtrpsw() {
		if (txtrpsw == null) {
			txtrpsw = new JPasswordField();
			txtrpsw.setBounds(382, 5, 141, 21);
		}
		return txtrpsw;
	}

	private JPanel getPn1() {
		if (pn1 == null) {
			pn1 = new JPanel();
			pn1.add(getLblNombre());
			pn1.add(getTxtNombre());
			pn1.add(getLblApellidos());
			pn1.add(getTxtApellidos());
		}
		return pn1;
	}

	private JPanel getPn2() {
		if (pn2 == null) {
			pn2 = new JPanel();
			pn2.setLayout(new GridLayout(0, 1, 0, 0));
			pn2.add(getPanel_4());
		}
		return pn2;
	}

	private JPanel getPn3() {
		if (pn3 == null) {
			pn3 = new JPanel();
			pn3.add(getLblPerfil());
			pn3.add(getCbPerfil());
			pn3.add(getLblFechaDeNacimiento());
			pn3.add(getCBdias());
			pn3.add(getCBmeses());
			pn3.add(getCBa�os());
		}
		return pn3;
	}

	private JPanel getPanel_1() {
		if (pn4 == null) {
			pn4 = new JPanel();
			pn4.setLayout(new BorderLayout(0, 0));
			pn4.add(getPanel_5(), BorderLayout.CENTER);
		}
		return pn4;
	}

	private JPanel getPanel_2() {
		if (pnRegistroUsuario == null) {
			pnRegistroUsuario = new JPanel();
			pnRegistroUsuario.setBorder(
					new TitledBorder(null, "Registro usuario", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnRegistroUsuario.setLayout(new GridLayout(0, 1, 0, 0));
			pnRegistroUsuario.add(getPanel_2_1());
			pnRegistroUsuario.add(getPanel_1_1());
			pnRegistroUsuario.add(getPanel_3());
		}
		return pnRegistroUsuario;
	}

	private JPanel getPanel_3() {
		if (panel == null) {
			panel = new JPanel();
			panel.setLayout(null);
			panel.add(getLblRepetirContrasea());
			panel.add(getTxtrpsw());
		}
		return panel;
	}

	private JPanel getPanel_1_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setLayout(null);
			panel_1.add(getLblContrasea());
			panel_1.add(getPwdcontrase�a());
		}
		return panel_1;
	}

	private JPanel getPanel_2_1() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.add(getLblNombreDeUsuario());
			panel_2.add(getTxtnombreusuario());
		}
		return panel_2;
	}

	private JPanel getPanel_3_1() {
		if (pncentercenter == null) {
			pncentercenter = new JPanel();
			pncentercenter.setLayout(new GridLayout(0, 1, 0, 0));
			pncentercenter.add(getPanel_1());
			pncentercenter.add(getPn2());
			pncentercenter.add(getPanel_3_2());
			pncentercenter.add(getPn3());
		}
		return pncentercenter;
	}

	private JRadioButton getRdbtnDni() {
		if (rdbtnDni == null) {
			rdbtnDni = new JRadioButton("DNI");
			rdbtnDni.setBounds(276, 5, 56, 23);
			id.add(rdbtnDni);
			rdbtnDni.setSelected(true);
		}
		return rdbtnDni;
	}

	private JRadioButton getRdbtnPasaporte() {
		if (rdbtnPasaporte == null) {
			rdbtnPasaporte = new JRadioButton("Pasaporte");
			rdbtnPasaporte.setBounds(337, 5, 93, 23);
			id.add(rdbtnPasaporte);

		}
		return rdbtnPasaporte;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(213, 8, 233, 67);
			scrollPane.setViewportView(getTextArea());
		}
		return scrollPane;
	}

	private JTextArea getTextArea() {
		if (textArea == null) {
			textArea = new JTextArea();
		}
		return textArea;
	}

	private JLabel getLblTelefono() {
		if (lblTelefono == null) {
			lblTelefono = new JLabel("Telefono:");
		}
		return lblTelefono;
	}

	private JTextField getTxtTelefono() {
		if (txtTelefono == null) {
			txtTelefono = new JTextField();
			txtTelefono.setColumns(10);
		}
		return txtTelefono;
	}

	private JLabel getLblCiudad() {
		if (lblCiudad == null) {
			lblCiudad = new JLabel("Ciudad:");
			lblCiudad.setBounds(453, 21, 48, 16);
		}
		return lblCiudad;
	}

	private JTextField getTxtCiudad() {
		if (txtCiudad == null) {
			txtCiudad = new JTextField();
			txtCiudad.setBounds(515, 16, 130, 26);
			txtCiudad.setColumns(10);
		}
		return txtCiudad;
	}

	private JLabel getLblCp() {
		if (lblCp == null) {
			lblCp = new JLabel("CP:");
			lblCp.setBounds(481, 59, 20, 16);
		}
		return lblCp;
	}

	private JTextField getTxtCodigoPostal() {
		if (txtCodigoPostal == null) {
			txtCodigoPostal = new JTextField();
			txtCodigoPostal.setBounds(515, 54, 130, 26);
			txtCodigoPostal.setColumns(10);
		}
		return txtCodigoPostal;
	}

	private JPanel getPanel_3_2() {
		if (panel_3 == null) {
			panel_3 = new JPanel();
			panel_3.setLayout(null);
			panel_3.add(getLblDireccin());
			panel_3.add(getScrollPane());
			panel_3.add(getLblCiudad());
			panel_3.add(getTxtCiudad());
			panel_3.add(getLblCp());
			panel_3.add(getTxtCodigoPostal());
		}
		return panel_3;
	}

	private JPanel getPanel_4() {
		if (panel_4 == null) {
			panel_4 = new JPanel();
			panel_4.add(getLblEmail());
			panel_4.add(getTxtemail());
			panel_4.add(getLblTelefono());
			panel_4.add(getTxtTelefono());
		}
		return panel_4;
	}

	private JRadioButton getRdbtnNie() {
		if (rdbtnNie == null) {
			rdbtnNie = new JRadioButton("NIE");
			rdbtnNie.setBounds(435, 5, 53, 23);
			id.add(rdbtnNie);
		}
		return rdbtnNie;
	}

	private JPanel getPanel_5() {
		if (panel_5 == null) {
			panel_5 = new JPanel();
			panel_5.setLayout(null);
			panel_5.add(getRdbtnDni());
			panel_5.add(getRdbtnPasaporte());
			panel_5.add(getRdbtnNie());
			panel_5.add(getTxtDNIoPasa());
		}
		return panel_5;
	}

	private JPanel getPnSouth() {
		if (pnSouth == null) {
			pnSouth = new JPanel();
			pnSouth.setLayout(new BorderLayout(0, 0));
			pnSouth.add(getPnEast(), BorderLayout.EAST);
		}
		return pnSouth;
	}

	private JButton getBtnRegistrar() {
		if (btnRegistrar == null) {
			btnRegistrar = new JButton("Registrar");
			btnRegistrar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (camposVacios()) {
						JOptionPane.showMessageDialog(null, "Se han encontrado campos vac�os.");
					} else if (!checkID()) {
						JOptionPane.showMessageDialog(null,
								"ID no valido, comprueba que el pasaporte,dni o nie introducido sea valido.");
					} else if (checkEmail()) {
						JOptionPane.showMessageDialog(null,
								"e-mail no valido");
					} else if (checkCP()) {
						JOptionPane.showMessageDialog(null,
								"Codigo postal no valido");
					} else if (checkUserName()) {
						JOptionPane.showMessageDialog(null,
								"Nombre de usuario repetido!");
						getTxtnombreusuario().setText("");
						
					} else if (checkPassword()) {
						JOptionPane.showMessageDialog(null,
								"Vuelva a introducir la contrase�a.");

					}else if(checkDniRepetido())
					{
						JOptionPane.showMessageDialog(null,
								"DNI ya introducido previamente.");
					} 
					else {
						// A�adir a tabla nuevo trabajador.
						try {
							String id=getTxtDNIoPasa().getText().toString();
							String nombre = getTxtNombre().getText().toString()+" "+getTxtApellidos().getText().toString();
							String area = getCbPerfil().getSelectedItem().toString();
							String email = getTxtemail().getText().toString();
							String telefono = getTxtTelefono().getText().toString();
							String direccion = getTextArea().getText().toString();
							String ciudad = getTxtCiudad().getText().toString();
							String cp = getTxtCodigoPostal().getText().toString();
							
							SimpleDateFormat Formato = new SimpleDateFormat("dd/MM/yyyy");
							String nacimiento = getCBdias().getSelectedItem().toString()+"/"
									+getCBmeses().getSelectedItem().toString()+"/"
									+getCBa�os().getSelectedItem().toString();
							
							java.sql.Date nacimientosql = null;
							try {
								nacimientosql = new java.sql.Date(Formato.parse(nacimiento).getTime());
							} catch (ParseException e1) {
								e1.printStackTrace();
							}

							
							String username=getTxtnombreusuario().getText().toString();
							char[] pass = getPwdcontrase�a().getPassword();
							String password = new String(pass);
							
							PreparedStatement trabajador = conn.prepareStatement("INSERT INTO TRABAJADOR VALUES (?,?,?,?,?,?,?,?,?,?,?)");
							trabajador.setString(1,id);
							trabajador.setString(2,nombre);
							trabajador.setString(3,area);
							trabajador.setString(4,email);
							trabajador.setString(5,telefono);
							trabajador.setString(6,direccion);
							trabajador.setString(7,ciudad);
							trabajador.setString(8,cp);
							
							trabajador.setDate(9, (java.sql.Date) nacimientosql );
							
							trabajador.setString(10,username);
							trabajador.setString(11,password);
							
							trabajador.executeUpdate();
							trabajador.close();
							reiniciarPantalla();
							JOptionPane.showMessageDialog(null, "Nuevo trabajador a�adido");

						} catch (SQLException e1) {
							e1.printStackTrace();
						}
						
					}
				}

			});
		}
		return btnRegistrar;
	}

	protected void reiniciarPantalla() {
		getTxtDNIoPasa().setText("");
		getTxtNombre().setText("");
		getTxtApellidos().setText("");
		getCbPerfil().setSelectedIndex(0);
		getTxtemail().setText("");
		getTxtTelefono().setText("");
		getTextArea().setText("");
		getTxtCiudad().setText("");
		getTxtCodigoPostal().setText("");
		
		getCBdias().setSelectedIndex(0);
		getCBmeses().setSelectedIndex(0);
		getCBa�os().setSelectedIndex(0);
		
		getTxtnombreusuario().setText("");
		getPwdcontrase�a().setText("");
		getTxtrpsw().setText("");
		
	}

	protected boolean checkPassword() {
		boolean repeatpassword = false;
		
		char[] pass = getPwdcontrase�a().getPassword();
		String passs = new String(pass);
		
		char[] rpass = getTxtrpsw().getPassword();
		String rpasss = new String(rpass);
		if(!passs.equals(rpasss))
		{
			repeatpassword = true;
		}
		return repeatpassword;
	}

	protected boolean checkUserName() {
		int numusers = 0;
		try {
			PreparedStatement repeated = conn.prepareStatement("SELECT count(USERNAME) FROM TRABAJADOR WHERE USERNAME =?");
			repeated.setString(1, getTxtnombreusuario().getText().toString());
			ResultSet rs = repeated.executeQuery();
			while(rs.next())
			{
				numusers = rs.getInt(1);
			}
			repeated.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(numusers>0)
		{
			return true;
		}
		
		return false;
	}
	private boolean checkDniRepetido() {
		int numusers = 0;
		try {
			PreparedStatement repeated = conn.prepareStatement("SELECT count(DNI) FROM TRABAJADOR WHERE DNI =?");
			repeated.setString(1, getTxtDNIoPasa().getText().toString());
			ResultSet rs = repeated.executeQuery();
			while(rs.next())
			{
				numusers = rs.getInt(1);
			}
			repeated.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(numusers>0)
		{
			return true;
		}
		
		return false;
	}

	protected boolean checkCP() {
		int lengthCP = getTxtCodigoPostal().getText().toString().length();
		if(lengthCP != 5)
		{
			return true;
		}
		return false;
	}

	protected boolean checkEmail() {
		boolean ema = false;
		String email = getTxtemail().getText().toString();
		if(email.contains("@"))
		{
			String[] ee =email.split("@");
			if(ee.length==2)
			{

				if(!ee[1].contains("."))
				{
					ema = true;
				}
			}
			else
			{
				ema = true;
			}

		}
		else
		{
			ema = true;
		}
		
		return ema;
	}

	protected boolean checkID() {
		
		boolean id = false;
		if(getRdbtnDni().isSelected())
		{
			if(getTxtDNIoPasa().getText().toString().length()==9)
			{
				DNI dni = new DNI(getTxtDNIoPasa().getText().toString());
				id = dni.comprobarDNI();
			}
			else
			{
				id = true;
			}
		}
		else if(getRdbtnNie().isSelected()){
			ValidadorNIE vNIE = new ValidadorNIE();
			id = vNIE.validarNIE(getTxtDNIoPasa().getText().toString());
			if(id == true)
			{
				id = false;
			}
			if(id == false)
			{
				id = true;
			}
			
		}
		else{
			if(getTxtDNIoPasa().getText().toString().length()==9)
			{
				DNI pasaporte = new DNI(getTxtDNIoPasa().getText().toString());
				id = pasaporte.comprobarPasaporte();
			}
			else
			{
				id = true;
			}
			
		}
		return id;
	}

	private boolean camposVacios() {
		boolean vacios = false;
		if (getTxtNombre().getText().toString().equals("")) {
			vacios = true;
		}
		if (getTxtApellidos().getText().toString().equals("")) {
			vacios = true;
		}
		if (getTxtDNIoPasa().getText().toString().equals("")) {
			vacios = true;
		}
		if (getTxtemail().getText().toString().equals("")) {
			vacios = true;
		}
		if (getTxtTelefono().getText().toString().equals("")) {
			vacios = true;

		}
		if (getTextArea().getText().toString().equals("")) {
			vacios = true;

		}
		if (getTxtCiudad().getText().toString().equals("")) {
			vacios = true;

		}
		if (getTxtCodigoPostal().getText().toString().equals("")) {
			vacios = true;

		}
		if (getTxtnombreusuario().getText().toString().equals("")) {
			vacios = true;

		}
		char[] pass = getPwdcontrase�a().getPassword();
		String passs = new String(pass);
		if (passs.equals("")) {
			vacios = true;

		}
		char[] rpass = getTxtrpsw().getPassword();
		String rpasss = new String(rpass);
		if (rpasss.equals("")) {
			vacios = true;
		}
		return vacios;
	}

	private JButton getBtnAtras() {
		if (btnAtras == null) {
			btnAtras = new JButton("Atras");
		}
		return btnAtras;
	}

	private JPanel getPnEast() {
		if (pnEast == null) {
			pnEast = new JPanel();
			pnEast.add(getBtnAtras());
			pnEast.add(getBtnRegistrar());
		}
		return pnEast;
	}
}
